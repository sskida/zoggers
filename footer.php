<footer>
<div class="container">
<div class="col-md-5 col-sm-5 col-xs-12">
<h1>Newsletter</h1>
<p>Keep up on our always evolving product
features and technology. enter your e-mail
and subscribe to our newsletter.</p>
<input type="text" placeholder="Email Address ">
<input type="submit" value="go!">
</div>

<div class="col-md-5 col-sm-4 col-xs-12">
<h1>Contact Us</h1>
<ul class="contact">
<li><i class="fa fa-map-marker"></i>1234, Street Name, City Name, India</li>
<li><i class="fa fa-phone"></i>(123)-456-7890</li>
<li><i class="fa fa-envelope-o"></i><a href="mailto:mail@desileaks.com">mail@desileaks.com</a></li>
</ul>

</div>

<div class="col-md-2 col-sm-3 col-xs-12">
<h1>Follow Us</h1>
<ul class="social">
<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
<li><a href="#"><i class="fa fa-facebook"></i></a></li>
<li><a href="#"><i class="fa fa-twitter"></i></a></li>
<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
<li><a href="#"><i class="fa fa-dribbble"></i></a></li>



<li><a href="#"><i class="fa fa-tumblr"></i></a></li>



<li><a href="#"><i class="fa fa-wordpress"></i></a></li>
<li><a href="#"><i class="fa fa-behance"></i></a></li>
<li><a href="#"><i class="fa fa-rss"></i></a></li>
<li><a href="#"><i class="fa fa-youtube"></i></a></li>
<li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
<li><a href="#"><i class="fa fa-flickr"></i></a></li>
</ul>

</div>


</div>
<div class="bottom"><div class="container">

<div class="col-md-3 col-sm-3 col-sx-12"><img src="<?=URL?>images/footer_logo.png">
� Copyright 2013 Desileaks.com.
<br>All Rights Reserved
</div>
<div class="col-md-9 col-sm-9 col-xs-12">
<ul>
<li><a href="#">ABOUT US</a></li>
<li><a href="#">customer service</a></li>
<li><a href="#">careers</a></li>
<li><a href="#">contact us</a></li>
</ul>
</div>
</div></div>
</footer>