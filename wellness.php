
		 <?php
      include"header-front-end.php";
    ?>
    <link rel="stylesheet" type="text/css" href="front-end-css/wellness_style.css" />
		<div class="main">
			<h3 class="caption">			
				Let's Begin! Select a Service Below
				<img src="front-end-images/arrow_down.png" alt="" />
			</h3>
			
			<div class="tab_c">
				<!-- Main tabs  -->
				<div class="tab deactivate_tab top_radius_corner_3 margin_left-1 left">
					<a href="index.php" class="active_tab"><span  class="healthcare_icon">Healthcare</span></a>
				</div>
				<div class="tab top_radius_corner_3 margin_right-1 right">
					<a class="active_tab" href="#"><span  class="wellness_icon" href="#">Wellness</span></a>
				</div>
				
				
				<!-- Sub tabs  -->
				<div class="tab_sub_btn_c top_radius_corner_3 clear">				
					<div class="sub_tab sub_tab1 left">
						<h4><span class="salon_spa active_tab">Salon & Spa</span></h4>
					</div>
					<div class="sub_tab sub_tab2 border_right left">
						<h4><span class="gym">Gym</span></h4>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="tab_info_c radius_corner_3 border_top_0">
					<form method="post" action="search-front-end.php">
						<div class="left"><input type="text" id="spe_salon" name="speciality" class="search_speciality" placeholder="Speciality" value="" /></div>
						<div class="left"><input type="text" id="location_salon" name="area" class="area_speciality" placeholder="Area" value="" /></div>
						<input type="hidden" id="salon_city" value="" name="city_id">
						<input type="submit" class="btn left" name="search" placeholder="Speciality" value="Search" />
						<!-- <a href="search-front-end.php" style ="margin-left: 15px;height: 17px;padding-top: 2px;font-size: 1em; width: 53px;"class="btn left">Search</a> -->
					</form>
					<div class="clear"></div>
				</div>
				<div class="tab_info_c radius_corner_3 border_top_0">
					<form method="post" action="search-front-end.php">
						<div class="left"><input type="text" id="spe_gym" name="speciality" class="search_speciality" placeholder="Speciality" value="" /></div>
						<div class="left"><input type="text" id="location_gym" name="area" class="area_speciality" placeholder="Area" value="" /></div>
						<input type="hidden" id="gym_city" value="" name="city_id">
						<input type="submit" class="btn left" name="search" placeholder="Speciality" value="Search" />
						<!-- <a href="search-front-end.php" style ="margin-left: 15px;height: 17px;padding-top: 2px;font-size: 1em; width: 53px;"class="btn left">Search</a> -->
					</form>
					<div class="clear"></div>
				</div>				
				<div class="tab_info_c radius_corner_3 border_top_0">
					<div class="left"><input type="text" class="search_speciality" placeholder="Speciality" value="" /></div>
					<div class="left"><input type="text" class="area_speciality" placeholder="Area" value="" /></div>
					<!-- <input type="button" class="btn left" placeholder="Speciality" value="Search" /> -->
					<a href="search-front-end.php" style ="margin-left: 15px;height: 17px;padding-top: 2px;font-size: 1em; width: 53px;"class="btn left">Search</a>
					<div class="clear"></div>
				</div>		
			</div>
			
			<div class="clear"></div>
			<div class="section or">
				<h1>OR</h1>
			</div>
			<div class="felling_lazy_c">
				<div class="felling_lazy_icon_c left">
					<a href="#recommendedForYou" class="right">
						Feeling Lazy?
						<span></span>
					</a>
					<div class="clear"></div>
				</div>
				<div class="felling_lazy_info_c left">
					<div class="left">
						<h4>Quickly access list of Healthcare and Wellness centers carefully selected by Zoggrs just for you</h4>
					</div>	
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			
			
			
			<!-- <div  style="padding-bottom: 7em;"></div> -->
			<span id="howItWork" style="display:block;padding-bottom: 3em;"></span>
			<span id="howItWork"></span>
			<div class="center_page_icon">			
				<div class="icon_c">
					<img src="front-end-images/how_it_works_icon.png" alt="how it works icon" />
				</div>
			</div>
			<div class="clear"></div>
			
			<div class="section how_it_work_c">
				<h1>How does Zoggrs work?</h1>
				<h5>An awesome experience in just three steps</h5>
				
				<div class="how_it_work_icon_c">
					<div class="icon_info_c left">
						<div class="icon_c">
							<a href="#"><img src="front-end-images/search_icon2.png" alt="Search" /></a>
						</div>
						<h2>Search</h2>
						<p>Search through our comprehensive list of healthcare and wellness centers</p>
					</div>
					<div class="arrow_c left">						
					</div>
					<div class="icon_info_c left">
						<div class="icon_c">
							<a href="#"><img src="front-end-images/select_icon.png" alt="Search" /></a>							
						</div>
						<h2>Select</h2>
						<p>Compare and pick up the best option for you</p>
					</div>
					<div class="arrow_c left">						
					</div>
					<div class="icon_info_c left">
						<div class="icon_c">
							<a href="#"><img src="front-end-images/login_and_access_icon.png" alt="Search" /></a>							
						</div>
						<h2>Login & Access Perks</h2>
						<p>Gain access to various offers and features just by login in to Zoggrs<span id="recommendedForYou"></span></p>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>				
			</div>
			
			<div class="clear"></div>			
			<div class="center_page_icon">				
				<div class="icon_c">
					<img src="front-end-images/well-recommended_for_you_icon.png" alt="how it works icon" />
				</div>
			</div>
			<div class="clear"></div>
			
			<div class="section how_it_work_c">
				<h1>Recommended For You</h1>
				<h5>We hand pick the best for you when you're in need of quick beauty fix or seamless health treatment</h5>
				
				<div class="recommended_for_you_c">
					<div id="recomanded"></div>
					<!-- <div class="img_c img_c_l_margin left">
						<img src="front-end-images/wellness_img_1.jpg" alt="Search" />
						<div>
							<h4>With Dr. XYZ</h4>
							<a class="read_more_arrow" href="#">Read More</a>
						</div>
					</div>
					<div class="img_c img_c_l_margin left">
						<img src="front-end-images/wellness_img_2.jpg" alt="Search" />	
						<div>
							<h4>With Dr. XYZ</h4>
							<a class="read_more_arrow" href="#">Read More</a>
						</div>
					</div>
					<div class="img_c left">
						<img src="front-end-images/wellness_img_3.jpg" alt="Search" />	
						<div>
							<h4>With Dr. XYZ</h4>
							<a class="read_more_arrow" href="#">Read More</a>
						</div>
					</div>
					
					<div class="clear" style="margin-bottom: 1.2em;"></div>
					
					<div class="img_c img_c_l_margin left">
						<img src="front-end-images/wellness_img_4.jpg" alt="Search" />	
						<div>
							<h4>With Dr. XYZ</h4>
							<a class="read_more_arrow" href="#">Read More</a>
						</div>
					</div>
					<div class="img_c img_c_l_margin left">
						<img src="front-end-images/wellness_img_5.jpg" alt="Search" />	
						<div>
							<h4>With Dr. XYZ</h4>
							<a class="read_more_arrow" href="#">Read More</a>
						</div>
					</div>
					<div class="img_c left">
						<img src="front-end-images/wellness_img_6.jpg" alt="Search" />	
						<div>
							<h4>With Dr. XYZ</h4>
							<a class="read_more_arrow" href="#">Read More</a>
						</div>
					</div> -->
					<div class="clear"></div>
				</div>				
			</div>
			
			<div class="clear"></div>			
			<div class="center_page_icon"  style="margin-top: 8em;">				
				<div class="icon_c">
					<img src="front-end-images/well-mission_icon.png" alt="how it works icon" />
				</div>
			</div>
			<div class="clear"></div>
			
			<div class="section our_mission_c">
				<h1>Our Mission</h1>
				<!-- <h5>Some line will go here</h5> -->
				
				<div class="four_column_c our_mission_icon_c">				
					<div class="col_four mission_info_icon left">
						<img src="front-end-images/find_joy_in_simplicity.png" alt="Search" />
						<div class="download_arrow">
						</div>
					</div>
					<div class="col_four mission_info_icon col_four_l_margin left">
						<img src="front-end-images/regain_focus.png" alt="Search" />
						<div class="download_arrow">
						</div>
					</div>
					<div class="col_four mission_info_icon col_four_l_margin left">
						<img src="front-end-images/deal_with_fear.png" alt="Search" />
						<div class="download_arrow">
						</div>						
					</div>		
					<div class="col_four mission_info_icon right">
						<img src="front-end-images/but_eventually.png" alt="Search" />	
						<div class="download_arrow">
						</div>
					</div>		
					<div class="clear" style="margin-bottom: 1.2em;"></div>
				</div>
				
				<div class="our_mission_info_c">
					<div class="mission_info">
						<h4>Find joy in Simplicity</h4>
						<p>We want you to quickly take an informed decision through a carefully designed simple and soothing interface </p>
					</div>
					<div class="mission_info">
						<h4>Regain Focus</h4>
						<p>We think that focus on your health and personality can make your life simpler</p>
					</div>
					<div class="mission_info">
						<h4>Deal with fear</h4>
						<p>People are sceptical about their looks &amp; health for many reasons. If we can somehow ease that fear, then why not !!</p>
					</div>
					<div class="mission_info">
						<h4>But eventually</h4>
						<p>We just honour your smile</p>
					</div>
				</div>
				
			</div>
			
		</div>
		<!-- <div class="footer_c">
			<div class="footer_con">
				<p class="left">Copyright will go here</p>
				<ul class="social_ul right">
					<li><a href="#"><img src="images/fb_icon.png" alt="Facebook Icon" /></a></li>
					<li><a href="#"><img src="images/google_icon.png" alt="Facebook Icon" /></a></li>
					<li><a href="#"><img src="images/image_icon.png" alt="Facebook Icon" /></a></li>
					<li><a href="#"><img src="images/p_icon.png" alt="Facebook Icon" /></a></li>
					<li><a href="#"><img src="images/twitter_icon.png" alt="Facebook Icon" /></a></li>
				</ul>
				<div class="clear"></div>
			</div>
		</div> -->
  <?php include "footer-front-end.php"?>