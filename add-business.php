
<?php
require_once "chk_login_mer.php";
$objgen = new general();


include_once "classes/fileUpload.class.php";
$upload_success = true;
$shop_id = $_GET['id'];
$cat_id = base64_decode($_GET['cat']);
$insert_cat_id = $cat_id;
$merchant_id = $_SESSION['ma_log_id_mer'];

if (isset($_GET['id']) || (empty($_POST))) {
    if (empty($_POST)) {
        unset($_SESSION['refer_id']);
        unset($_SESSION['ext_photos_id']);
        unset($_SESSION['int_photos_id']);
        unset($_SESSION['team_photos_id']);
        unset($_SESSION['status']);
    }

    $shop = $objgen->get_Onerow("shop", "AND id=" . $shop_id);
    $shop_type = $shop['shop_type'];
    $_SESSION['status'] = $shop['status'];

    $ad_exp = $objgen->get_Onerow("ad_exp", "AND shop_id=" . $shop_id);
    $other_info = $objgen->get_Onerow("other_info", "AND shop_id=" . $shop_id);

    $gym = $objgen->get_Onerow("gym", "AND shop_id=" . $shop_id);


    $qry = "select * from photos where shop_id='" . $shop_id . "' and merchant_id='" . $merchant_id . "'";
    $photos = $objgen->get_AllRows_qry($qry);


    $ext_image = $int_image = $team_image = array();
    $ext_photos_id = $int_photos_id = $team_id = array();

     $Exterior_count = $Interior_count = $Team_count = 1;
    for ($i = 0; $i < count($photos); $i++) {
        if ($photos[$i]['catgeory'] == "Exterior") {
            $ext_image[$Exterior_count] = $objgen->check_tag($photos[$i]['thumb']);
            $ext_photos_id[$Exterior_count] = $objgen->check_tag($photos[$i]['id']);
            $Exterior_count++;
        } else if ($photos[$i]['catgeory'] == "Interior") {
            if ( $photos[$i]['set_profile_pic'] == "true") {
                $set_pic[$Interior_count] = $objgen->check_tag($photos[$i]['set_profile_pic']);
                $set_pic_id[$Interior_count] = $objgen->check_tag($photos[$i]['id']);
            }
            $int_image[$Interior_count] = $objgen->check_tag($photos[$i]['thumb']);
            $int_photos_id[$Interior_count] = $objgen->check_tag($photos[$i]['id']);
            $Interior_count++;
        } else if ($photos[$i]['catgeory'] == "Team") {
            $team_image[$Team_count] = $objgen->check_tag($photos[$i]['thumb']);
            $team_photos_id[$Team_count] = $objgen->check_tag($photos[$i]['id']);
            $Team_count++;
        }
    }
   

    $_SESSION['ext_photos_id'] = $ext_photos_id;
    $_SESSION['int_photos_id'] = $int_photos_id;
    $_SESSION['team_photos_id'] = $team_photos_id;



    $qry = "select * from refer where shop_id='" . $shop_id . "' and merchant_id='" . $merchant_id . "'";
    $refer = $objgen->get_AllRows_qry($qry);

    $name = array();
    $mobile = array();
    $refer_id = array();
    for ($i = 1; $i < 5; $i++) {

        $name[$i] = $objgen->check_tag($refer[$i - 1]['name']);
        $mobile[$i] = $objgen->check_tag($refer[$i - 1]['mobile']);
        $refer_id[$i] = $objgen->check_tag($refer[$i - 1]['id']);
    }

    $_SESSION['refer_id'] = $refer_id;


    $spend_on = explode(",", $ad_exp['spend_on']);

    if (!strpos($spend_on[0], "~")) {
        $spend_on_main = $spend_on[0];
        $j = 1;
    } else {
        $j = 0;
    }

    for ($i = $j; $i < count($spend_on); $i++) {
        $spend_on_type = explode("~", $spend_on[$i]);
        $this_spend_on = "spend_on_" . $spend_on_type[0];
        $$this_spend_on = $spend_on_type[1];
    }


    $categories = explode(",", $shop['categories']);

    foreach ($categories as $key => $value) {
        $value = Str_replace(" ", "_", str_replace("/", "_", $value));
        $categ_checked = $value . "_checked";
        $$categ_checked = "checked='true'";
    }


    //$shop['franch'] == "yes" ? $franch_yes = "checked='checked'" : $franch_no = "checked='checked'";


    $week_off = explode(",", $gym['week_off']);

    foreach ($week_off as $key => $value) {
        $week_off_checked = $value . "_checked";
        $$week_off_checked = "checked='true'";
    }


    $dumbell_type = explode(",", $gym['dumbell_type']);

    foreach ($dumbell_type as $key => $value) {
        $dumbell_type_checked = $value . "_checked";
        $$dumbell_type_checked = "checked='true'";
    }


    $parking = explode(",", $gym['parking']);

    foreach ($parking as $key => $value) {
        $parking_checked = $value . "_checked";
        $$parking_checked = "checked='true'";
    }


    $mobile_use = explode(",", $other_info['mobile_use']);

    foreach ($mobile_use as $key => $value) {
        $mobile_use_checked = $value . "_checked";
        $$mobile_use_checked = "checked='true'";
    }


    $social_media = explode(",", $other_info['social_media']);

    foreach ($social_media as $key => $value) {
        $value = str_replace(" ", "_", str_replace("/", "_", $value));
        if ($value == "Others")
            $value = "Others_social";
        $social_media_checked = $value . "_checked";
        $$social_media_checked = "checked='true'";
    }


    $enrol_sys = explode(",", $other_info['enrol_sys']);

    foreach ($enrol_sys as $key => $value) {
        if ($value == "Others")
            $value = "Others_enrol";
        $enrol_sys_checked = $value . "_checked";
        $$enrol_sys_checked = "checked='true'";
    }


    $gym['recep'] == "yes" ? $recep_yes = "checked='checked'" : $recep_no = "checked='checked'";

    $gym['diate'] == "yes" ? $diate_yes = "checked='checked'" : $diate_no = "checked='checked'";

    $gym['chang_room'] == "yes" ? $chang_room_yes = "checked='checked'" : $chang_room_no = "checked='checked'";

    $gym['wash_room'] == "yes" ? $wash_room_yes = "checked='checked'" : $wash_room_no = "checked='checked'";

    $gym['filt_water'] == "yes" ? $filt_water_yes = "checked='checked'" : $filt_water_no = "checked='checked'";

    $gym['ac'] == "yes" ? $ac_yes = "checked='checked'" : $ac_no = "checked='checked'";

    $gym['supple'] == "yes" ? $supple_yes = "checked='checked'" : $supple_no = "checked='checked'";

    $gym['heav_weigt'] == "yes" ? $heav_weigt_yes = "checked='checked'" : $heav_weigt_no = "checked='checked'";

    $gym['sell_consum'] == "yes" ? $sell_consum_yes = "checked='checked'" : $sell_consum_no = "checked='checked'";

    $gym['medit_room'] == "yes" ? $medit_room_yes = "checked='checked'" : $medit_room_no = "checked='checked'";

    $gym['credit_card'] == "yes" ? $credit_card_yes = "checked='checked'" : $credit_card_no = "checked='checked'";

    $gym['rate_card'] == "Provided" ? $rate_card_yes = "checked='checked'" : $rate_card_no = "checked='checked'";

    $gym['wifi'] == "yes" ? $wifi_yes = "checked='checked'" : $wifi_no = "checked='checked'";


    // switch ($gym['sub_type']) {
    //     case 'Monthly';
    //         $sub_type_Monthly = "checked='checked'";
    //         break;
    //     case 'Quarterly';
    //         $sub_type_Quarterly = "checked='checked'";
    //         break;
    //     case 'Half_Yearly';
    //         $sub_type_Half_Yearly = "checked='checked'";
    //         break;
    //     case 'Annual';
    //         $sub_type_Annual = "checked='checked'";
    //         break;
    // }


    // switch ($gym['discount']) {
    //     case '20';
    //         $discount_20 = "checked='checked'";
    //         break;
    //     case '30';
    //         $discount_30 = "checked='checked'";
    //         break;
    //     case '40';
    //         $discount_40 = "checked='checked'";
    //         break;
    // }


    $ad_exp['anywhere'] == "yes" ? $anywhere_yes = "checked='checked'" : $anywhere_no = "checked='checked'";

    $ad_exp['reg_srch'] == "yes" ? $reg_srch_yes = "checked='checked'" : $reg_srch_no = "checked='checked'";

    $ad_exp['web_page'] == "yes" ? $web_page_yes = "checked='checked'" : $web_page_no = "checked='checked'";

    $ad_exp['per_serv'] == "yes" ? $per_serv_yes = "checked='checked'" : $per_serv_no = "checked='checked'";

    $ad_exp['ads_us'] == "yes" ? $ads_us_yes = "checked='checked'" : $ads_us_no = "checked='checked'";

    $date_parts = explode("-", $shop['create_date']);
    $date_parts = array_reverse($date_parts);
    $create_date = implode("/", $date_parts);
}


if (!empty($_POST)) {
    foreach ($_POST as $key => $value) {
        if (!is_array($value)) {
            $$key = $objgen->check_input($value);
        }
    }

    $errors = array();

    if (!isset($_GET['id'])) {

        $shop_exit = $objgen->chk_Ext("shop", "merchant_id='" . $merchant_id . "' and shop_type='" . $shop_type . "' and shop_name='" . $shop_name . "' and status='active'");

        if ($shop_exit > 0) {
            $errors[] = "This shop is already exists.";
        }
    }

    if (empty($errors)) {

        $status = $_SESSION['status'];

        $upd_msg_success = $upd_msg_fail = "";
        $msg_shop = $msg_gym = $msg_oi = $msg_ad_exp = $msg_refer = "";
        $img_path = "uploads/";

        $cat_checked = "";
        if (!empty($_POST['categories'])) {
            foreach ($_POST['categories'] as $cat) {
                $cat_checked .= $cat . ",";
            }
        }

        $cat_checked = rtrim($cat_checked, ",");

        $date_parts = explode("/", $create_date);
        $date_parts = array_reverse($date_parts);
        $create_date = implode("-", $date_parts);


        /*$week_off_checked = "";
        if (!empty($_POST['week_off'])) {
            foreach ($_POST['week_off'] as $wk_off) {
                $week_off_checked .= $wk_off . ",";
            }
        }*/

         /*$week_off_checked = rtrim($week_off_checked, ",");*/


        $db_type_checked = "";
        if (!empty($_POST['dumbell_type'])) {
            foreach ($_POST['dumbell_type'] as $db_type) {
                $db_type_checked .= $db_type . ",";
            }
        }

        $db_type_checked = rtrim($db_type_checked, ",");


        $park_checked = "";
        if (!empty($_POST['parking'])) {
            foreach ($_POST['parking'] as $park) {
                $park_checked .= $park . ",";
            }
        }

        $park_checked = rtrim($park_checked, ",");



        $mob_use_checked = "";
        if (!empty($_POST['mobile_use'])) {
            foreach ($_POST['mobile_use'] as $mob_use) {
                $mob_use_checked .= $mob_use . ",";
            }
        }

        $mob_use_checked = rtrim($mob_use_checked, ",");



        $soc_media_checked = "";
        if (!empty($_POST['social_media'])) {
            foreach ($_POST['social_media'] as $soc_media) {
                $soc_media_checked .= $soc_media . ",";
            }
        }

        $soc_media_checked = rtrim($soc_media_checked, ",");



        $enrol_sys_checked = "";
        if (!empty($_POST['enrol_sys'])) {
            foreach ($_POST['enrol_sys'] as $enrol) {
                $enrol_sys_checked .= $enrol . ",";
            }
        }

        $enrol_sys_checked = rtrim($enrol_sys_checked, ",");



        $spend_on_all = "";

        if ($_POST['spend_on'] != "") {
            $spend_on_all .= $spend_on . ",";
        }

        if ($_POST['jdial'] != "") {
            $spend_on_all .= "jdial~" . $jdial . ",";
        }

        if ($_POST['google'] != "") {
            $spend_on_all .= "google~" . $google . ",";
        }

        if ($_POST['mag'] != "") {
            $spend_on_all .= "mag~" . $mag . ",";
        }

        if ($_POST['pamph'] != "") {
            $spend_on_all .= "pamph~" . $pamph . ",";
        }

        if ($_POST['news'] != "") {
            $spend_on_all .= "news~" . $news . ",";
        }

        if ($_POST['other_sites'] != "") {
            $spend_on_all .= "other_sites~" . $other_sites;
        }

        $spend_on_all = rtrim($spend_on_all, ",");
        
        if(empty($lattitude) && empty($longitude)){
            $address= $building_name . ' , ' . $street . ' , ' . $landmark. ' , ' . $locality. ' , ' . $city;
            $address = str_replace(" ", "+", $address);
            $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
            $json = json_decode($json);
            $lattitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $longitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
        }

        if (isset($_GET['id'])) {
            $catId = $objgen->get_Onerow('shop', "AND id=" . $shop_id);
            $phoneno = $phone . "-" . $phone1;
            $msg_shop = $objgen->upd_Row('shop', "merchant_id='" . $merchant_id . "',shop_type='" . $shop_type . "',`shop_name`='" . $shop_name . "',`building_name`='" . $building_name . "',`street`='" . $street . "',`state_id`='" . $state . "',`locality`='" . $locality . "',`landmark`='" . $landmark . "',`website`='" . $website . "',`city`='" . $city . "',`country`='" . $country . "',`pincode`='" . $pincode . "',`email`='" . $email . "',`phone`='" . $phoneno . "',`mobile`='" . $mobile . "',`categories`='" . $catId['categories'] . "',`esta_date`='" . $esta_date . "',`create_date`='" . $create_date . "',`status`='" . $status . "',`work_start_time`='" . $work_morn_from . "',`work_end_time`='" . $work_morn_to . "',`overview`='".$overview."',`longitude`='" . $longitude . "',`lattitude`='" . $lattitude . "'", "id=" . $shop_id);
        } else {
            $main_cat = $_POST['main_cat'];
            $create_date = date('Y-m-d');
            $phoneno = $phone . "-" . $phone1;
            $msg_shop = $objgen->ins_Row('shop', '`merchant_id`,`shop_type`,`shop_name`,`building_name`,`street`,`state_id`,`locality`,`landmark`,`website`,`city`,`country`,`pincode`,`email`,`phone`,`mobile`,`categories`,`esta_date`,`create_date`,`status`,`work_start_time`,`work_end_time`,`overview`,`longitude`,`lattitude`', "'" . $merchant_id . "','" . $insert_cat_id . "','" . $shop_name . "','" . $building_name . "','" . $street . "','" . $state . "','" . $locality . "','" . $landmark . "','" . $website . "','" . $city . "','" . $country . "','" . $pincode . "','" . $email . "','" . $phoneno . "','" . $mobile . "','" . $main_cat . "','" . $esta_date . "','" . $create_date . "','inactive','" . $work_morn_from . "','" . $work_morn_to . "','".$overview."','" . $longitude . "','" . $lattitude . "'");
            $shop_id = $objgen->get_insetId();
        }







        foreach ($_POST as $key => $value) {
            if (!is_array($value)) {
                $$key = $objgen->check_input($value);
                 // $key . " = " . $$key . ;
            }
        }


        if (isset($_GET['id'])) {

            $del_subcat = $objgen->del_Row('shop_categories', "shop_id=" . $shop_id);
            for ($g = 0; $g < count($_POST['subcategory']); $g++) {
                if ($_POST['subcategory'][$g] == 1) {
                    $_POST['m_sub_category_price'][$g];
                    $_POST['sub_cat'][$g];
                    $_POST['group'][$g];
                    $_POST['m_sub_category_time'][$g];
                    $main_cat = $_POST['main_cat'];
                    $main_cat = $_POST['main_cat'];
                    $msg_shop_sub_cat = $objgen->ins_Row('shop_categories', '`m_id`,`shop_id`,`cat_id`,`group_id`,`sub_cat`,`price`,`time`', "'" . $merchant_id . "','" . $shop_id . "','" . $main_cat . "','" . $_POST['group'][$g] . "','" . $_POST['sub_cat'][$g] . "','" . $_POST['m_sub_category_price'][$g] . "','" . $_POST['m_sub_category_time'][$g] . "'");
                    // $msg_shop_sub_cat = $objgen->upd_Row('shop_categories',"m_id='".$merchant_id."',
                    //  cat_id='".$main_cat."',sub_cat='".$_POST['sub_cat'][$g]."',
                    //  price='".$_POST['m_sub_category_price'][$g]."',time='".$_POST['m_sub_category_time'][$g]."'","shop_id=".$shop_id);
                }
            }
        } else {
            for ($g = 0; $g < count($_POST['subcategory']); $g++) {
                if ($_POST['subcategory'][$g] == 1) {
                    $_POST['m_sub_category_price'][$g];
                    $_POST['sub_cat'][$g];
                    $_POST['group'][$g];
                    $_POST['m_sub_category_time'][$g];
                    $main_cat = $_POST['main_cat'];
                    $msg_shop_sub_cat = $objgen->ins_Row('shop_categories', '`m_id`,`shop_id`,`cat_id`,`group_id`,`sub_cat`,`price`,`time`', "'" . $merchant_id . "','" . $shop_id . "','" . $main_cat . "','" . $_POST['group'][$g] . "','" . $_POST['sub_cat'][$g] . "','" . $_POST['m_sub_category_price'][$g] . "','" . $_POST['m_sub_category_time'][$g] . "'");
                }
            }
        }
 /*         * *****  Code Added By Shridhar **** */
        //$facility = explode(" ",$_POST['facility']);
        if($_POST['facility']) {
            
        
        
            //echo $fact_arr;exit;
            if (isset($_GET['id'])) {
                $del_fsa = $objgen->del_Row('shop_facilities', "shop_id=" . $shop_id);
                $catId = $objgen->get_Onerow('shop', "AND id=" . $shop_id);
                for ($g = 0; $g < count($_POST['facility']); $g++) {
                         $_POST['facility'][$g];
                        $msg_facility = $objgen->ins_Row('shop_facilities', '`shop_id`,`cat_id`,`merchant_id`,`facility_id`', "'" . $shop_id . "','" . $catId['categories'] . "','" . $merchant_id . "','" . $_POST['facility'][$g] . "'");
                    }
                // $msg_facility = $objgen->upd_Row('shop_facilities', "cat_id='" . $catId['categories'] . "',
                // merchant_id='" . $merchant_id . "',`facility_id`='" . $fact_arr . "'", "shop_id=" . $shop_id);
            } else {

                $main_cat = $_POST['main_cat'];

                for ($g = 0; $g < count($_POST['facility']); $g++) {
                         $_POST['facility'][$g];
                        $msg_facility = $objgen->ins_Row('shop_facilities', '`shop_id`,`cat_id`,`merchant_id`,`facility_id`', "'" . $shop_id . "','" . $main_cat . "','" . $merchant_id . "','" . $_POST['facility'][$g]  . "'");
                    }
                
            }
        }
        if($_POST['product']) { 
           //$product_arr = implode(",",$_POST['product']);
            //echo $product_arr;exit;
            if(isset($_GET['id']))
            {
                $del_subcat = $objgen->del_Row('shop_products', "shop_id=" . $shop_id);
                for ($g = 0; $g < count($_POST['product']); $g++) {
                    $_POST['product'][$g];
                $msg_product = $objgen->ins_Row('shop_products','`shop_id`,`merchant_id`,`cat_id`,`product_id`',
                    "'".$shop_id."','".$merchant_id."','".$catId['categories']."','".$_POST['product'][$g]."'");
                 }
                /*$msg_facility = $objgen->upd_Row('shop_products',"merchant_id='".$merchant_id."',cat_id='".$catId['categories']."',
                    `product_id`='".$product_arr."'","shop_id=".$shop_id);*/
            }
            else {

                $main_cat = $_POST['main_cat'];
                for ($g = 0; $g < count($_POST['product']); $g++) {
                    $_POST['product'][$g];
                   

                    $msg_product = $objgen->ins_Row('shop_products','`shop_id`,`merchant_id`,`cat_id`,`product_id`',
                    "'".$shop_id."','".$merchant_id."','".$main_cat."','".$_POST['product'][$g]."'");
                }
                /*$msg_facility = $objgen->ins_Row('shop_products','`shop_id`,`merchant_id`,`cat_id`,`product_id`',
                "'".$shop_id."','".$merchant_id."','".$main_cat."','".$product_arr."'");*/
            
            }
        }


        if ($msg_shop == "") {


            if (isset($_GET['id'])) {
                $msg_gym = $objgen->upd_Row('gym', "`work_morn_from`='" . $work_morn_from . "', `work_morn_to`='" . $work_morn_to . "', `work_even_from`='" . $work_even_from . "', `work_even_to`='" . $work_even_to . "', `gym_morn_from`='" . $gym_morn_from . "', `gym_morn_to`='" . $gym_morn_to . "', `gym_even_from`='" . $gym_even_from . "', `gym_even_to`='" . $gym_even_to . "', `yog_morn_from`='" . $yog_morn_from . "', `yog_morn_to`='" . $yog_morn_to . "', `yog_even_from`='" . $yog_even_from . "', `yog_even_to`='" . $yog_even_to . "', `aer_morn_from`='" . $aer_morn_from . "', `aer_morn_to`='" . $aer_morn_to . "', `aer_even_from`='" . $aer_even_from . "', `aer_even_to`='" . $aer_even_to . "', `dan_morn_from`='" . $dan_morn_from . "', `dan_morn_to`='" . $dan_morn_to . "', `dan_even_from`='" . $dan_even_from . "', `dan_even_to`='" . $dan_even_to . "', `week_off`='" . $week_off . "', `gym_tra`='" . $gym_tra . "', `aer_tra`='" . $aer_tra . "', `recep`='" . $recep . "', `yog_tra`='" . $yog_tra . "', `dac_tra`='" . $dac_tra . "', `diate`='" . $diate . "', `tread_no`='" . $tread_no . "', `chang_room`='" . $chang_room . "', `wash_room`='" . $wash_room . "', `exec_cycle`='" . $exec_cycle . "', `filt_water`='" . $filt_water . "', `ac`='" . $ac . "', `area_sqr`='" . $area_sqr . "', `supple`='" . $supple . "', `heav_weigt`='" . $heav_weigt . "', `sell_consum`='" . $sell_consum . "', `dumbell`='" . $dumbell . "', `dumbell_type`='" . $db_type_checked . "', `combo_gym`='" . $combo_gym . "', `non_bench`='" . $non_bench . "', `medit_room`='" . $medit_room . "', `credit_card`='" . $credit_card . "', `rate_card`='" . $rate_card . "', `rate_card_file`='" . $rate_card_file . "',  `parking`='" . $park_checked . "', `wifi`='" . $wifi . "'", "merchant_id='" . $merchant_id . "' AND`shop_id`='" . $shop_id . "'");
            } else {

                $msg_gym = $objgen->ins_Row('gym', '`merchant_id`, `shop_id`, `work_morn_from`, `work_morn_to`, `work_even_from`, `work_even_to`, `gym_morn_from`, `gym_morn_to`, `gym_even_from`, `gym_even_to`, `yog_morn_from`, `yog_morn_to`, `yog_even_from`, `yog_even_to`, `aer_morn_from`, `aer_morn_to`, `aer_even_from`, `aer_even_to`, `dan_morn_from`, `dan_morn_to`, `dan_even_from`, `dan_even_to`, `week_off`, `gym_tra`, `aer_tra`, `recep`, `yog_tra`, `dac_tra`, `diate`, `tread_no`, `chang_room`, `wash_room`, `exec_cycle`, `filt_water`, `ac`, `area_sqr`, `supple`, `heav_weigt`, `sell_consum`, `dumbell`, `dumbell_type`, `combo_gym`, `non_bench`, `medit_room`, `credit_card`, `rate_card`, `rate_card_file`, `parking`, `wifi`', "'" . $merchant_id . "','" . $shop_id . "','" . $work_morn_from . "','" . $work_morn_to . "','" . $work_even_from . "','" . $work_even_to . "','" . $gym_morn_from . "','" . $gym_morn_to . "','" . $gym_even_from . "','" . $gym_even_to . "','" . $yog_morn_from . "','" . $yog_morn_to . "','" . $yog_even_from . "','" . $yog_even_to . "','" . $aer_morn_from . "','" . $aer_morn_to . "','" . $aer_even_from . "','" . $aer_even_to . "','" . $dan_morn_from . "','" . $dan_morn_to . "','" . $dan_even_from . "','" . $dan_even_to . "','" . $week_off . "','" . $gym_tra . "','" . $aer_tra . "','" . $recep . "','" . $yog_tra . "','" . $dac_tra . "','" . $diate . "','" . $tread_no . "','" . $chang_room . "','" . $wash_room . "','" . $exec_cycle . "','" . $filt_water . "','" . $ac . "','" . $area_sqr . "','" . $supple . "','" . $heav_weigt . "','" . $sell_consum . "','" . $dumbell . "','" . $db_type_checked . "','" . $combo_gym . "','" . $non_bench . "','" . $medit_room . "','" . $credit_card . "','" . $rate_card . "','" . $rate_card_file . "','" . $park_checked . "','" . $wifi . "'");
            }
        }


        if ($msg_gym == "") {

            if (isset($_GET['id'])) {
                $msg_oi = $objgen->upd_Row('other_info', "`mobile_use`='" . $mob_use_checked . "',`social_media`='" . $soc_media_checked . "',`enrol_sys`='" . $enrol_sys_checked . "'", "merchant_id='" . $merchant_id . "' AND`shop_id`='" . $shop_id . "'");
            } else {
                $msg_oi = $objgen->ins_Row('other_info', '`merchant_id`,`shop_id`,`mobile_use`,`social_media`,`enrol_sys`', "'" . $merchant_id . "','" . $shop_id . "','" . $mob_use_checked . "','" . $soc_media_checked . "','" . $enrol_sys_checked . "'");
            }
        }


        if ($msg_oi == "") {

            if (isset($_GET['id'])) {
                $msg_ad_exp = $objgen->upd_Row('ad_exp', "`anywhere`='" . $anywhere . "',`spend`='" . $spend . "',`spend_on`='" . $spend_on_all . "',`reg_srch`='" . $reg_srch . "',`web_page`='" . $web_page . "',`per_serv`='" . $per_serv . "',`ads_us`='" . $ads_us . "',`budget`='" . $budget . "'", "merchant_id='" . $merchant_id . "' AND`shop_id`='" . $shop_id . "'");
            } else {
                $msg_ad_exp = $objgen->ins_Row('ad_exp', '`merchant_id`,`shop_id`,`anywhere`,`spend`,`spend_on`,`reg_srch`,`web_page`,`per_serv`,`ads_us`,`budget`', "'" . $merchant_id . "','" . $shop_id . "','" . $anywhere . "','" . $spend . "','" . $spend_on_all . "','" . $reg_srch . "','" . $web_page . "','" . $per_serv . "','" . $ads_us . "','" . $budget . "'");
            }
        }


        if ($msg_ad_exp == "") {

            $qry = "select * from refer where shop_id='" . $shop_id . "' and merchant_id='" . $merchant_id . "' order by id asc";
            $refer = $objgen->get_AllRows_qry($qry);

            $name = array();
            $mobile = array();


            for ($i = 1; $i < 5; $i++) {
                $name = "name" . $i;
                $mobile = "mobile" . $i;
                $id = "id" . $i;


                $name_data = ($$name != '') ? $$name : $objgen->check_tag($refer[$i - 1]['name']);
                $mobile_data = ($$mobile != '') ? $$mobile : $objgen->check_tag($refer[$i - 1]['mobile']);

                $id_data = ($$id != '') ? $$id : $objgen->check_tag($refer[$i - 1]['id']);


                if ($id_data != '') {
                    $msg_refer = $objgen->upd_Row('refer', "`name`='" . $name_data . "',`mobile`='" . $mobile_data . "'", "merchant_id='" . $merchant_id . "' AND`shop_id`='" . $shop_id . "' AND id='" . $id_data . "'");
                } else {
                    $msg_refer = $objgen->ins_Row('refer', '`merchant_id`,`shop_id`,`name`,`mobile`', "'" . $merchant_id . "','" . $shop_id . "','" . $name_data . "','" . $mobile_data . "'");
                }
            }
        }



        $catgeory_array = array();

        if (isset($_FILES['Exterior1']['name']) || isset($_FILES['Exterior2']['name'])) {
            array_push($catgeory_array, "Exterior");
        }
        if (isset($_FILES['Interior1']['name']) || isset($_FILES['Interior2']['name']) || isset($_FILES['Interior3']['name']) || isset($_FILES['Interior4']['name']) || isset($_FILES['Interior5']['name']) || isset($_FILES['Interior6']['name'])) {
            array_push($catgeory_array, "Interior");
        }
        if (isset($_FILES['Team1']['name']) || isset($_FILES['Team2']['name'])) {
            array_push($catgeory_array, "Team");
        }


       
        if ($msg_refer == "") {

            $ext_count = $int_count = $team_count = 1;
            /*if ($_POST['profile_pic']=='1') {
                $set_profile_pic='true';
            }elseif ($_POST['profile_pic']=='2') {
                 $set_profile_pic='true';
            }elseif ($_POST['profile_pic']=='3') {
                $set_profile_pic='true';
            }elseif ($_POST['profile_pic']=='4') {
                $set_profile_pic='true';
            }else{
                $set_profile_pic='false';
            }*/
            //$set_profile_pic=$_POST['profile_pic'];
                        //echo '<pre>';print_r($catgeory_array);exit; 
            foreach ($catgeory_array as $cat_photos) {

                switch ($cat_photos) {
                   
                    case 'Interior':
                        if ($_POST['profile_pic']=='1' ) {
                           $set_profile_pic='true';
                        $int_photos_id = $_SESSION['int_photos_id'][$int_count++];
                       
                        $upd_msg_arr = upload_file("Interior1", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                        }else{
                        $set_profile_pic='false';
                         $int_photos_id = $_SESSION['int_photos_id'][$int_count++];
                       
                        $upd_msg_arr = upload_file("Interior1", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                        }
                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];
                        if ($_POST['profile_pic']=='2') {
                         $set_profile_pic='true';
           
                        $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                        $upd_msg_arr = upload_file("Interior2", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                         }else{
                         $set_profile_pic='false';
                            $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                            $upd_msg_arr = upload_file("Interior2", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                         }
                         
                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];
                        if ($_POST['profile_pic']=='3') {
                            
                            $set_profile_pic='true';
                            $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                            $upd_msg_arr = upload_file("Interior3", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                        }else{
                            $set_profile_pic='false';
                            $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                            $upd_msg_arr = upload_file("Interior3", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                         }
                       

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];
                        if ($_POST['profile_pic']=='4') {
                            $set_profile_pic='true';
                        $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                        $upd_msg_arr = upload_file("Interior4", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                        }else{
                            $set_profile_pic='false';
                            $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                        $upd_msg_arr = upload_file("Interior4", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                         }
                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                       /* $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                        $upd_msg_arr = upload_file("Interior5", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id,$set_profile_pic="0");

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                        $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                        $upd_msg_arr = upload_file("Interior6", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id);

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];*/

                        break;
                    case 'Team':

                        $team_photos_id = $_SESSION['team_photos_id'][$team_count++];
                        $set_profile_pic='false';
                        $upd_msg_arr = upload_file("Team1", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $team_photos_id, $set_profile_pic);

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                        $team_photos_id = $_SESSION['team_photos_id'][$team_count++];
                        $set_profile_pic='false';
                        $upd_msg_arr = upload_file("Team2", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $team_photos_id, $set_profile_pic);

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                        break;
                        case 'Exterior':
                        $set_profile_pic='false';
                        $ext_photos_id = $_SESSION['ext_photos_id'][$ext_count++];

                        $upd_msg_arr = upload_file("Exterior1", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $ext_photos_id, $set_profile_pic);

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                        $ext_photos_id = $_SESSION['ext_photos_id'][$ext_count++];
                        $set_profile_pic='false';
                        $upd_msg_arr = upload_file("Exterior2", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $ext_photos_id, $set_profile_pic);

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                        break;
                }
            }
            
        }
    }


    if ($upload_success == true) {
        @header("location:" . URL . "manage-business.php?msg=1");
    } else {
        header("location:" . URL . "manage-business.php?msg=2");
    }
}
?>
<!doctype html>
<html autocomplete="off">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>
</head>

<body>

<div class="header-top"> <a href="<?=URL?>home" class="logo"> <img src="<?=URL?>front-end-images/zoggrs_logo.png" alt="zoggrs"> </a>
</div>

<div class="wrapper row-offcanvas row-offcanvas-left">

<?php include 'merchant-menu.php'; ?>

<aside class="right-side">

  <section class="content-header-top">
    <h1> <i class="fa fa-suitcase"></i> Business Profile </h1>
  </section>

  <section class="content">
    <div class="row">

                <?php
                if($msg2!="")
                {
                ?>
                <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <b>Alert!</b> <?php echo $msg2; ?>
                </div>

                <?php
                }

                if (!empty($errors)) {
                ?>
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Please fix the following errors:</b> <br>
                                <?php
                                    foreach ($errors as $error1)
                                     echo "<div> - ".$error1." </div>";
                                ?>
                    </div>

                <?php
                }

                if($msg!="")
                {
                ?>
             <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Alert!</b> <?php echo $msg; ?>
                </div>
                <?php
                }
                ?>

        <form name="add_business" id="add_business" method="post" enctype="multipart/form-data" autocomplete="off">

        <div id="wizard">

                <h2></h2>

        <section>

        <div class="main-hed">
          <h5>A Basic Information</h5>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Shop/Business Name <span style="color:red;"> *</span></label>
            <input type="text" class="form-control" name="shop_name" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['shop_name']):''; ?>" required >
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label>Building Name <span style="color:red;"> *</span></label>
            <input type="text" class="form-control" name="building_name" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['building_name']):''; ?>" required >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Street <span style="color:red;"> *</span></label>
            <input type="text" class="form-control" name="street" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['street']):''; ?>" required >
          </div>
        </div>
        
       

<?php

    $qry = "select * from city";
    $city = $objgen->get_AllRows_qry($qry);

    
    $city_fromdb = isset($_GET['id'])?$objgen->check_tag($shop['city']):'';

    $city_selected = '';
?>

        <div class="col-md-4">
          <div class="form-group">
            <label>City <span style="color:red;"> *</span></label><br>
                        <select name="city" id="city" required  class="form-control medium" onChange="getCity(this.value);" autocomplete="off">
                        <option value="">Select City</option>
                        <?php
                            for($i=0;$i<count($city);$i++){
                                if($city_fromdb == $city[$i]['id'])
                                {
                                    
                                    $city_selected = 'selected="selected"';
                                }
                                else
                                {

                                    $city_selected = '';
                                }
                        ?>
                                 <option value="<?php echo $city[$i]['id'];?>" <?php echo $city_selected; ?>><?php echo $city[$i]['city'];?></option>
                                 
                                
                        <?php
                            }
                        ?>
                        </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            
            <label>Locality <span style="color:red;"> *</span></label>
            <?php if (isset($_GET['id'])) {?>
                <div id="local1"></div>
           <?php  } else {?>
                <div id="local">
                    <select required class="form-control medium">
                        <option value="">Select Location</option>
                    </select>
                </div>
            <?php }?>
             
           
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Landmark*</label>
            <input type="text" class="form-control" name="landmark" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['landmark']):''; ?>" required >
          </div>
        </div>
         


        <div class="col-md-4">
          <div class="form-group">
            <label>Pincode <span style="color:red;"> *</span></label>
            <input type="number" class="form-control" name="pincode" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['pincode']):''; ?>" required >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" name="email" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['email']):''; ?>" >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Phone </label>
            <div>
                <?php if(isset($_GET['id'])) {
                $phone = explode('-',$objgen->check_tag($shop['phone']));
                 }?>
            <input type="text" class="form-control" id="phone" maxlength="5" name="phone" value="<?php if(isset($_GET['id'])) echo $phone[0]; ?>" style="width:25%;float:left;" >
            <input type="text" class="form-control" id="phone1" name="phone1" value="<?php if(isset($_GET['id'])) echo $phone[1]; ?>"style="width:70%;float:left;margin-left:10px;"  >
            <span id="errmsg"></span>
        </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Mobile <span style="color:red;"> *</span></label>
            <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['mobile']):''; ?>" required>
          </div>
        </div>
<script type="text/javascript">
  jQuery("#phone").keypress(function (e) {
   if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    }
   });
  jQuery("#phone1").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       return false;
    }
   });
  jQuery("#mobile").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       return false;
    }
   });
</script>

        

          
        

        

<!-- <div class="col-md-12">
          <div class="main-hed">
            <h5><strong>Operating Hours</strong></h5>
          </div>
        </div> -->


<div class="col-md-4">
          <div class="form-group">
            <h5>Working Hours</h5>
              <select class="form-control smal" name="work_morn_from">
                <option value="01:00:00" <?php echo $gym['work_morn_from']=="01:00:00"?"selected='selected'":""; ?> >01.00 </option>
                <option value="02:00:00" <?php echo $gym['work_morn_from']=="02:00:00"?"selected='selected'":""; ?> >02.00 </option>
                <option value="03:00:00" <?php echo $gym['work_morn_from']=="03:00:00"?"selected='selected'":""; ?> >03.00 </option>
                <option value="04:00:00" <?php echo $gym['work_morn_from']=="04:00:00"?"selected='selected'":""; ?> >04.00 </option>
                <option value="05:00:00" <?php echo $gym['work_morn_from']=="05:00:00"?"selected='selected'":""; ?> >05.00 </option>
                <option value="06:00:00" <?php echo $gym['work_morn_from']=="06:00:00"?"selected='selected'":""; ?> >06.00 </option>
                <option value="07:00:00" <?php echo $gym['work_morn_from']=="07:00:00"?"selected='selected'":""; ?> >07.00 </option>
                <option value="08:00:00" <?php echo $gym['work_morn_from']=="08:00:00"?"selected='selected'":""; ?> >08.00 </option>
                <option value="09:00:00" <?php echo $gym['work_morn_from']=="09:00:00"?"selected='selected'":""; ?> >09.00 </option>
                <option value="10:00:00" <?php echo $gym['work_morn_from']=="10:00:00"?"selected='selected'":""; ?> >10.00 </option>
                <option value="11:00:00" <?php echo $gym['work_morn_from']=="11:00:00"?"selected='selected'":""; ?> >11.00 </option>
                <option value="12:00:00" <?php echo $gym['work_morn_from']=="12:00:00"?"selected='selected'":""; ?> >12.00</option>
                <option value="13:00:00" <?php echo $gym['work_morn_from']=="13:00:00"?"selected='selected'":""; ?> >13.00 </option>
                <option value="14:00:00" <?php echo $gym['work_morn_from']=="14:00:00"?"selected='selected'":""; ?> >14.00 </option>
                <option value="15:00:00" <?php echo $gym['work_morn_from']=="15:00:00"?"selected='selected'":""; ?> >15.00 </option>
                <option value="16:00:00" <?php echo $gym['work_morn_from']=="16:00:00"?"selected='selected'":""; ?> >16.00 </option>
                <option value="17:00:00" <?php echo $gym['work_morn_from']=="17:00:00"?"selected='selected'":""; ?> >17.00 </option>
                <option value="18:00:00" <?php echo $gym['work_morn_from']=="18:00:00"?"selected='selected'":""; ?> >18.00 </option>
                <option value="19:00:00" <?php echo $gym['work_morn_from']=="19:00:00"?"selected='selected'":""; ?> >19.00 </option>
                <option value="20:00:00" <?php echo $gym['work_morn_from']=="20:00:00"?"selected='selected'":""; ?> >20.00 </option>
                <option value="21:00:00" <?php echo $gym['work_morn_from']=="21:00:00"?"selected='selected'":""; ?> >21.00 </option>
                <option value="22:00:00" <?php echo $gym['work_morn_from']=="22:00:00"?"selected='selected'":""; ?> >22.00 </option>
                <option value="23:00:00" <?php echo $gym['work_morn_from']=="23:00:00"?"selected='selected'":""; ?> >23.00 </option>
                <option value="23:59:59" <?php echo $gym['work_morn_from']=="23:59:59"?"selected='selected'":""; ?> >23.59 </option>
                
              </select>
              <div class="to">to</div>
              <select class="form-control smal" name="work_morn_to">
                <option value="01:00:00" <?php echo $gym['work_morn_to']=="01:00:00"?"selected='selected'":""; ?> >01.00 </option>
                <option value="02:00:00" <?php echo $gym['work_morn_to']=="02:00:00"?"selected='selected'":""; ?> >02.00 </option>
                <option value="03:00:00" <?php echo $gym['work_morn_to']=="03:00:00"?"selected='selected'":""; ?>  >03.00 </option>
                <option value="04:00:00" <?php echo $gym['work_morn_to']=="04:00:00"?"selected='selected'":""; ?> >04.00 </option>
                <option value="05:00:00" <?php echo $gym['work_morn_to']=="05:00:00"?"selected='selected'":""; ?> >05.00 </option>
                <option value="06:00:00" <?php echo $gym['work_morn_to']=="06:00:00"?"selected='selected'":""; ?> >06.00 </option>
                <option value="07:00:00" <?php echo $gym['work_morn_to']=="07:00:00"?"selected='selected'":""; ?> >07.00 </option>
                <option value="08:00:00" <?php echo $gym['work_morn_to']=="08:00:00"?"selected='selected'":""; ?> >08.00 </option>
                <option value="09:00:00" <?php echo $gym['work_morn_to']=="09:00:00"?"selected='selected'":""; ?> >09.00 </option>
                <option value="10:00:00" <?php echo $gym['work_morn_to']=="10:00:00"?"selected='selected'":""; ?> >10.00 </option>
                <option value="11:00:00" <?php echo $gym['work_morn_to']=="11:00:00"?"selected='selected'":""; ?> >11.00 </option>
                <option value="12:00:00" <?php echo $gym['work_morn_to']=="12:00:00"?"selected='selected'":""; ?> >12.00 </option>
                <option value="13:00:00" <?php echo $gym['work_morn_to']=="13:00:00"?"selected='selected'":""; ?> >13.00 </option>
                <option value="14:00:00" <?php echo $gym['work_morn_to']=="14:00:00"?"selected='selected'":""; ?> >14.00 </option>
                <option value="15:00:00" <?php echo $gym['work_morn_to']=="15:00:00"?"selected='selected'":""; ?> >15.00 </option>
                <option value="16:00:00" <?php echo $gym['work_morn_to']=="16:00:00"?"selected='selected'":""; ?> >16.00 </option>
                <option value="17:00:00" <?php echo $gym['work_morn_to']=="17:00:00"?"selected='selected'":""; ?> >17.00 </option>
                <option value="18:00:00" <?php echo $gym['work_morn_to']=="18:00:00 M"?"selected='selected'":""; ?>>18.00 </option>
                <option value="19:00:00" <?php echo $gym['work_morn_to']=="19:00:00"?"selected='selected'":""; ?> >19.00 </option>
                <option value="20:00:00" <?php echo $gym['work_morn_to']=="20:00:00"?"selected='selected'":""; ?> >20.00 </option>
                <option value="21:00:00" <?php echo $gym['work_morn_to']=="21:00:00"?"selected='selected'":""; ?> >21.00 </option>
                <option value="22:00:00" <?php echo $gym['work_morn_to']=="22:00:00"?"selected='selected'":""; ?> >22.00 </option>
                <option value="23:00:00" <?php echo $gym['work_morn_to']=="23:00:00"?"selected='selected'":""; ?> >23.00 </option>
                <option value="23:59:59" <?php echo $gym['work_morn_to']=="23:59:59"?"selected='selected'":""; ?> >23.59</option>
              </select>
          </div>
        </div>
<div class="col-md-4">
  <div class="form-group">
    <h5> Break  Hours</h5>
              <select class="form-control smal" name="work_even_from">
                
                <option value="01:00:00" <?php echo $gym['work_even_from']=="01:00:00"?"selected='selected'":""; ?> >01.00 </option>
                <option value="02:00:00" <?php echo $gym['work_even_from']=="02:00:00"?"selected='selected'":""; ?> >02.00 </option>
                <option value="03:00:00" <?php echo $gym['work_even_from']=="03:00:00"?"selected='selected'":""; ?>  >03.00 </option>
                <option value="04:00:00" <?php echo $gym['work_even_from']=="04:00:00"?"selected='selected'":""; ?> >04.00 </option>
                <option value="05:00:00" <?php echo $gym['work_even_from']=="05:00:00"?"selected='selected'":""; ?> >05.00 </option>
                <option value="06:00:00" <?php echo $gym['work_even_from']=="06:00:00"?"selected='selected'":""; ?> >06.00 </option>
                <option value="07:00:00" <?php echo $gym['work_even_from']=="07:00:00"?"selected='selected'":""; ?> >07.00 </option>
                <option value="08:00:00" <?php echo $gym['work_even_from']=="08:00:00"?"selected='selected'":""; ?> >08.00 </option>
                <option value="09:00:00" <?php echo $gym['work_even_from']=="09:00:00"?"selected='selected'":""; ?> >09.00 </option>
                <option value="10:00:00" <?php echo $gym['work_even_from']=="10:00:00"?"selected='selected'":""; ?> >10.00 </option>
                <option value="11:00:00" <?php echo $gym['work_even_from']=="11:00:00"?"selected='selected'":""; ?> >11.00 </option>
                <option value="12:00:00" <?php echo $gym['work_even_from']=="12:00:00"?"selected='selected'":""; ?> >12.00 </option>
                <option value="13:00:00" <?php echo $gym['work_even_from']=="13:00:00"?"selected='selected'":""; ?> >13.00 </option>
                <option value="14:00:00" <?php echo $gym['work_even_from']=="14:00:00"?"selected='selected'":""; ?> >14.00 </option>
                <option value="15:00:00" <?php echo $gym['work_even_from']=="15:00:00"?"selected='selected'":""; ?> >15.00 </option>
                <option value="16:00:00" <?php echo $gym['work_even_from']=="16:00:00"?"selected='selected'":""; ?> >16.00 </option>
                <option value="17:00:00" <?php echo $gym['work_even_from']=="17:00:00"?"selected='selected'":""; ?> >17.00 </option>
                <option value="18:00:00" <?php echo $gym['work_even_from']=="18:00:00"?"selected='selected'":""; ?> >18.00 </option>
                <option value="19:00:00" <?php echo $gym['work_even_from']=="19:00:00"?"selected='selected'":""; ?> >19.00 </option>
                <option value="20:00:00" <?php echo $gym['work_even_from']=="20:00:00"?"selected='selected'":""; ?> >20.00 </option>
                <option value="21:00:00" <?php echo $gym['work_even_from']=="21:00:00"?"selected='selected'":""; ?> >21.00 </option>
                <option value="22:00:00" <?php echo $gym['work_even_from']=="22:00:00"?"selected='selected'":""; ?> >22.00 </option>
                <option value="23:00:00" <?php echo $gym['work_even_from']=="23:00:00"?"selected='selected'":""; ?> >23.00 </option>
                <option value="23:59:59" <?php echo $gym['work_even_from']=="23:59:59"?"selected='selected'":""; ?> >23.59</option>
              </select>

              <div class="to">to</div>
              <select class="form-control smal" name="work_even_to">
                
                <option value="01:00:00" <?php echo $gym['work_even_to']=="01:00:00"?"selected='selected'":""; ?> >01.00 </option>
                <option value="02:00:00" <?php echo $gym['work_even_to']=="02:00:00"?"selected='selected'":""; ?> >02.00 </option>
                <option value="03:00:00" <?php echo $gym['work_even_to']=="03:00:00"?"selected='selected'":""; ?>  >03.00 </option>
                <option value="04:00:00" <?php echo $gym['work_even_to']=="04:00:00"?"selected='selected'":""; ?> >04.00 </option>
                <option value="05:00:00" <?php echo $gym['work_even_to']=="05:00:00"?"selected='selected'":""; ?> >05.00 </option>
                <option value="06:00:00" <?php echo $gym['work_even_to']=="06:00:00"?"selected='selected'":""; ?> >06.00 </option>
                <option value="07:00:00" <?php echo $gym['work_even_to']=="07:00:00"?"selected='selected'":""; ?> >07.00 </option>
                <option value="08:00:00" <?php echo $gym['work_even_to']=="08:00:00"?"selected='selected'":""; ?> >08.00 </option>
                <option value="09:00:00" <?php echo $gym['work_even_to']=="09:00:00"?"selected='selected'":""; ?> >09.00 </option>
                <option value="10:00:00" <?php echo $gym['work_even_to']=="10:00:00"?"selected='selected'":""; ?> >10.00 </option>
                <option value="11:00:00" <?php echo $gym['work_even_to']=="11:00:00"?"selected='selected'":""; ?> >11.00 </option>
                <option value="12:00:00" <?php echo $gym['work_even_to']=="12:00:00"?"selected='selected'":""; ?> >12.00 </option>
                <option value="13:00:00" <?php echo $gym['work_even_to']=="13:00:00"?"selected='selected'":""; ?> >13.00 </option>
                <option value="14:00:00" <?php echo $gym['work_even_to']=="14:00:00"?"selected='selected'":""; ?> >14.00 </option>
                <option value="15:00:00" <?php echo $gym['work_even_to']=="15:00:00"?"selected='selected'":""; ?> >15.00 </option>
                <option value="16:00:00" <?php echo $gym['work_even_to']=="16:00:00"?"selected='selected'":""; ?> >16.00 </option>
                <option value="17:00:00" <?php echo $gym['work_even_to']=="17:00:00"?"selected='selected'":""; ?> >17.00 </option>
                <option value="18:00:00" <?php echo $gym['work_even_to']=="18:00:00"?"selected='selected'":""; ?> >18.00 </option>
                <option value="19:00:00" <?php echo $gym['work_even_to']=="19:00:00"?"selected='selected'":""; ?> >19.00 </option>
                <option value="20:00:00" <?php echo $gym['work_even_to']=="20:00:00"?"selected='selected'":""; ?> >20.00 </option>
                <option value="21:00:00" <?php echo $gym['work_even_to']=="21:00:00"?"selected='selected'":""; ?> >21.00 </option>
                <option value="22:00:00" <?php echo $gym['work_even_to']=="22:00:00"?"selected='selected'":""; ?> >22.00 </option>
                <option value="23:00:00" <?php echo $gym['work_even_to']=="23:00:00"?"selected='selected'":""; ?> >23.00 </option>
                <option value="23:59:59" <?php echo $gym['work_even_to']=="23:59:59"?"selected='selected'":""; ?> >23.59</option>
              </select>
  </div>
</div>

       
       
        <?php 
        if(isset($_GET['id'])){ 
                $weekOff = $objgen->get_Onerow('gym',"AND shop_id=".$shop_id);
                $WeekOffChk = $weekOff['week_off'];
        }
        ?>  
        
        <div class="col-md-4">
          <div class="form-group">
             <label>Weekly off</label>
              <select class="form-control medium" name="week_off">
                <option value="">Select Day</option>
                <option value="Sunday" <?php if($WeekOffChk == 'Sunday'){ echo "selected='selected'";}?>>Sunday</option>
                <option value="Monday" <?php if($WeekOffChk == 'Monday'){ echo "selected='selected'";}?>>Monday</option>
                <option value="Tuesday" <?php if($WeekOffChk == 'Tuesday'){ echo "selected='selected'";}?>>Tuesday</option>
                <option value="Wednesday" <?php if($WeekOffChk == 'Wednesday'){ echo "selected='selected'";}?>>Wednesday</option>
                <option value="Thursday" <?php if($WeekOffChk == 'Thursday'){ echo "selected='selected'";}?>>Thursday</option>
                <option value="Friday" <?php if($WeekOffChk == 'Friday'){ echo "selected='selected'";}?>>Friday</option>
                <option value="Saturday" <?php if($WeekOffChk == 'Saturday'){ echo "selected='selected'";}?>>Saturday</option>

              </select>
           
          </div>
        </div>
<div class="clearfix"></div>
         <div class="col-md-4">
          <div class="form-group">
            <label>Website</label>
            <input type="url" class="form-control" name="website" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['website']):''; ?>" >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Lattitude</label>
            <input type="text" class="form-control" name="lattitude" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['lattitude']):''; ?>"  >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Longitude</label>
            <input type="text" class="form-control" name="longitude" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['longitude']):''; ?>" >
          </div>
        </div>
        
        <div class="col-md-4">
          <div class="form-group">
            <label>Month-Year of Establishment</label>
            <input type="text" class="form-control" id="datepicker12" placeholder="mm/yy" name="esta_date" value="<?php echo isset($_GET['id'])?$objgen->check_tag($shop['esta_date']):''; ?>" >
          </div>
        </div>
        <?php 
            $getShopId     = $objgen->get_Onerow("shop","AND id=".$shop_id);
            $qry = "select * from categories where id='".$cat_id."'";
            if (isset($_GET['id'])) {
                $cats = $objgen->get_AllRows_qry("select * from categories where id='".$getShopId['categories']."'");    
            } else {
                $cats = $objgen->get_AllRows_qry("select * from categories where id='".$cat_id."'");    
            }
        ?>
        <div class="col-md-4">
          <div class="form-group">
            <label>Number of <?php echo $cats[0]['category'];?> Staff</label>
            <input type="text" class="form-control" name="gym_tra" value="<?php echo isset($_GET['id'])?$objgen->check_tag($gym['gym_tra']):''; ?>" >
          </div>
        </div>
    </section>
    <!-- <input type="submit" name="first_step" value="Save & Next"> -->
</form>

          <h2></h2>
          <section>
<?php 
            $getShopId     = $objgen->get_Onerow("shop","AND id=".$shop_id);
                //echo "id=".$getShopId['id'];
                //echo "----cat_id=".$getShopId['categories'];exit;
            $qry = "select * from categories where id='".$cat_id."'";
            if (isset($_GET['id'])) {
                $photos = $objgen->get_AllRows_qry("select * from categories where id='".$getShopId['categories']."'");    
            } else {
                $photos = $objgen->get_AllRows_qry("select * from categories where id='".$cat_id."'");    
            }
        ?>
        <?php 

        if(isset($_GET['id'])) {

            $qry = "select * from shop_categories where shop_id='".$shop_id."' AND m_id='".$merchant_id."'";
            $getSub = $objgen->get_AllRows_qry($qry);
            
           
            $subCatChk = array( );
            foreach ($getSub as $key => $value) {

                $subArr = $value['sub_cat'];
                array_push($subCatChk,$subArr);
            }?>
<div class="col-md-12">
          <div class="form-group">
            <label>Sub Categories For Business Category - <b><?php echo $photos[0]['category'];?></b></label>
            <input type="hidden" name="main_cat" value="<?php echo $photos[0]['id'];?>" />
            <div class="clearfix"></div>
            <?php 
            
            $subcategories = $objgen->get_AllRows_qry("select * from subcategories where cat_id='".$cat_id."' OR cat_id='".$photos[0]['id']."'"); 
            //echo count($subcategories);
            //echo'<pre>';print_r($subcategories);
            if (!empty($subcategories)){
             foreach ($subcategories as $subcategory) {
                $group_id = $subcategory['group_id'];
            ?>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <label><input type="checkbox" name="subcategory[]" value="1" <?php if(in_array($subcategory['id'], $subCatChk)){ echo "checked='checked'"; } ?> /><?php echo $subcategory['subcategory']; ?></label>
                    <input type="hidden" name="sub_cat[]" value="<?php echo $subcategory['id']?>" />
                    <input type="hidden" name="group[]" value="<?php echo $subcategory['group_id'];?>" />
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <label>Price</label>
                    <input type="text" name="m_sub_category_price[]" placeholder="Price" value="<?php echo $subcategory['price']; ?>">
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <label>Time</label>
                    <input type="text" name="m_sub_category_time[]" placeholder="Time in min" value="<?php echo $subcategory['time']; ?>">
                </div>
            </div>
            <?php  } } else {?>
            <p style="color:red">Sorry! There is no subcategories added for current category.</p>  
            <?php } ?>
            </div>
        </div>

        <?php  
            }else {?>
        <div class="col-md-12">
          <div class="form-group">
            <label>Sub Categories For Business Category - <b><?php echo $photos[0]['category'];?></b></label>
            <input type="hidden" name="main_cat" value="<?php echo $photos[0]['id'];?>" />
            <div class="clearfix"></div>
            <?php 
            
            $subcategories = $objgen->get_AllRows_qry("select * from subcategories where cat_id='".$cat_id."' OR cat_id='".$photos[0]['id']."'"); 
            //echo count($subcategories);
            //echo'<pre>';print_r($subcategories);
            if (!empty($subcategories)){
             foreach ($subcategories as $subcategory) {
            ?>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <label><input type="checkbox" name="subcategory[]" value="1" /><?php echo $subcategory['subcategory']; ?></label>
                    <input type="hidden" name="sub_cat[]" value="<?php echo $subcategory['id']?>" />
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <label>Price</label>
                    <input type="text" name="m_sub_category_price[]" placeholder="Price" value="<?php echo $subcategory['price']; ?>">
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <label>Time</label>
                    <input type="text" name="m_sub_category_time[]" placeholder="Time in min" value="<?php echo $subcategory['time']; ?>">
                </div>
            </div>
            <?php  } } else {?>
            <p style="color:red">Sorry! There is no subcategories added for current category.</p>  
            <?php } ?>

            
              </div>
            </div>
        <?php } ?>

        <div class="col-md-12">
          <div class="main-hed">
            <h5><strong>Facilities</strong></h5>
          </div>
        </div> 
        <?php if (isset($_GET['id'])) { ?>
            <div class="col-md-12">
          <div class="main-hed" style="border-bottom: 0px;">
             
            
              <?php 
                $ss= "SELECT * FROM shop_facilities WHERE shop_id='".$shop_id."'";
                $getFaclity = $objgen->get_AllRows_qry($ss);
                $getFaclityChk = array( );
            foreach ($getFaclity as $key => $value) {

                $subArr1 = $value['facility_id'];
                array_push($getFaclityChk,$subArr1);
            }

            
             $qry2 = "select * from facilities where cat_id='".$cat_id."' OR cat_id='".$photos[0]['id']."'";
             $facility = $objgen->get_AllRows_qry($qry2);
              if (!empty($facility)){
                 foreach ($facility as $fact) {
                    if(in_array($fact['id'], $getFaclityChk)){
                       $check1='checked="checked"';
                     }
                    else {
                      $check1='';
                    }
                    ?>
                    <div class="col-md-4">
                    <div class="form-group">
                    <label><?php echo $fact['facility'];?></label>
                            
                    <div class="col-md-4">
                    <input type="checkbox" value="<?php echo $fact['id'];?>" name="facility[]"  <?php echo $check1;?> >
                    </div>
                            
                    </div>
              </div>
                <?php } 
                  } else {
                      echo '<p style="color:red">Sorry! There is no facilities added for current category.</p>';

                  } ?>
            </div>
          </div> 
        <?php } else { ?>
        <div class="col-md-12">
          <div class="main-hed" style="border-bottom: 0px;">
              <?php 
               $qry = "select * from facilities where cat_id='".$cat_id."' OR cat_id='".$photos[0]['id']."'";
               $facility = $objgen->get_AllRows_qry($qry);
              if (!empty($facility)){
             foreach ($facility as $fact) {
                    ?>
                    <div class="col-md-3">
                    <div class="form-group">
                    
                            
                    
                    <input type="checkbox" value="<?php echo $fact['id'];?>" name="facility[]">
                   <label><?php echo $fact['facility'];?></label>
                            
                    </div>
              </div>
                 <?php   } 
                  } else {
                      echo '<p style="color:red">Sorry! There is no facilities added for current category.</p>';

                  }?>
            </div>
          </div>   
          <?php }?>
        

        </section>



   
     <h2></h2>

     <section>

       <div class="col-md-12">
        <div class="main-hed">
          <h5><strong>Photographs (Mandatory)</strong></h5>
        </div>
      </div>
      <div class="clearfix"></div>
      <br>
      <div class="col-md-12">
        <div class="form-group">
          <div class="clearfix"></div>
          
          <div class="col-md-4">
            <label>Interior (Min 4)</label>

                        <div class="form-group">
                            <div class="clearfix">
                            </div>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("uploads/thumb/".$int_image[1])){
                            ?>
                                <img src="../uploads/thumb/<?php echo $int_image[1];?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            
                            </div>
                            <div class="col-md-2"><input type="file" name="Interior1"  />
                            </div>
                            <br><br>
                            <div class="form-group">
                            <input type="radio" name="profile_pic" value="1" <?php if($set_pic[1]=="true") { echo "checked='checked'";} ?> ><label style="padding-left: 5px;">Set Profile Pic</label> 
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="clearfix">
                            </div><br>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("uploads/thumb/".$int_image[2])){
                            ?>
                                <img src="../uploads/thumb/<?php echo $int_image[2]; ?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            </div>
                            <div class="col-md-2"><input type="file" name="Interior2" />
                            </div>
                            <br><br>
                            <div class="form-group">
                            <input type="radio" name="profile_pic" value="2" <?php if($set_pic[2]=="true") { echo "checked='checked'";} ?> ><label style="padding-left: 5px;">Set Profile Pic</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="clearfix">
                            </div><br/>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("uploads/thumb/".$int_image[3])){
                            ?>
                                <img src="../uploads/thumb/<?php echo $int_image[3];?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            
                            </div>
                            <div class="col-md-2">
                                <input type="file" name="Interior3" />

                            </div>
                            <br><br>
                            <div class="form-group">
                            <input type="radio" name="profile_pic" value="3" <?php if($set_pic[3]=="true") { echo "checked='checked'";} ?> ><label style="padding-left: 5px;">Set Profile Pic</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="clearfix">
                            </div><br>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("uploads/thumb/".$int_image[4])){
                            ?>
                                <img src="../uploads/thumb/<?php echo $int_image[4]; ?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            
                            </div>
                            <div class="col-md-2"><input type="file" name="Interior4" />
                            </div>
                            <br><br>
                            <div class="form-group">
                            <input type="radio" name="profile_pic" value="4" <?php if($set_pic[4]=="true") { echo "checked='checked'";} ?> ><label style="padding-left: 5px;">Set Profile Pic</label>
                            </div>
                        </div>

                        

          </div>
          
        </div>
      <div class="col-md-12">
          <div class="main-hed">
            <h5><strong>Products</strong></h5>
          </div>
        </div>
        <?php 

        if(isset($_GET['id'])) {
            
            $sp= "SELECT * FROM shop_products WHERE shop_id='".$shop_id."'";
                $getProduct = $objgen->get_AllRows_qry($sp);
                $getProductChk = array( );
            foreach ($getProduct as $key => $value) {

                $subArr2 = $value['product_id'];
                array_push($getProductChk,$subArr2);
            }


            $getShopId     = $objgen->get_Onerow("shop","AND id=".$shop_id);
                // echo "shop_id=".$getShopId['id'];
                // echo "cat_idinshop=".$getShopId['categories'];
            //$qry = "select * from categories where id='".$cat_id."'";
            $photos = $objgen->get_Onerow("categories", "AND id=".$getShopId['categories']);    
            //echo "cat_id==".$photos['id'];
        
            $qry2 = "select * from products where cat_id='".$photos['id']."'";
            $product = $objgen->get_AllRows_qry($qry2);
            
         foreach ($product as $pro) {

            
                
            ?>
          <div class="col-md-4">
             <div class="form-group">
            <label><?php echo $pro['product_name'];?></label>
            
            <div class="col-md-4">
              <input type="checkbox" value="<?php echo $pro['id'];?>" <?php if(in_array($pro['id'], $getProductChk)){ echo "checked='checked'"; } ?>  name="product[]">
              
            </div>
            
        </div>
         </div>
            

        <?php  }    } else {
            $qry = "select * from products where cat_id='".$insert_cat_id."'";
            $product = $objgen->get_AllRows_qry($qry);
            
         foreach ($product as $pro) {
        ?>
          <div class="col-md-4">
        <div class="form-group">
            <label><?php echo $pro['product_name'];?></label>
            
            <div class="col-md-4">
              <input type="checkbox" value="<?php echo $pro['id'];?>" name="product[]">
              
            </div>
            
        </div>
         </div>
           <?php } }

         ?>
      
      <div class="clearfix"></div>
      <div class="col-md-12">
          <div class="main-hed">
            <h5><strong>Overview</strong></h5>
          </div>
        </div>
        <div class="col-md-8">
          <div class="form-group">
           <!--  <label>Overview</label> -->
            <textarea class="form-control" name="overview" rows="10" cols="30"><?php echo isset($_GET['id'])?$objgen->check_tag($shop['overview']):''; ?></textarea> 
            
          </div>
        </div>
      <br>
      </div>

        </section>
      </div>
            </form>

        </div>

  </section>

</aside>
</div>
<!-- child of the body tag -->
<span id="top-link-block" class="hidden"> <a href="#top" class="well well-sm" onClick="$('html,body').animate({scrollTop:0},'slow');return false;"> <i class="glyphicon glyphicon-chevron-up"></i> Back to Top </a> </span><!-- /top-link-block -->
<script src="<?=URL?>js/jquery.min.js"></script>
<script src="<?=URL?>js/jquery-ui.js" type="text/javascript"></script>
<script src="<?=URL?>js/custom.js" type="text/javascript"></script>
<script src="<?=URL?>js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=URL?>js/jquery.steps.js"></script>
<script>
 function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
  </script>

<script>


$(document).ready(function(){



    var city = $("#city ").val();
    
    var edit =  GetURLParameter('id');
        //alert(edit);
    $.ajax({
      type: "POST",
      url: "../get-city.php",
      data: {city_id : city, edit:edit },
      success: function(data){
        //$("#locality1" ).hide(); // Hide the previous box.
        $("#local1").html(data);
        //$("#locality").innerHTML=data;
        }
    });


    });

</script>

<script>
        $(function ()
        {

            var form = $("#add_business");

            $("#wizard").steps({
                headerTag: "h2",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    //alert('1');
                        form.validate().settings.ignore = ":disabled,:hidden";
                        return form.valid();
                },
                onFinishing: function (event, currentIndex)
                {
                    //alert('2');
                        form.validate().settings.ignore = ":disabled";
                        return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    //alert('3');
                        // Submit form input
                        form.submit();
                }

            });

        });
</script>
<script src="<?=URL?>js/dashboardbootstrap.min.js" type="text/javascript"></script>
<script src="<?=URL?>js/nhf-script.js" type="text/javascript"></script>
<script>jQuery(function($) {
$('.navbar .dropdown').hover(function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

}, function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

});

$('.navbar .dropdown > a').click(function(){
location.href = this.href;
});

});</script>
<script src="<?=URL?>js/down.min.js"></script>
<script>$(document).ready(function(){


    $('.box-bounce').waypoint(function(down){
        $(this).addClass('animation');
        $(this).addClass('bounceIn');
    }, { offset: '80%' });

        $('.box-flip').waypoint(function(down){
        $(this).addClass('animation');
        $(this).addClass('flipInY');
    }, { offset: '80%' });
        $('.box-left').waypoint(function(down){
        $(this).addClass('animation');
        $(this).addClass('fadeInLeft');
    }, { offset: '100%' });
    $('.box-right').waypoint(function(down){
        $(this).addClass('animation');
        $(this).addClass('fadeInRight');
    }, { offset: '100%' });
    });</script>
<script src="<?=URL?>js/slide.js" type="text/javascript"></script>
<script src="<?=URL?>js/skycons.js"></script>
<script src="<?=URL?>js/jquery.flot.canvas.min.js"></script>

<!--Page Level JS-->
<script src="<?=URL?>js/jquery.countTo.js"></script>
<script src="<?=URL?>js/jquery-jvectormap-1.js"></script>
<script src="<?=URL?>js/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?=URL?>js/dashboard.js" type="text/javascript"></script>
<script src="<?=URL?>js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=URL?>js/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
        $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": false
                });

        });

</script>

<script>
  $( "#user_login" ).click(function() {
      var email = $('#user_email').val();
      var pass = $('#user_pass').val();
    $.ajax({
          type: "POST",
          url: "user-login-ajax.php",
          data: {email : email, pass :  pass },
          success: function (data) {
            jQuery('.error_msg').fadeOut();
                if(jQuery.trim(data) == jQuery.trim('success')){
                   jQuery('.success_msg').text('Login successful. Redirecting....').fadeIn();  
                     location.reload();   
                }
                else{
                    jQuery('.error_msg').text(data).fadeIn();   
                }
          }
       });
    });
</script>
<script>
function getCity(val)
{

//alert(val);

    $.ajax({
          type: "POST",
          url: "../get-city.php",
          data: {id : val },
          success: function(data){
            //alert(data);
            //$("#locality" ).hide(); // Hide the previous box.
            $("#local").html(data);
            //$("#locality").innerHTML=data;
            }
        });
}
</script>

</body>
</html>
