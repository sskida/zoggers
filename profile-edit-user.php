<?php
require_once "chk_login_usr.php";

$objgen		=	new general();
$objval	    =   new validate();

$id 		  = $_SESSION['ma_log_id_usr'];
$result       = $objgen->get_Onerow("users","AND id=".$id);
$name         = $objgen->check_tag($result['name']);
$phone        = $objgen->check_tag($result['phone']);
$gender       = $objgen->check_tag($result['gender']);
$city         = $objgen->check_tag($result['city']);
$email        = $objgen->check_tag($result['email']);

if(isset($_POST['update']))
{    
   
   $name 	   		= $objgen->check_input($_POST['name']);
   $phone       	= $objgen->check_input($_POST['phone']);
   $gender 	        = $objgen->check_input($_POST['gender']);
   $city 	        = $objgen->check_input($_POST['city']);
   $email 	        = $objgen->check_input($_POST['email']);

   
 
   $rules		=	array();
   $rules[] 	= "required,name,Enter the Name";
   $rules[] 	= "required,phone,Enter the Phone";
   $errors  	= $objval->validateFields($_POST, $rules);
   
     $sh_exit = $objgen->chk_Ext("users","email='$email' and id <>".$id);
	if($sh_exit>0)
	{
		$errors[] = "This email is already exists.";
	}


   if(empty($errors))
	{
		 			 
	  $msg = $objgen->upd_Row('users',"name='".$name."',phone='".$phone."',gender='".$gender."',city='".$city."',email='".$email."'","id=".$id);
	  
	  if($msg=="")
		 {
		   $msg2 = "Profile Modified Successfully.";
		 }

	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>
</head>

<body>
<?php include 'header-dash-user.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
      <?php include 'user-menu.php'; ?>


<aside class="right-side">
  <section class="content-header-top">
    <h1> <i class="fa fa-suitcase"></i> Edit Profile </h1>
   
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-6">
            <form role="form" action="" method="post" enctype="multipart/form-data" >
            <div class="box padding-both">
			
			   <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
              
              <div class="form-group">
                <label>Name *</label>
                <input type="text"  class="form-control" name="name" value="<?=$name?>" required />
              </div>
              <div class="form-group">
                <label>Email *</label>
                <input type="text"  class="form-control" name="email" value="<?=$email?>"  required />
              </div>
                <div class="form-group">
                <label>Gender *</label>
                <select name="gender" required class="form-control">
					<option value="">Select</option>
					<option value="Male" <?php if($gender=="Male") { ?> selected="selected" <?php } ?> >Male</option>
					<option value="Female" <?php if($gender=="Female") { ?> selected="selected" <?php } ?>  >Female</option>
				</select>
              </div>
                <div class="form-group">
                <label>Location/City *</label>
                <input type="text"  class="form-control" name="city" value="<?=$city?>" required />
              </div>
                <div class="form-group">
                <label>Phone Number *</label>
                <input type="text"  class="form-control" name="phone" value="<?=$phone?>"  required/>
              </div>
                
            
                
              <div class="box-footer">
                <button id="submit" class="cl2-green common-btn" name="update" type="submit">Update</button>
              </div>
            </div>
          </form>
        </div>
    </div>
  </section>
</aside>
</div>
<?php include 'footer-script-dash.php'; ?>
</body>
</html>
