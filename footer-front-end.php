

    </div>    

  </div>
<div class="bottom">
  <div class="container" style="margin-left:10px !important; margin-right:0px !important;">
    <div class="col-md-6 col-sm-6 col-sx-10" style="text-align: left;">
      <img src="<?=URL?>front-end-images/zoggrs_logo.png" style="height: 40px; background-color: #DADADA;">
      <span class="sign_up" id="usr_sign_up">User Sign up</span>
         <span class="sign_up" id="mer_sign_up">Merchant Sign up</span>
         <span class="sign_up" id="merchant_login">Merchant Login</span>
    </div>
    <div class="col-md-3 col-sm-3 col-sx-12">
      <p style="margin-top: 10px;">Copyright &copy; 2015 Zoggers.com.</p>

    </div>
    <div class="col-md-3 col-sm-3 col-sx-12">
      <p style="text-align: right;margin-top: 10px;">All Rights Reserved.</p>
</div>

</div>
</div>

<!-- Rang slider  -->   
        <link rel="stylesheet" href="front-end-css/themes/smoothness/jquery-ui.css">
        <link href="front-end-css/jquery-ui.css" rel="stylesheet">
        <script src="front-end-js/jquery-ui.js"></script>
    <!-- End -->
     
     <script>
        var jquery = $.noConflict();
         jquery(function() {
            jquery( "#slider-3" ).slider({
               range:true,
               min: 0,
               max: 5000,
               values: [ 1000, 3000 ],
               slide: function( event, ui ) {
                  jquery( "#price" ).text( "Rs" + ui.values[ 0 ] + " - Rs" + ui.values[ 1 ] );
               }
           });
         jquery( "#price" ).text( "Rs" + jquery( "#slider-3" ).slider( "values", 0 ) +
            " - Rs" + jquery( "#slider-3" ).slider( "values", 1 ) );
         });
      </script>
    
    <script type="text/javascript" src="front-end-js/jquery.js"></script>
     <script type='text/javascript' src='front-end-js/jquery.autocomplete.js'></script>
    <link rel="stylesheet" type="text/css" href="front-end-css/jquery.autocomplete.css" />

    <script type="text/javascript" src="front-end-js/custom.js"></script>
   <script src="front-end-js/jquery.session.js"></script>
    <script type="text/javascript" src="front-end-js/jquery.selectbox-0.2.min.js"></script>
    
    <script src="front-end-js/my_js.js"></script>
    <script type="text/javascript" src="front-end-js/script.js"></script>
    
        <style type="text/css">
          .fade_1{
    display: none;
    position:absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #ababab;
    z-index: 1001;
    -moz-opacity: 0.8;
    opacity: .70;
    filter: alpha(opacity=80);
}

.modal_1 {
    display: none;

    position: absolute;
    top: 25%;
    left: 45%;
    width: 130px;
    height: 130px;
    padding: 30px 15px 0px;
    border-radius: 20px;
    background-color: white;
    z-index: 1002;
    text-align: center;
    overflow: auto;
}

.results_1 {
    font-size:1.25em;
    color:red
}
        </style>
        
    <script type="text/javascript">
    $(document).ready(function(){   

        if (!$.browser.opera) {
    
            // select element styling
            $('select.select').each(function(){
                var title = $(this).attr('title');
                if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
                $(this)
                    .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
                    .after('<span class="select">' + title + '</span>')
                    .change(function(){
                        val = $('option:selected',this).text();
                        $(this).next().text(val);
                        })
            });

        };
        
    });
</script>
 <script>
    $(document).ready(function(){
    
//MERCHANT LOGIN  
  $("#add_err").css('display', 'none', 'important');
  $("#login").click(function(){
    
    username=$("#email").val();
    password=$("#password").val();
    $('#cover').show();
    $.ajax({
      type: "POST",
      url: "login-front-end.php",
     data: "mail="+username+"&pwd="+password,
     success: function(html){
        
        var res3 = $.trim(html);
        if(res3 =='true'){
              
              window.location="<?URL?>dashboard.php";
        }
        else{
          $("#add_err").css('display', 'inline', 'important');
          $("#add_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Wrong username or password");
        }

     },
     beforeSend:function()
       {
        $(".modal_1").show();
        $(".fade_1").show();
          
       },
       complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
       }
    });
  return false;
  });


  //USER LOGIN


  $("#user_err").css('display', 'none', 'important');
  $("#user-login").click(function(){
      
      username=$("#username").val();
      pass=$("#pass").val();
      
      $.ajax({
       type: "POST",
       url: "user-login-front-end.php",
      data: "mail="+username+"&pwd="+pass,
       success: function(html){
        var res2 = $.trim(html);
       
          if(res2!='0'){
               
            if($.session.get("myVar")=='normal_login'){
                          
                location.reload(true);
                

            } else if($.session.get("myVar")=='user-dashboard'){
                window.location="<?=URL?>user-dashboard.php";
            }else if($.session.get("myVar")=='my-favourites'){
                window.location="<?=URL?>my-favourites.php";
            }else {
             
              var user_id = res2;
              var shop_id = ($.session.get("myVar"));
               
               
                $.ajax({
                      type: "POST",
                      url: "add-favorites.php",
                      data: {
                        user_id : user_id,
                        shop_id : shop_id 
                      },
                     success: function(result){
                      var res =$.trim(result)
                        $(".overlay").css("display", "none");
                        $("#user_login_popup_c").css("display", "none");
                        if (res=='inserted') {
                          
                             $(".fave_"+shop_id).removeClass("add_d");
                             $(".fave_"+shop_id).addClass("added_d");
                             $('#deco_'+shop_id).text("Added");
                          
                        }
                        if (res=='deleted') {
                          
                             $(".fave_"+shop_id).removeClass("added_d");
                             $(".fave_"+shop_id).addClass("add_d");
                          $('#deco_'+shop_id).text("Add");
                          
                        }
                      
                     },
                });  
            }
                
          }
          else{
            $("#user_err").css('display', 'inline', 'important');
            $("#user_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Wrong username or password");
          }

       },
       beforeSend:function()
       {
        $(".modal_1").show();
        $(".fade_1").show();
          
       },
       complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
          
       }
    });
  return false;
  });



  //USER SIGNUP

  $("#user_signup_err").css('display', 'none', 'important');
  $("#user_submit").click(function(){
   
    name = $("#user_name").val();
    phone = $("#user_phone").val();
    city = $("#user_city").val();
    username = $("#user_email").val();
    conf_pwd = $("#user_conf_pwd").val();
    password = $("#user_pwd").val();
    
    $.ajax({
      type: "POST",
      url: "user-signup.php",
     data: "name="+name+"&city="+city+"&phone="+phone+"&confirm_pwd="+conf_pwd+"&email="+username+"&pwd="+password,
     success: function(html){
       
        var res7 = $.trim(html);
        
        if(res7=='true'){
           
              window.location="index.php";
              
        }
        else if(res7=='confirm')    {
          $("#user_signup_err").css('display', 'inline', 'important');
          $("#user_signup_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Password and confirm password doesn't match.");
        }
        else if(res7=='email')    {
          $("#user_signup_err").css('display', 'inline', 'important');
          $("#user_signup_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Email already existed.");
        }
        else if(res7=='mobile')    {
          $("#user_signup_err").css('display', 'inline', 'important');
          $("#user_signup_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Mobile No Should be 10 digits.");
        }
        else     {
          $("#user_signup_err").css('display', 'inline', 'important');
          $("#user_signup_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />All Fields Are Required.");
        }

     },
     beforeSend:function()
       {
        $(".modal_1").show();
        $(".fade_1").show();
          
       },
       complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
       }
     
    });
  return false;
  
  });

  //MERCHANT SIGNUP

  
  $("#mer_signup_err").css('display', 'none', 'important');
  $("#mer_submit").click(function(){
   
    name = $("#mer_name").val();
    phone = $("#mer_phone").val();
    city = $("#mer_city").val();
    username = $("#mer_email").val();
    conf_pwd = $("#mer_conf_pwd").val();
    password = $("#mer_pwd").val();
    
    $.ajax({
      type: "POST",
      url: "merchant-sign-up.php",
     data: "name="+name+"&city="+city+"&phone="+phone+"&confirm_pwd="+conf_pwd+"&email="+username+"&pwd="+password,
     success: function(html){
        
        var res6 = $.trim(html);
        if(res6 =='true'){
            
              window.location="<?=URL?>index.php";
              
        }
        else if(res6=='confirm')    {
          $("#mer_signup_err").css('display', 'inline', 'important');
          $("#mer_signup_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Password and confirm password doesn't match.");
        }
        else if(res6=='email')    {
          $("#mer_signup_err").css('display', 'inline', 'important');
          $("#mer_signup_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Email already existed.");
        }
        else if(res6=='mobile')    {
          $("#mer_signup_err").css('display', 'inline', 'important');
          $("#mer_signup_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Mobile No Should be 10 digits.");
        }
        else     {
          $("#mer_signup_err").css('display', 'inline', 'important');
          $("#mer_signup_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />All Fields Are Required.");
        }

     },
     beforeSend:function()
       {
        $(".modal_1").show();
        $(".fade_1").show();
          
       },
       complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
       }
    });
  return false;
  
  });
  // forgot password user
  $("#forgot_user_err").css('display', 'none', 'important');
  $("#user_forgot").click(function(){
   
    
    var phone = $("#for_user_mob").val();
    
    var username = $("#for_user_email").val();
    
    
    $.ajax({
      type: "POST",
      url: "forgot-password-user.php",
     data: "phone="+phone+"&email="+username,
     success: function(html){
        
        var res5 = $.trim(html);
        
        if(res5 =='true'){
              window.location="<?=URL?>index.php";
              
        }
        else if(res5=='email')    {
          $("#forgot_user_err").css('display', 'inline', 'important');
          $("#forgot_user_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Email Not Existed OR Phone Number Not Existed  .");
        }
        else if(res5=='mobile')    {
          $("#forgot_user_err").css('display', 'inline', 'important');
          $("#forgot_user_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Mobile No Should be 10 digits.");
        }
        else     {
          $("#forgot_user_err").css('display', 'inline', 'important');
          $("#forgot_user_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />All Fields Are Required.");
        }

     },
     beforeSend:function()
       {
        $(".modal_1").show();
        $(".fade_1").show();
          
       },
       complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
       }
    });
  return false;
  
  });
// forgot password user
  $("#forgot_mer_err").css('display', 'none', 'important');
  $("#mer_forgot").click(function(){
   
    
    var phone = $("#for_mer_mob").val();
    
    var username = $("#for_mer_email").val();
    
    
    $.ajax({
      type: "POST",
      url: "forgot-password-merchant.php",
     data: "phone="+phone+"&email="+username,
     success: function(html){
        
        var res4 = $.trim(html);
        
        if(res4 =='true'){
            
              window.location="<?=URL?>index.php";
              
        }
        else if(res4=='email')    {
          $("#forgot_mer_err").css('display', 'inline', 'important');
          $("#forgot_mer_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Email Not Existed OR Phone Number Not Existed  .");
        }
        else if(res4=='mobile')    {
          $("#forgot_mer_err").css('display', 'inline', 'important');
          $("#forgot_mer_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />Mobile No Should be 10 digits.");
        }
        else     {
          $("#forgot_mer_err").css('display', 'inline', 'important');
          $("#forgot_mer_err").html("<img src='front-end-images/alert.png' style='height: 18px;' />All Fields Are Required.");
        }

     },
     beforeSend:function()
       {
        $(".modal_1").show();
        $(".fade_1").show();
          
       },
       complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
       }
    });
  return false;
  
  });

  $("#user_dashboard").click(function(){
   var user_id ='<?php echo $_SESSION['ma_log_id_usr']?>';
    if(user_id){
      window.location = "<?URL?>user-dashboard.php";
    }else {
      $.session.set("myVar", 'user-dashboard');
      $(".overlay").fadeIn(300);
        $("#user_login_popup_c").delay(200).fadeIn(300);
        $(".pop_up_close").click(function(){    
        $(this).parent().fadeOut(300);
        $(".overlay").delay(200).fadeOut(300);
      });
    }
  });
  $("#user_favourites").click(function(){
   var user_id ='<?php echo $_SESSION['ma_log_id_usr']?>';
    if(user_id){
      window.location = "<?URL?>my-favourites.php";
    }else {
      $.session.set("myVar", 'my-favourites');
      $(".overlay").fadeIn(300);
        $("#user_login_popup_c").delay(200).fadeIn(300);
        $(".pop_up_close").click(function(){    
        $(this).parent().fadeOut(300);
        $(".overlay").delay(200).fadeOut(300);
      });
    }
  });

});





function setsession(){
  
  $.session.set("review",1);
 
}</script>





</body>

</html>