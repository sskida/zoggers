<?php 
require_once "includes/includepath.php";
$objgen = new general();

$groups  = $_POST['service'];
$product  = $_POST['product'];
$location = $_POST['location'];
$facility = $_POST['facility'];

if($facility){
$fac = '`fac_id` int NOT NULL AUTO_INCREMENT,`shop_id` int NOT NULL,';
$facility_sql ="SELECT distinct facility_id FROM shop_facilities order by facility_id ";
$ids_arr = $objgen->get_AllRows_qry($facility_sql);
	foreach ($ids_arr as $ids) {
		$fac .= '`faci_'.$ids['facility_id'].'` int NOT NULL,';
	}
	$fac .="PRIMARY KEY(fac_id)";
	$maketemp = "
    CREATE TEMPORARY TABLE temp_table_facility (
      $fac
    )
  "; 
$objgen->new_table($maketemp);

$facility_sql ="SELECT distinct shop_id FROM shop_facilities order by facility_id";
$id_arr = $objgen->get_AllRows_qry($facility_sql);
foreach ($id_arr as $idr) { 
$facility_sql_ ="SELECT distinct facility_id FROM shop_facilities where shop_id='".$idr['shop_id']."' order by facility_id ";
$id_arr_ = $objgen->get_AllRows_qry($facility_sql_);

$cnt = 1 ;
$d = '';
$val = '';
foreach ($id_arr_ as $ida) {

	if($cnt == count($id_arr_)) {
		$fa_id = $ida['facility_id'];
		$d .="faci_$fa_id";
		$val .="$fa_id";
	}else {
		$fa_id = $ida['facility_id'];
		$d .="faci_$fa_id,";
		$val .="$fa_id,";
	}
	$cnt++;
}
$s = $idr['shop_id'];
$val ="$s,$val";
$msg_refer = $objgen->ins_Row('temp_table_facility', "shop_id,$d", "$val");

$id_arr_ = '';
$id_arr = '';

}
}
/********  END FACILITY ***************/

/********  START LOCATION ***************/

/********  END LOCATION ***************/

/********  START SERVICE ***************/
if($groups){
$grp = '`grp_id` int NOT NULL AUTO_INCREMENT,`shop_id` int NOT NULL,';
$grp_sql ="SELECT distinct group_id FROM shop_categories order by group_id ";
$grp_arr = $objgen->get_AllRows_qry($grp_sql);
	foreach ($grp_arr as $ids) {
		$grp .= '`grp_'.$ids['group_id'].'` int NOT NULL,';
	}
	$grp .="PRIMARY KEY(grp_id)";
	$maketemp_group = "
    CREATE TEMPORARY TABLE temp_table_group (
      $grp
    )
  "; 
$x = $objgen->new_table($maketemp_group);

$grp_one_sql ="SELECT distinct shop_id FROM shop_categories order by group_id";
$grp_id_arr = $objgen->get_AllRows_qry($grp_one_sql);
foreach ($grp_id_arr as $idr) { 
$grp_sql_ ="SELECT distinct group_id FROM shop_categories where shop_id='".$idr['shop_id']."' order by group_id ";
$grp_id_arr_ = $objgen->get_AllRows_qry($grp_sql_);

$cnt_grp = 1 ;
$d_grp = '';
$val_grp = '';
//echo '<pre>';print_r($pro_id_arr_);
foreach ($grp_id_arr_ as $ida) {

	if($cnt_grp == count($grp_id_arr_)) {
		$gp_id = $ida['group_id'];
		$d_grp .="grp_$gp_id";
		$val_grp .="$gp_id";
	}else {
		$gp_id = $ida['group_id'];
		$d_grp .="grp_$gp_id,";
		$val_grp .="$gp_id,";
	}
	$cnt_grp++;
}
$s_grp = $idr['shop_id'];
$val_grp ="$s_grp,$val_grp";
$msg_refer = $objgen->ins_Row('temp_table_group', "shop_id,$d_grp", "$val_grp");

$grp_id_arr_ = '';
$grp_arr = '';

}
}
/********  END SERVICE ***************/

/********  START PRODUCT ***************/
if($product){
$pro = '`pro_id` int NOT NULL AUTO_INCREMENT,`shop_id` int NOT NULL,';
$pro_sql ="SELECT distinct product_id FROM shop_products order by product_id ";
$pro_arr = $objgen->get_AllRows_qry($pro_sql);
	foreach ($pro_arr as $ids) {
		$pro .= '`pro_'.$ids['product_id'].'` int NOT NULL,';
	}
	$pro .="PRIMARY KEY(pro_id)";
	$maketemp_pro = "
    CREATE TEMPORARY TABLE temp_table_product (
      $pro
    )
  "; 
$x = $objgen->new_table($maketemp_pro);

$pro_one_sql ="SELECT distinct shop_id FROM shop_products order by product_id";
$pro_id_arr = $objgen->get_AllRows_qry($pro_one_sql);
foreach ($pro_id_arr as $idr) { 
$pro_sql_ ="SELECT distinct product_id FROM shop_products where shop_id='".$idr['shop_id']."' order by product_id ";
$pro_id_arr_ = $objgen->get_AllRows_qry($pro_sql_);

$cnt_pro = 1 ;
$d_pro = '';
$val_pro = '';
//echo '<pre>';print_r($pro_id_arr_);
foreach ($pro_id_arr_ as $ida) {

	if($cnt_pro == count($pro_id_arr_)) {
		$po_id = $ida['product_id'];
		$d_pro .="pro_$po_id";
		$val_pro .="$po_id";
	}else {
		$po_id = $ida['product_id'];
		$d_pro .="pro_$po_id,";
		$val_pro .="$po_id,";
	}
	$cnt_pro++;
}
$s_pro = $idr['shop_id'];
$val_pro ="$s_pro,$val_pro";
$msg_refer = $objgen->ins_Row('temp_table_product', "shop_id,$d_pro", "$val_pro");

$pro_id_arr_ = '';
$pro_arr = '';

}
}



$where = array();
$inner = $w = '';

if($location) {
	$locality = implode(',',$location);
	if(strstr($locality,',')) {
		$data1 = explode(',',$locality);
		$barray = array();
		foreach($data1 as $c) {
			$barray[] = "shop.locality = $c";
		}
		$WHERE[] = '('.implode(' AND ',$barray).')';
	} else {
		$WHERE[] = '(shop.locality = '.$locality.')';
	}
	$inner.= 'inner join shop_categories sc on shop.id=sc.shop_id ';
	 
}


if ($facility) {
	$fact = implode(',',$facility);
	 
	 if(strstr($fact,',')) {
		$data2 = explode(',',$fact);
		$carray = array();
		foreach($data2 as $c) {
			$carray[] = "sf.faci_".$c." = $c";
		}
		$WHERE[] = ''.implode(' AND ',$carray).'';
	} else {
		$WHERE[] = 'sf.faci_'.$fact.' = '.$fact.'';
	}
	$inner.= 'inner join temp_table_facility sf on shop.id=sf.shop_id ';
}


if ($product) {
	$prod = implode(',',$product);
	 
	 if(strstr($prod,',')) {
		$data3 = explode(',',$prod);
		$sarray = array();
		foreach($data3 as $c) {
			$sarray[] = "sp.pro_".$c." = $c";
		}
		$WHERE[] = ''.implode(' OR ',$sarray).'';
	} else {
		$WHERE[] = 'sp.pro_'.$prod.' = '.$prod.'';
	}
	$inner.= 'inner join temp_table_product sp on shop.id=sp.shop_id ';
}



if ($groups) {
	$groups = implode(',',$groups);
	
	 if(strstr($groups,',')) {
		$data4 = explode(',',$groups);
		$sarray1 = array();
		foreach($data4 as $c) {
			$sarray1[] = "sg.grp_".$c." = $c";
		}
		$WHERE[] = ''.implode(' OR ',$sarray1).'';
	} else {
		$WHERE[] = 'sg.grp_'.$groups.' = '.$groups.'';
	}
	$inner.= 'inner join temp_table_group sg on shop.id=sg.shop_id ';
}

if ( sizeof($w) > 0 ) {
$w = @implode(' AND ',$WHERE);
}
	if(!empty($w)) $w = 'WHERE '.$w;

if($inner=="" && $w == "") {
	$sql = $_SESSION['query'];
	//echo $sql = ;
}else {
	$sql = "Select distinct shop.id FROM shop  $inner $w ";
}
	
$arr1 = $objgen->get_AllRows_qry($sql);


if($arr1) {
	$arr = array();
foreach ($arr1 as $val) {
	array_push($arr, $val['id']);
}
}
$shops = array_unique($arr);
//echo '<pre>';print_r($shops);
$shop_ids=implode(',',$shops);
$shop_ids;  
$cat = $_SESSION['cat'];
if(!empty($shop_ids)) {
	 $sql_shop ="SELECT shop.*,c.category as shop_category,c.icon as cat_icon, location.location,city.city as city_name FROM shop INNER JOIN city ON city.id = shop.city INNER JOIN location ON location.id=shop.locality INNER JOIN categories c ON c.id=shop.categories WHERE  shop.categories IN ($cat) AND shop.id IN ($shop_ids) AND shop.status='active' AND shop.city='".$_SESSION['city']."' ";
}
 /*else {
	  $sql_shop ="SELECT shop.*,c.category as shop_category,c.icon as cat_icon,location.location,city.city as city_name FROM shop INNER JOIN city ON city.id = shop.city INNER JOIN location ON location.id=shop.locality INNER JOIN categories c ON c.id=shop.categories WHERE shop.status='active' AND shop.categories IN ( $cat) AND shop.city='".$_SESSION['city']."' ";
}*/

$shop_arr = $objgen->get_AllRows_qry($sql_shop);
$cc1 = array();
foreach ($shop_arr as $dd1) 
{
		$ca1 =  $dd1['categories'];
		array_push($cc1,$ca1);
}
$result_cat1 = array_unique($cc1);	
$res_session1 = implode(",", $result_cat1);
$ss="SELECT * FROM categories WHERE id IN ($res_session1)";
$pp =$objgen->get_AllRows_qry($ss);
$p_cat=array();
foreach ($pp as $v)
{
 	$sp = $v['category'];
 	array_push($p_cat, $sp);
} 
 $p_cat_name = implode(",", $p_cat);
  $city = $objgen->get_Onerow("city","AND id='".$_SESSION['city']."'");
 $city_name = $city['city'];

?>
 <div class="row" id="result" style="display:none;">
			<div  class="searched_string_c" style="margin-top: 130px;width: 97.5%;margin-left: 14px;">
				<h3 ><span class="result_counter"><?php echo count($shop_arr); ?></span> Results for <span class="searched_string">
					<?php
					
					 if(!empty($p_cat_name )){echo $p_cat_name ;} else {echo "Services"; }?> in <?php if(!empty($city_name)){ echo $city_name;} else {echo "all cities";}?></span></h3>
								
			</div>
		</div> 

<?php 
if($shop_arr) { 
$i=0; 
foreach ($shop_arr as $value) {


?>
<div class="searched_reasult_c">
<div class="reasult_info col-md-6">
<div class="name_no_c">
<div class="address left">
<a href="<?=URL?>search-details.php?shop_id=<?php echo  base64_encode($value['id']);  ?>" style="color:black;"><h3><?php echo $value['shop_name'];?></h3></a>	
<p class="searched_location"><?php echo $value['location'];?>, <?php echo $value['city_name']; ?></p>


</div>		
<table class="tbl tbl_border right" border="0" cellpadding="0" cellspacing="0">
<tr>
<td><div class="specification_c">
<i class="<?php echo $value['cat_icon'];?> fa-lg" ></i>
</div>
<div class="clear"></div></td>

</tr>
<tr>
<td colspan="1">
<div class="contact_no_website_c">
<span class="contact_no"><?php echo $value['shop_category'];?></span>
<!-- <span  class="website"><a href="http://desileaks.com/" target="_blank">Visit Website</a></span> -->
</div>
</td>									
</tr>
</table>
<div class="clear"></div>
</div>
<div class="description"><p><?php echo substr($value['overview'], 0, 200);?></p></div>



</div>
<?php

$sql_shop_photo = "SELECT medium FROM photos WHERE shop_id='".$value['id']."'  AND set_profile_pic='true' AND catgeory='Interior' ";
$shop_photo = $objgen->get_AllRows_qry($sql_shop_photo);
//echo "photo=".$shop_photo[0]['thumb'];
?>
<div class="reasult_img col-md-6">
<a href="<?=URL?>search-details.php?shop_id=<?php echo  base64_encode($value['id']);  ?>"><img src="uploads/profile/<?php echo $shop_photo[0]['medium'];?>" alt="" style="height: 206px; width: 100%;" /></a>
</div>					
<div class="clear"></div>



<div class="rating_facilities_c">


<table class="rating_tbl" border="0" width="40%" cellpadding="0" cellspacing="0">
<tr>
<td>
<span class="rating_span">
<?php 
$sql_review ="SELECT * FROM review WHERE shop_id='".$value['id']."'";
$result_review= $objgen->get_AllRows_qry($sql_review);

$count = count($result_review);
$stars = 0;
foreach ($result_review as $review) {
$stars=$stars+$review['star'];
}
//echo $stars."--";
if($count>0) {
$avg = ($stars/$count);

switch ($avg) {
case ($avg > 0 && $avg < 2):
$message='Below Average';
break;
case ($avg == 2 && $avg < 2.5):
$message='Average';
break;
case ($avg == 2.5 && $avg < 3):
$message='Above average';
break;
case ($avg == 3 && $avg < 3.5):
$message='Good';
break;
case ($avg == 3.5 && $avg < 4):
$message='Very good';
break;	
case ($avg == 4 && $avg <= 5):
$message='Superb';
break;
default:
$message = 'Poor';
break;
}
} else {
$avg=0;
$message ="Below Average";
}


?>
<span class="rating_bg"><?php echo $avg; ?></span>
<span><?php  echo $message;?></span>
</span>

</td>

<td>
<span class="rating_span">

<?php 
//$sql_shop_review = "SELECT * FROM review WHERE shop_id='".$value['id']."'  AND status='active'";
$shop_review = $objgen->get_AllRowscnt("review","AND shop_id='".$value['id']."'AND status='active'");

?>
<span class="rating_bg"><?php echo $shop_review; ?></span>
<span>Reviews</span>
</span>
</td>
<td>
<?php 
if(empty($_SESSION['ma_log_id_usr'])) {?>
<span class="rating_span">
<a href="#" id="<?php echo $value['id']?>" onclick="add_fave(this.id,0)">
<img src="front-end-images/add_d.png"/>
<span  class="deco">Add</span>
</a>
</span>
<?php } else {
$favourite=$objgen->chk_Ext("favourites","shop_id='".$value['id']."' AND user_id='".$_SESSION['ma_log_id_usr']."'");

if($favourite>0) {?>
<span class="rating_span added_d" >
<span>&nbsp;</span>
<span class="deco">Added</span>


</span>
<?php } else {?>
<span class="rating_span">
<a href="#" id="<?php echo $value['id']?>" onclick="add_fave(this.id,<?php echo $_SESSION['ma_log_id_usr'];?>)">
<img src="front-end-images/add_d.png"/>
<span  class="deco">Add</span>
</a>
</span>
<?php } }?>

</td>
</tr>
</table>





<div class="table-responsive">
<table class="table facilities_tbl" border="0" width="60%" cellpadding="0" cellspacing="0">
<?php 
$sql_shop_facility = "SELECT facility_id FROM shop_facilities WHERE shop_id='".$value['id']."' 
AND  cat_id='".$value['categories']."'"; 

$shop_facility_ids = $objgen->get_AllRows_qry($sql_shop_facility); 
$getFaclityChk = array( );

foreach ($shop_facility_ids as $shop_facility) {

$subArr2 = $shop_facility['facility_id'];

array_push($getFaclityChk,$subArr2);

}
$sql_facility = "SELECT * FROM facilities WHERE  cat_id='".$value['categories']."'LIMIT 10";
$facility =$objgen->get_AllRows_qry($sql_facility);
$facilityCount=1;
?>

<tr>
<?php foreach ($facility as $fact) {

?>
<td><i class="<?php echo $fact['icon']; ?> fa-lg" <?php if(in_array($fact['id'], $getFaclityChk)) {?>style="color:#CD5C5C;"<?php } else {?>style="#B1B1B1;"<?php } ?>></i><span class="facilities_span"><?php echo $fact['facility']; ?></span></td>

<?php if($facilityCount==5) {?>
</tr>
<tr>
<?php } ?>

<?php $facilityCount++; }
?>
</tr>
</table>
</div> 
<div class="clear"></div>
</div>


<!-- Featureed Services Horizontal scroll -->
<div>
<div id="reatured_services_c">
<div id="arrowL">
</div>
<div id="arrowR">
</div>
<div class="over_div">
<div id="list-reatured_services">
<div class='list'>
<?php 
$sql_shop_groups = "SELECT distinct g.icon,g.group_title,sc.* FROM shop_categories sc INNER JOIN 	groups g ON g.id=sc.group_id WHERE sc.shop_id='".$value['id']."' 
AND  sc.cat_id='".$value['categories']."' GROUP BY g.group_title LIMIT 5"; 

$shop_groups_id = $objgen->get_AllRows_qry($sql_shop_groups); 
foreach ($shop_groups_id as $groups_id) {?>
<div class='item'>

<span><i class="<?php echo $groups_id['icon'];?> fa-lg" ></i></span>
<h4><?php echo ucfirst($groups_id['group_title']);?></h4>

</div>
<?php } ?>							


</div>
</div>
</div>
</div>

<div class="clear"></div>
</div>
</div>
<?php  $i++; } ?>
<?php }else { ?>

 <div class="row" id="result" style="display:none;">
			<div  class="searched_string_c" style="margin-top: 130px;width: 97.5%;margin-left: 14px;">
				<h3 ><span class="result_counter"><?php echo count($arr1); ?></span> Results for <span class="searched_string">
					selected criteria 
					 </span></h3>
								
			</div>
		</div> 
<?php } ?>

