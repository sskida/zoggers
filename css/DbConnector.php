<?php

//ini_set("error_reporting", E_ALL & ~E_DEPRECATED);

// DbConnector

require_once 'SystemComponent.php';

require_once 'settings.php';

class DbConnector extends SystemComponent {



    var $theQuery;

    var $link;

    var  $formaction;



//*** Function: DbConnector, Purpose: Connect to the database ***

    function DbConnector() {



// Load settings from parent class

        $settings = SystemComponent::getSettings();



// Get the main settings from the array we just loaded

        $host  = $settings['dbhost'];

        $db    = $settings['dbname'];

        $user  = $settings['dbusername'];

        $pass  = $settings['dbpassword'];

        $this->formaction=$settings['formaction'];



// Connect to the database

        $this->link = mysql_connect($host, $user, $pass) or die('Database error');

        mysql_select_db($db) or die('Please contact developer immediately (Make payment)');

        register_shutdown_function(array(&$this, 'close'));

    }



//*** Function: query, Purpose: Execute a database query ***

    function query($query) {



        $this->theQuery = $query;

        return mysql_query($query, $this->link);

    }



//*** Function: fetchArray, Purpose: Get array of query results ***

    function fetchArray($result) {



        return mysql_fetch_array($result);

    }



    function countRows($result)

    {

        return mysql_num_rows($result);

    }

//*** Function: close, Purpose: Close the connection ***

    function close() {



        mysql_close($this->link);

    }

	

  function dbRowInsert($table_name, $form_data)

		{

			

			// retrieve the keys of the array (column titles)

			$fields = array_keys($form_data);

		

			// build the query

			 $sql = "INSERT INTO ".$table_name."

			(`".implode('`,`', $fields)."`)

			VALUES('".implode("','", $form_data)."')";

			

		

			// run and return the query result resource

			 mysql_query($sql,$this->link);

			 return mysql_insert_id();

		}



			// again where clause is left optional

		 function dbRowUpdate($table_name, $form_data, $where_clause='')

		{ 

			

			// check for optional where clause

			$whereSQL = '';

			if(!empty($where_clause))

			{

				// check to see if the 'where' keyword exists

				if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE')

				{

					// not found, add key word

					$whereSQL = " WHERE ".$where_clause;

				} else

				{

					$whereSQL = " ".trim($where_clause);

				}

			}

			// start the actual SQL statement

			$sql = "UPDATE ".$table_name." SET ";

		

			// loop and build the column /

			$sets = array();

			foreach($form_data as $column => $value)

			{

				 $sets[] = "`".$column."` = '".$value."'";

			}

			$sql .= implode(', ', $sets);

		

			// append the where statement

			 $sql .= $whereSQL;

			 

			

		  

			// run and return the query result

			return mysql_query($sql,$this->link);

		}

		

			// the where clause is left optional incase the user wants to delete every row!

		 function dbRowDelete($table_name, $where_clause='')

		{

			

			

			// check for optional where clause

			$whereSQL = '';

			if(!empty($where_clause))

			{

				// check to see if the 'where' keyword exists

				if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE')

				{

					// not found, add keyword

					$whereSQL = " WHERE ".$where_clause;

				} else

				{

					$whereSQL = " ".trim($where_clause);

				}

			}

			// build the query

			 $sql = "DELETE FROM ".$table_name.$whereSQL;

			

			// run and return the query result resource

			return mysql_query($sql,$this->link);

		}

	



}



?>

