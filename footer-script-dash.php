<!-- child of the body tag --> 
<span id="top-link-block" class="hidden"> <a href="#top" class="well well-sm" onClick="$('html,body').animate({scrollTop:0},'slow');return false;"> <i class="glyphicon glyphicon-chevron-up"></i> Back to Top </a> </span><!-- /top-link-block --> 
<script src="<?=URL?>js/jquery.min.js"></script> 
<script src="<?=URL?>js/jquery-ui-1.10.3.min.js" type="text/javascript"></script> 
<script src="<?=URL?>js/jquery-ui.js" type="text/javascript"></script> 
<script src="<?=URL?>js/dashboardbootstrap.min.js" type="text/javascript"></script> 
<script src="<?=URL?>js/nhf-script.js" type="text/javascript"></script> 
<script>jQuery(function($) {
$('.navbar .dropdown').hover(function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

}, function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

});

$('.navbar .dropdown > a').click(function(){
location.href = this.href;
});

});</script> 
<script src="<?=URL?>js/down.min.js"></script> 
<script>$(document).ready(function(){


	$('.box-bounce').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('bounceIn');
	}, { offset: '80%' });
	
		$('.box-flip').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('flipInY');
	}, { offset: '80%' });
		$('.box-left').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInLeft');
	}, { offset: '100%' });
	$('.box-right').waypoint(function(down){
		$(this).addClass('animation');
		$(this).addClass('fadeInRight');
	}, { offset: '100%' });
	});</script> 
<script src="<?=URL?>js/slide.js" type="text/javascript"></script> 
<script src="<?=URL?>js/application.js"></script> 
<script src="<?=URL?>js/skycons.js"></script> 
<script src="<?=URL?>js/jquery.flot.canvas.min.js"></script> 

<!--Page Level JS--> 
<script src="<?=URL?>js/jquery.countTo.js"></script> 
<script src="<?=URL?>js/jquery-jvectormap-1.js"></script> 
<script src="<?=URL?>js/jquery-jvectormap-world-mill-en.js"></script> 
<script>
        $(document).ready(function() {
            app.timer();
            app.map();
            app.weather();
            app.morrisPie();
        });
    </script> 
<script src="<?=URL?>js/dashboard.js" type="text/javascript"></script>
<?php unset($_SESSION['msg']);?>