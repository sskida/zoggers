<?php
$objgen		=	new general();

if(isset($_POST['Login']))
{
  $email = trim($_POST['email_login']);
  $password = trim($_POST['password_login']);
  
  if($email!="" && $password!="")
  {
	  $msg5 = $objgen->chk_Login('merchants',$email,$password,'','id','email','password','active',0);
	  if($msg5=="")
	  {

		header("location:".URL."dashboard.php");
	  }
	  else
	  {
	    echo "<script>alert('".$msg5."');</script>";
	  }
  }
  else
  {
   echo "<script>alert('Enter Username and Password.')</script>";
  }
}


if(isset($_POST['Login2']))
{
  $email = trim($_POST['email_login']);
  $password = trim($_POST['password_login']);
  
  if($email!="" && $password!="")
  {
	  $msg5 = $objgen->chk_Login('users',$email,$password,'','id','email','password','active',2);
	  if($msg5=="")
	  {

		header("location:".URL."user-dashboard.php");
	  }
	  else
	  {
	    echo "<script>alert('".$msg5."');</script>";
	  }
  }
  else
  {
   echo "<script>alert('Enter Username and Password.')</script>";
  }
}

if(isset($_POST['signup']))
{
    $email  	    = $objgen->check_input($_POST['email']);
	$name           = $objgen->check_input($_POST['name']);
	$gender  	    = $objgen->check_input($_POST['gender']);
	$phone  	    = $objgen->check_input($_POST['phone']);
	$city   	    = $objgen->check_input($_POST['city']);
		
	$password  		= $_POST['password'];
	$status  		= "active";
		

	$msg = "";
    $sh_exit = $objgen->chk_Ext("users","email='$email'");
	if($sh_exit>0)
	{
		echo "<script>alert('This email is already exists.')</script>";
		$msg = "Err";
	}

   if($msg=="")
   {

		 $msg = $objgen->ins_Row('users','name,email,password,phone,gender,city,status',"'".$name."','".$email."','".$objgen->encrypt_pass($password)."','".$phone."','".$gender."','".$city."','".$status."'");
		 
		 $my_id = $objgen->get_insetId();
		 
		 if($msg=="")
		 {
					
				   //  $name = $name;
					
					$subject = SITE_NAME." - Registration Completed";
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
					$headers .= 'From: Admin <'.ADMINMAIL.'>' . "\r\n";
									
					$to =  $email;
	
						
					$message = 'Dear '.$name.',<br /><br />
					Thank you for Registering with '.WEBSITE.' <br /><br />
					<a href="http://'.ROOT_PATH.'" >'.WEBSITE.'</a> is the '.DESCRIP.'.<br /><br />
					To Login, <a href="http://'.ROOT_PATH.'" >Click Here</a><br /><br />
					Happy Searching! <br />
					Team '.SITE_NAME.' <br />';

					//echo $message;exit;
					mail($to,$subject,$message,$headers);
					
					$_SESSION['ma_log_id_usr'] 	   	= $my_id;
					$_SESSION['ma_usr_name_usr']	= $email;
					$_SESSION['ma_name_usr']		= $name;
					
					header("location:".URL."user-dashboard.php");


					/*echo '<script>alert("Registration Completed Successfully")</script>';*/
		 }
		 else
		 {
		    	echo "<script>alert('".$msg."')</script>";
		 }
	}
}

if(isset($_POST['submerch']))
{
    $email  	    = $objgen->check_input($_POST['mer_email']);
	$name           = $objgen->check_input($_POST['mer_name']);
	$email_update   = $objgen->check_input($_POST['email_update']);
	$phone  	    = $objgen->check_input($_POST['mer_phone']);
	$city   	    = $objgen->check_input($_POST['mer_city']);
	$address   	    = $objgen->check_input($_POST['mer_add']);
		
	$password  		= $_POST['mer_pwd'];
	$status  		= "active";
		

	$msg = "";
    $sh_exit = $objgen->chk_Ext("merchants","email='$email'");
	if($sh_exit>0)
	{
		echo "<script>alert('This email is already exists.')</script>";
		$msg = "Err";
	}

   if($msg=="")
   {

		 $msg = $objgen->ins_Row('merchants','name,email,password,phone,email_update,city,status,address',"'".$name."','".$email."','".$objgen->encrypt_pass($password)."','".$phone."','".$email_update."','".$city."','".$status."','".nl2br($address)."'");
		 
		 $my_id = $objgen->get_insetId();
		 
		 if($msg=="")
		 {
					
				    // $name = $first_name." ".$last_name;
					
					$subject = SITE_NAME." - Registration Completed";
					$headers = "MIME-Version: 1.0" . "\r\n";
					$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
					$headers .= 'From: Admin <'.ADMINMAIL.'>' . "\r\n";
									
					$to =  $email;
	
						
					$message = 'Dear '.$name.',<br /><br />
					Thank you for Registering with '.WEBSITE.' <br /><br />
					<a href="http://'.ROOT_PATH.'" >'.WEBSITE.'</a> is the '.DESCRIP.'.<br /><br />
					To Login, <a href="http://'.ROOT_PATH.'" >Click Here</a><br /><br />
					Happy Searching! <br />
					Team '.SITE_NAME.' <br />';

					//echo $message;exit;
					mail($to,$subject,$message,$headers);
					
					//$_SESSION['ma_log_id'] 	   	= $my_id;
					//$_SESSION['ma_usr_name']	= $email;
					//$_SESSION['ma_name']		= $name;
					
						$_SESSION['ma_log_id_mer']		=  $my_id;
						$_SESSION['ma_usr_name_mer']	=  $email;
						$_SESSION['ma_name_mer']	    =  $name;
                    
					header("location:".URL."dashboard.php");

					/*echo '<script>alert("Registration Completed Successfully")</script>';*/
		 }
	}
}
?>
<div id="shortModal" class="modal modal-wide fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <div class="modal-body">
        <div class="pop-up-inner">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h1>Register</h1>
       <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
	   <form method="post" action="" >
         <div class="modal-body">
         <div class="form-class">
         <label> Name</label>
         <input type="text" placeholder="Name" name="name" required />
         </div>
         <div class="form-class">
         <label>Email</label>
         <input type="email" placeholder="Email" name="email" required class="form-control" />
         </div>
         <div class="form-class">
         <label>Password</label>
         <input type="password" placeholder="Password"  name="password" id="password" required  class="form-control" />
         </div>
         <div class="form-class">
         <label>ConfirmPassword</label>
         <input type="password" placeholder="Confirm Password"  name="conf_password"  id="conf_password"  required class="form-control" />
         </div>
         <div class="form-class">
         <label>Contact Number</label>
         <input type="text" placeholder="Contact Number" name="phone" required class="form-control" />
         </div>
		 
		  <div class="form-class">
         <label>Gender</label>
        <input name="gender" type="radio" value="Male"  /> Male
		<input name="gender" type="radio" value="Female" /> Female
         </div>
		 
		   <div class="form-class">
         <label>Location/City</label>
         <input type="text" placeholder="City" name="city" required />
         </div>
		 

         

        <div class="form-class">
         <input type="submit" value="Register" name="signup" onclick="return val_signup();">
       
        </div>
       
      </div>
	  
	  </form>
      </div>
      </div>
     
      </div>
      
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal -->

<div id="shortModal2" class="modal modal-wide fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <div class="pop-up-inner">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h1>Merchant Login</h1>
       <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
	   
	    <form method="post" action="" >
         <div class="modal-body">
         <div class="form-class">
         <label>Email</label>
         <input type="email" placeholder="Email" name="email_login" required  class="form-control" >
         </div>
         <div class="form-class">
         <label>Password</label>
         <input type="password" placeholder="Password" name="password_login" required  class="form-control" >
         </div>
        <div class="form-class">
         <input type="submit" value="Login" name="Login">
        <input type="reset" value="cancel">
        </div>
       
      </div>
	  </form>
	  
      </div></div>
     
     
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>

<div id="shortModal3" class="modal modal-wide fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <div class="pop-up-inner">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h1>User Login</h1>
        <form method="post" action="" >
         <div class="modal-body">
         <div class="form-class">
         <label>User Name</label>
         <input type="email" placeholder="Email" name="email_login" required class="form-control" />
         </div>
         <div class="form-class">
         <label>Password</label>
         <input type="passeword" placeholder="Password" name="password_login" required  class="form-control" />
         </div>
        <div class="form-class">
         <input type="submit" value="Login" name="Login2">
        <input type="reset" value="cancel">
        </div>
       
      </div>
	  </form>
      </div></div>
     
     
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal -->

<div id="shortModal4" class="modal modal-wide fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <div class="pop-up-inner">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h1>Merchant Registration</h1>
       <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
	      <form method="post" action="" >
         <div class="modal-body">
         <div class="form-class">
         <label>Merchant Name</label>
         <input type="text" placeholder="Enter Your Business Name" name="mer_name" required />
         </div>
         <div class="form-class">
         <label>Location/City</label>
         <input type="text" placeholder="Enter Your City OR Location Name" required name="mer_city" />
         </div>
         <div class="form-class">
         <label>Merchant Address</label>
         <textarea placeholder="Address" required name="mer_add" ></textarea>
         </div>
         <div class="form-class">
         <label>Contact No.</label>
         <input type="text" placeholder="+91-9XXXXXXXXXX"  required name="mer_phone"/>
         </div>
         <div class="form-class">
         <label>Email</label>
         <input type="email" placeholder="example@gmail.com" required name="mer_email" class="form-control" />
         </div>
         <div class="form-class">
         <label>Password</label>
         <input type="password" placeholder="Enter Your Password" required name="mer_pwd" id="mer_pwd" class="form-control" />
         </div>
         <div class="form-class">
         <label>Confirm Password</label>
         <input type="password" placeholder="Re-Enter Your Password" required name="mer_conf_pwd" id="mer_conf_pwd" class="form-control" />
         </div>
         <div class="form-class">
         <input type="checkbox" name="email_update" value="yes"  style="visibility:visible" >
        <label style="width:90%;">Send me occasional email updates</label>
         </div>
        <div class="form-class">
         <input type="submit" value="Register" name="submerch" onclick="return val_merchant();" />
        
        </div>
       
      </div>
	  </form>
      </div>
      </div>
      
    </div>
    <!-- /.modal-content --> 
  </div>
  <!-- /.modal-dialog --> 
</div>
<!-- /.modal --> 

<script>
function val_signup()
{
var pass = $('#password').val();
var conf_pass = $('#conf_password').val();
if(pass!=conf_pass)
{
  alert("Confirm Password is incorrect.");
  return false;
}

}

function val_merchant()
{
var pass = $('#mer_pwd').val();
var conf_pass = $('#mer_conf_pwd').val();
if(pass!=conf_pass)
{
  alert("Confirm Password is incorrect.");
  return false;
}

}

</script>
