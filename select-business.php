<?php

//require_once "includes/includepath.php";
require_once "chk_login_mer.php";
$objgen   = new general();
if(isset($_POST['Select']))
{    
  
 $choose = base64_encode($_POST['choose']);
  if($choose) { 
    header("location:".URL."add-business.php?cat=".$choose);
    exit;
  }
  
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>
</head>

<body>
<?php include 'header-dash.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <?php include 'merchant-menu.php'; ?>


<aside class="right-side">
  <section class="content-header-top">
    <h1> <i class="fa fa-suitcase"></i> Edit Profile </h1>
   
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-6">
            <form role="form" action="" method="post" enctype="multipart/form-data" >
            <div class="box padding-both">
			
			   <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
            
              <div class="form-group">
                <label>Select your business</label>
				<select name="choose" required class="form-control" >
				<option value="">Select</option>
			<?php
              $pagesize   =   1000;
              $page       = isset($_REQUEST['page'])  ?   $_REQUEST['page']   :   "1";

              $row_count = $objgen->get_AllRowscnt("categories",$where);

              if($row_count>0)
              {
                  $res_arr = $objgen->get_AllRows("categories",$pagesize*($page-1),$pagesize,"category ASC",$where);
              }
              //echo '<pre>';print_r($res_arr);exit;
              ?>
				  <?php foreach($res_arr as $key=>$val) {?>
          <option value="<?php echo $objgen->check_tag($val['id']); ?>" <?php $nowid=$objgen->check_tag($val['id']); if($categoryid == $nowid){ echo 'selected'; } ?>><?php echo $objgen->check_tag($val['category']); ?></option>
          <?php } ?>
				</select>
           
              </div>
              
                
              <div class="box-footer">
                <button id="submit" class="cl2-green common-btn" name="Select" type="submit">Go</button>
              </div>
            </div>
          </form>
        </div>
    </div>
  </section>
</aside>
</div>
<?php include 'footer-script-dash.php'; ?>
</body>
</html>
