s<header>
  <div class="header-top"> <a href="<?=URL?>home" class="logo"> <img src="<?=URL?>zoggrs_logo.png" alt="zoggrs"> </a>
    <nav class="navbar navbar-static-top" role="navigation"> <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
      <div class="navbar-right">
        <ul class="nav navbar-nav">
          
          <!-- Tasks: style can be found in dropdown.less --> 
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa-color-top fa fa-user"></i> <span> <i class="caret"></i></span> </a>
            <ul class="dropdown-menu">
              <li class="user-header bg-light-blue"><i class="fa fa-user fa-5x"></i>
                <p> <?=$_SESSION['ma_name_mer']?> </p>
              </li>
              <li class="user-footer">
                <div class="pull-left"> <a href="<?=URL?>view-profile" class="btn btn-default btn-flat">Profile</a> </div>
                <div class="pull-right"> <a href="<?=URL?>logout-mer" class="btn btn-default btn-flat">Sign out</a> </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</header>