<?php

header("location:index.php");
exit;


require_once "includes/includepath.php";
$objgen		=	new general();

$where = "";
$row_count = $objgen->get_AllRowscnt("categories",$where);
if($row_count>0)
{
  $res_arr = $objgen->get_AllRows("categories",0,$row_count,"category desc",$where);
}

sort($res_arr);
$cat_names = "";

for($i=0;$i<count($res_arr);$i++) {
	$cat_names .= "\"".$res_arr[$i]['category']."\",";
}

//city
$row_count_city = $objgen->get_AllRowscnt("city",$where);
if($row_count_city>0)
{
  $res_arr_city = $objgen->get_AllRows("city",0,$row_count_city,"city asc",$where);
}

sort($res_arr_city);
$city_names = "";

for($i=0;$i<count($res_arr_city);$i++) {
	$city_names .= "\"".$res_arr_city[$i]['city']."\",";
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?=TITLE?></title>
<!--css styles here-->
<?php include 'head-script.php'; ?>

<link href="css/jquery-ui.css" rel="Stylesheet"></link>
<!-- <script src="<?=URL?>js/jquery-2.1.3.min.js"></script> -->
<script src="js/jquery-ui.js" ></script>

<script>
	$(function() {
		var availableTags = [
			<?php echo $cat_names; ?>
		];
		$( "#tags" ).autocomplete({
			source: availableTags
		});
		
		
		var availableTagsLoc = [
			<?php echo $city_names; ?>
		];
		$( "#tags_city" ).autocomplete({
			source: availableTagsLoc
		});

		// Overrides the default autocomplete filter function to search only from the beginning of the string
		$.ui.autocomplete.filter = function (array, term) {
				var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(term), "i");
				return $.grep(array, function (value) {
						return matcher.test(value.label || value.value || value);
				});
		};

	});
</script>

</head>

<body>
<?php include 'header.php'; ?>
<div class="clearfix"></div>
<div id="slider">
  <div class="container content-top">
    <div class="slider-art"><img src="images/lady_art.png"></div>
    <div class="search-title">
      <h1>SEARCH, COMPARE AND BOOK</h1>
    </div>
    <h2>CHOOSE FROM 100,000 SERVICES AND 5000 OUTLETS</h2>

    <div class="search-box-outer">
		<form name="search" method="post" action="<?=URL?>search" enctype="application/x-www-form-urlencoded">

      <input name="cat" id="tags" type="text" class="search-icon" placeholder="Book a service">
      <input name="city" type="text" id="tags_city" class="location-icon" placeholder="Select City">
			<input type="hidden" name="reference" value="home">
			
      <input type="submit" value="Go">
		</form>
    </div>

  </div>
  <div class="round-wrap">
    <div class="circle-one"> <a href="<?=URL?>search/?cat=salon" class="bg-circle">
      <div class="bg-white"> <img src="images/circle_01.png"> </div>
      <div class="bg-blue">salon</div>
      </a> </div>
    <div class="circle-one"> <a href="<?=URL?>search/?cat=beauty" class="bg-circle">
      <div class="bg-white"> <img src="images/circle_02.png"> </div>
      <div class="bg-blue">BEAUTY PARLOUR </div>
      </a> </div>
    <div class="circle-one"> <a href="<?=URL?>search/?cat=tatto" class="bg-circle">
      <div class="bg-white"> <img src="images/circle_03.png"> </div>
      <div class="bg-blue">TATTOO</div>
      </a> </div>
    <div class="circle-one"> <a href="<?=URL?>search/?cat=mehandi" class="bg-circle">
      <div class="bg-white"> <img src="images/circle_04.png"> </div>
      <div class="bg-blue">MEHANDI</div>
      </a> </div>
    <div class="circle-one"> <a href="<?=URL?>search/?cat=gym" class="bg-circle bg-circle2">
      <div class="bg-white"> <img src="images/circle_05.png"> </div>
      <div class="bg-blue bg-circle2">GYM,
        AREOBICS</div>
      </a> </div>
    <div class="circle-one"> <a href="<?=URL?>search/?cat=spa" class="bg-circle bg-circle2">
      <div class="bg-white"> <img src="images/circle_06.png"> </div>
      <div class="bg-blue bg-circle2">spa</div>
      </a> </div>
    <div class="circle-one"> <a href="<?=URL?>search/?cat=weight" class="bg-circle bg-circle2">
      <div class="bg-white"> <img src="images/circle_07.png"> </div>
      <div class="bg-blue bg-circle2">Weight Loss gain</div>
      </a> </div>
    <div class="circle-one"> <a href="<?=URL?>search/?cat=service" class="bg-circle bg-circle2">
      <div class="bg-white"> <img src="images/circle_08.png"> </div>
      <div class="bg-blue bg-circle2">Home Service</div>
      </a> </div>
  </div>
</div>
<section>
  <div class="container">
    <div class="center-part">
      <div class="round-shape">
        <ul>
          <li>
            <div class="circle-img"><img src="images/book.png"></div>
            select</li>
          <li>
            <div class="circle-img"><img src="images/find.png"></div>
            search</li>
          <li>
            <div class="circle-img"><img src="images/calender.png"></div>
            book</li>
        </ul>
      </div>
      <div class="phone-hand"><img src="images/hand-phone.png"></div>
    </div>
    <h1>how it works?</h1>
    </div>
    <div class="clearfix"></div>
    <div class="border-bottom"></div>
    <div class="container">
    <div class="tab-chart">
     <ul class="nav nav-tabs">
        <li ><a data-toggle="tab" href="#sectionA"><div class="circle-shape"><img src="images/icon_01.png"></div><h1>download app</h1></a></li>
        <li class="active"><a data-toggle="tab" href="#sectionB"><div class="circle-shape"><img src="images/icon_1.jpg"></div><h1>Search</h1></a></li>
        <li class="active"><a data-toggle="tab" href="#sectionC"><div class="circle-shape"><img src="images/icon_02.png"></div><h1>select</h1></a></li>
        <li class="active"><a data-toggle="tab" href="#sectionD"><div class="circle-shape"><img src="images/icon_03.png"></div><h1>book</h1></a></li>

    </ul>
    <div class="tab-content">
        <div id="sectionA" class="tab-pane fade in ">
           <h3>download app</h3>
            <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus adipiscing.</p>

        </div>
        <div id="sectionB" class="tab-pane fade in active">
             <img src="images/iphone.png" class="iphone">
        </div>

        <div id="sectionC" class="tab-pane fade in">
           <h3>select</h3>
            <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus adipiscing.</p>

        </div>

         <div id="sectionD" class="tab-pane fade in">
           <h3>book</h3>
            <p>Vestibulum nec erat eu nulla rhoncus fringilla ut non neque. Vivamus nibh urna, ornare id gravida ut, mollis a magna. Aliquam porttitor condimentum nisi, eu viverra ipsum porta ut. Nam hendrerit bibendum turpis, sed molestie mi fermentum id. Aenean volutpat velit sem. Sed consequat ante in rutrum convallis. Nunc facilisis leo at faucibus adipiscing.</p>

        </div>

    </div>

    </div>

  </div>
</section>
<div class="bg_story">
  <h1>OUR BRAND STORY</h1>
  <div class="border-circle"><a href="#"><img src="images/play.png"></a></div>
</div>
<div class="bottomg-bg">
  <div class="container">
    <div class="col-md-4 col-sm-4 col-xs-12">
      <h1>Total Verified Listings</h1>
      <h1>100,000</h1>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <h1>New Listings Added</h1>
      <h1>100</h1>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <h1>Happy Visitors</h1>
      <h1>100,000</h1>
    </div>
  </div>
</div>
<div class="container">
  <div class="grid">
    <figure class="effect-oscar"> <img src="images/gallery_01.jpg" alt="img01"/>
      <figcaption>
        <h2>Some Heading Here</h2>
        <p>Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Nullam eget eros eget
          nisi tempus tinccibus non velited ut
          eros euismod.</p>
      </figcaption>
    </figure>
    <figure class="effect-oscar"> <img src="images/gallery_02.jpg" alt="img02"/>
      <figcaption>
        <h2>Some Heading Here</h2>
        <p>Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Nullam eget eros eget
          nisi tempus tinccibus non velited ut
          eros euismod.</p>
      </figcaption>
    </figure>
    <figure class="effect-oscar"> <img src="images/gallery_03.jpg" alt="img02"/>
      <figcaption>
        <h2>Some Heading Here</h2>
        <p>Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Nullam eget eros eget
          nisi tempus tinccibus non velited ut
          eros euismod.</p>
      </figcaption>
    </figure>
    <figure class="effect-oscar"> <img src="images/gallery_04.jpg" alt="img02"/>
      <figcaption>
        <h2>Some Heading Here</h2>
        <p>Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Nullam eget eros eget
          nisi tempus tinccibus non velited ut
          eros euismod.</p>
      </figcaption>
    </figure>
    <figure class="effect-oscar"> <img src="images/gallery_04.jpg" alt="img01"/>
      <figcaption>
        <h2>Some Heading Here</h2>
        <p>Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Nullam eget eros eget
          nisi tempus tinccibus non velited ut
          eros euismod.</p>
      </figcaption>
    </figure>
    <figure class="effect-oscar"> <img src="images/gallery_03.jpg" alt="img02"/>
      <figcaption>
        <h2>Some Heading Here</h2>
        <p>Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Nullam eget eros eget
          nisi tempus tinccibus non velited ut
          eros euismod.</p>
      </figcaption>
    </figure>
    <figure class="effect-oscar"> <img src="images/gallery_02.jpg" alt="img02"/>
      <figcaption>
        <h2>Some Heading Here</h2>
        <p>Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Nullam eget eros eget
          nisi tempus tinccibus non velited ut
          eros euismod.</p>
      </figcaption>
    </figure>
    <figure class="effect-oscar"> <img src="images/gallery_01.jpg" alt="img02"/>
      <figcaption>
        <h2>Some Heading Here</h2>
        <p>Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Nullam eget eros eget
          nisi tempus tinccibus non velited ut
          eros euismod.</p>
      </figcaption>
    </figure>
  </div>
</div>
<?php include 'footer.php'; ?>
<?php include 'login-form.php'; ?>
<?php include 'footer-script.php'; ?>
<script>// when .modal-wide opened, set content-body height based on browser height; 200 is appx height of modal padding, modal title and button bar

$(".modal-wide").on("show.bs.modal", function() {
  var height = $(window).height() - 200;
  $(this).find(".modal-body").css("max-height", height);
});</script>
</body>
</html>
