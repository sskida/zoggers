<header <?=$class?>>
<div class="top">
  <div class="container">
    <ul>
	  <?php
	  if($_SESSION['ma_log_id_usr']=="" || $_SESSION['ma_usr_name_usr']=="")
	  {
	  ?>
      <li><a data-toggle="modal" href="#shortModal"><i class="fa fa-user"></i> Sign up</a></li>
	  <?php
	  }
	  if($_SESSION['ma_log_id_mer']=="" || $_SESSION['ma_usr_name_mer']=="")
	  {
	  ?>
      <li><a data-toggle="modal" href="#shortModal2"><i class="fa fa-lock"></i> Merchant Login</a></li>
	  <?php
	  }
	 ?>
	   <?php
	  if($_SESSION['ma_log_id_usr']=="" || $_SESSION['ma_usr_name_usr']=="")
	  {
	  ?>
      <li><a data-toggle="modal" href="#shortModal3"><i class="fa fa-lock"></i> User Login</a></li>
	    <?php
		}
	  if($_SESSION['ma_log_id_mer']=="" || $_SESSION['ma_usr_name_mer']=="")
	  {
	  ?>
	    <li><a data-toggle="modal" href="#shortModal4"><i class="fa fa-list"></i> List Your Business</a></li>
	  <?php
        }
	   else
	  {
	  ?>
	  <li><i class="fa fa-dashboard "></i><a href="<?=URL?>dashboard">My Dashbaord</a></li>
	  <li><i class="fa fa-sign-out"></i><a href="<?=URL?>logout-mer">Logout</a></li>
	  <?php
	  }
	  ?>
	  
	     <?php
	  if($_SESSION['ma_log_id_usr']!="" || $_SESSION['ma_usr_name_usr']!="")
	  {
	  ?>
	   <li><i class="fa fa-dashboard "></i><a href="<?=URL?>user-dashboard">My Dashbaord</a></li>
	   <li><i class="fa fa-sign-out"></i><a href="<?=URL?>logout-usr">Logout</a></li>
	  <?php
	  }
	  ?>
    
    </ul>
  </div>
</div>
<div class="container">
<div id="logo"><a href="<?=URL?>index"><img src="<?=URL?>front-end-images/zoggers_logo.png"></a></div>
<div class="menu-section">
<nav class="navbar navbar-default">
<div class="navbar-header">
  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
</div>

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
  <ul class="nav navbar-nav">
    <li class="active"><a href="#">home</a></li>
    <li><a href="#"> how it works</a></li>
    <li><a href="#"> info</a></li>
    <li><a href="#"> contact us</a></li>
    <li><a href="#"> download app</a></li>
  </ul>
</div>
</nav>
</div>
</div>
</header>