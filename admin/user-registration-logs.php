
<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
require_once "../country/country.php";
$objgen		=	new general();

$con_arr = country();

$pagehead = "User Registration Logs";

$add_url = "";

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if(isset($_GET['del']))
{
   $id= $_GET['del'];
   $msg     = $objgen->del_Row("users","id=".$id);
    if($msg=="")
   {
	header("location:".URLAD."user-registration-logs?msg=3&page=".$page);
   }
}




if($_GET['msg']==3)
{

  $msg2 = "Log Deleted Successfully.";
}





if(isset($_POST['Reset']))
{
	 unset($_REQUEST);
	 header("location:".URLAD."user-registration-logs");
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo TITLE; ?></title>
		<?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">
	<?php require_once "header.php"; ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
        <?php require_once "menu.php"; ?>
		 	<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        List <?=$pagehead?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">List <?=$pagehead?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				
			
				
				<div class="row">
                     <div class="col-xs-12">
						
							
                           <div class="box">
                                <div class="box-body table-responsive">
								 <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Login With Website</th>
                                                <th>Login With Facebook</th>
                                                <th>Login With Google</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
										  	<?php
										  	$email = $objgen->get_AllRowscnt("users","AND registered_with='email'");
										  	$facebook = $objgen->get_AllRowscnt("users","AND registered_with='facebook'");
										  	$google = $objgen->get_AllRowscnt("users","AND registered_with='google'");
											  

											?>
                                            <tr>
                                                <td><?php echo $email;?> Users</td>
                                                <td><?php echo $facebook;?> Users</td>
                                                <td><?php echo $google;?> Users</td>
                                                
                                            </tr>
                                         
                                        </tbody>
                                       
                                    </table>
									<?php
									if($row_count > $pagesize) 
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">
										
											<div class="dataTables_paginate paging_bootstrap">
											    		
												<?php echo $pages; ?>

												
											</div>
											
									   </div>
									</div>
									<?php
										 }
									?>
									
                                </div><!-- /.box-body -->
                            </div><!-- /.box --> 
                        </div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
      </body>
</html>
