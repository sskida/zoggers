<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
require_once "../country/country.php";
$objgen		=	new general();

$con_arr = country();

$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

        $id = $_GET['id'];
	    $shop   = $objgen->get_Onerow("shop","AND id=".$id);
		
		 $shop_type =  $shop['shop_type'];
		 
		 $merchant     = $objgen->get_Onerow("merchants","AND id=".$shop['merchant_id']);
	
		 $ad_exp     = $objgen->get_Onerow("ad_exp","AND shop_id=".$id);
   		 $other_info = $objgen->get_Onerow("other_info","AND shop_id=".$id);
   		 $refer      = $objgen->get_Onerow("refer","AND shop_id=".$id);
   		 $spa        = $objgen->get_Onerow("spa","AND shop_id=".$id);
   		 $gym        = $objgen->get_Onerow("gym","AND shop_id=".$id);
   		
		 

		$create_date  = date("j F Y",strtotime($shop['create_date']));
		
	

//echo $encod = $objgen->baseencode('');

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
		
    </head>
    <body class="skin-blue">
	
         <?php require_once "header.php"; ?>
		 
        <div class="wrapper row-offcanvas row-offcanvas-left">
        
		<?php require_once "menu.php"; ?>
		 
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        View Shop
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">View Shop</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
					<div class="row">
					<?php
					 $width = "style='width: 350px'";
					?>
					
                        <div class="col-md-12">
						<h2><?php echo $objgen->check_tag($shop['shop_name']); ?></h2>
						<div class="pull-right">
						<button type="button" class="btn btn-success" name="back" onClick="history.back();"><span class="fa fa-backward"></span>&nbsp;Back</button>
						</div>
						<br clear="all" /><br clear="all" />
					
						 <div class="col-md-6">
							  <div class="box box-primary">
							  <div class="box-header">
                                    <h3 class="box-title">Basic Informations</h3>
                                </div>
							    <div class="box-body">
                 						<table class="table table-bordered">
										     <tr>
												<td>Shop/Business Name</td>
												<td><?php echo $objgen->check_tag($shop['shop_name']); ?></td>
											</tr>
											 <tr>
												<td>Building Name</td>
												<td><?php echo $objgen->check_tag($shop['building_name']); ?></td>
											</tr>
											 <tr>
												<td>Street</td>
												<td><?php echo $objgen->check_tag($shop['street']); ?></td>
											</tr>
											 <tr>
												<td>Locality</td>
												<td>
												<?php
												$location     = $objgen->get_Onerow("location","AND id=".$shop['locality']);
												echo $objgen->check_tag($location['location']); 
												?>
												</td>
											</tr>
											 <tr>
												<td>Landmark</td>
												<td><?php echo $objgen->check_tag($shop['landmark']); ?></td>
											</tr>
											<tr>
												<td>City</td>
												<td><?php 
												
											    $city     = $objgen->get_Onerow("city","AND id=".$shop['city']);
												echo $objgen->check_tag($city['city']); 
												
												?></td>
											</tr>
											<tr>
												<td>Street</td>
												<td><?php echo $objgen->check_tag($shop['street']); ?></td>
											</tr>
											<tr>
												<td>Country</td>
												<td><?=country_name($shop['country'])?></td>
											</tr>
											<tr>
												<td>Pincode</td>
												<td><?php echo $objgen->check_tag($shop['pincode']); ?></td>
											</tr>
											<tr>
												<td>Website</td>
												<td><a href="http://<?php echo $objgen->check_tag($shop['website']); ?>" target="_blank"><?php echo $objgen->check_tag($shop['website']); ?></a></td>
											</tr>
											<tr>
												<td>Email</td>
												<td><a href="mailto:<?php echo $objgen->check_tag($shop['email']); ?>"><?php echo $objgen->check_tag($shop['email']); ?></a></td>
											</tr>
											<tr>
												<td>Phone</td>
												<td><?php echo $objgen->check_tag($shop['phone']); ?></td>
											</tr>
											<tr>
												<td>Mobile</td>
												<td><?php echo $objgen->check_tag($shop['mobile']); ?></td>
											</tr>
											<tr>
												<td>Shop/Business Categories</td>
												<td><?php echo $objgen->check_tag($shop['categories']); ?></td>
											</tr>
											<?php
											if($shop_type=='spa')
											{
											?>
											<tr>
												<td>Type of Spa</td>
												<td><?php echo $objgen->check_tag($spa['spa_type']); ?></td>
											</tr>
											<?php
											}
											?>
											<tr>
												<td>Month-Year of Establishment</td>
												<td><?php echo $objgen->check_tag($shop['esta_date']); ?></td>
											</tr>
											<tr>
												<td>How many braches you have</td>
												<td><?php echo $objgen->check_tag($shop['no_branches']); ?></td>
											</tr>
											<tr>
												<td>Have you taken any Franchise</td>
												<td><?php echo $objgen->check_tag($shop['franch']); ?></td>
											</tr>
											<tr>
												<td>Created Date</td>
												<td><?=$create_date?></td>
											</tr>
											
											</table>    
									</div>
							   </div>
							</div>
							
							<div class="col-md-6">
							  <div class="box box-success">
							  <div class="box-header">
                                    <h3 class="box-title">Facilities</h3>
                                </div>
							    <div class="box-body">
								  <?php
								if($shop_type=='spa')
								{
								?> 
                 						<table class="table table-bordered">
											<tr>
												<td>Total number of beauty treatment rooms </td>
												<td><?php echo $objgen->check_tag($spa['treatmeant_rooms']); ?></td>
											</tr>
											<tr>
												<td>Changing room</td>
												<td><?php echo $objgen->check_tag($spa['changing_room']); ?></td>
											</tr>
											<tr>
												<td>Washroom</td>
												<td><?php echo $objgen->check_tag($spa['wash_room']); ?></td>
											</tr>
											<tr>
												<td>Number of Treatment Chairs</td>
												<td><?php echo $objgen->check_tag($spa['tratment_chairs']); ?></td>
											</tr>
											<tr>
												<td>Number of Stylists</td>
												<td><?php echo $objgen->check_tag($spa['no_stylist']); ?></td>
											</tr>
											<tr>
												<td>AC</td>
												<td><?php echo $objgen->check_tag($spa['ac']); ?></td>
											</tr>
											<tr>
												<td>Type of chairs</td>
												<td><?php echo $objgen->check_tag($spa['chair_type']); ?></td>
											</tr>
											<tr>
												<td>Receptionist</td>
												<td><?php echo $objgen->check_tag($spa['receptionist']); ?></td>
											</tr>
											<tr>
												<td>Entertainment services</td>
												<td><?php echo $objgen->check_tag($spa['entertainment']); ?></td>
											</tr>
											<tr>
												<td>Credit Cards accepted</td>
												<td><?php echo $objgen->check_tag($spa['entertainment']); ?></td>
											</tr>
											<tr>
												<td>Rate Card </td>
												<td><?php echo $objgen->check_tag($spa['rate_card']); ?></td>
											</tr>
											<tr>
												<td>Rate Card  File</td>
												<td><a href="<?=URL?>photos/file/<?php echo $objgen->check_tag($spa['rate_card_file']); ?>" target="_blank" >View</a></td>
											</tr>
											<tr>
												<td>Price range INR</td>
												<td><?php echo $objgen->check_tag($spa['price_range_from']); ?> INR - <?php echo $objgen->check_tag($spa['price_range_to']); ?> INR</td>
											</tr>
											<tr>
												<td>Parking</td>
												<td><?php echo $objgen->check_tag($spa['parking']); ?></td>
											</tr>
											<tr>
												<td>WiFi</td>
												<td><?php echo $objgen->check_tag($spa['wifi']); ?></td>
											</tr>
											<tr>
												<td>How much discount you can offer on week days (Mon to Thu)</td>
												<td><?php echo $objgen->check_tag($spa['discount']); ?>%</td>
											</tr>
											
											</table>  
									<?php
									}
									?>  
									
									<?php
								if($shop_type=='gym')
								{
								?> 
                 						<table class="table table-bordered">
											<tr>
												<td>Number of Tread Mills </td>
												<td><?php echo $objgen->check_tag($gym['tread_no']); ?></td>
											</tr>
											<tr>
												<td>Changing room </td>
												<td><?php echo $objgen->check_tag($gym['chang_room']); ?></td>
											</tr>
											<tr>
												<td>Washroom</td>
												<td><?php echo $objgen->check_tag($gym['wash_room']); ?></td>
											</tr>
											<tr>
												<td>Number of Exercise Cycles</td>
												<td><?php echo $objgen->check_tag($gym['exec_cycle']); ?></td>
											</tr>
											<tr>
												<td>Filtered Water</td>
												<td><?php echo $objgen->check_tag($gym['filt_water']); ?></td>
											</tr>
											<tr>
												<td>AC</td>
												<td><?php echo $objgen->check_tag($gym['ac']); ?></td>
											</tr>
											<tr>
												<td>Area in Square Feet</td>
												<td><?php echo $objgen->check_tag($gym['area_sqr']); ?></td>
											</tr>
											<tr>
												<td>Do you Sell Supplements</td>
												<td><?php echo $objgen->check_tag($gym['supple']); ?></td>
											</tr>
											<tr>
												<td>Heavy Weight Lifting</td>
												<td><?php echo $objgen->check_tag($gym['heav_weigt']); ?></td>
											</tr>
											<tr>
												<td>Do you Sell Costumes</td>
												<td><?php echo $objgen->check_tag($gym['sell_consum']); ?></td>
											</tr>
											<tr>
												<td>Set of Dumbells</td>
												<td><?php echo $objgen->check_tag($gym['dumbell']); ?></td>
											</tr>
											<tr>
												<td>Type of Dumbells</td>
												<td><?php echo $objgen->check_tag($gym['dumbell_type']); ?></td>
											</tr>
											<tr>
												<td>Number of Combo Gym Sets</td>
												<td><?php echo $objgen->check_tag($gym['combo_gym']); ?></td>
											</tr>
											<tr>
												<td>Number of Benches</td>
												<td><?php echo $objgen->check_tag($gym['non_bench']); ?></td>
											</tr>
											<tr>
												<td>Meditation Room </td>
												<td><?php echo $objgen->check_tag($gym['medit_room']); ?></td>
											</tr>
											<tr>
												<td>Credit Cards accepted</td>
												<td><?php echo $objgen->check_tag($gym['credit_card']); ?></td>
											</tr>
											</tr>
											<tr>
												<td>Rate Card </td>
												<td><?php echo $objgen->check_tag($gym['rate_card']); ?></td>
											</tr>
											</tr>
											<tr>
												<td>Rate Card File</td>
												<td><a href="<?=URL?>photos/file/<?php echo $objgen->check_tag($gym['rate_card_file']); ?>" target="_blank" >View</a></td>
											</tr>
											</tr>
											<tr>
												<td>Price range INR</td>
												<td><?php echo $objgen->check_tag($gym['price_range_from']); ?> INR - <?php echo $objgen->check_tag($gym['price_range_to']); ?> INR</td>
											</tr>
											</tr>
											<tr>
												<td>Parking</td>
												<td><?php echo $objgen->check_tag($gym['parking']); ?></td>
											</tr>
											<tr>
												<td>WiFi</td>
												<td><?php echo $objgen->check_tag($gym['wifi']); ?></td>
											</tr>
											<tr>
												<td>Enrollment / SubscriptionType</td>
												<td><?php echo $objgen->check_tag($gym['sub_type']); ?></td>
											</tr>
											<tr>
												<td>How much discount you can offer on week days (Mon to Thu)</td>
												<td><?php echo $objgen->check_tag($gym['discount']); ?>%</td>
											</tr>
								
											</table>  
									<?php
									}
									?> 
									</div>
							   </div>
							</div>
							<br clear="all" />
							
							 <div class="col-md-6">
							  <div class="box box-success">
							  <div class="box-header">
                                    <h3 class="box-title">Merchant Details</h3>
                                </div>
							    <div class="box-body">
                 						<table class="table table-bordered">
											<tr>
												<td>Merchant Name</td>
												<td><?php echo $objgen->check_tag($merchant['name']); ?></td>
											</tr>
											<tr>
												<td>E-mail</td>
												<td><?php echo $objgen->check_tag($merchant['email']); ?></td>
											</tr>
											<tr>
												<td>Phone</td>
												<td><?php echo $objgen->check_tag($merchant['phone']); ?></td>
											</tr>
											<tr>
												<td>Address</td>
												<td><?php echo $objgen->check_tag($merchant['address']); ?></td>
											</tr>
											<tr>
												<td>City</td>
												<td><?php 
												
											    $city     = $objgen->get_Onerow("city","AND id=".$merchant['city']);
												echo $objgen->check_tag($city['city']); 
												
												?></td>
											</tr>
											<tr>
												<td>Categories</td>
												<td>
												<?php 
												$expcat = explode(",",$merchant['categories']);
											
												foreach($expcat as $key=>$val)
												{
												  $categories     = $objgen->get_Onerow("categories","AND id=".$val);
												  echo $objgen->check_tag($categories['category'])."<br />"; 
												}
												
												?>
												</td>
											</tr>
											</table>    
									</div>
							   </div>
							</div>
							
                           <?php
								if($shop_type=='spa')
								{
								?>  
							<div class="col-md-6">
							  <div class="box box-danger">
							  <div class="box-header">
                                    <h3 class="box-title">Services</h3>
                                </div>
							    <div class="box-body">
                 						<table class="table table-bordered">
											<tr>
												<td>Service</td>
												<td><?php echo $objgen->check_tag($spa['serv_offer']); ?></td>
											</tr>
											<tr>
												<td>Other Service</td>
												<td><?php echo $objgen->check_tag($spa['other_serv']); ?></td>
											</tr>
											</table>    
									</div>
							   </div>
							</div>
							<?php
							}
							?>
							
							<div class="col-md-6">
							  <div class="box box-primary">
							  <div class="box-header">
                                    <h3 class="box-title">Operating Hours</h3>
                                </div>
							    <div class="box-body">
								<?php
								if($shop_type=='spa')
								{
								?>
                 						<table class="table table-bordered">
											<tr>
												<td>Working time</td>
												<td><?php echo $objgen->check_tag($spa['working_time_from']); ?> AM - <?php echo $objgen->check_tag($spa['working_time_to']); ?> PM</td>
											</tr>
											<tr>
												<td>Weekly off</td>
												<td><?php echo $objgen->check_tag($spa['week_off']); ?></td>
											</tr>
										</table> 
							   <?php
								}
								?> 
								<?php
								if($shop_type=='gym')
								{
								?>
                 						<table class="table table-bordered">
											<tr>
												<td>Working time</td>
												<td>Morning - <?php echo $objgen->check_tag($gym['work_morn_from']); ?> AM - <?php echo $objgen->check_tag($gym['work_morn_to']); ?> PM Evening <?php echo $objgen->check_tag($gym['work_even_from']); ?> AM - <?php echo $objgen->check_tag($gym['work_even_to']); ?> PM</td>
											</tr>
											<tr>
												<td>Gym Hours</td>
												<td>Morning - <?php echo $objgen->check_tag($gym['gym_morn_from']); ?> AM - <?php echo $objgen->check_tag($gym['gym_morn_to']); ?> PM Evening <?php echo $objgen->check_tag($gym['gym_even_from']); ?> AM - <?php echo $objgen->check_tag($gym['gym_even_to']); ?> PM</td>
											</tr>
											<tr>
												<td>Yoga Hours</td>
												<td>Morning - <?php echo $objgen->check_tag($gym['yog_morn_from']); ?> AM - <?php echo $objgen->check_tag($gym['yog_morn_to']); ?> PM Evening <?php echo $objgen->check_tag($gym['yog_even_from']); ?> AM - <?php echo $objgen->check_tag($gym['yog_even_to']); ?> PM</td>
											</tr>
											<tr>
												<td>Aerobics Hours</td>
												<td>Morning - <?php echo $objgen->check_tag($gym['aer_morn_from']); ?> AM - <?php echo $objgen->check_tag($gym['aer_morn_to']); ?> PM Evening <?php echo $objgen->check_tag($gym['aer_even_from']); ?> AM - <?php echo $objgen->check_tag($gym['aer_even_to']); ?> PM</td>
											</tr>
												<tr>
												<td>Dance/Solo Hours</td>
												<td>Morning - <?php echo $objgen->check_tag($gym['dan_morn_from']); ?> AM - <?php echo $objgen->check_tag($gym['dan_morn_to']); ?> PM Evening <?php echo $objgen->check_tag($gym['dan_even_from']); ?> AM - <?php echo $objgen->check_tag($gym['dan_even_to']); ?> PM</td>
											</tr>
											<tr>
												<td>Weekly off</td>
												<td><?php echo $objgen->check_tag($gym['week_off']); ?></td>
											</tr>
											</table> 
							   <?php
								}
								?>     
									</div>
							   </div>
							</div>
							
							<br clear="all" />
							
								<?php
								if($shop_type=='gym')
								{
								?>
							<div class="col-md-6">
							  <div class="box box-danger">
							  <div class="box-header">
                                    <h3 class="box-title">Staff</h3>
                                </div>
							    <div class="box-body">
                 						<table class="table table-bordered">
											<tr>
												<td>Number of Gym Trainers</td>
												<td><?php echo $objgen->check_tag($gym['gym_tra']); ?></td>
											</tr>
											<tr>
												<td>Number of Yoga Trainers</td>
												<td><?php echo $objgen->check_tag($gym['yog_tra']); ?></td>
											</tr>
											<tr>
												<td>Number of Aerobics Trainers</td>
												<td><?php echo $objgen->check_tag($gym['aer_tra']); ?></td>
											</tr>
											<tr>
												<td>Number of Dance Trainers</td>
												<td><?php echo $objgen->check_tag($gym['dac_tra']); ?></td>
											</tr>
											<tr>
												<td>Receptioninst </td>
												<td><?php echo $objgen->check_tag($gym['recep']); ?></td>
											</tr>
											<tr>
												<td>Diatetician </td>
												<td><?php echo $objgen->check_tag($gym['diate']); ?></td>
											</tr>
											</table>    
									</div>
							   </div>
							</div>
							<?php
							}
							?>
							
							
							
					
					<div class="col-md-6">
							  <div class="box box-primary">
							  <div class="box-header">
                                    <h3 class="box-title">Other Information</h3>
                                </div>
							    <div class="box-body">
                 						<table class="table table-bordered">
											<tr>
												<td>Mobile phones used</td>
												<td><?php echo $objgen->check_tag($other_info['address']); ?></td>
											</tr>
											<tr>
												<td>Social media sites you use</td>
												<td><?php echo $objgen->check_tag($other_info['social_media']); ?></td>
											</tr>
											<tr>
												<td>Current enrolling system</td>
												<td><?php echo $objgen->check_tag($other_info['enrol_sys']); ?></td>
											</tr>
											</table>    
									</div>
							   </div>
							</div>
	
					<div class="col-md-6">
							  <div class="box box-danger">
							  <div class="box-header">
                                    <h3 class="box-title">Advertising Expenditure </h3>
                                </div>
							    <div class="box-body">
                 						<table class="table table-bordered">
											<tr>
												<td>Do you advertise yourself anywhere currently </td>
												<td><?php echo $objgen->check_tag($ad_exp['anywhere']); ?></td>
											</tr>
											<tr>
												<td>Current advertising spend in INR (Montly)</td>
												<td><?php echo $objgen->check_tag($ad_exp['spend']); ?> INR</td>
											</tr>
											<tr>
												<td>Monthly Adv Spend on</td>
												<td><?php echo $objgen->check_tag($ad_exp['spend_on']); ?></td>
											</tr>
											<tr>
												<td>Are you willing to register under regular search </td>
												<td><?php echo $objgen->check_tag($ad_exp['reg_srch']); ?></td>
											</tr>
											<tr>
												<td>Are you interested in creating web page</td>
												<td><?php echo $objgen->check_tag($ad_exp['web_page']); ?></td>
											</tr>
											<tr>
												<td>Are you interested in personalized service</td>
												<td><?php echo $objgen->check_tag($ad_exp['per_serv']); ?></td>
											</tr>
											<tr>
												<td>Do you want to advertize with us</td>
												<td><?php echo $objgen->check_tag($ad_exp['ads_us']); ?></td>
											</tr>
											<tr>
												<td>Monthly Budget</td>
												<td><?php echo $objgen->check_tag($ad_exp['budget']); ?> INR</td>
											</tr>
											</table>    
									</div>
							   </div>
							</div>
							
							<br clear="all" />	
						
                        <div class="col-md-6">
							  <div class="box box-success">
							  <div class="box-header">
                                    <h3 class="box-title">Photographs</h3>
                                </div>
							    <div class="box-body">
                 				
										<?php
										 $count1 = $objgen->get_AllRowscnt("photos","AND catgeory='exterior' AND shop_id=".$id);
										 $count2 = $objgen->get_AllRowscnt("photos","AND catgeory='interior' AND shop_id=".$id);
										 $count3 = $objgen->get_AllRowscnt("photos","AND catgeory='team' AND shop_id=".$id);
	
										?>
										<?php
										if($count1>0)
										{
										   $photos1     = $objgen->get_AllRows("photos",0,$count1,"AND catgeory='exterior' AND shop_id=".$id);
										   foreach($photos1 as $key1=>$val1)
										   {
										?>
											<b>Exterior</b><br clear="all" />
											<img src="<?=URL?>photos/small/<?php echo $val1['image']; ?>"   />
									   <?php
									      }
										}
									   ?>
											<?php
										if($count2>0)
										{
										   $photos2     = $objgen->get_Onerow("photos",0,$count2,"AND catgeory='interior' AND shop_id=".$id);
										   foreach($photos2 as $key2=>$val2)
										   {
										?>	
											<b>Interior</b><br clear="all" />
											<img src="<?=URL?>photos/small/<?php echo $val2['image']; ?>"   />
										<?php
									      }
										}
									   ?>
										
										<?php
										if($count3>0)
										{
										  $photos3     = $objgen->get_Onerow("photos",0,$count3,"AND catgeory='team' AND shop_id=".$id);
										   foreach($photos3 as $key3=>$val3)
										   {
										?>			
											<b>Team</b><br clear="all" />
											<img src="<?=URL?>photos/small/<?php echo $val3['image']; ?>"   />
										
										<?php
									      }
										}
									   ?>
											
											</table>    
									</div>
							   </div>
							</div>
                          
							 <div class="col-md-6">
							  <div class="box box-primary">
							  <div class="box-header">
                                    <h3 class="box-title">References</h3>
                                </div>
							    <div class="box-body">
                 						<table class="table table-bordered">
											<tr>
												<td>Name</td>
												<td><?php echo $objgen->check_tag($refer['name']); ?></td>
											</tr>
											<tr>
												<td>Mobile</td>
												<td><?php echo $objgen->check_tag($refer['mobile']); ?></td>
											</tr>
											</table>    
									</div>
							   </div>
							</div>
 
           
                        </div><!-- /.col -->

                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
	
      </body>
</html>
