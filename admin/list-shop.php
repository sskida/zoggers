<?php
require_once "includes/includepath.php";
require_once "chk_login.php";

$objgen		=	new general();

$pagehead = "Shop";

$add_url = "add-shop";

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if(isset($_GET['del']))
{
   $id= $_GET['del'];
   $msg     = $objgen->del_Row("ad_exp","shop_id=".$id);
   $msg     = $objgen->del_Row("other_info","shop_id=".$id);
   $msg     = $objgen->del_Row("refer","shop_id=".$id);
   $msg     = $objgen->del_Row("spa","shop_id=".$id);
   $msg     = $objgen->del_Row("gym","shop_id=".$id);
   $msg     = $objgen->del_Row("photos","shop_id=".$id);
   $msg     = $objgen->del_Row("shop_categories","shop_id=".$id);
   $msg     = $objgen->del_Row("shop_facilities","shop_id=".$id);
   $msg     = $objgen->del_Row("shop_products","shop_id=".$id);

   $msg     = $objgen->del_Row("shop","id=".$id);
    if($msg=="")
   {
	header("location:".URLAD."list-shop/?msg=3&page=".$page);
   }
}

if(isset($_GET['st']))
{
	 $id = $_GET['id'];
	 
	 if($_GET['st']=='active')
	  $status = "inactive";
	 if($_GET['st']=='inactive')
	  $status = "active";

	 $msg	=	$objgen->upd_Row("shop","status='$status'","id=".$id);
	 header('location:'.URLAD.'list-shop/?msg=4&page='.$page);
}

if(isset($_GET['recomm_now']))
{
	 $id = $_GET['id'];
	 
	 if($_GET['recomm_now']=='Yes')
	  $recommended_now = "No";
	 if($_GET['recomm_now']=='No')
	  $recommended_now = "Yes";

	 $msg	=	$objgen->upd_Row("shop","recommended_now='$recommended_now'","id=".$id);
	 header('location:'.URLAD.'list-shop/?msg=9&page='.$page);
}
if(isset($_GET['sponsored']))
{
	 $id = $_GET['id'];
	 
	 if($_GET['sponsored']=='Yes') {
	    $sponsored = 'No';
	  }
	 if($_GET['sponsored']=='No') {
	  	 $sponsored = 'Yes';
	  }
	 $msg	=	$objgen->upd_Row("shop","sponsored='$sponsored'","id=".$id);
	 header('location:'.URLAD.'list-shop/?msg=10&page='.$page);
}
if($_GET['msg']==2)
{

  $msg2 = "Shop Updated Successfully.";
}

if($_GET['msg']==3)
{

  $msg2 = "Shop Deleted Successfully.";
}

if($_GET['msg']==4)
{

  $msg2 = "Status Changed Successfully.";
}
if($_GET['msg']==9)
{

  $msg2 = "Recommended Changed Successfully.";
}
if($_GET['msg']==10)
{

  $msg2 = "Sponsored Changed Successfully.";
}

$where = "";
if(isset($_REQUEST['un']) &&  trim($_REQUEST['un'])!="")
{
  $un = trim($_REQUEST['un']);
  $where .= " and shop_name like '%".$un."%'";
}

if(isset($_REQUEST['ut']) &&  trim($_REQUEST['ut'])!="")
{
  $ut = trim($_REQUEST['ut']);
  $where .= " and merchant_id = '".$ut."'";
}


$row_count = $objgen->get_AllRowscnt("shop",$where);
if($row_count>0)
{
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array("un" => $un, "ut" => $ut), 1, WEBLINKAD."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("shop",$pagesize*($page-1),$pagesize,"id desc",$where);
}

if(isset($_POST['Reset']))
{
	 unset($_REQUEST);
	  header("location:".URLAD."list-shop");
}

$where = "";
$mer_count = $objgen->get_AllRowscnt("merchants",$where);
if($mer_count>0)
{
  $mer_arr = $objgen->get_AllRows("merchants",0,$mer_count,"name asc",$where);
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">

         <?php require_once "header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">

		<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        List <?=$pagehead?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">List <?=$pagehead?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">


				<div class="pull-right">
				<button type="buttom" class="btn btn-success" name="Reset" onClick="window.location='<?=URLAD?><?=$add_url?>'"><span class="fa fa-plus"></span>&nbsp;Add New</button>
				</div>
				<br clear="all"><br clear="all">



					<div class="row">
                        <div class="col-xs-12">
						   <div class="box box-primary">

                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                       <!-- text input -->
									   <div class="col-xs-6">
											<div class="form-group" >
												<label>Shop/Business Name</label>
												<input type="text" class="form-control" value="<?=$un?>" name="un"  />
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group">
												<label>Merchant</label>

										   <select class="form-control" name="ut">
												<option value="">Select</option>
												<?php
		     									if($mer_count>0)
												{
													foreach($mer_arr as $key=>$val)
													{
												?>
                                                <option value="<?=$val['id']?>" <?php if($val['id']==$ut) { ?> selected="selected" <?php } ?> ><?=$objgen->check_tag($val['name']);?></option>
												<?php
													}
												}

												?>
                                             </select>

											</div>
										</div>
                                        <div class="box-footer">
											<button type="submit" class="btn btn-primary" name="Reset"><span class="fa fa-eraser"></span>&nbsp;Reset</button>
											<button type="submit" class="btn btn-primary" name="Search"><span class="fa fa-search"></span>&nbsp;Search</button>
										</div>
									</form>
								</div>
							</div>

                           <div class="box">
                                <div class="box-body table-responsive">
								 <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>

                                    <?php
                                    }
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
											    <th>Shop/Business Name</th>
											    <th>Shop Type</th>
													<th>Merchant</th>
													<th>Date</th>
													<th>View</th>
													<th>Approve</th>
													<th>Recommended</th>
													<th>Sponsored</th>
													<th>Edit</th>
													<th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										  	<?php
										if($row_count>0)
										{
										  foreach($res_arr as $key=>$val)
											  {

                                                 $result     	 = $objgen->get_Onerow("merchants","AND id=".$val['merchant_id']);
                                                 $cat_shop     	 = $objgen->get_Onerow("categories","AND id=".$val['shop_type']);

											?>
                                            <tr>
                                                <td><?php echo $objgen->check_tag($val['shop_name']); ?></td>
																								
												<td><?php echo $objgen->check_tag($cat_shop['category']); ?></td>
                                                <td><?php echo $objgen->check_tag($result['name']); ?></td>
												<td><?php echo date("j F Y",strtotime($val['create_date'])); ?></td>
												<td><a href="<?=URLAD?>view-shop/?id=<?=$val['id']?>&page=<?=$page?>" role="button" ><span class="fa fa-search-plus"></span></a></td>
                                                <td>
												<?php
												if($val['status']=='active')
												{
												?>
												<a href="<?=URLAD?>list-shop/?id=<?=$val['id']?>&page=<?=$page?>&st=<?php echo $val['status']; ?>" role="button" class="btn btn-success" ><span class="fa fa-unlock"></span> Approved</a>
												<?php
												}
												else
												{
												?>
												<a href="<?=URLAD?>list-shop/?id=<?=$val['id']?>&page=<?=$page?>&st=<?php echo $val['status']; ?>" role="button"  class="btn btn-danger" ><span class="fa fa-lock"></span> Approve</a>
												<?php
												}
												?>
												</td>
												<td>
												<?php
												if($val['recommended_now']=='Yes')
												{
												?>
												<a href="<?=URLAD?>list-shop/?id=<?=$val['id']?>&page=<?=$page?>&recomm_now=<?php echo $val['recommended_now']; ?>" role="button" class="btn btn-success" ><span class="fa fa-unlock"></span> YES</a>
												<?php
												}
												else
												{
												?>
												<a href="<?=URLAD?>list-shop/?id=<?=$val['id']?>&page=<?=$page?>&recomm_now=<?php echo $val['recommended_now']; ?>" role="button"  class="btn btn-danger" ><span class="fa fa-lock"></span> NO</a>
												<?php
												}
												?>
												</td>
												<td>
												<?php
												if($val['sponsored']=='Yes')
												{
												?>
												<a href="<?=URLAD?>list-shop/?id=<?=$val['id']?>&page=<?=$page?>&sponsored=<?php echo $val['sponsored']; ?>" role="button" class="btn btn-success" ><span class="fa fa-unlock"></span> YES</a>
												<?php
												}
												else
												{
												?>
												<a href="<?=URLAD?>list-shop/?id=<?=$val['id']?>&page=<?=$page?>&sponsored=<?php echo $val['sponsored']; ?>" role="button"  class="btn btn-danger" ><span class="fa fa-lock"></span> NO</a>
												<?php
												}
												?>
												</td>
												<td><a href="<?=URLAD?>add-shop/?edit=<?=$val['id']?>&page=<?=$page?>" role="button" ><span class="fa fa-edit"></span></a></td>
												<td><a href="<?=URLAD?>list-shop/?del=<?=$val['id']?>&page=<?=$page?>" role="button" onClick="return confirm('Do you want to delete this Shop?')"><span class="fa fa-trash-o"></span></a></td>
                                            </tr>

                                         <?php
												}
											}
										 ?>
                                        </tbody>

                                    </table>
									<?php
									if($row_count > $pagesize)
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">

											<div class="dataTables_paginate paging_bootstrap">

												<?php echo $pages; ?>


											</div>

									   </div>
									</div>
									<?php
										 }
									?>

                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
      </body>
</html>
