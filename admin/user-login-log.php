<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
require_once "../country/country.php";
$objgen		=	new general();

$con_arr = country();

$pagehead = "User Login Logs";

$add_url = "";

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";




$where = "";

if(isset($_REQUEST['x']) &&  trim($_REQUEST['x'])!="")
{
  $x = trim($_REQUEST['x']);
  $where .= " and login_with  = '".$x."'";
}
if(isset($_REQUEST['sdate']) &&  trim($_REQUEST['sdate'])!="" && isset($_REQUEST['edate']) &&  trim($_REQUEST['edate'])!="")
{
  $sdate = trim($_REQUEST['sdate']);
  $edate = trim($_REQUEST['edate']);
  $where .= " and date between '".$sdate."' and '".$edate."'";
}

$where;
$row_count = $objgen->get_AllRowscnt("user_login_logs ",$where);

if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array("x"=>$x), 1, WEBLINKAD."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("user_login_logs",$pagesize*($page-1),$pagesize,"id desc",$where);
}

if(isset($_POST['Reset']))
{
	 unset($_REQUEST);
	 header("location:".URLAD."user-login-log");
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo TITLE; ?></title>
		<?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">
	<?php require_once "header.php"; ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
        <?php require_once "menu.php"; ?>
		 	<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        List <?=$pagehead?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">List <?=$pagehead?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				
			
				<br clear="all"><br clear="all">
				<div class="row">
                     <div class="col-xs-12">
						<div class="box box-primary">
              <div class="box-body">
                  <form role="form" action="" method="post" enctype="ultipart/form-data" >
                                       <!-- text input -->
							        <div class="col-xs-6">
										 <div class="form-group">
                      <label>User Login With </label>
                      <select class="form-control" name="x">
												<option value="">Select</option>
												<option value="Normal Login" <?php if("Normal Login"==$x) { ?> selected="selected" <?php } ?> >Normal Login</option>
                        <option value="Facebook Login" <?php if("Facebook Login"==$x) { ?> selected="selected" <?php } ?> >Facebook Login</option>
                        <option value="Google Login" <?php if("Google Login"==$x) { ?> selected="selected" <?php } ?> >Google Login</option>
											</select>
                    </div>
										</div>
										<div class="col-xs-6">
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>From Date: </label>
                            <input type="text" id="logindate" name="sdate" placeholder="Start Date" class="form-control" value="<?php if(isset($sdate)) echo $sdate; ?>"> 
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>End Date: </label>
                            <input type="text" id="logoutdate" name="edate" placeholder="End Date" class="form-control" value="<?php if(isset($edate)) echo $edate; ?>">
                        </div>
                      </div>
                    </div>
										
					                     <br clear="all">
                                        <div class="box-footer">
											<button type="submit" class="btn btn-primary" name="Reset"><span class="fa fa-eraser"></span>&nbsp;Reset</button>
											<button type="submit" class="btn btn-primary" name="Search"><span class="fa fa-search"></span>&nbsp;Search</button>
										</div>
									</form>
								</div>
							</div>
							
                           <div class="box">
                                <div class="box-body table-responsive">
								 <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                
                                                <th>User Login With</th>
                                                <th>Login Date</th>
                                                <th>User Ip Address</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
										  	<?php

										if($row_count>0)
										{
												
										  foreach($res_arr as $key=>$val)
											  {
											  

											?>
                                            <tr>
                                                <td><?php echo $val['login_with'];?></td>
                                                <td><?php echo $val['date'];?></td>
                                                <td><?php echo $val['ip_address'];?></td>
                                            </tr>
                                          
                                         <?php
												}
											}
										 ?>
                                        </tbody>
                                       
                                    </table>
									<?php
									if($row_count > $pagesize) 
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">
										
											<div class="dataTables_paginate paging_bootstrap">
											    		
												<?php echo $pages; ?>

												
											</div>
											
									   </div>
									</div>
									<?php
										 }
									?>
									
                                </div><!-- /.box-body -->
                            </div><!-- /.box --> 
                        </div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
       <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
     <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      </body>
</html>
<script>
    $(function() {
    $( "#logindate" ).datepicker({
       dateFormat: 'yy-mm-dd',       
       onSelect : function(selected_date){
        var selectedDate = new Date(selected_date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);
        
         $("#logoutdate").datepicker( "option", "minDate", endDate );
      }
    });
    
    $( "#logoutdate" ).datepicker({
      dateFormat: 'yy-mm-dd'
    });
    
  });
</script> 