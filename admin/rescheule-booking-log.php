<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
require_once "../country/country.php";
include ("rescheduleexcelexport.php");

  if(isset($_POST["submit"]))
  {
    ExportExcel("appointments a INNER JOIN shop s ON s.id=a.shop_id INNER JOIN categories c ON c.id=s.categories INNER JOIN users u ON u.id=a.user_id INNER JOIN subcategories sb ON sb.id=a.subcat_id");
  }
$objgen		=	new general();

$con_arr = country();

$pagehead = "Rescheuled Booking Logs";

$add_url = "";

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";


$where = "and a.rescheduled='1' ";

if(isset($_REQUEST['x']) &&  trim($_REQUEST['x'])!="")
{
  $x = trim($_REQUEST['x']);
  $where .= " and c.id = '".$x."'";
}
if(isset($_REQUEST['y']) &&  trim($_REQUEST['y'])!="")
{
  $y = trim($_REQUEST['y']);
  $where .= " and user_id = '".$y."'";
}
if(isset($_REQUEST['z']) &&  trim($_REQUEST['z'])!="")
{
  $z = trim($_REQUEST['z']);
  $where .= " and shop_id = '".$z."'";
}
if(isset($_REQUEST['sdate']) &&  trim($_REQUEST['sdate'])!="" && isset($_REQUEST['edate']) &&  trim($_REQUEST['edate'])!="")
{
  $sdate = trim($_REQUEST['sdate']);
  $edate = trim($_REQUEST['edate']);
  $where .= " and a.app_date between '".$sdate."' and '".$edate."'";
}
$col ="a.*,s.shop_name,s.id as shop_id,u.name, c.category,c.id as cat_id, sb.subcategory";


$row_count = $objgen->get_AllRowscnt("appointments a INNER JOIN shop s ON s.id=a.shop_id INNER JOIN categories c ON c.id=s.categories INNER JOIN users u ON u.id=a.user_id INNER JOIN subcategories sb ON sb.id=a.subcat_id ",$where,"",$col);
$row_count;
if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array("x"=>$x), 1, WEBLINKAD."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("appointments a INNER JOIN shop s ON s.id=a.shop_id INNER JOIN categories c ON c.id=s.categories INNER JOIN users u ON u.id=a.user_id INNER JOIN subcategories sb ON sb.id=a.subcat_id",$pagesize*($page-1),$pagesize,"a.id desc",$where,"",$col);
}

if(isset($_POST['Reset']))
{
	 unset($_REQUEST);
	 header("location:".URLAD."rescheule-booking-log");
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title><?php echo TITLE; ?></title>
		<?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">
	<?php require_once "header.php"; ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
        <?php require_once "menu.php"; ?>
		 	<aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        List <?=$pagehead?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">List <?=$pagehead?></li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content invoice">
				<div class="pull-right">
          <form name="export" method="post">
          <button type="buttom" class="btn btn-success" name="submit" ><span class="fa fa-file-excel-o"></span>&nbsp;Export To Excel</button>
          </form>
        </div>
			
				<br clear="all"><br clear="all">

				<div class="row">
          <div class="col-xs-12">
						<div class="box box-primary">
               <div class="box-body">
                  <form role="form" action="" method="post" enctype="ultipart/form-data" >
                   <!-- text input -->
							        <div class="col-xs-6">
										 <div class="form-group">
                       <label>Entity Name</label>
                        <select class="form-control" name="x">
  												<option value="">Select</option>
  												<?php
  												$cat_arr =$objgen->get_AllRows_qry("select * from categories");
  												foreach($cat_arr as $key=>$val)
  													{
  												?>
                            <option value="<?php echo $val['id']?>" <?php if($val['id']==$x) { ?> selected="selected" <?php } ?> ><?php echo $val['category']?>
                            </option>
  												<?php
  													}
  												?>
                        </select>
                      </div>
										</div>
                    <div class="col-xs-6">
                     <div class="form-group">
                        <label>User Name</label>
                        <select class="form-control" name="y">
                        <option value="">Select</option>
                        <?php
                        $user_arr =$objgen->get_AllRows_qry("select * from users");
                        foreach($user_arr as $key=>$val)
                          {
                        ?>
                            <option value="<?php echo $val['id']?>" <?php if($val['id']==$y) { ?> selected="selected" <?php } ?> ><?php echo $val['name']?></option>
                        <?php
                          }
                        ?>
                         </select>
                    </div>
                    </div>
                    <div class="col-xs-6">
                     <div class="form-group">
                        <label>Shop Name</label>
                        <select class="form-control" name="z">
                        <option value="">Select</option>
                        <?php
                        $ss_arr =$objgen->get_AllRows_qry("select * from shop");
                        foreach($ss_arr as $key=>$val)
                          {
                        ?>
                              <option value="<?php echo $val['id']?>" <?php if($val['id']==$z) { ?> selected="selected" <?php } ?> ><?php echo $val['shop_name']?>
                              </option>
                        <?php
                          }
                        ?>
                       </select>
                     </div>
                    </div>
										 <div class="col-xs-6">
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>From Date: </label>
                            <input type="text" id="gdate" name="sdate" placeholder="Start Date" class="form-control" value="<?php if(isset($sdate)) echo $sdate; ?>"> 
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>End Date: </label>
                            <input type="text" id="fdate" name="edate" placeholder="End Date" class="form-control" value="<?php if(isset($edate)) echo $edate; ?>">
                        </div>
                      </div>
                    </div>
										
                   
					                     <br clear="all">
                                        <div class="box-footer">
											<button type="submit" class="btn btn-primary" name="Reset"><span class="fa fa-eraser"></span>&nbsp;Reset</button>
											<button type="submit" class="btn btn-primary" name="Search"><span class="fa fa-search"></span>&nbsp;Search</button>
										</div>
									</form>
								</div>
							</div>
							
               <div class="box">
                                <div class="box-body table-responsive">
								 <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                             
                                                <th>Entity Name</th>
                                                <th>Shop Name</th>
                                                <th>User Name</th>
                                                <th>Booking Date</th>
                                                <th>Service Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										  	<?php

										
											      /*$sql ="SELECT count(a.user_id) as user_count ,c.category from appointments a INNER JOIN shop s ON s.id=a.shop_id INNER JOIN categories c ON c.id=s.categories WHERE a.rescheduled='1' group by c.category ";
                            $app = $objgen->get_AllRows_qry($sql);*/
                           if($row_count>0)
                            {
                            foreach ($res_arr as $key => $value) {
                                         
                            
											?>
                                            <tr>
                                                
                                                <td><?php echo $value['category'];?></td>
                                                <td><?php echo $value['shop_name'];?></td>
                                                <td><?php echo $value['name'];?></td>
                                                <td><?php echo date("d-m-Y",strtotime($value['app_date']));?></td>
                                                <td><?php echo $value['subcategory'];?></td>
                                                
                                               
                                               
                                            </tr>
                                          
                                         <?php
												}
											
										 ?>
                                        </tbody>
                                       
                                    </table>
									<?php
									if($row_count > $pagesize) 
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">
										
											<div class="dataTables_paginate paging_bootstrap">
											    		
												<?php echo $pages; ?>

												
											</div>
											
									   </div>
									</div>
									<?php
										} }
									?>
									
                                </div><!-- /.box-body -->
                            </div><!-- /.box --> 
                        </div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
        <?php require_once "footer.php"; ?>
     <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
     <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      </body>
</html>
<script>
    $(function() {
    $( "#gdate" ).datepicker({
       dateFormat: 'yy-mm-dd',       
       onSelect : function(selected_date){
        var selectedDate = new Date(selected_date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);
        
         $("#fdate").datepicker( "option", "minDate", endDate );
      }
    });
    
    $( "#fdate" ).datepicker({
      dateFormat: 'yy-mm-dd'
    });
    
  });
</script> 