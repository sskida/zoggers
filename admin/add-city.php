<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
require_once "../country/country.php";
$objval	=   new validate();
$objgen		=	new general();

$con_arr = country();

$pagehead = "City";

$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if($_GET['msg']==1)
{
  $msg2 = "City Created Successfully.";
}


if(isset($_POST['Create']))
{
   
   	$city   	    = $objgen->check_input($_POST['city']);
	$country   	    = $objgen->check_input($_POST['country']);
	
	$rules		=	array();
	$rules[] 	= "required,country,Select the Country";
	$rules[] 	= "required,city,Enter the City";
	$rules[] 	= "letters_only,city,City field must only contains letters (a-Z)";
	$errors  	= $objval->validateFields($_POST, $rules);
   
    $city_exit = $objgen->chk_Ext("city","city='$city' and country='$country'");
	if($city_exit>0)
	{
		$errors[] = "This city is already exists.";
	}

   if(empty($errors))
	{
		 
		 $msg = $objgen->ins_Row('city','city,country',"'".$city."','".$country."'");
		 if($msg=="")
		 {
			   header("location:".URLAD."add-city/?msg=1");
		 }
	}
}

if(isset($_GET['edit']))
{

       $id = $_GET['edit'];
	   $result     		= $objgen->get_Onerow("city","AND id=".$id);
	   $city     = $objgen->check_tag($result['city']);
	   $country   = $objgen->check_tag($result['country']);

}
if(isset($_POST['Update']))
{    
    $city   	    = $objgen->check_input($_POST['city']);
	$country   	    = $objgen->check_input($_POST['country']);

	$city_exit = $objgen->chk_Ext("city","city='$city' and country='$country' and id<>".$id);
	if($city_exit>0)
	{
		$errors[] = "This city is already exists.";
	}
	
	$errors = array();
   $rules		=	array();
   $rules[] 	= "required,country,Select the Country";
   $rules[] 	= "required,city,Enter the City";
   $rules[] 	= "letters_only,city,City field must only contains letters (a-Z)";
   $errors  	= $objval->validateFields($_POST, $rules);

   if(empty($errors))
	{
		 			 
	  $msg = $objgen->upd_Row('city',"city='".$city."',country='".$country."'","id=".$id);
	  if($msg=="")
	  {
		  header("location:".URLAD."list-city/?msg=2&page=".$page);
	  }
	  
	}
}
if(isset($_POST['Cancel']))
{
	  header("location:".URLAD."list-city");
}





?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">

         <?php require_once "header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">  

		<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
				<section class="content-header">
                    <h1><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></li>
                    </ol>
                </section>
      <!-- Main content -->
                <section class="content">
					<div class="row" >
                        <div class="col-md-6">
                          <div class="box box-primary">
                            <div class="box-header">
                                    <h3 class="box-title">Enter <?=$pagehead?> Informations</h3>
                            </div><!-- /.box-header -->
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                     <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                      <div class="form-group">
                                            <label>Country</label>
                                            <select class="form-control" name="country">
												<option value="">Select</option>
												<?php
												foreach($con_arr as $key=>$val)
													{
												?>
                                                <option value="<?=$key?>" <?php if($key==$country) { ?> selected="selected" <?php } ?> ><?=$val?></option>
												<?php
													}
												?>
                                             </select>
                                        </div>
                                        <!-- text input -->
                                        <div class="form-group" >
                                            <label>City</label>
                                            <input type="text" class="form-control" value="<?=$city?>" name="city"  required />
                                        </div>
                                        <div class="box-footer">
                                           <?php
                                            if(isset($_GET['edit']))
                                            {
                                            ?>
                                            <button class="btn btn-primary" type="submit" name="Update"><span class="fa fa-thumbs-o-up"></span>&nbsp;Update</button>
                                            <button class="btn btn-primary" type="submit" name="Cancel"><span class="fa fa-undo"></span>&nbsp;Cancel</button>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <button type="submit" class="btn btn-primary" name="Create"><span class="fa fa-save"></span>&nbsp;Save</button>
                                         
                                         <?php
                                            }
                                            ?>
                                    </div>
									</form>
								</div>
							</div>
						</div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>

    </body>
</html>
