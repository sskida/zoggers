<?php
require_once "includes/includepath.php";
require_once "chk_login.php";

$objval	=   new validate();
$objgen		=	new general();


$pagehead = "Category";

$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if($_GET['msg']==1)
{
  $msg2 = "Category Created Successfully.";
}

if(isset($_POST['Create']))
{
   
    $category  	= $objgen->check_input($_POST['category']);
    $icon       = $objgen->check_input($_POST['icon']);
	
	$rules		=	array();
	$rules[] 	= "required,category,Enter the Category";
	$errors  	= $objval->validateFields($_POST, $rules);
   
    $cat_exit = $objgen->chk_Ext("categories","category='$category'");
	if($cat_exit>0)
	{
		$errors[] = "This category is already exists.";
	}

   if(empty($errors))
	{
		 
		 $msg = $objgen->ins_Row('categories','category,icon',"'".$category."','".$icon."'");
		 if($msg=="")
		 {
			   header("location:".URLAD."add-category/?msg=1");
		 }
	}
}

if(isset($_GET['edit']))
{

       $id = $_GET['edit'];
	   $result     	 = $objgen->get_Onerow("categories","AND id=".$id);
	   $category     = $objgen->check_tag($result['category']);
       $icon     = $objgen->check_tag($result['icon']);

}
if(isset($_POST['Update']))
{    
   	$category  	    = $objgen->check_input($_POST['category']);
    $icon       = $objgen->check_input($_POST['icon']);
	
	$city_exit = $objgen->chk_Ext("categories","category='$category' and id<>".$id);
	if($cat_exit>0)
	{
		$cat_exit[] = "This category is already exists.";
	}
	
   $errors = array();
   $rules		=	array();
   $rules[] 	= "required,category,Enter the Category";
   $errors  	= $objval->validateFields($_POST, $rules);

   if(empty($errors))
	{
		 			 
	  $msg = $objgen->upd_Row('categories',"category='".$category."',icon='".$icon."'","id=".$id);
	  if($msg=="")
	  {
		  header("location:".URLAD."list-category/?msg=2&page=".$page);
	  }
	  
	}
}
if(isset($_POST['Cancel']))
{
	  header("location:".URLAD."list-category");
}





?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">

         <?php require_once "header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">  

		<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
				<section class="content-header">
                    <h1><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></li>
                    </ol>
                </section>
      <!-- Main content -->
                <section class="content">
					<div class="row" >
                        <div class="col-md-6">
                          <div class="box box-primary">
                            <div class="box-header">
                                    <h3 class="box-title">Enter <?=$pagehead?> Informations</h3>
                            </div><!-- /.box-header -->
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                     <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                        <div class="form-group" >
                                            <label>Category</label>
                                            <input type="text" class="form-control" value="<?=$category?>" name="category"  required />
                                        </div>
                                        <div class="form-group" >
                                            <label>Icon</label>
                                            <input type="text" class="form-control" value="<?=$icon?>" name="icon"  />
                                        </div>
										
                                        <div class="box-footer">
                                           <?php
                                            if(isset($_GET['edit']))
                                            {
                                            ?>
                                            <button class="btn btn-primary" type="submit" name="Update"><span class="fa fa-thumbs-o-up"></span>&nbsp;Update</button>
                                            <button class="btn btn-primary" type="submit" name="Cancel"><span class="fa fa-undo"></span>&nbsp;Cancel</button>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <button type="submit" class="btn btn-primary" name="Create"><span class="fa fa-save"></span>&nbsp;Save</button>
                                         
                                         <?php
                                            }
                                            ?>
                                    </div>
									</form>
								</div>
							</div>
						</div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>

    </body>
</html>
