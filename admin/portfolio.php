<?php
require_once "includes/includepath.php";
require_once "chk_login.php";

$objgen   = new general();



$objPN    =   new page(1);
/** Page Settings  **/
$pagesize = 20;
$page   = isset($_REQUEST['page'])  ? $_REQUEST['page'] : "1";

if(isset($_GET['del']))
{
   $id= $_GET['del'];

   $msg     = $objgen->del_Row("appointments","id=".$id);
    if($msg=="")
   {
  header("location:".URLAD."list-app/?msg=3&page=".$page);
   }
}



if($_GET['msg']==2)
{

  $msg2 = "Appointment Updated Successfully.";
}

if($_GET['msg']==3)
{

  $msg2 = "Appointment Deleted Successfully.";
}

if($_GET['msg']==4)
{

  $msg2 = "Status Changed Successfully.";
}

$where = "";

if(isset($_REQUEST['sh']) &&  trim($_REQUEST['sh'])!="")
{
  $sh = trim($_REQUEST['sh']);
  $where .= " and shop_id = '".$sh."'";
}


$row_count = $objgen->get_AllRowscnt("appointments",$where);

if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array("sh" => $sh), 1, WEBLINKAD."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("appointments",$pagesize*($page-1),$pagesize,"id desc",$where);
}

if(isset($_POST['Reset']))
{
   unset($_REQUEST);
    header("location:".URLAD."list-app");
}

$where = "";
$shop_count = $objgen->get_AllRowscnt("shop",$where);
if($shop_count>0)
{
  $shop_arr = $objgen->get_AllRows("shop",0,$shop_count,"shop_name asc",$where);
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
        <link href="<?=URLAD?>css/uploadfilemulti.css" rel="stylesheet">
    </head>
    <body class="skin-blue">
  
         <?php require_once "header.php"; ?>
     
        <div class="wrapper row-offcanvas row-offcanvas-left">
        
    <?php require_once "menu.php"; ?>
     
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Portfolio
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Portfolio</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
          <div class="row">
                        <div class="col-xs-12">
                                        
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                       <!-- text input -->
                    <div class="col-xs-6">
                      <div class="form-group">
                        <label>Shop</label>
                          <select class="form-control" name="sh" id="select_shop">
                        <option value="">Select</option>
                        <?php
                        if($shop_count>0)
                        {
                          foreach($shop_arr as $key=>$val)
                          {
                        ?>
                            <option value="<?=$val['id']?>" <?php if($val['id']==$sh) { ?> selected="selected" <?php } ?> ><?=$objgen->check_tag($val['shop_name']);?></option>
                        <?php
                          }
                        }
                        ?>
                       </select>
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <div class="form-group" id="hide_img" style="display:none">
                        
                        <div id="mulitplefileuploader">Upload</div>
                        <div id="status"></div>
                       
                      </div>
                    </div>
                    <div class="col-xs-12">
                      <div class="form-group">
                        <div id="get">
                         
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              
                          
                        </div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
     <script src="<?=URLAD?>js/jquery.fileuploadmulti.min.js"></script> 
      </body>
</html>
<script>
$(document).ready(function(){
  $( "#select_shop" ).on('change', function() {
      //$('#mulitplefileuploader').trigger("reset");
      $('#hide_img').show();
      var id = this.value;
      //$('#mulitplefileuploader').append(id);
      $('#mulitplefileuploader').attr('id','mulitplefileuploader'+id);
       var xxx = {
          type: "POST",
          url: "<?=URLAD?>upload.php",
          formData:{id:id},
          allowedTypes:"jpg,png",
          fileName: "myfile",
          multiple: true,
          onSuccess:function(files,data,xhr)
          {
            $('#hide_img').show();
              $("#status").html("<font color='green'>Upload is success</font>");
              
          },

          onError: function(files,status,errMsg)
          {       
              $("#status").html("<font color='red'>Upload is Failed</font>");
          }
      }
  $("#mulitplefileuploader"+id).uploadFile(xxx) ;
    $.ajax({
        type: "POST",
        url: "get-upload.php",
        data: {id : id },
        success: function (data) {
          $("#get").html(data);
        }
    });
  });
});

  function delete_img (val,shop) {
  $.ajax({
      type: "POST",
      url: "delete-img.php",
      data: {id : val,shop:shop },
      success: function (data) {
        $("#get").html(data);
      }
   });
}
</script>
