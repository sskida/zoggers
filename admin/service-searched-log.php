<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
require_once "../country/country.php";
$objgen   = new general();

$con_arr = country();

$pagehead = "Service Searched Logs";

$add_url = "";

$objPN    =   new page(1);
/** Page Settings  **/
$pagesize = 20;
$page   = isset($_REQUEST['page'])  ? $_REQUEST['page'] : "1";




$where = "";

if(isset($_REQUEST['x']) &&  trim($_REQUEST['x'])!="")
{
  $x = trim($_REQUEST['x']);
  $where .= " and service_id = '".$x."'";
}
if(isset($_REQUEST['sub_cat']) &&  trim($_REQUEST['sub_cat'])!="")
{
  $sub_cat = trim($_REQUEST['sub_cat']);
  $where .= " and subcategory_id = '".$sub_cat."'";
}
if(isset($_REQUEST['y']) &&  trim($_REQUEST['y'])!="")
{
  $y = trim($_REQUEST['y']);
  $where .= " and user_id = '".$y."'";
}
if(isset($_REQUEST['z']) &&  trim($_REQUEST['z'])!="")
{
  $z = trim($_REQUEST['z']);
  $where .= " and city_id = '".$z."'";
}
if(isset($_REQUEST['area']) &&  trim($_REQUEST['area'])!="")
{
  $area = trim($_REQUEST['area']);
  $where .= " and location_id = '".$area."'";
}
if(isset($_REQUEST['sdate']) &&  trim($_REQUEST['sdate'])!="" && isset($_REQUEST['edate']) &&  trim($_REQUEST['edate'])!="")
{
  $sdate = trim($_REQUEST['sdate']);
  $edate = trim($_REQUEST['edate']);
  $where .= " and date between '".$sdate."' and '".$edate."'";
}
echo $where;
$row_count = $objgen->get_AllRowscnt("visitor_logs ",$where);

if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array("area"=>$area,"z"=>$z,"x"=>$x, "y" => $y), 1, WEBLINKAD."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("visitor_logs",$pagesize*($page-1),$pagesize,"id desc",$where );
}

if(isset($_POST['Reset']))
{
   unset($_REQUEST);
   header("location:".URLAD."service-visitor-logs");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo TITLE; ?></title>
    <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">
  <?php require_once "header.php"; ?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <?php require_once "menu.php"; ?>
      <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        List <?=$pagehead?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">List <?=$pagehead?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
        
      
        <br clear="all"><br clear="all">
        <div class="row">
                     <div class="col-xs-12">
            <div class="box box-primary">
                          <div class="box-body">
                                <form role="form" action="" method="post" enctype="ultipart/form-data" >
                                       <!-- text input -->
                      <div class="col-xs-6">
                     <div class="form-group">
                        <label>Entity Name</label>
                        <select class="form-control" name="x">
                        <option value="">Select</option>
                        <?php
                        $cat_arr =$objgen->get_AllRows_qry("select * from categories");
                        foreach($cat_arr as $key=>$val)
                          {
                        ?>
                              <option value="<?php echo $val['id']?>" <?php if($val['id']==$x) { ?> selected="selected" <?php } ?> ><?php echo $val['category']?>
                              </option>
                        <?php
                          }
                        ?>
                       </select>
                     </div>
                    </div>
                    <div class="col-xs-6">
                     <div class="form-group">
                        <label>Service Name</label>
                        <select class="form-control" name="sub_cat">
                        <option value="">Select</option>
                        <?php
                        $sub_cat_arr =$objgen->get_AllRows_qry("select * from subcategories");
                        foreach($sub_cat_arr as $key=>$val)
                          {
                        ?>
                              <option value="<?php echo $val['id']?>" <?php if($val['id']==$sub_cat) { ?> selected="selected" <?php } ?> ><?php echo $val['subcategory']?>
                              </option>
                        <?php
                          }
                        ?>
                       </select>
                     </div>
                    </div>
                    <div class="col-xs-6">
                     <div class="form-group">
                                            <label>User Name</label>
                                            <select class="form-control" name="y">
                        <option value="">Select</option>
                        <?php
                        $user_arr =$objgen->get_AllRows_qry("select * from users");
                        foreach($user_arr as $key=>$val)
                          {
                        ?>
                                                <option value="<?php echo $val['id']?>" <?php if($val['id']==$y) { ?> selected="selected" <?php } ?> ><?php echo $val['name']?></option>
                        <?php
                          }
                        ?>
                                             </select>
                                        </div>
                    </div>
                    <div class="col-xs-6">
                     <div class="form-group">
                        <label>City</label>
                        <select class="form-control" name="z">
                          <option value="">Select</option>
                          <?php
                          $city_arr =$objgen->get_AllRows_qry("select * from city");
                          foreach($city_arr as $key=>$val)
                            {
                          ?>
                              <option value="<?php echo $val['id']?>" <?php if($val['id']==$z) { ?> selected="selected" <?php } ?> ><?php echo $val['city']?></option>
                          <?php
                            }
                          ?>
                      </select>
                      </div>
                    </div>
                    <div class="col-xs-6">
                     <div class="form-group">
                        <label>Area</label>
                        <select class="form-control" name="area">
                          <option value="">Select</option>
                          <?php
                          $location_arr =$objgen->get_AllRows_qry("select * from location");
                          foreach($location_arr as $key=>$val)
                            {
                          ?>
                              <option value="<?php echo $val['id']?>" <?php if($val['id']==$area) { ?> selected="selected" <?php } ?> ><?php echo $val['location']?></option>
                          <?php
                            }
                          ?>
                      </select>
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>From Date: </label>
                            <input type="text" id="stdate" name="sdate" placeholder="Start Date" class="form-control" value="<?php if(isset($sdate)) echo $sdate; ?>"> 
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>End Date: </label>
                            <input type="text" id="etsdate" name="edate" placeholder="End Date" class="form-control" value="<?php if(isset($edate)) echo $edate; ?>">
                        </div>
                      </div>
                    </div>
                               <br clear="all">
                                        <div class="box-footer">
                      <button type="submit" class="btn btn-primary" name="Reset"><span class="fa fa-eraser"></span>&nbsp;Reset</button>
                      <button type="submit" class="btn btn-primary" name="Search"><span class="fa fa-search"></span>&nbsp;Search</button>
                    </div>
                  </form>
                </div>
              </div>
              
                           <div class="box">
                                <div class="box-body table-responsive">
                 <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Service Name</th>
                                                <th>Entity Name</th>
                                                <th>User Name</th>
                                                <th>User Mobile</th>
                                                <th>Area</th>
                                                <th>City</th>
                                                <th>User IP Address</th>
                                                <th>Date</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                        <?php

                    if($row_count>0)
                    {
                        
                      foreach($res_arr as $key=>$val)
                        {
                        
                            $city=$objgen->get_Onerow("city","AND id=".$val['city_id']);
                            $location=$objgen->get_Onerow("location","AND id=".$val['location_id']);
                      ?>
                                            <tr>
                                                <td><?php echo $val['subcategory_name'];?></td>
                                                <td><?php echo $val['service_name'];?></td>
                                                <td><?php echo $val['user_name'];?></td>
                                                
                                                <td><?php echo $val['mobile'];?></td>
                                                <td><?php echo $location['location'];?></td>
                                                <td><?php echo $city['city'];?></td>
                                                <td><?php echo $val['ip_address'];?></td>
                                                <td><?php echo date('d-m-Y',strtotime($val['date']));?></td>
                                                
                                            </tr>
                                          
                                         <?php
                        }
                      }
                     ?>
                                        </tbody>
                                       
                                    </table>
                  <?php
                  if($row_count > $pagesize) 
                  {
                  ?>
                  <div class="row pull-right">
                                      <div class="col-xs-12">
                    
                      <div class="dataTables_paginate paging_bootstrap">
                              
                        <?php echo $pages; ?>

                        
                      </div>
                      
                     </div>
                  </div>
                  <?php
                     }
                  ?>
                  
                                </div><!-- /.box-body -->
                            </div><!-- /.box --> 
                        </div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
       <?php require_once "footer.php"; ?>
     <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
     <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      </body>
</html>
<script>
    $(function() {
    $( "#stdate" ).datepicker({
       dateFormat: 'yy-mm-dd',       
       onSelect : function(selected_date){
        var selectedDate = new Date(selected_date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);
        
         $("#etsdate").datepicker( "option", "minDate", endDate );
      }
    });
    
    $( "#etsdate" ).datepicker({
      dateFormat: 'yy-mm-dd'
    });
    
  });
</script> 