<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
include_once "../classes/fileUpload.class.php";

$objval = new validate();
$objgen = new general();

$upload_success = true;

$pagehead = "Shop";

$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : "1";

//$merchant_id = $_SESSION['ma_log_id_mer'];

if (isset($_GET['edit']) || (empty($_POST))) {
    if (empty($_POST)) {
        unset($_SESSION['refer_id']);
        unset($_SESSION['ext_photos_id']);
        unset($_SESSION['int_photos_id']);
        unset($_SESSION['team_photos_id']);
        unset($_SESSION['status']);
    }
    $shop_id = $_GET['edit'];
    $shop = $objgen->get_Onerow("shop", "AND id=" . $shop_id);
    $shop_type = $shop['shop_type'];
    $merchant_id = $shop['merchant_id'];
    $_SESSION['status'] = $shop['status'];

    $ad_exp = $objgen->get_Onerow("ad_exp", "AND shop_id=" . $shop_id);
    $other_info = $objgen->get_Onerow("other_info", "AND shop_id=" . $shop_id);

    $gym = $objgen->get_Onerow("gym", "AND shop_id=" . $shop_id);


   $qry = "select * from photos where shop_id='" . $shop_id . "' and merchant_id='" . $merchant_id . "'";
    $photos = $objgen->get_AllRows_qry($qry);
//print_r($photos);exit;

    $ext_image = $int_image = $team_image = array();
    $ext_photos_id = $int_photos_id = $team_id = array();

    $Exterior_count = $Interior_count = $Team_count = 1;

    for ($i = 0; $i < count($photos); $i++) {
        if ($photos[$i]['catgeory'] == "Exterior") {
            $ext_image[$Exterior_count] = $objgen->check_tag($photos[$i]['thumb']);
            $ext_photos_id[$Exterior_count] = $objgen->check_tag($photos[$i]['id']);
            $Exterior_count++;
        } else if ($photos[$i]['catgeory'] == "Interior") {
            if ( $photos[$i]['set_profile_pic'] == "true") {
                $set_pic[$Interior_count] = $objgen->check_tag($photos[$i]['set_profile_pic']);
                $set_pic_id[$Interior_count] = $objgen->check_tag($photos[$i]['id']);
            }
            $int_image[$Interior_count] = $objgen->check_tag($photos[$i]['thumb']);
            $int_photos_id[$Interior_count] = $objgen->check_tag($photos[$i]['id']);
            $Interior_count++;
        } else if ($photos[$i]['catgeory'] == "Team") {
            $team_image[$Team_count] = $objgen->check_tag($photos[$i]['thumb']);
            $team_photos_id[$Team_count] = $objgen->check_tag($photos[$i]['id']);
            $Team_count++;
        }
    }

    $_SESSION['ext_photos_id'] = $ext_photos_id;
    $_SESSION['int_photos_id'] = $int_photos_id;
    $_SESSION['team_photos_id'] = $team_photos_id;




    $name = array();
    $mobile = array();
    $refer_id = array();
    for ($i = 1; $i < 5; $i++) {

        $name[$i] = $objgen->check_tag($refer[$i - 1]['name']);
        $mobile[$i] = $objgen->check_tag($refer[$i - 1]['mobile']);
        $refer_id[$i] = $objgen->check_tag($refer[$i - 1]['id']);
    }

    $_SESSION['refer_id'] = $refer_id;


    $spend_on = explode(",", $ad_exp['spend_on']);

    if (!strpos($spend_on[0], "~")) {
        $spend_on_main = $spend_on[0];
        $j = 1;
    } else {
        $j = 0;
    }

    for ($i = $j; $i < count($spend_on); $i++) {
        $spend_on_type = explode("~", $spend_on[$i]);
        $this_spend_on = "spend_on_" . $spend_on_type[0];
        $$this_spend_on = $spend_on_type[1];
    }


    $categories = explode(",", $shop['categories']);

    foreach ($categories as $key => $value) {
        $value = Str_replace(" ", "_", str_replace("/", "_", $value));
        $categ_checked = $value . "_checked";
        $$categ_checked = "checked='true'";
    }


    $shop['franch'] == "yes" ? $franch_yes = "checked='checked'" : $franch_no = "checked='checked'";


    // $week_off = explode(",", $gym['week_off']);

    // foreach ($week_off as $key => $value) {
    //     $week_off_checked = $value . "_checked";
    //     $$week_off_checked = "checked='true'";
    // }


    $dumbell_type = explode(",", $gym['dumbell_type']);

    foreach ($dumbell_type as $key => $value) {
        $dumbell_type_checked = $value . "_checked";
        $$dumbell_type_checked = "checked='true'";
    }


    $parking = explode(",", $gym['parking']);

    foreach ($parking as $key => $value) {
        $parking_checked = $value . "_checked";
        $$parking_checked = "checked='true'";
    }


    $mobile_use = explode(",", $other_info['mobile_use']);

    foreach ($mobile_use as $key => $value) {
        $mobile_use_checked = $value . "_checked";
        $$mobile_use_checked = "checked='true'";
    }


    $social_media = explode(",", $other_info['social_media']);

    foreach ($social_media as $key => $value) {
        $value = str_replace(" ", "_", str_replace("/", "_", $value));
        if ($value == "Others")
            $value = "Others_social";
        $social_media_checked = $value . "_checked";
        $$social_media_checked = "checked='true'";
    }


    $enrol_sys = explode(",", $other_info['enrol_sys']);

    foreach ($enrol_sys as $key => $value) {
        if ($value == "Others")
            $value = "Others_enrol";
        $enrol_sys_checked = $value . "_checked";
        $$enrol_sys_checked = "checked='true'";
    }




    switch ($gym['sub_type']) {
        case 'Monthly';
            $sub_type_Monthly = "checked='checked'";
            break;
        case 'Quarterly';
            $sub_type_Quarterly = "checked='checked'";
            break;
        case 'Half_Yearly';
            $sub_type_Half_Yearly = "checked='checked'";
            break;
        case 'Annual';
            $sub_type_Annual = "checked='checked'";
            break;
    }


    switch ($gym['discount']) {
        case '10';
            $discount_10 = "checked='checked'";
            break;
        case '15';
            $discount_15 = "checked='checked'";
            break;
        case '20';
            $discount_20 = "checked='checked'";
            break;
        case '30';
            $discount_30 = "checked='checked'";
            break;
        case '40';
            $discount_40 = "checked='checked'";
            break;
    }


    $ad_exp['anywhere'] == "yes" ? $anywhere_yes = "checked='checked'" : $anywhere_no = "checked='checked'";

    $ad_exp['reg_srch'] == "yes" ? $reg_srch_yes = "checked='checked'" : $reg_srch_no = "checked='checked'";

    $ad_exp['web_page'] == "yes" ? $web_page_yes = "checked='checked'" : $web_page_no = "checked='checked'";

    $ad_exp['per_serv'] == "yes" ? $per_serv_yes = "checked='checked'" : $per_serv_no = "checked='checked'";

    $ad_exp['ads_us'] == "yes" ? $ads_us_yes = "checked='checked'" : $ads_us_no = "checked='checked'";

    $date_parts = explode("-", $shop['create_date']);
    $date_parts = array_reverse($date_parts);
    $create_date = implode("/", $date_parts);
}


if (!empty($_POST)) {




    foreach ($_POST as $key => $value) {
        if (!is_array($value)) {
            $$key = $objgen->check_input($value);
        }
    }

    $errors = array();

    if (!isset($_GET['edit'])) {

        $shop_exit = $objgen->chk_Ext("shop", "merchant_id='" . $merchant_id . "' and shop_type='" . $shop_type . "' and shop_name='" . $shop_name . "' and status='active'");

        if ($shop_exit > 0) {
            $errors[] = "This shop is already exists.";
        }
    }

    if (empty($errors)) {

        $status = $_SESSION['status'];

        $upd_msg_success = $upd_msg_fail = "";
        $msg_shop = $msg_gym = $msg_oi = $msg_ad_exp = $msg_refer = "";
        $img_path = '../uploads';

        $cat_checked = "";
        if (!empty($_POST['categories'])) {
            foreach ($_POST['categories'] as $cat) {
                $cat_checked .= $cat . ",";
            }
        }

        $cat_checked = rtrim($cat_checked, ",");

        $date_parts = explode("/", $create_date);
        $date_parts = array_reverse($date_parts);
        $create_date = implode("-", $date_parts);


        /*$week_off_checked = "";
        if (!empty($_POST['week_off'])) {
            foreach ($_POST['week_off'] as $wk_off) {
                $week_off_checked .= $wk_off . ",";
            }
        }

        $week_off_checked = rtrim($week_off_checked, ",");*/


        $db_type_checked = "";
        if (!empty($_POST['dumbell_type'])) {
            foreach ($_POST['dumbell_type'] as $db_type) {
                $db_type_checked .= $db_type . ",";
            }
        }

        $db_type_checked = rtrim($db_type_checked, ",");


        $park_checked = "";
        if (!empty($_POST['parking'])) {
            foreach ($_POST['parking'] as $park) {
                $park_checked .= $park . ",";
            }
        }

        $park_checked = rtrim($park_checked, ",");



        $mob_use_checked = "";
        if (!empty($_POST['mobile_use'])) {
            foreach ($_POST['mobile_use'] as $mob_use) {
                $mob_use_checked .= $mob_use . ",";
            }
        }

        $mob_use_checked = rtrim($mob_use_checked, ",");



        $soc_media_checked = "";
        if (!empty($_POST['social_media'])) {
            foreach ($_POST['social_media'] as $soc_media) {
                $soc_media_checked .= $soc_media . ",";
            }
        }

        $soc_media_checked = rtrim($soc_media_checked, ",");



        $enrol_sys_checked = "";
        if (!empty($_POST['enrol_sys'])) {
            foreach ($_POST['enrol_sys'] as $enrol) {
                $enrol_sys_checked .= $enrol . ",";
            }
        }

        $enrol_sys_checked = rtrim($enrol_sys_checked, ",");



        $spend_on_all = "";

        if ($_POST['spend_on'] != "") {
            $spend_on_all .= $spend_on . ",";
        }

        if ($_POST['jdial'] != "") {
            $spend_on_all .= "jdial~" . $jdial . ",";
        }

        if ($_POST['google'] != "") {
            $spend_on_all .= "google~" . $google . ",";
        }

        if ($_POST['mag'] != "") {
            $spend_on_all .= "mag~" . $mag . ",";
        }

        if ($_POST['pamph'] != "") {
            $spend_on_all .= "pamph~" . $pamph . ",";
        }

        if ($_POST['news'] != "") {
            $spend_on_all .= "news~" . $news . ",";
        }

        if ($_POST['other_sites'] != "") {
            $spend_on_all .= "other_sites~" . $other_sites;
        }

        $spend_on_all = rtrim($spend_on_all, ",");

        if (($_POST['phone']!="") && ($_POST['phone1']!="")) {
            $phoneno = $_POST['phone'] . "-" . $_POST['phone1'];
        }
        
        if(empty($lattitude) && empty($longitude)){
            $sql_city=$objgen->get_Onerow("city" , "AND id =" . $city);
            $sql_locality=$objgen->get_Onerow("location" , "AND id =" . $locality);
            $address= $building_name . ', ' . $street . ', ' . $landmark. ', ' . $sql_locality['location']. ', ' . $sql_city['city'];
            $address = str_replace(" ", "+", $address);
            $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
            $json = json_decode($json);
            $lattitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $longitude = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
        
        } 
        
        if (isset($_GET['edit'])) {

            $catId = $objgen->get_Onerow('shop', "AND id=" . $shop_id);
            

            $msg_shop = $objgen->upd_Row('shop', "merchant_id='" . $merchant_id . "',`shop_type`='" . $shop_type . "',`shop_name`='" . $shop_name . "',`building_name`='" . $building_name . "',`street`='" . $street . "',`locality`='" . $locality . "',`longitude`='" . $longitude . "',`lattitude`='" . $lattitude . "',`landmark`='" . $landmark . "',`website`='" . $website . "',`city`='" . $city . "',`country`='" . $country . "',`pincode`='" . $pincode . "',`email`='" . $email . "',`phone`='" . $phoneno . "',`mobile`='" . $mobile . "',`categories`='" . $shop_type . "',`esta_date`='" . $esta_date . "',`no_branches`='" . $no_branches . "',`franch`='" . $franch . "',`overview`='".$overview."',`status`='" . $status . "',`work_start_time`='" . $work_morn_from . "',`work_end_time`='" . $work_morn_to . "'", "id=" . $shop_id);
        } else {
            
            $main_cat = $_POST['shop_type'];
            $date = date("Y-m-d");
            
            $msg_shop = $objgen->ins_Row('shop', '`merchant_id`,`shop_type`,`shop_name`,`building_name`,`street`,`locality`,`landmark`,`longitude`,`lattitude`,`website`,`city`,`country`,`pincode`,`email`,`phone`,`mobile`,`categories`,`esta_date`,`no_branches`,`franch`,`overview`,`create_date`,`status`,`work_start_time`,`work_end_time`', "'" . $merchant_id . "','" . $shop_type . "','" . $shop_name . "','" . $building_name . "','" . $street . "','" . $locality . "','" . $landmark . "','" . $longitude . "','" . $lattitude . "','" . $website . "','" . $city . "','" . $country . "','" . $pincode . "','" . $email . "','" . $phoneno . "','" . $mobile . "','" . $main_cat . "','" . $esta_date . "','" . $no_branches . "','" . $franch . "','".$overview."','" . $date . "','inactive','" . $work_morn_from . "','" . $work_morn_to . "'");
            $shop_id = $objgen->get_insetId();
        }





        foreach ($_POST as $key => $value) {
            if (!is_array($value)) {
                $$key = $objgen->check_input($value);
                echo $key . " = " . $$key . "<br>";
            }
        }

        if (isset($_GET['edit'])) {
            $del_subcat = $objgen->del_Row('shop_categories', "shop_id=" . $shop_id);
            for ($g = 0; $g < count($_POST['subcategory']); $g++) {
                if ($_POST['subcategory'][$g] == 1) {
                    $_POST['m_sub_category_price'][$g];
                    $_POST['sub_cat'][$g];
                    $_POST['group'][$g];
                    $_POST['m_sub_category_time'][$g];
                    $main_cat = $_POST['main_cat'];
                    $_POST['m_sub_offer'][$g];
                    $_POST['m_sub_month'][$g];
                    $msg_shop_sub_cat = $objgen->ins_Row('shop_categories', '`m_id`,`shop_id`,`cat_id`,`group_id`,`sub_cat`,`price`,`time`,`offers`,`months`', "'" . $merchant_id . "','" . $shop_id . "','" . $main_cat . "','" . $_POST['group'][$g] . "','" . $_POST['sub_cat'][$g] . "','" . $_POST['m_sub_category_price'][$g] . "','" . $_POST['m_sub_category_time'][$g] . "','".$_POST['m_sub_offer'][$g]."','". $_POST['m_sub_month'][$g]."'");
                    // $msg_shop_sub_cat = $objgen->upd_Row('shop_categories',"m_id='".$merchant_id."',
                    //  cat_id='".$main_cat."',sub_cat='".$_POST['sub_cat'][$g]."',
                    //  price='".$_POST['m_sub_category_price'][$g]."',time='".$_POST['m_sub_category_time'][$g]."'","shop_id=".$shop_id);
                }
            }
        } else {
            //echo $main_cat = $_POST['shop_type'];echo'<br>';
            $main_cat = $_POST['main_cat'];

            for ($g = 0; $g < count($_POST['subcategory']); $g++) {
                if ($_POST['subcategory'][$g] == 1) {
                    $_POST['m_sub_category_price'][$g];
                    $_POST['sub_cat'][$g];
                    $_POST['group'][$g];
                    $_POST['m_sub_category_time'][$g];
                    $_POST['m_sub_offer'][$g];
                    $_POST['m_sub_month'][$g];
                    $msg_shop_sub_cat = $objgen->ins_Row('shop_categories', '`m_id`,`shop_id`,`cat_id`,`group_id`,`sub_cat`,`price`,`time`,`offers`,`months`', "'" . $merchant_id . "','" . $shop_id . "','" . $main_cat . "','" . $_POST['group'][$g] . "','" . $_POST['sub_cat'][$g] . "','" . $_POST['m_sub_category_price'][$g] . "','" . $_POST['m_sub_category_time'][$g] . "','".$_POST['m_sub_offer'][$g]."','". $_POST['m_sub_month'][$g]."'");
                }
            }
        }
        /*         * *****  Code Added By Shridhar **** */
        //$facility = explode(" ",$_POST['facility']);
       
        if(!empty($_POST['facility'])) {
            
            //echo $fact_arr;exit;
            if (isset($_GET['edit'])) {
              
                $del_fsa = $objgen->del_Row('shop_facilities', "shop_id=" . $shop_id);
                for ($g = 0; $g < count($_POST['facility']); $g++) {
                	
                	
                         $_POST['facility'][$g];
                        $msg_facility = $objgen->ins_Row('shop_facilities', '`shop_id`,`cat_id`,`merchant_id`,`facility_id`', "'" . $shop_id . "','" . $main_cat . "','" . $merchant_id . "','" . $_POST['facility'][$g] . "'");
                       
                    }
            } else {
                    $main_cat = $_POST['shop_type'];
                    for ($g = 0; $g < count($_POST['facility']); $g++) {

                         $_POST['facility'][$g];
                        $msg_facility = $objgen->ins_Row('shop_facilities', '`shop_id`,`cat_id`,`merchant_id`,`facility_id`', "'" . $shop_id . "','" . $main_cat . "','" . $merchant_id . "','" . $_POST['facility'][$g] . "'");
                    }
            }
        }
        if(isset($_POST['product'])){
                
            //echo $fact_arr;exit;
            if(isset($_GET['edit']))
            {
                $del_subcat = $objgen->del_Row('shop_products', "shop_id=" . $shop_id);
                for ($g = 0; $g < count($_POST['product']); $g++) {
                    $_POST['product'][$g];
                $msg_product = $objgen->ins_Row('shop_products','`shop_id`,`merchant_id`,`cat_id`,`product_id`',
                    "'".$shop_id."','".$merchant_id."','".$main_cat."','".$_POST['product'][$g]."'");
                 }
            }
            else {
                for ($g = 0; $g < count($_POST['product']); $g++) {
                    $_POST['product'][$g];
                    $main_cat = $_POST['main_cat'];

                    $msg_product = $objgen->ins_Row('shop_products','`shop_id`,`merchant_id`,`cat_id`,`product_id`',
                    "'".$shop_id."','".$merchant_id."','".$main_cat."','".$_POST['product'][$g]."'");
                }
            }
    
        }

/****************************************************************************************/




        if ($msg_shop == "") {


            if (isset($_GET['edit'])) {
                $msg_gym = $objgen->upd_Row('gym', "`work_morn_from`='" . $work_morn_from . "', `work_morn_to`='" . $work_morn_to . "', `work_even_from`='" . $work_even_from . "', `work_even_to`='" . $work_even_to . "', `gym_morn_from`='" . $gym_morn_from . "', `gym_morn_to`='" . $gym_morn_to . "', `gym_even_from`='" . $gym_even_from . "', `gym_even_to`='" . $gym_even_to . "', `yog_morn_from`='" . $yog_morn_from . "', `yog_morn_to`='" . $yog_morn_to . "', `yog_even_from`='" . $yog_even_from . "', `yog_even_to`='" . $yog_even_to . "', `aer_morn_from`='" . $aer_morn_from . "', `aer_morn_to`='" . $aer_morn_to . "', `aer_even_from`='" . $aer_even_from . "', `aer_even_to`='" . $aer_even_to . "', `dan_morn_from`='" . $dan_morn_from . "', `dan_morn_to`='" . $dan_morn_to . "', `dan_even_from`='" . $dan_even_from . "', `dan_even_to`='" . $dan_even_to . "', `week_off`='" . $week_off_checked . "', `gym_tra`='" . $gym_tra . "', `aer_tra`='" . $aer_tra . "', `recep`='" . $recep . "', `yog_tra`='" . $yog_tra . "', `dac_tra`='" . $dac_tra . "', `diate`='" . $diate . "', `tread_no`='" . $tread_no . "', `chang_room`='" . $chang_room . "', `wash_room`='" . $wash_room . "', `exec_cycle`='" . $exec_cycle . "', `filt_water`='" . $filt_water . "', `ac`='" . $ac . "', `area_sqr`='" . $area_sqr . "', `supple`='" . $supple . "', `heav_weigt`='" . $heav_weigt . "', `sell_consum`='" . $sell_consum . "', `dumbell`='" . $dumbell . "', `dumbell_type`='" . $db_type_checked . "', `combo_gym`='" . $combo_gym . "', `non_bench`='" . $non_bench . "', `medit_room`='" . $medit_room . "', `credit_card`='" . $credit_card . "', `rate_card`='" . $rate_card . "', `rate_card_file`='" . $rate_card_file . "', `price`='" . $price . "', `price_rang_to`='" . $price_rang_to . "', `parking`='" . $park_checked . "', `wifi`='" . $wifi . "', `sub_type`='" . $sub_type . "', `discount`='" . $discount . "'", "merchant_id='" . $merchant_id . "' AND`shop_id`='" . $shop_id . "'");
            } else {

                $msg_gym = $objgen->ins_Row('gym', '`merchant_id`, `shop_id`, `work_morn_from`, `work_morn_to`, `work_even_from`, `work_even_to`, `gym_morn_from`, `gym_morn_to`, `gym_even_from`, `gym_even_to`, `yog_morn_from`, `yog_morn_to`, `yog_even_from`, `yog_even_to`, `aer_morn_from`, `aer_morn_to`, `aer_even_from`, `aer_even_to`, `dan_morn_from`, `dan_morn_to`, `dan_even_from`, `dan_even_to`, `week_off`, `gym_tra`, `aer_tra`, `recep`, `yog_tra`, `dac_tra`, `diate`, `tread_no`, `chang_room`, `wash_room`, `exec_cycle`, `filt_water`, `ac`, `area_sqr`, `supple`, `heav_weigt`, `sell_consum`, `dumbell`, `dumbell_type`, `combo_gym`, `non_bench`, `medit_room`, `credit_card`, `rate_card`, `rate_card_file`, `price`, `price_rang_to`, `parking`, `wifi`, `sub_type`, `discount`', "'" . $merchant_id . "','" . $shop_id . "','" . $work_morn_from . "','" . $work_morn_to . "','" . $work_even_from . "','" . $work_even_to . "','" . $gym_morn_from . "','" . $gym_morn_to . "','" . $gym_even_from . "','" . $gym_even_to . "','" . $yog_morn_from . "','" . $yog_morn_to . "','" . $yog_even_from . "','" . $yog_even_to . "','" . $aer_morn_from . "','" . $aer_morn_to . "','" . $aer_even_from . "','" . $aer_even_to . "','" . $dan_morn_from . "','" . $dan_morn_to . "','" . $dan_even_from . "','" . $dan_even_to . "','" . $week_off_checked . "','" . $gym_tra . "','" . $aer_tra . "','" . $recep . "','" . $yog_tra . "','" . $dac_tra . "','" . $diate . "','" . $tread_no . "','" . $chang_room . "','" . $wash_room . "','" . $exec_cycle . "','" . $filt_water . "','" . $ac . "','" . $area_sqr . "','" . $supple . "','" . $heav_weigt . "','" . $sell_consum . "','" . $dumbell . "','" . $db_type_checked . "','" . $combo_gym . "','" . $non_bench . "','" . $medit_room . "','" . $credit_card . "','" . $rate_card . "','" . $rate_card_file . "','" . $price . "','" . $price_rang_to . "','" . $park_checked . "','" . $wifi . "','" . $sub_type . "','" . $discount . "'");
            }
        }


        if ($msg_gym == "") {

            if (isset($_GET['edit'])) {
                $msg_oi = $objgen->upd_Row('other_info', "`mobile_use`='" . $mob_use_checked . "',`social_media`='" . $soc_media_checked . "',`enrol_sys`='" . $enrol_sys_checked . "'", "merchant_id='" . $merchant_id . "' AND`shop_id`='" . $shop_id . "'");
            } else {
                $msg_oi = $objgen->ins_Row('other_info', '`merchant_id`,`shop_id`,`mobile_use`,`social_media`,`enrol_sys`', "'" . $merchant_id . "','" . $shop_id . "','" . $mob_use_checked . "','" . $soc_media_checked . "','" . $enrol_sys_checked . "'");
            }
        }


        if ($msg_oi == "") {

            if (isset($_GET['edit'])) {
                $msg_ad_exp = $objgen->upd_Row('ad_exp', "`anywhere`='" . $anywhere . "',`spend`='" . $spend . "',`spend_on`='" . $spend_on_all . "',`reg_srch`='" . $reg_srch . "',`web_page`='" . $web_page . "',`per_serv`='" . $per_serv . "',`ads_us`='" . $ads_us . "',`budget`='" . $budget . "'", "merchant_id='" . $merchant_id . "' AND`shop_id`='" . $shop_id . "'");
            } else {
                $msg_ad_exp = $objgen->ins_Row('ad_exp', '`merchant_id`,`shop_id`,`anywhere`,`spend`,`spend_on`,`reg_srch`,`web_page`,`per_serv`,`ads_us`,`budget`', "'" . $merchant_id . "','" . $shop_id . "','" . $anywhere . "','" . $spend . "','" . $spend_on_all . "','" . $reg_srch . "','" . $web_page . "','" . $per_serv . "','" . $ads_us . "','" . $budget . "'");
            }
        }


        if ($msg_ad_exp == "") {

            $qry = "select * from refer where shop_id='" . $shop_id . "' and merchant_id='" . $merchant_id . "' order by id asc";
            $refer = $objgen->get_AllRows_qry($qry);

            $name = array();
            $mobile = array();


            for ($i = 1; $i < 5; $i++) {
                $name = "name" . $i;
                $mobile = "mobile" . $i;
                $id = "id" . $i;


                $name_data = ($$name != '') ? $$name : $objgen->check_tag($refer[$i - 1]['name']);
                $mobile_data = ($$mobile != '') ? $$mobile : $objgen->check_tag($refer[$i - 1]['mobile']);

                $id_data = ($$id != '') ? $$id : $objgen->check_tag($refer[$i - 1]['id']);


                if ($id_data != '') {
                    $msg_refer = $objgen->upd_Row('refer', "`name`='" . $name_data . "',`mobile`='" . $mobile_data . "'", "merchant_id='" . $merchant_id . "' AND`shop_id`='" . $shop_id . "' AND id='" . $id_data . "'");
                } else {
                    $msg_refer = $objgen->ins_Row('refer', '`merchant_id`,`shop_id`,`name`,`mobile`', "'" . $merchant_id . "','" . $shop_id . "','" . $name_data . "','" . $mobile_data . "'");
                }
            }
        }

        $catgeory_array = array();

        if (isset($_FILES['Exterior1']['name']) || isset($_FILES['Exterior2']['name'])) {
            array_push($catgeory_array, "Exterior");
        }
        if (isset($_FILES['Interior1']['name']) || isset($_FILES['Interior2']['name']) || isset($_FILES['Interior3']['name']) || isset($_FILES['Interior4']['name']) || isset($_FILES['Interior5']['name']) || isset($_FILES['Interior6']['name'])) {
            array_push($catgeory_array, "Interior");
        }
        if (isset($_FILES['Team1']['name']) || isset($_FILES['Team2']['name'])) {
            array_push($catgeory_array, "Team");
        }


        if ($msg_refer == "") {

            $ext_count = $int_count = $team_count = 1;
            
            foreach ($catgeory_array as $cat_photos) {

                switch ($cat_photos) {
                   
                    case 'Interior':
                        if ($_POST['profile_pic']=='1' ) {
                           $set_profile_pic='true';
                        $int_photos_id = $_SESSION['int_photos_id'][$int_count++];
                       
                        $upd_msg_arr = upload_file("Interior1", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                        }else{
                        $set_profile_pic='false';
                         $int_photos_id = $_SESSION['int_photos_id'][$int_count++];
                       
                        $upd_msg_arr = upload_file("Interior1", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                        }
                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];
                        if ($_POST['profile_pic']=='2') {
                         $set_profile_pic='true';
           
                        $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                        $upd_msg_arr = upload_file("Interior2", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                         }else{
                         $set_profile_pic='false';
                            $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                            $upd_msg_arr = upload_file("Interior2", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                         }
                         
                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];
                        if ($_POST['profile_pic']=='3') {
                            $cat_photos; $int_photos_id;
                            $set_profile_pic='true';
                            $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                            $upd_msg_arr = upload_file("Interior3", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                        }else{
                            $set_profile_pic='false';
                            $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                            $upd_msg_arr = upload_file("Interior3", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                         }
                       

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];
                        if ($_POST['profile_pic']=='4') {
                            $set_profile_pic='true';
                        $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                        $upd_msg_arr = upload_file("Interior4", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                        }else{
                            $set_profile_pic='false';
                            $int_photos_id = $_SESSION['int_photos_id'][$int_count++];

                        $upd_msg_arr = upload_file("Interior4", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $int_photos_id, $set_profile_pic);
                         }
                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                       

                        break;
                    case 'Team':

                        $team_photos_id = $_SESSION['team_photos_id'][$team_count++];
                        $set_profile_pic='false';
                        $upd_msg_arr = upload_file("Team1", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $team_photos_id, $set_profile_pic);

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                        $team_photos_id = $_SESSION['team_photos_id'][$team_count++];
                        $set_profile_pic='false';
                        $upd_msg_arr = upload_file("Team2", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $team_photos_id, $set_profile_pic);

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                        break;
                        case 'Exterior':
                        $set_profile_pic='false';
                        $ext_photos_id = $_SESSION['ext_photos_id'][$ext_count++];

                        $upd_msg_arr = upload_file("Exterior1", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $ext_photos_id, $set_profile_pic);

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                        $ext_photos_id = $_SESSION['ext_photos_id'][$ext_count++];
                        $set_profile_pic='false';
                        $upd_msg_arr = upload_file("Exterior2", $img_path, $objgen, $merchant_id, $shop_id, $cat_photos, $ext_photos_id, $set_profile_pic);

                        $upd_msg_success .= $upd_msg_arr[0];
                        $upd_msg_fail .= $upd_msg_arr[1];

                        break;
                }
            }
            
        }





        if ($upload_success == true) {
            //if (isset($_POST['Create'])) {
                header("location:" . URLAD . "list-shop/?msg=1&page=" . $page);
            } else {
                header("location:" . URLAD . "list-shop/?msg=2&page=" . $page);
            }
        //}
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo TITLE; ?></title>
    <?php require_once "header-script.php"; ?>
<link rel="stylesheet" type="text/css" href="<?=URL?>css/nhf-style.css"/>

  <link href="<?=URLAD?>css/new-styles.css" rel="stylesheet" type="text/css" />
</head>
<body class="skin-blue">

    <?php require_once "header.php"; ?>

    <div class="wrapper row-offcanvas row-offcanvas-left">

<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">

  <section class="content-header-top">
    <h1> <i class="fa fa-suitcase"></i> Business Profile </h1>
  </section>

  <section class="content">
    <div class="row">

                <?php
                if($msg2!="")
                {
                ?>
                <div class="alert alert-success alert-dismissable">
                        <i class="fa fa-check"></i>
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <b>Alert!</b> <?php echo $msg2; ?>
                </div>

                <?php
                }

                if (!empty($errors)) {
                ?>
                    <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Please fix the following errors:</b> <br>
                                <?php
                                    foreach ($errors as $error1)
                                     echo "<div> - ".$error1." </div>";
                                ?>
                    </div>

                <?php
                }

                if($msg!="")
                {
                ?>
             <div class="alert alert-danger alert-dismissable">
                        <i class="fa fa-ban"></i>
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <b>Alert!</b> <?php echo $msg; ?>
                </div>
                <?php
                }
                ?>

        <form name="add_business" id="add_business" method="post" enctype="multipart/form-data" autocomplete="off">

        <div id="wizard">

                <h2></h2>

        <section>

        <div class="main-hed">
          <h5>A Basic Information</h5>
        </div>
        <div class="col-md-12">
        <div class="form-group">
                <label>Select Merchant Name</label>
                <select name="merchant_id" required class="form-control" >
                <option value="">Select</option>
            <?php
              $pagesize   =   1000;
              $page       = isset($_REQUEST['page'])  ?   $_REQUEST['page']   :   "1";

              $merchants = $objgen->get_AllRowscnt("merchants",$where);

              if($merchants>0)
              {
                  $merchants_arr1 = $objgen->get_AllRows("merchants",$pagesize*($page-1),$pagesize,"name ASC",$where);
              }

              $merchantChk = $objgen->get_Onerow("shop","AND id=".$_GET['edit']);
              $merchantid =$merchantChk['merchant_id'];
              //echo '<pre>';print_r($res_arr);exit;
              ?>
                  <?php foreach($merchants_arr1 as $key=>$val) {?>
          <option value="<?php echo $objgen->check_tag($val['id']); ?>" <?php $nowid=$objgen->check_tag($val['id']); if($merchantid == $nowid){ echo 'selected'; } ?>><?php echo $objgen->check_tag($val['name']); ?></option>
          <?php } ?>
                </select>
                </div>
              </div>
        <div class="col-md-12">
        <div class="form-group">
                <label>Select your business</label>
                <select name="shop_type" required class="form-control" id="category" onChange="getCat(this.value);" >
                <option value="">Select</option>
            <?php

            if (isset($_GET['edit'])) {
                $shopId =$objgen->get_Onerow('shop',"AND id=".$_GET['edit']);
                $categoryid=$shopId['shop_type'];
            }
              $pagesize   =   1000;
              $page       = isset($_REQUEST['page'])  ?   $_REQUEST['page']   :   "1";

              $row_count = $objgen->get_AllRowscnt("categories",$where);

              if($row_count>0)
              {
                  $res_arr = $objgen->get_AllRows("categories",$pagesize*($page-1),$pagesize,"category ASC",$where);
              }
              //echo '<pre>';print_r($res_arr);exit;
              ?>
                  <?php foreach($res_arr as $key=>$val) {?>
          <option value="<?php echo $objgen->check_tag($val['id']); ?>" <?php $nowid=$objgen->check_tag($val['id']); if($categoryid == $nowid){ echo 'selected'; } ?>><?php echo $objgen->check_tag($val['category']); ?></option>
          <?php } ?>
                </select>
                </div>
              </div>
        <div class="col-md-12">
          <div class="form-group">
            <label>Shop/Business Name *</label>
            <input type="text" class="form-control" name="shop_name" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['shop_name']):''; ?>" required >
          </div>
        </div>

        <div class="col-md-4">
          <div class="form-group">
            <label>Building Name *</label>
            <input type="text" class="form-control" name="building_name" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['building_name']):''; ?>" required >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Street*</label>
            <input type="text" class="form-control" name="street" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['street']):''; ?>" required >
          </div>
        </div>
       <?php

            $qry = "select * from city";
            $city = $objgen->get_AllRows_qry($qry);

            
            $city_fromdb = isset($_GET['edit'])?$objgen->check_tag($shop['city']):'';

            $city_selected = '';
        ?>

        <div class="col-md-4">
          <div class="form-group">
            <label>City <span style="color:red;"> *</span></label><br>
                        <select name="city" id="city" required  class="form-control medium" onChange="getCity(this.value);" autocomplete="off">
                        <option value="">Select City</option>
                        <?php
                            for($i=0;$i<count($city);$i++){
                                if($city_fromdb == $city[$i]['id'])
                                {
                                    
                                    $city_selected = 'selected="selected"';
                                }
                                else
                                {

                                    $city_selected = '';
                                }
                        ?>
                                 <option value="<?php echo $city[$i]['id'];?>" <?php echo $city_selected; ?>><?php echo $city[$i]['city'];?></option>
                                 
                                
                        <?php
                            }
                        ?>
                        </select>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            
            <label>Locality <span style="color:red;"> *</span></label>
            <?php if (isset($_GET['edit'])) {?>
                <div id="local1">
                    
                </div>
           <?php  } else {?>
                <div id="local">
                    <select required class="form-control medium">
                        <option value="">Select Location</option>
                    </select>
                </div>
            <?php }?>
             
           
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Landmark*</label>
            <input type="text" class="form-control" name="landmark" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['landmark']):''; ?>" required >
          </div>
        </div>
        
        <div class="col-md-4">
          <div class="form-group">
            <label>Pincode*</label>
            <input type="number" class="form-control" name="pincode" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['pincode']):''; ?>" required >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" name="email" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['email']):''; ?>" >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Phone </label>
            <div>
                <?php if(isset($_GET['edit'])) {
                $phone = explode('-',$objgen->check_tag($shop['phone']));
                 }?>
            <input type="text" class="form-control" id="phone" name="phone" value="<?php if(isset($_GET['edit'])) echo $phone[0]; ?>" style="width:25%;float:left;" >
            <input type="text" class="form-control" id="phone1" name="phone1" value="<?php if(isset($_GET['edit'])) echo $phone[1]; ?>"style="width:70%;float:left;margin-left:10px;"  >
        </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Mobile*</label>
            <input type="text" class="form-control" id="mobile" name="mobile" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['mobile']):''; ?>"required >
          </div>
        </div>
        <script type="text/javascript">
  $("#phone").keypress(function (e) {
   if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
    }
   });
  $("#phone1").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       return false;
    }
   });
  $("#mobile").keypress(function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
       return false;
    }
   });
</script>

    <div class="col-md-4">
          <div class="form-group">
            
           
              <h5>Working Hours</h5>
              <select class="form-control smal" name="work_morn_from">
                <option value="01:00:00" <?php echo $gym['work_morn_from']=="01:00:00"?"selected='selected'":""; ?> >01.00 </option>
                <option value="02:00:00" <?php echo $gym['work_morn_from']=="02:00:00"?"selected='selected'":""; ?> >02.00 </option>
                <option value="03:00:00" <?php echo $gym['work_morn_from']=="03:00:00"?"selected='selected'":""; ?> >03.00 </option>
                <option value="04:00:00" <?php echo $gym['work_morn_from']=="04:00:00"?"selected='selected'":""; ?> >04.00 </option>
                <option value="05:00:00" <?php echo $gym['work_morn_from']=="05:00:00"?"selected='selected'":""; ?> >05.00 </option>
                <option value="06:00:00" <?php echo $gym['work_morn_from']=="06:00:00"?"selected='selected'":""; ?> >06.00 </option>
                <option value="07:00:00" <?php echo $gym['work_morn_from']=="07:00:00"?"selected='selected'":""; ?> >07.00 </option>
                <option value="08:00:00" <?php echo $gym['work_morn_from']=="08:00:00"?"selected='selected'":""; ?> >08.00 </option>
                <option value="09:00:00" <?php echo $gym['work_morn_from']=="09:00:00"?"selected='selected'":""; ?> >09.00 </option>
                <option value="10:00:00" <?php echo $gym['work_morn_from']=="10:00:00"?"selected='selected'":""; ?> >10.00 </option>
                <option value="11:00:00" <?php echo $gym['work_morn_from']=="11:00:00"?"selected='selected'":""; ?> >11.00 </option>
                <option value="12:00:00" <?php echo $gym['work_morn_from']=="12:00:00"?"selected='selected'":""; ?> >12.00</option>
                <option value="13:00:00" <?php echo $gym['work_morn_from']=="13:00:00"?"selected='selected'":""; ?> >13.00 </option>
                <option value="14:00:00" <?php echo $gym['work_morn_from']=="14:00:00"?"selected='selected'":""; ?> >14.00 </option>
                <option value="15:00:00" <?php echo $gym['work_morn_from']=="15:00:00"?"selected='selected'":""; ?> >15.00 </option>
                <option value="16:00:00" <?php echo $gym['work_morn_from']=="16:00:00"?"selected='selected'":""; ?> >16.00 </option>
                <option value="17:00:00" <?php echo $gym['work_morn_from']=="17:00:00"?"selected='selected'":""; ?> >17.00 </option>
                <option value="18:00:00" <?php echo $gym['work_morn_from']=="18:00:00"?"selected='selected'":""; ?> >18.00 </option>
                <option value="19:00:00" <?php echo $gym['work_morn_from']=="19:00:00"?"selected='selected'":""; ?> >19.00 </option>
                <option value="20:00:00" <?php echo $gym['work_morn_from']=="20:00:00"?"selected='selected'":""; ?> >20.00 </option>
                <option value="21:00:00" <?php echo $gym['work_morn_from']=="21:00:00"?"selected='selected'":""; ?> >21.00 </option>
                <option value="22:00:00" <?php echo $gym['work_morn_from']=="22:00:00"?"selected='selected'":""; ?> >22.00 </option>
                <option value="23:00:00" <?php echo $gym['work_morn_from']=="23:00:00"?"selected='selected'":""; ?> >23.00 </option>
                <option value="23:59:59" <?php echo $gym['work_morn_from']=="23:59:59"?"selected='selected'":""; ?> >23.59 </option>
                
              </select>
              <div class="to">to</div>
              <select class="form-control smal" name="work_morn_to">
                <option value="01:00:00" <?php echo $gym['work_morn_to']=="01:00:00"?"selected='selected'":""; ?> >01.00 </option>
                <option value="02:00:00" <?php echo $gym['work_morn_to']=="02:00:00"?"selected='selected'":""; ?> >02.00 </option>
                <option value="03:00:00" <?php echo $gym['work_morn_to']=="03:00:00"?"selected='selected'":""; ?>  >03.00 </option>
                <option value="04:00:00" <?php echo $gym['work_morn_to']=="04:00:00"?"selected='selected'":""; ?> >04.00 </option>
                <option value="05:00:00" <?php echo $gym['work_morn_to']=="05:00:00"?"selected='selected'":""; ?> >05.00 </option>
                <option value="06:00:00" <?php echo $gym['work_morn_to']=="06:00:00"?"selected='selected'":""; ?> >06.00 </option>
                <option value="07:00:00" <?php echo $gym['work_morn_to']=="07:00:00"?"selected='selected'":""; ?> >07.00 </option>
                <option value="08:00:00" <?php echo $gym['work_morn_to']=="08:00:00"?"selected='selected'":""; ?> >08.00 </option>
                <option value="09:00:00" <?php echo $gym['work_morn_to']=="09:00:00"?"selected='selected'":""; ?> >09.00 </option>
                <option value="10:00:00" <?php echo $gym['work_morn_to']=="10:00:00"?"selected='selected'":""; ?> >10.00 </option>
                <option value="11:00:00" <?php echo $gym['work_morn_to']=="11:00:00"?"selected='selected'":""; ?> >11.00 </option>
                <option value="12:00:00" <?php echo $gym['work_morn_to']=="12:00:00"?"selected='selected'":""; ?> >12.00 </option>
                <option value="13:00:00" <?php echo $gym['work_morn_to']=="13:00:00"?"selected='selected'":""; ?> >13.00 </option>
                <option value="14:00:00" <?php echo $gym['work_morn_to']=="14:00:00"?"selected='selected'":""; ?> >14.00 </option>
                <option value="15:00:00" <?php echo $gym['work_morn_to']=="15:00:00"?"selected='selected'":""; ?> >15.00 </option>
                <option value="16:00:00" <?php echo $gym['work_morn_to']=="16:00:00"?"selected='selected'":""; ?> >16.00 </option>
                <option value="17:00:00" <?php echo $gym['work_morn_to']=="17:00:00"?"selected='selected'":""; ?> >17.00 </option>
                <option value="18:00:00" <?php echo $gym['work_morn_to']=="18:00:00 M"?"selected='selected'":""; ?>>18.00 </option>
                <option value="19:00:00" <?php echo $gym['work_morn_to']=="19:00:00"?"selected='selected'":""; ?> >19.00 </option>
                <option value="20:00:00" <?php echo $gym['work_morn_to']=="20:00:00"?"selected='selected'":""; ?> >20.00 </option>
                <option value="21:00:00" <?php echo $gym['work_morn_to']=="21:00:00"?"selected='selected'":""; ?> >21.00 </option>
                <option value="22:00:00" <?php echo $gym['work_morn_to']=="22:00:00"?"selected='selected'":""; ?> >22.00 </option>
                <option value="23:00:00" <?php echo $gym['work_morn_to']=="23:00:00"?"selected='selected'":""; ?> >23.00 </option>
                <option value="23:59:59" <?php echo $gym['work_morn_to']=="23:59:59"?"selected='selected'":""; ?> >23.59</option>
              </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
              <h5> Break  Hours</h5>
              <select class="form-control smal" name="work_even_from">
                
                <option value="01:00:00" <?php echo $gym['work_even_from']=="01:00:00"?"selected='selected'":""; ?> >01.00 </option>
                <option value="02:00:00" <?php echo $gym['work_even_from']=="02:00:00"?"selected='selected'":""; ?> >02.00 </option>
                <option value="03:00:00" <?php echo $gym['work_even_from']=="03:00:00"?"selected='selected'":""; ?>  >03.00 </option>
                <option value="04:00:00" <?php echo $gym['work_even_from']=="04:00:00"?"selected='selected'":""; ?> >04.00 </option>
                <option value="05:00:00" <?php echo $gym['work_even_from']=="05:00:00"?"selected='selected'":""; ?> >05.00 </option>
                <option value="06:00:00" <?php echo $gym['work_even_from']=="06:00:00"?"selected='selected'":""; ?> >06.00 </option>
                <option value="07:00:00" <?php echo $gym['work_even_from']=="07:00:00"?"selected='selected'":""; ?> >07.00 </option>
                <option value="08:00:00" <?php echo $gym['work_even_from']=="08:00:00"?"selected='selected'":""; ?> >08.00 </option>
                <option value="09:00:00" <?php echo $gym['work_even_from']=="09:00:00"?"selected='selected'":""; ?> >09.00 </option>
                <option value="10:00:00" <?php echo $gym['work_even_from']=="10:00:00"?"selected='selected'":""; ?> >10.00 </option>
                <option value="11:00:00" <?php echo $gym['work_even_from']=="11:00:00"?"selected='selected'":""; ?> >11.00 </option>
                <option value="12:00:00" <?php echo $gym['work_even_from']=="12:00:00"?"selected='selected'":""; ?> >12.00 </option>
                <option value="13:00:00" <?php echo $gym['work_even_from']=="13:00:00"?"selected='selected'":""; ?> >13.00 </option>
                <option value="14:00:00" <?php echo $gym['work_even_from']=="14:00:00"?"selected='selected'":""; ?> >14.00 </option>
                <option value="15:00:00" <?php echo $gym['work_even_from']=="15:00:00"?"selected='selected'":""; ?> >15.00 </option>
                <option value="16:00:00" <?php echo $gym['work_even_from']=="16:00:00"?"selected='selected'":""; ?> >16.00 </option>
                <option value="17:00:00" <?php echo $gym['work_even_from']=="17:00:00"?"selected='selected'":""; ?> >17.00 </option>
                <option value="18:00:00" <?php echo $gym['work_even_from']=="18:00:00"?"selected='selected'":""; ?> >18.00 </option>
                <option value="19:00:00" <?php echo $gym['work_even_from']=="19:00:00"?"selected='selected'":""; ?> >19.00 </option>
                <option value="20:00:00" <?php echo $gym['work_even_from']=="20:00:00"?"selected='selected'":""; ?> >20.00 </option>
                <option value="21:00:00" <?php echo $gym['work_even_from']=="21:00:00"?"selected='selected'":""; ?> >21.00 </option>
                <option value="22:00:00" <?php echo $gym['work_even_from']=="22:00:00"?"selected='selected'":""; ?> >22.00 </option>
                <option value="23:00:00" <?php echo $gym['work_even_from']=="23:00:00"?"selected='selected'":""; ?> >23.00 </option>
                <option value="23:59:59" <?php echo $gym['work_even_from']=="23:59:59"?"selected='selected'":""; ?> >23.59</option>
              </select>

              <div class="to">to</div>
              <select class="form-control smal" name="work_even_to">
                
                <option value="01:00:00" <?php echo $gym['work_even_to']=="01:00:00"?"selected='selected'":""; ?> >01.00 </option>
                <option value="02:00:00" <?php echo $gym['work_even_to']=="02:00:00"?"selected='selected'":""; ?> >02.00 </option>
                <option value="03:00:00" <?php echo $gym['work_even_to']=="03:00:00"?"selected='selected'":""; ?>  >03.00 </option>
                <option value="04:00:00" <?php echo $gym['work_even_to']=="04:00:00"?"selected='selected'":""; ?> >04.00 </option>
                <option value="05:00:00" <?php echo $gym['work_even_to']=="05:00:00"?"selected='selected'":""; ?> >05.00 </option>
                <option value="06:00:00" <?php echo $gym['work_even_to']=="06:00:00"?"selected='selected'":""; ?> >06.00 </option>
                <option value="07:00:00" <?php echo $gym['work_even_to']=="07:00:00"?"selected='selected'":""; ?> >07.00 </option>
                <option value="08:00:00" <?php echo $gym['work_even_to']=="08:00:00"?"selected='selected'":""; ?> >08.00 </option>
                <option value="09:00:00" <?php echo $gym['work_even_to']=="09:00:00"?"selected='selected'":""; ?> >09.00 </option>
                <option value="10:00:00" <?php echo $gym['work_even_to']=="10:00:00"?"selected='selected'":""; ?> >10.00 </option>
                <option value="11:00:00" <?php echo $gym['work_even_to']=="11:00:00"?"selected='selected'":""; ?> >11.00 </option>
                <option value="12:00:00" <?php echo $gym['work_even_to']=="12:00:00"?"selected='selected'":""; ?> >12.00 </option>
                <option value="13:00:00" <?php echo $gym['work_even_to']=="13:00:00"?"selected='selected'":""; ?> >13.00 </option>
                <option value="14:00:00" <?php echo $gym['work_even_to']=="14:00:00"?"selected='selected'":""; ?> >14.00 </option>
                <option value="15:00:00" <?php echo $gym['work_even_to']=="15:00:00"?"selected='selected'":""; ?> >15.00 </option>
                <option value="16:00:00" <?php echo $gym['work_even_to']=="16:00:00"?"selected='selected'":""; ?> >16.00 </option>
                <option value="17:00:00" <?php echo $gym['work_even_to']=="17:00:00"?"selected='selected'":""; ?> >17.00 </option>
                <option value="18:00:00" <?php echo $gym['work_even_to']=="18:00:00"?"selected='selected'":""; ?> >18.00 </option>
                <option value="19:00:00" <?php echo $gym['work_even_to']=="19:00:00"?"selected='selected'":""; ?> >19.00 </option>
                <option value="20:00:00" <?php echo $gym['work_even_to']=="20:00:00"?"selected='selected'":""; ?> >20.00 </option>
                <option value="21:00:00" <?php echo $gym['work_even_to']=="21:00:00"?"selected='selected'":""; ?> >21.00 </option>
                <option value="22:00:00" <?php echo $gym['work_even_to']=="22:00:00"?"selected='selected'":""; ?> >22.00 </option>
                <option value="23:00:00" <?php echo $gym['work_even_to']=="23:00:00"?"selected='selected'":""; ?> >23.00 </option>
                <option value="23:59:59" <?php echo $gym['work_even_to']=="23:59:59"?"selected='selected'":""; ?> >23.59</option>
              </select>
            </div>
          </div>
        <?php 
        if(isset($_GET['edit'])){ 
                $weekOff = $objgen->get_Onerow('gym',"AND shop_id=".$shop_id);
                 $WeekOffChk = $weekOff['week_off'];
        }
        ?>  
        
        <div class="col-md-4">
          <div class="form-group">
             <label>Weekly off</label>
              <select class="form-control medium" name="week_off_checked">
                <option value="">Select Day</option>
                <option value="Sunday" <?php if($WeekOffChk == 'Sunday'){ echo "selected='selected'";}?>>Sunday</option>
                <option value="Monday" <?php if($WeekOffChk == 'Monday'){ echo "selected='selected'";}?>>Monday</option>
                <option value="Tuesday" <?php if($WeekOffChk == 'Tuesday'){ echo "selected='selected'";}?>>Tuesday</option>
                <option value="Wednesday" <?php if($WeekOffChk == 'Wednesday'){ echo "selected='selected'";}?>>Wednesday</option>
                <option value="Thursday" <?php if($WeekOffChk == 'Thursday'){ echo "selected='selected'";}?>>Thursday</option>
                <option value="Friday" <?php if($WeekOffChk == 'Friday'){ echo "selected='selected'";}?>>Friday</option>
                <option value="Saturday" <?php if($WeekOffChk == 'Saturday'){ echo "selected='selected'";}?>>Saturday</option>

              </select>
           
          </div>
        </div>

        <div class="clearfix"></div>
        <div class="arg-both"></div>
         <div class="col-md-4">
          <div class="form-group">
            <label>Website</label>
            <input type="url" class="form-control" name="website" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['website']):''; ?>" >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Longitude</label>
            <input type="text" class="form-control" name="longitude" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['longitude']):''; ?>"  >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Latitude </label>
            <input type="text" class="form-control" name="lattitude" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['lattitude']):''; ?>" >
          </div>
        </div>
        <div class="col-md-4">
          <div class="form-group">
            <label>Month-Year of Establishment</label>
            <input type="text" id="esta_date" class="form-control" placeholder="mm/yyyy" name="esta_date" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($shop['esta_date']):''; ?>" >
          </div>
        </div>
       
        <div class="col-md-4">
          <div class="form-group">
            <label>Number of <?php echo "Category"//$cats[0]['category'];?> Staff</label>
            <input type="text" class="form-control" name="gym_tra" value="<?php echo isset($_GET['edit'])?$objgen->check_tag($gym['gym_tra']):''; ?>" >
          </div>
        </div>
    </section>


          <h2></h2>
          <section>


                <?php 
                    $getShopId     = $objgen->get_Onerow("shop","AND id=".$shop_id);
                    
                    $qry = "select * from categories where id='".$cat_id."'";
                    if (isset($_GET['edit'])) {
                        $photos = $objgen->get_AllRows_qry("select * from categories where id='".$getShopId['categories']."'");    
                    } else {
                        $photos = $objgen->get_AllRows_qry("select * from categories where id='".$cat_id."'");    
                    }
               if(isset($_GET['edit'])) {?>
                        
                        <div id="sub-edit">

                    </div>
            <?php  
                    } else {?>
                    
                    <div id="subcategory">
                    
                    </div>
                <?php  }?>
    </section>
        
    
   
     <h2></h2>

     <section>

       <div class="col-md-12">
        <div class="main-hed">
          <h5><strong>Photographs (Mandatory)</strong></h5>
        </div>
      </div>
      <div class="clearfix"></div>
      <br>
      <div class="col-md-12">
        <div class="form-group">
          <div class="clearfix"></div>
          <div class="col-md-4">
            <label>Exterior (Min 2)</label>

                        <div class="form-group">
                            <div class="clearfix">
                            </div>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("../uploads/thumb/".$ext_image[1])){
                            ?>
                                <img src="../../uploads/thumb/<?php echo $ext_image[1];?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            </div>
                            <div class="col-md-2"><input type="file" name="Exterior1" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="clearfix">
                            </div><br>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("../uploads/thumb/".$ext_image[2]) !=''){
                            ?>
                                <img src="../../uploads/thumb/<?php echo $ext_image[2]; ?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            </div>
                            <div class="col-md-2"><input type="file" name="Exterior2" />
                            </div>
                        </div>
                    </div>

          <div class="col-md-4">
            <label>Interior (Min 4)</label>

                        <div class="form-group">
                            <div class="clearfix">
                            </div>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("../uploads/thumb/".$int_image[1])){
                            ?>
                                <img src="../../uploads/thumb/<?php echo $int_image[1];?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            
                            </div>
                            <div class="col-md-2"><input type="file" name="Interior1"  />
                            </div>
                            <br><br>
                            <div class="form-group">
                            <input type="radio" name="profile_pic" value="1" <?php if($set_pic[1]=="true") { echo "checked='checked'";} ?> ><label style="padding-left: 5px;">Set Profile Pic</label> 
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="clearfix">
                            </div><br>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("../uploads/thumb/".$int_image[2])){
                            ?>
                                <img src="../../uploads/thumb/<?php echo $int_image[2]; ?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            </div>
                            <div class="col-md-2"><input type="file" name="Interior2" />
                            </div>
                            <br><br>
                            <div class="form-group">
                            <input type="radio" name="profile_pic" value="2" <?php if($set_pic[2]=="true") { echo "checked='checked'";} ?> ><label style="padding-left: 5px;">Set Profile Pic</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="clearfix">
                            </div><br/>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("../uploads/thumb/".$int_image[3])){
                            ?>
                                <img src="../../uploads/thumb/<?php echo $int_image[3];?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            
                            </div>
                            <div class="col-md-2">
                                <input type="file" name="Interior3" />

                            </div>
                            <br><br>
                            <div class="form-group">
                            <input type="radio" name="profile_pic" value="3" <?php if($set_pic[3]=="true") { echo "checked='checked'";} ?> ><label style="padding-left: 5px;">Set Profile Pic</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="clearfix">
                            </div><br>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("../uploads/thumb/".$int_image[4])){
                            ?>
                                <img src="../../uploads/thumb/<?php echo $int_image[4]; ?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            
                            </div>
                            <div class="col-md-2"><input type="file" name="Interior4" />
                            </div>
                            <br><br>
                            <div class="form-group">
                            <input type="radio" name="profile_pic" value="4" <?php if($set_pic[4]=="true") { echo "checked='checked'";} ?> ><label style="padding-left: 5px;">Set Profile Pic</label>
                            </div>
                        </div>

                        

          </div>
          <div class="col-md-4">
            <label>Team (Min 2)</label>

                        <div class="form-group">
                            <div class="clearfix">
                            </div>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("../uploads/thumb/".$team_image[1])){
                            ?>
                                <img src="../../uploads/thumb/<?php echo $team_image[1];?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            </div>
                            <div class="col-md-2"><input type="file" name="Team1" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="clearfix">
                            </div><br>
                            <div class="col-md-3">
                            <?php
                                if(file_exists("../uploads/thumb/".$team_image[2])){
                            ?>
                                <img src="../../uploads/thumb/<?php echo $team_image[2]; ?>" width="50" height="50" >
                            <?php
                                }
                            ?>
                            </div>
                            <div class="col-md-2"><input type="file" name="Team2" />
                            </div>
                        </div>


          </div>
        </div>
      <div class="clearfix"></div>
      <div class="col-md-12">
          <div class="main-hed">
            <h5><strong>Overview</strong></h5>
          </div>
        </div>
        <div class="col-md-8">
          <div class="form-group">
           <!--  <label>Overview</label> -->
            <textarea class="form-control" name="overview" rows="10" cols="30"><?php echo isset($_GET['edit'])?$objgen->check_tag($shop['overview']):''; ?></textarea> 
            
          </div>
        </div>
      <br>
      </div>
  </section>
</form>

        </div>
</aside>
</div>
<!-- child of the body tag -->
<span id="top-link-block" class="hidden"> <a href="#top" class="well well-sm" onClick="$('html,body').animate({scrollTop:0},'slow');return false;"> <i class="glyphicon glyphicon-chevron-up"></i> Back to Top </a> </span><!-- /top-link-block -->
<script src="<?=URL?>js/jquery.min.js"></script>
<script src="<?=URL?>js/jquery-ui.js" type="text/javascript"></script>
 <script>
 
  $(function() {
    //alert("Shri");
    $( "#esta_date" ).datepicker();
  });
  </script>
<script src="<?=URL?>js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?=URL?>js/jquery.steps.js"></script>


<script>
        $(function ()
        {

            var form = $("#add_business");

            $("#wizard").steps({
                headerTag: "h2",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    //alert('1');
                        form.validate().settings.ignore = ":disabled,:hidden";
                        return form.valid();
                },
                onFinishing: function (event, currentIndex)
                {
                    //alert('2');
                        form.validate().settings.ignore = ":disabled";
                        return form.valid();
                },
                onFinished: function (event, currentIndex)
                {
                    //alert('3');
                        // Submit form input
                        form.submit();
                }

            });

        });
</script>
<script>

 function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
  </script>

<script>


$(document).ready(function(){



    var cat_id = $("#category").val();
    
    var edit =  GetURLParameter('edit');
        //alert(edit);
        //alert(cat_id);
    $.ajax({
      type: "POST",
      url: "<?=URL?>admin/get-subcategory-ajax.php",
      data: {catId : cat_id, edit:edit },
      success: function(data){
        
        $("#sub-edit").html(data);
        
        }
    });


    });

</script>
<script>


$(document).ready(function(){



    var city = $("#city ").val();
    
    var edit =  GetURLParameter('edit');
        //alert(edit);
    $.ajax({
      type: "POST",
      url: "<?=URL?>admin/get-city.php",
      data: {city_id : city, edit:edit },
      success: function(data){
        //$("#locality1" ).hide(); // Hide the previous box.
        $("#local1").html(data);
        //$("#locality").innerHTML=data;
        }
    });
    var cat_id = $("#category").val();
    $.ajax({
          type: "POST",
          url: "<?=URL?>admin/get-trainer-ajax.php",
          data: {cat_id : cat_id },
          success: function(data){
            //$("#trainer").hide();
            $("#trainer").html(data);
            
            }
        });

    });

</script>
<script src="<?=URL?>js/dashboardbootstrap.min.js" type="text/javascript"></script>
<script src="<?=URL?>js/nhf-script.js" type="text/javascript"></script>
<script>jQuery(function($) {
$('.navbar .dropdown').hover(function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();

}, function() {
$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();

});

$('.navbar .dropdown > a').click(function(){
location.href = this.href;
});

});</script>
<script src="<?=URL?>js/down.min.js"></script>
<script>$(document).ready(function(){


    $('.box-bounce').waypoint(function(down){
        $(this).addClass('animation');
        $(this).addClass('bounceIn');
    }, { offset: '80%' });

        $('.box-flip').waypoint(function(down){
        $(this).addClass('animation');
        $(this).addClass('flipInY');
    }, { offset: '80%' });
        $('.box-left').waypoint(function(down){
        $(this).addClass('animation');
        $(this).addClass('fadeInLeft');
    }, { offset: '100%' });
    $('.box-right').waypoint(function(down){
        $(this).addClass('animation');
        $(this).addClass('fadeInRight');
    }, { offset: '100%' });
    });</script>
<script src="<?=URL?>js/slide.js" type="text/javascript"></script>
<script src="<?=URL?>js/skycons.js"></script>
<script src="<?=URL?>js/jquery.flot.canvas.min.js"></script>

<!--Page Level JS-->
<script src="<?=URL?>js/jquery.countTo.js"></script>
<script src="<?=URL?>js/jquery-jvectormap-1.js"></script>
<script src="<?=URL?>js/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?=URL?>js/dashboard.js" type="text/javascript"></script>
<script src="<?=URL?>js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=URL?>js/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
        $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                        "bPaginate": true,
                        "bLengthChange": false,
                        "bFilter": false,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": false
                });

        });
        

</script>


<script>
function getCity(val)
{



    $.ajax({
          type: "POST",
          url: "<?=URL?>admin/get-city.php",
          data: {id : val },
          success: function(data){
            //alert(data);
            //$("#locality" ).hide(); // Hide the previous box.
            $("#local").html(data);
            //$("#locality").innerHTML=data;
            }
        });
}
</script>
<script>

function getCat(val)
{
$.ajax({
          type: "POST",
          url: "<?=URL?>admin/get-subcategory-ajax.php",
          data: {cat_id : val },
          success: function(data){
            $("#sub" ).hide(); // Hide the previous box.
           $("#subcategory").html(data);
            gettrainer(val);
            }
        });
}

function gettrainer(val){
    //alert(val);
    $.ajax({
          type: "POST",
          url: "<?=URL?>admin/get-trainer-ajax.php",
          data: {cat_id : val },
          success: function(data){
            //$("#trainer").hide();
            $("#trainer").html(data);
            
            }
        });
    
}


</script>
</body>
</html>