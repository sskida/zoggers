<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
require_once "../country/country.php";
$objgen		=	new general();

$con_arr = country();

$pagehead = "Location";

$add_url = "add-location";

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if(isset($_GET['del']))
{
   $id= $_GET['del'];
   $msg     = $objgen->del_Row("location","id=".$id);
    if($msg=="")
   {
	header("location:".URLAD."list-location/?msg=3&page=".$page);
   }
}


if($_GET['msg']==2)
{

  $msg2 = "Location Updated Successfully.";
}

if($_GET['msg']==3)
{

  $msg2 = "Location Deleted Successfully.";
}



$where = "";
if(isset($_REQUEST['l']) &&  trim($_REQUEST['l'])!="")
{
  $l = trim($_REQUEST['l']);
  $where .= " and location like '%".$l."%'";
}
if(isset($_REQUEST['y']) &&  trim($_REQUEST['y'])!="")
{
  $y = trim($_REQUEST['y']);
  $where .= " and country = '".$y."'";
}
if(isset($_REQUEST['c']) &&  trim($_REQUEST['c'])!="")
{
  $c = trim($_REQUEST['c']);
  $where .= " and city_id = ".$c;
}



$row_count = $objgen->get_AllRowscnt("location",$where);

if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array("c" => $c, "l" => $l, "y" => $y), 1, WEBLINKAD."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("location",$pagesize*($page-1),$pagesize,"id desc",$where);
}

if(isset($_POST['Reset']))
{
	 unset($_REQUEST);
	 header("location:".URLAD."list-location");
}

if($y!="")
{
$where = " and country='".$y."'";
$city_count = $objgen->get_AllRowscnt("city",$where);
if($city_count>0)
{
  $city_arr = $objgen->get_AllRows("city",0,$city_count,"city asc",$where);
}
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">
	
         <?php require_once "header.php"; ?>
		 
        <div class="wrapper row-offcanvas row-offcanvas-left">
        
		<?php require_once "menu.php"; ?>
		 
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        List <?=$pagehead?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">List <?=$pagehead?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				
				<div class="pull-right">
				<button type="buttom" class="btn btn-success" name="Reset" onClick="window.location='<?=URLAD?><?=$add_url?>'"><span class="fa fa-plus"></span>&nbsp;Add New</button>
				</div>
				<br clear="all"><br clear="all">
				
					<div class="row">
                        <div class="col-xs-12">
						   <div class="box box-primary">
                         
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                   <!-- text input -->
									   <div class="col-xs-6">
											<div class="form-group" >
												<label>Location</label>
												<input type="text" class="form-control" value="<?=$l?>" name="l"  />
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group">
												<label>Country</label>
												<select class="form-control" name="y" id="y">
													<option value="">Select</option>
													<?php
												
													foreach($con_arr as $key=>$val)
														{
													?>
													<option value="<?=$key?>" <?php if($key==$y) { ?> selected="selected" <?php } ?> ><?=$val?></option>
													<?php
														}
													
													?>
												 </select>
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group" id="ajax_div">
												 <label>City</label>
                                            <select class="form-control" name="c">
												<option value="">Select</option>
												<?php
											
												if($city_count>0)
												{
													foreach($city_arr as $key=>$val)
													{
												?>
                                                <option value="<?=$val['id']?>" <?php if($val['id']==$c) { ?> selected="selected" <?php } ?> ><?=$objgen->check_tag($val['city']);?></option>
												<?php
													}
												}
									
												?>
                                             </select>
											</div>
										</div>
					                     <br clear="all">
                                        <div class="box-footer">
											<button type="submit" class="btn btn-primary" name="Reset"><span class="fa fa-eraser"></span>&nbsp;Reset</button>
											<button type="submit" class="btn btn-primary" name="Search"><span class="fa fa-search"></span>&nbsp;Search</button>
										</div>
									</form>
								</div>
							</div>
							
                           <div class="box">
                                <div class="box-body table-responsive">
								 <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                               <th>Location</th>
												<th>City</th>
												<th>Country</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										  	<?php
										if($row_count>0)
										{
										  foreach($res_arr as $key=>$val)
											  {
											  $sql_city =$objgen->get_Onerow("city","AND id=".$val['city_id']);
											   
											?>
                                            <tr>
                                                <td><?php echo $objgen->check_tag($val['location']); ?></td>
												<td><?php echo $objgen->check_tag($sql_city['city']); ?></td>
												<td><?=country_name($val['country'])?></td>
                                                <td><a href="<?=URLAD?>add-location/?edit=<?=$val['id']?>&page=<?=$page?>" role="button" ><span class="fa fa-edit"></span></a></td>
                                                <td><a href="<?=URLAD?>list-location/?del=<?=$val['id']?>&page=<?=$page?>" role="button" onClick="return confirm('Do you want to delete this Location?')"><span class="fa fa-trash-o"></span></a></td>
                                            </tr>
                                          
                                         <?php
												}
											}
										 ?>
                                        </tbody>
                                       
                                    </table>
									<?php
									if($row_count > $pagesize) 
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">
										
											<div class="dataTables_paginate paging_bootstrap">
											    		
												<?php echo $pages; ?>

												
											</div>
											
									   </div>
									</div>
									<?php
										 }
									?>
									
                                </div><!-- /.box-body -->
                            </div><!-- /.box --> 
                        </div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
	  <script>
		$( "#y" ).change(function() {
		    var val = $(this).val();
			$.ajax({
					  type: "GET",
					  url: "<?=URLAD?>ajax.php",
					  data: {pid : 2, c :  val },
					  success: function (result) {
					      // alert(result);
						   //do somthing here
						   $('#ajax_div').html(result);
					  }
				 });
			});
		</script>
      </body>
</html>
