<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
$objval	=   new validate();
$objgen		=	new general();

$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if($_GET['msg']==1)
{
  $msg2 = "User Created Successfully.";
}

if(isset($_POST['Change']))
{
   $old 		= trim($_POST['old_pwd']);
   $pass 		= trim($_POST['new_pwd']);
   $conf_pass 	= trim($_POST['conf_password']);
   
   $rules		=	array();
   $rules[] 	= "required,old_pwd,Enter the Old Password";
   $rules[] 	= "required,new_pwd,Enter the New Password";
   $rules[] 	= "required,conf_password,Enter the Conf. Password";
   
   $errors  	= $objval->validateFields($_POST, $rules);

	if(empty($errors))
	{
   
	   $msg = $objgen->match_Pass($pass,$conf_pass);
	   if($msg=="")
	   {
		 $msg = $objgen->chng_password('admin','password',$_POST,'admin_id',$_SESSION['MYPR_adm_id']);
		 if($msg=="")
		 {
		   $msg2 = "Password Changed Successfully.";
		 }
	   }
	}
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">

         <?php require_once "header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">  

		<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
				<section class="content-header">
                    <h1>Reset Password</h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Reset Password</li>
                    </ol>
                </section>
      <!-- Main content -->
                <section class="content">
					<div class="row" >
                        <div class="col-md-6">
                          <div class="box box-primary">
                            <div class="box-header">
                                    <h3 class="box-title">Enter Password Informations</h3>
                            </div><!-- /.box-header -->
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                     <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                        <!-- text input -->
                                        <div class="form-group" >
                                            <label>Old Password</label>
                                            <input type="password" class="form-control" value="<?=$old?>" name="old_pwd"  maxlength="20" required />
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputPassword1">New Password</label>
                                            <input type="password" class="form-control"  name="new_pwd"  value="<?=$pass?>" required  maxlength="20" />
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputPassword1">Confirm Password</label>
                                            <input type="password" class="form-control"  name="conf_password"  value="<?=$conf_pass?>" required  maxlength="20" />
                                        </div>
                                        <div class="box-footer">
                                         
                                            <button class="btn btn-primary" type="submit" name="Change"><span class="fa fa-thumbs-o-up"></span>&nbsp;Change</button>
                                            
                                    </div>
									</form>
								</div>
							</div>
						</div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>

    </body>
</html>
