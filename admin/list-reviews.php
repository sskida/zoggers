<?php
require_once "includes/includepath.php";
require_once "chk_login.php";

$objgen		=	new general();

$pagehead = "Reviews";

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if(isset($_GET['del']))
{
   $id= $_GET['del'];

   $msg     = $objgen->del_Row("review","id=".$id);
    if($msg=="")
   {
	header("location:".URLAD."list-reviews/?msg=3&page=".$page);
   }
}

if(isset($_GET['st']))
{
	 $id = $_GET['id'];
	 $st = $_GET['st'];
	 if($st=='active')
	  $status = "inactive";
	 if($st=='inactive')
	  $status = "active";
	 
	 $msg	=	$objgen->upd_Row("review","status='$status'","id=".$id);
	 header('location:'.URLAD.'list-reviews/?msg=4&page='.$page);
}

if($_GET['msg']==2)
{

  $msg2 = "Review Updated Successfully.";
}

if($_GET['msg']==3)
{

  $msg2 = "Review Deleted Successfully.";
}

if($_GET['msg']==4)
{

  $msg2 = "Status Changed Successfully.";
}

$where = "";
if(isset($_REQUEST['un']) &&  trim($_REQUEST['un'])!="")
{
  $un = trim($_REQUEST['un']);
  $where .= " and title like '%".$un."%'";
}

if(isset($_REQUEST['sh']) &&  trim($_REQUEST['sh'])!="")
{
  $sh = trim($_REQUEST['sh']);
  $where .= " and shop_id = '".$sh."'";
}


$row_count = $objgen->get_AllRowscnt("review",$where);

if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array("un" => $un, "ut" => $ut), 1, WEBLINKAD."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("review",$pagesize*($page-1),$pagesize,"id desc",$where);
}

if(isset($_POST['Reset']))
{
	 unset($_REQUEST);
	  header("location:".URLAD."list-reviews");
}

$where = "";
$shop_count = $objgen->get_AllRowscnt("shop",$where);
if($shop_count>0)
{
	$shop_arr = $objgen->get_AllRows("shop",0,$shop_count,"shop_name asc",$where);
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">
	
         <?php require_once "header.php"; ?>
		 
        <div class="wrapper row-offcanvas row-offcanvas-left">
        
		<?php require_once "menu.php"; ?>
		 
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        List <?=$pagehead?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">List <?=$pagehead?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
					<div class="row">
                        <div class="col-xs-12">
						   <div class="box box-primary">
                         
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                       <!-- text input -->
									   <div class="col-xs-6">
											<div class="form-group" >
												<label>Title</label>
												<input type="text" class="form-control" value="<?=$un?>" name="un"  />
											</div>
										</div>
										<div class="col-xs-6">
											<div class="form-group">
												<label>Shop</label>
												  <select class="form-control" name="sh" >
												<option value="">Select</option>
												<?php
												if($shop_count>0)
												{
													foreach($shop_arr as $key=>$val)
													{
												?>
                                                <option value="<?=$val['id']?>" <?php if($val['id']==$sh) { ?> selected="selected" <?php } ?> ><?=$objgen->check_tag($val['shop_name']);?></option>
												<?php
													}
												}
												?>
                                             </select>
											</div>
										</div>
                                        <div class="box-footer">
											<button type="submit" class="btn btn-primary" name="Reset"><span class="fa fa-eraser"></span>&nbsp;Reset</button>
											<button type="submit" class="btn btn-primary" name="Search"><span class="fa fa-search"></span>&nbsp;Search</button>
										</div>
									</form>
								</div>
							</div>
							
                           <div class="box">
                                <div class="box-body table-responsive">
								 <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
											    <th>Title</th>
                                                <th>Description</th>
                                                <th>Shop</th>
												<th>User</th>
												<th>Star</th>
												<th>Date</th>
                                                <th>Approve</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										  	<?php
										if($row_count>0)
										{
										  foreach($res_arr as $key=>$val)
											  {
											
											  $shop     	 = $objgen->get_Onerow("shop","AND id=".$val['shop_id']);
											  $user     	 = $objgen->get_Onerow("users","AND id=".$val['user_id']);  

											?>
                                            <tr>
                                                <td><?php echo $objgen->check_tag($val['title']); ?></td>
                                                <td><?php echo nl2br($objgen->check_tag($val['long_desc'])); ?></td>
												<td><?php echo $objgen->check_tag($shop['shop_name']); ?></td>
												<td><?php echo $objgen->check_tag($user['name']); ?></td>
												<td><?php echo $objgen->check_tag($val['star']); ?></td>
												<td><?php echo date("j F Y",strtotime($val['created_date'])); ?></td>
                                                <td>
												<?php
												if($val['status']=='active')
												{
												?>
												<a href="<?=URLAD?>list-reviews/?id=<?=$val['id']?>&page=<?=$page?>&st=<?php echo $val['status']; ?>" role="button" class="btn btn-success" ><span class="fa fa-unlock"></span> Approved</a>
												<?php
												}
												else
												{
												?>
												<a href="<?=URLAD?>list-reviews/?id=<?=$val['id']?>&page=<?=$page?>&st=<?php echo $val['status']; ?>" role="button" class="btn btn-danger" ><span class="fa fa-lock"></span> Approve</a>
												<?php
												}
												?>
												</td>
                                                <td><a href="<?=URLAD?>list-reviews/?del=<?=$val['id']?>&page=<?=$page?>" role="button" onClick="return confirm('Do you want to delete this Review?')"><span class="fa fa-trash-o"></span></a></td>
                                            </tr>
                                          
                                         <?php
												}
											}
										 ?>
                                        </tbody>
                                       
                                    </table>
									<?php
									if($row_count > $pagesize) 
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">
										
											<div class="dataTables_paginate paging_bootstrap">
											    		
												<?php echo $pages; ?>

												
											</div>
											
									   </div>
									</div>
									<?php
										 }
									?>
									
                                </div><!-- /.box-body -->
                            </div><!-- /.box --> 
                        </div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
      </body>
</html>
