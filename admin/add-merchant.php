<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
require_once "chk_type.php";
$objval	=   new validate();
$objgen		=	new general();

$pagehead = "Merchant";

$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if($_GET['msg']==1)
{
  $msg2 = "Merchant Created Successfully.";
}

if(isset($_GET['edit']))
{

       $id = $_GET['edit'];
       $result        = $objgen->get_Onerow("merchants","AND id=".$id);
       $name     = $objgen->check_tag($result['name']);
       $email     = $objgen->check_tag($result['email']);
       $address     = $objgen->check_tag($result['address']);
       $city     = $objgen->check_tag($result['city']);
       $phone     = $objgen->check_tag($result['phone']);
     

}
if(isset($_POST['Update']))
{    
   $email        = $objgen->check_input($_POST['mer_email']);
  $name         = $objgen->check_input($_POST['mer_name']);
  $phone        = $objgen->check_input($_POST['mer_phone']);
  $city         = $objgen->check_input($_POST['mer_city']);
  $address      = $objgen->check_input($_POST['mer_add']);
  
  $brd_exit = $objgen->chk_Ext("merchants","email='$email' and id<>".$id);
  if($brd_exit>0)
  {
    $errors[] = "This merchant email  is already exists.";
  }
  
  $errors = array();
   $rules   = array();
   $rules[]   = "required,mer_name,Enter the name";
   $rules[]   = "required,mer_email,Enter the email";
   $rules[]   = "required,mer_city,Enter the City";
   $rules[]   = "required,mer_add,Enter the Address";
   $errors    = $objval->validateFields($_POST, $rules);

   if(empty($errors))
  {
           
    $msg = $objgen->upd_Row('merchants',"email='".$email."',name='".$name."',city='".$city."',address='".$address."'","admin_id=".$id);
    if($msg=="")
    {
      header("location:".URLAD."list-merchant/?msg=2&page=".$page);
    }
    
  }
}
if(isset($_POST['Cancel']))
{
   header("location:".URLAD."list-merchant");

}

if(isset($_POST['submerch']))
{
  $email        = $objgen->check_input($_POST['mer_email']);
  $name         = $objgen->check_input($_POST['mer_name']);
  $phone        = $objgen->check_input($_POST['mer_phone']);
  $city         = $objgen->check_input($_POST['mer_city']);
  $address      = $objgen->check_input($_POST['mer_add']);
    
  $password   = $_POST['mer_pwd'];
  $status    = "active";
     $rules   = array();
  $rules[]   = "required,mer_name,Enter the name";
   $rules[]   = "required,mer_email,Enter the email";
   $rules[]   = "required,mer_pwd,Enter the Password";
   $rules[]   = "required,mer_city,Enter the City";
   $rules[]   = "required,mer_add,Enter the Address";
   $errors    = $objval->validateFields($_POST, $rules);

  $msg = "";
    $sh_exit = $objgen->chk_Ext("merchants","email='$email'");
  if($sh_exit>0)
  {
    $errors[] ="This email is already exists.";
    
  }
   if(empty($errors))
  {
     
     $msg = $objgen->ins_Row('merchants','name,email,password,phone,city,status,address',"'".$name."','".$email."','".$objgen->encrypt_pass($password)."','".$phone."','".$city."','".$status."','".nl2br($address)."'");
     if($msg=="")
     {
         header("location:".URLAD."add-merchant/?msg=1");
     }
  }

   
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">

         <?php require_once "header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">  

		<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
				<section class="content-header">
                    <h1><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></li>
                    </ol>
                </section>
      <!-- Main content -->
                <section class="content">
					<div class="row" >
                        <div class="col-md-6">
                          <div class="box box-primary">
                            <div class="box-header">
                                    <h3 class="box-title">Enter <?=$pagehead?> Informations</h3>
                            </div><!-- /.box-header -->
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                     <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                        <!-- text input -->
                                <div class="form-class">
                                  <label>Merchant Name</label>
                                  <input type="text" class="form-control" placeholder="Enter Your Business Name" name="mer_name" value="<?=$name?>" required />
                                </div>
                                <div class="form-class">
                                  <label>Location/City</label>
                                  <input type="text" class="form-control" placeholder="Enter Your City OR Location Name" required value="<?=$city?>" name="mer_city" />
                                </div>
                                <div class="form-class">
                                  <label>Merchant Address</label>
                                  <textarea placeholder="Address" class="form-control" required name="mer_add" ><?=$username?></textarea>
                                </div>
                                <div class="form-class">
                                  <label>Contact No.</label>
                                  <input type="text" class="form-control" placeholder="+91-9XXXXXXXXXX"  required value="<?=$phone?>" name="mer_phone"/>
                                </div>
                                <div class="form-class">
                                  <label>Email</label>
                                  <input type="email" placeholder="example@gmail.com" required name="mer_email" value="<?=$email?>" class="form-control" />
                                </div>
                                <div class="form-class">
                                  <label>Password</label>
                                  <input type="password" placeholder="Enter Your Password" required name="mer_pwd" id="mer_pwd1" class="form-control" />
                                </div>
                                <div class="form-class">
                                  <label>Confirm Password</label>
                                  <input type="password" placeholder="Re-Enter Your Password" required name="mer_conf_pwd" id="mer_conf_pwd1" class="form-control" />
                                </div>
                                <div class="box-footer">
                              
                                <button type="submit" class="btn btn-primary" name="submerch" onclick="return val_merchant1();"><span class="fa fa-save"></span>&nbsp;Save</button>
                             
                             </div>
                                
									</form>
								</div>
							</div>
						</div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
     <script>
     function val_merchant1()
      {
      var pass = $('#mer_pwd1').val();
      var conf_pass = $('#mer_conf_pwd1').val();
      if(pass!=conf_pass)
      {
        alert("Confirm Password is incorrect.");
        return false;
      }

      }

</script>
    </body>
</html>
