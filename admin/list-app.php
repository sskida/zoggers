<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
include ("appointmentsExport.php");

  if(isset($_POST["submit"]))
  {
    ExportExcel("appointments a INNER JOIN shop s ON s.id=a.shop_id INNER JOIN categories c ON c.id=s.categories INNER JOIN users u ON u.id=a.user_id INNER JOIN subcategories sb ON sb.id=a.subcat_id INNER JOIN merchants m ON m.id=a.mer_id");
  }
$objgen		=	new general();

$pagehead = "Appointments";

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if(isset($_GET['del']))
{
   $id= $_GET['del'];

   $msg     = $objgen->del_Row("appointments","id=".$id);
    if($msg=="")
   {
	header("location:".URLAD."list-app/?msg=3&page=".$page);
   }
}



if($_GET['msg']==2)
{

  $msg2 = "Appointment Updated Successfully.";
}

if($_GET['msg']==3)
{

  $msg2 = "Appointment Deleted Successfully.";
}

if($_GET['msg']==4)
{

  $msg2 = "Status Changed Successfully.";
}

$where = "";

if(isset($_REQUEST['sh']) &&  trim($_REQUEST['sh'])!="")
{
  $sh = trim($_REQUEST['sh']);
  $where .= " and shop_id = '".$sh."'";
}
if(isset($_REQUEST['sdate']) &&  trim($_REQUEST['sdate'])!="" && isset($_REQUEST['edate']) &&  trim($_REQUEST['edate'])!="")
{
  $sdate = trim($_REQUEST['sdate']);
  $edate = trim($_REQUEST['edate']);
  $where .= " and app_date between '".$sdate."' and '".$edate."'";
}

$row_count = $objgen->get_AllRowscnt("appointments",$where);

if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array("sh" => $sh), 1, WEBLINKAD."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("appointments",$pagesize*($page-1),$pagesize,"id desc",$where);
}

if(isset($_POST['Reset']))
{
	 unset($_REQUEST);
	  header("location:".URLAD."list-app");
}

$where = "";
$shop_count = $objgen->get_AllRowscnt("shop",$where);
if($shop_count>0)
{
	$shop_arr = $objgen->get_AllRows("shop",0,$shop_count,"shop_name asc",$where);
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">
	
         <?php require_once "header.php"; ?>
		 
        <div class="wrapper row-offcanvas row-offcanvas-left">
        
		<?php require_once "menu.php"; ?>
		 
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        List <?=$pagehead?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">List <?=$pagehead?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
                  <div class="pull-right">
                <form name="export" method="post">
                <button type="buttom" class="btn btn-success" name="submit" ><span class="fa fa-file-excel-o"></span>&nbsp;Export To Excel</button>
                </form>
              </div>
              <br clear="all"><br clear="all">
					<div class="row">
                        <div class="col-xs-12">
						   <div class="box box-primary">
                         
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                       <!-- text input -->
										<div class="col-xs-6">
											<div class="form-group">
												<label>Shop</label>
												  <select class="form-control" name="sh" >
    												<option value="">Select</option>
    												<?php
    												if($shop_count>0)
    												{
    													foreach($shop_arr as $key=>$val)
    													{
    												?>
                                <option value="<?=$val['id']?>" <?php if($val['id']==$sh) { ?> selected="selected" <?php } ?> ><?=$objgen->check_tag($val['shop_name']);?></option>
    												<?php
    													}
    												}
    												?>
                        </select>
											</div>
										</div>
                     <div class="col-xs-6">
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>From Date: </label>
                            <input type="text" id="sdate" name="sdate" placeholder="Start Date" class="form-control" value="<?php if(isset($sdate)) echo $sdate; ?>"> 
                        </div>
                      </div>
                      <div class="col-xs-6">
                        <div class="form-group">
                          <label>End Date: </label>
                            <input type="text" id="edate" name="edate" placeholder="End Date" class="form-control" value="<?php if(isset($edate)) echo $edate; ?>">
                        </div>
                      </div>
                    </div>
										<br clear="all" />
                                        <div class="box-footer">
											<button type="submit" class="btn btn-primary" name="Reset"><span class="fa fa-eraser"></span>&nbsp;Reset</button>
											<button type="submit" class="btn btn-primary" name="Search"><span class="fa fa-search"></span>&nbsp;Search</button>
										</div>
									</form>
								</div>
							</div>
							
                           <div class="box">
                                <div class="box-body table-responsive">
								 <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
											    <tr>
												<th>Shop Name</th>
												<th>Merchant Details</th>
												<th>Date and Time</th>
												<th>Amount</th>
												<th>Email </th>
												<th>Action</th>
											  </tr>
                                            </tr>
                                        </thead>
                                        <tbody>
										  	<?php
										if($row_count>0)
										{
										  foreach($res_arr as $key=>$val)
											  {
											
											 	$user_res     	 = $objgen->get_Onerow("merchants","AND id=".$val['mer_id']);
												$shop_res     	 = $objgen->get_Onerow("shop","AND id=".$val['shop_id']);

											?>
                                            <tr>
                                                <td><?php echo $objgen->check_tag($shop_res['shop_name']); ?></td>
                <td>Name : <?php echo $objgen->check_tag($user_res['name']); ?><br />Phone : <?php echo $objgen->check_tag($user_res['phone']); ?><br />City : <?php echo $objgen->check_tag($user_res['city']); ?><br />Address : <?php echo $objgen->check_tag($user_res['address']); ?></td>
                
                <td><?php echo date("d-m-Y",strtotime($val['app_date'])); ?><br /><?php echo $objgen->check_tag($val['app_time']); ?></td>
				<td>Quantity : <?php echo $objgen->check_tag($val['quantity']); ?><br />Price : <?php echo $objgen->check_tag($val['quantity']); ?><br />Total Price : <?php echo $objgen->check_tag($val['total_price']); ?></td>
                <td><?php echo $objgen->check_tag($user_res['email']); ?></td>
              
                <td><a role="button" href="<?=URLAD?>list-app/?del=<?=$val['id']?>&page=<?=$page?>" onClick="return confirm('Do you want to delete this Appointment?')" ><i class="fa fa-trash-o"></i></a></td>
                                            </tr>
                                          
                                         <?php
												}
											}
										 ?>
                                        </tbody>
                                       
                                    </table>
									<?php
									if($row_count > $pagesize) 
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">
										
											<div class="dataTables_paginate paging_bootstrap">
											    		
												<?php echo $pages; ?>

												
											</div>
											
									   </div>
									</div>
									<?php
										 }
									?>
									
                                </div><!-- /.box-body -->
                            </div><!-- /.box --> 
                        </div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
     <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
     <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
      </body>
</html>
<script>
    $(function() {
    $( "#sdate" ).datepicker({
       dateFormat: 'yy-mm-dd',       
       onSelect : function(selected_date){
        var selectedDate = new Date(selected_date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);
        
         $("#edate").datepicker( "option", "minDate", endDate );
      }
    });
    
    $( "#edate" ).datepicker({
      dateFormat: 'yy-mm-dd'
    });
    
  });
</script> 