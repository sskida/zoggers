<?php
require_once "includes/includepath.php";
require_once "chk_login.php";

$objval	=   new validate();
$objgen		=	new general();


$pagehead = "Sub Category";

$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if($_GET['msg']==1)
{
  $msg2 = "Sub Category Created Successfully.";
}

if(isset($_POST['Create']))
{
   
   	 $cat_id  	    = $objgen->check_input($_POST['cat_id']);
     $group_id = $objgen->check_input($_POST['group_id']);
     $subcategory = $objgen->check_input($_POST['subcategory']);
     $time       = $objgen->check_input($_POST['time']);
     $price       = $objgen->check_input($_POST['price']);
     $offer       = $objgen->check_input($_POST['offer']);
     $months       = $objgen->check_input($_POST['months']);

$errors = array();
	/*
	$rules		=	array();
	$rules[] 	= "required,category,Enter the Category";
	$errors  	= $objval->validateFields($_POST, $rules);*/
   
    $cat_exit = $objgen->chk_Ext("subcategories","subcategory='$subcategory'");
	if($cat_exit>0)
	{
		$errors[] = "This Sub category is already exists.";
	}



   if(empty($errors))
	{
		 
		 $msg = $objgen->ins_Row('subcategories','cat_id,group_id,subcategory,time,price,offers,months',"'".$cat_id."','".$group_id."','".$subcategory."','".$time."','".$price."','".$offer."','".$months."'");
     if($msg=="")
		 {
      //echo'<pre>';print_r($errors);exit;
			   header("location:".URLAD."add-subcategory/?msg=1");
		 }
	}
}

if(isset($_GET['edit']))
{

       $id = $_GET['edit'];
	     $result     	 = $objgen->get_Onerow("subcategories","AND id=".$id);
	     $subcategory     = $objgen->check_tag($result['subcategory']);
       $categoryid     = $objgen->check_tag($result['cat_id']);
       $groupid     = $objgen->check_tag($result['group_id']);
       $price     = $objgen->check_tag($result['price']);
       $time     = $objgen->check_tag($result['time']);
       $offer     = $objgen->check_tag($result['offers']);
       $months     = $objgen->check_tag($result['months']);

}
if(isset($_POST['Update']))
{    
    $cat_id         = $objgen->check_input($_POST['cat_id']);
   $group_id     = $objgen->check_input($_POST['group_id']);
    $subcategory       = $objgen->check_input($_POST['subcategory']);
    $time       = $objgen->check_input($_POST['time']);
    $price       = $objgen->check_input($_POST['price']);
    $offer       = $objgen->check_input($_POST['offer']);
    $months       = $objgen->check_input($_POST['months']);

	
	$city_exit = $objgen->chk_Ext("subcategories","subcategory='$subcategory' and id<>".$id);
	if($cat_exit>0)
	{
		$cat_exit[] = "This subcategory is already exists.";
	}
	
   $errors = array();
   /*$rules		=	array();
   $rules[] 	= "required,category,Enter the Category";
   $errors  	= $objval->validateFields($_POST, $rules);*/

   if(empty($errors))
	{
		 			 
	  $msg = $objgen->upd_Row('subcategories',"cat_id='".$cat_id."',group_id='".$group_id."',subcategory='".$subcategory."',time='".$time."',price='".$price."',offers='".$offer."',months='".$months."'","id=".$id);

      if($msg=="")
	  {
		  header("location:".URLAD."list-subcategory/?msg=2&page=".$page);
	  }
	  
	}
}
if(isset($_POST['Cancel']))
{
	  header("location:".URLAD."list-subcategory");
}


$pagesize   =   1000;
$page       = isset($_REQUEST['page'])  ?   $_REQUEST['page']   :   "1";

$row_count = $objgen->get_AllRowscnt("categories",$where);

if($row_count>0)
{
    $sql ="SELECT * FROM categories ORDER BY id DESC";
    $res_arr = $objgen->get_AllRows_qry($sql);
    
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">

         <?php require_once "header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">  

		<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
				<section class="content-header">
                    <h1><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></li>
                    </ol>
                </section>
      <!-- Main content -->
                <section class="content">
					<div class="row" >
                        <div class="col-md-6">
                          <div class="box box-primary">
                            <div class="box-header">
                                    <h3 class="box-title">Enter <?=$pagehead?> Informations</h3>
                            </div><!-- /.box-header -->
                                <div class="box-body">
                                    <form id="subcatform" role="form" action="" method="post" enctype="multipart/form-data" >
                                     <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>

                                  <div class="form-group" >
                                    <label>Choose Category*</label>
                                    <select class="form-control"  name="cat_id" onChange="getService(this.value);" id="get_service">
                                      <option value="">Select</option>
                                      <?php   

                                        if($row_count>0)
                                        {
                                            foreach($res_arr as $key=>$val)
                                            { ?>
                                            <option value="<?php echo $objgen->check_tag($val['id']); ?>" <?php $nowid=$objgen->check_tag($val['id']); if($categoryid == $nowid){ echo 'selected'; } ?>><?php echo $objgen->check_tag($val['category']); ?></option>

                                            <?php
                                                }
                                        }
                                      ?>
                                      </select>
                                    </div>
                                    
                                      <div class="form-group">
            
                                        <label>Choose Group*</label>
                                       
                                            
                                                <select  name="group_id" required class="form-control medium" id="local">
                                                    <option value="">Select</option>
                                                </select>
                                            
                                              
                                         
                                       
                                      </div>
                                        <div class="form-group" >
                                            <label>Sub Category*</label>
                                            <input type="text" class="form-control" value="<?=$subcategory?>" name="subcategory"  required />
                                        </div>
                                        <div class="form-group" >
                                            <label>Time (in minute)*</label>
                                            <input type="number" class="form-control" value="<?=$time?>" name="time"  required />
                                        </div>
                                        <div class="form-group" >
                                            <label>Price*</label>
                                            <input type="text" class="form-control" value="<?=$price?>" name="price"  required /> 
                                        </div>
                                        <div class="form-group" >
                                            <label>offers</label>
                                            <input type="text" class="form-control" value="<?=$offer?>" name="offer"   /> 
                                        </div>
                                        <div id="month" class="form-group" style="display:none;">
                                            <label>Months</label>
                                            <input type="text" class="form-control" value="<?=$months?>" name="months"   /> 
                                        </div>
										
                                        <div class="box-footer">
                                           <?php
                                            if(isset($_GET['edit']))
                                            {
                                            ?>
                                            <button class="btn btn-primary" type="submit" name="Update"><span class="fa fa-thumbs-o-up"></span>&nbsp;Update</button>
                                            <button class="btn btn-primary" type="submit" name="Cancel"><span class="fa fa-undo"></span>&nbsp;Cancel</button>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <button type="submit" class="btn btn-primary" name="Create"><span class="fa fa-save"></span>&nbsp;Save</button>
                                         
                                         <?php
                                            }
                                            ?>
                                    </div>
									</form>
								</div>
							</div>
						</div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>

    </body>
</html>
<script type="text/javascript">
jQuery(document).ready(function($) {
    jQuery('#subcatform').bootstrapValidator({
         
           message: 'This value is not valid',
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
           fields: {
                cat_id: {
                   validators: {
                       notEmpty: {
                           message: '*required'
                       }
                   }
               },
                group_id: {
                   validators: {
                       notEmpty: {
                           message: '*required'
                       }
                   }
               },
               subcategory: {
                   validators: {
                       notEmpty: {
                           message: '*required'
                       }
                   }
               },
               time: {
                   validators: {
                       notEmpty: {
                            message: '*required'
                       },
                         regexp: {
                            regexp: /^0?[0-9]?[0-9]$|^(100)$/,
                            message: '*invalid'
                        }
                   }
               },
               price: {
                   validators: {
                       notEmpty: {
                           message: '*required'
                       },
                        regexp: {
                            regexp: /^(\d|\d{1,9}|1\d{1,9}|20\d{8}|213\d{7}|2146\d{6}|21473\d{5}|214747\d{4}|2147482\d{3}|21474835\d{2}|214748364[0-7])$/,
                            message: '*invalid'
                        }
                   }
               },
              

           }


        });
    
    
});// End


 function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
 
$(document).ready(function(){
    $("#get_service").on('change', function() {
       var cat1 = $(this).val();
    
    if(cat1==15){
      $("#month").show();
    }else{
      $("#month").hide();
    }
  });
    var cat = $("#get_service ").val();
    if(cat==15){
      $("#month").show();
    }else{
      $("#month").hide();
    }
    var edit =  GetURLParameter('edit');
        //alert(edit);
    $.ajax({
          type: "POST",
          url: "<?=URL?>admin/get-groups.php",
          data: {cat_id : cat, edit:edit },
          success: function(data){
            //alert(data);
            //$("#locality" ).hide(); // Hide the previous box.
            $("#local").html(data);
            //$("#locality").innerHTML=data;
            }
  });
    

});


function getService(val)
{
  $.ajax({
          type: "POST",
          url: "<?=URL?>admin/get-groups.php",
          data: {id : val },
          success: function(data){
            //alert(data);
            //$("#locality" ).hide(); // Hide the previous box.
            $("#local").html(data);
            //$("#locality").innerHTML=data;
            }
  });
}
</script>