<?php
require_once "chk_login.php";
$objgen		=	new general();
//echo $objgen->decrypt_pass("de8EsXT2Oxeb+MnBX3JCEQHwRfBDnqey6pDXMDLg0h4=");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>

    </head>
    <body class="skin-blue">
	
         <?php require_once "header.php"; ?>
		 
        <div class="wrapper row-offcanvas row-offcanvas-left">
        
		<?php require_once "menu.php"; ?>
		 
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Control panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
					<div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                                <i class="fa fa-globe"></i> Welcome <?=ucfirst($_SESSION['MYPR_adm_username'])?>
                                <small class="pull-right">Date: <?=date("j/m/Y")?></small>
                            </h2>
							
                        </div><!-- /.col -->
                    </div>
					
					
					<div class="row">
					
					<?php
					
					/*
					http://ionicons.com/
					
					.bg-gray {
					  background-color: #eaeaec !important;
					}
					.bg-black {
					  background-color: #222222 !important;
					}
					.bg-red {
					  background-color: #f56954 !important;
					}
					.bg-yellow {
					  background-color: #f39c12 !important;
					}
					.bg-aqua {
					  background-color: #00c0ef !important;
					}
					.bg-blue {
					  background-color: #0073b7 !important;
					}
					.bg-light-blue {
					  background-color: #3c8dbc !important;
					}
					.bg-green {
					  background-color: #00a65a !important;
					}
					.bg-navy {
					  background-color: #001f3f !important;
					}
					.bg-teal {
					  background-color: #39cccc !important;
					}
					.bg-olive {
					  background-color: #3d9970 !important;
					}
					.bg-lime {
					  background-color: #01ff70 !important;
					}
					.bg-orange {
					  background-color: #ff851b !important;
					}
					.bg-fuchsia {
					  background-color: #f012be !important;
					}
					.bg-purple {
					  background-color: #932ab6 !important;
					}
					.bg-maroon {
					  background-color: #85144b !important;
					}
			    
					*/
					//dashboard_count($table,$title,$color,$image,$file)
					
					// $color : bg-aqua, bg-green, bg-yellow, bg-red
					// $image : ion-bag, ion-stats-bars, ion-person-add, ion-pie-graph
					
					//http://ionicons.com/ use ios7
					
				
                    echo $objgen->dashboard_count('admin','Admin Users','bg-yellow','fa-user', URLAD.'list-users'," and username<>'admin'");
					
					echo $objgen->dashboard_count('city','Cities','bg-green','fa-mobile', URLAD.'list-city');
                    echo $objgen->dashboard_count('location','Locations','bg-red','fa-desktop', URLAD.'list-location');
					echo $objgen->dashboard_count('categories','Entities','bg-aqua','fa-umbrella', URLAD.'list-category');
					echo $objgen->dashboard_count('users','Reg. Users','bg-purple','fa-truck', URLAD.'list-user');
					echo $objgen->dashboard_count('merchants','Reg. Merchants','bg-blue','fa-rocket', URLAD.'list-merchant');
					echo $objgen->dashboard_count('shop','Manage Shops','bg-green','fa-coffee', URLAD.'list-shop');
					echo $objgen->dashboard_count('review','Manage Reviews','bg-yellow','fa-comments', URLAD.'list-reviews');
					//echo $objgen->dashboard_count('appointments','Appointments','bg-aqua','fa-calendar', URLAD.'list-app');
 
					?>
    
                     
                     
                    </div>

                </section><!-- /.content -->
			
			                        
						
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
	       
    </body>
</html>
