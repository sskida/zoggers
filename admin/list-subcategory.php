<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
$objgen		=	new general();

$pagehead = "Sub Category";

$add_url = "add-subcategory";

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";
if(isset($_GET['del']))
{
   $id= $_GET['del'];
   $msg     = $objgen->del_Row("subcategories","id=".$id);
    if($msg=="")
   {
	header("location:".URLAD."list-subcategory/?msg=3&page=".$page);
   }
}


if($_GET['msg']==2)
{

  $msg2 = "Sub Category Updated Successfully.";
}

if($_GET['msg']==3)
{

  $msg2 = "Sub Category Deleted Successfully.";
}



$where = "";
if(isset($_REQUEST['sca']) &&  trim($_REQUEST['sca'])!="")
{
  $sca = trim($_REQUEST['sca']);
  $where .= " and subcategory like '%".$sca."%'";
}

$row_count = $objgen->get_AllRowscnt("subcategories",$where);

if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array( "sca" => $sca), 1, WEBLINKAD."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("subcategories",$pagesize*($page-1),$pagesize,"id desc",$where);
}

if(isset($_POST['Reset']))
{
	 unset($_REQUEST);
	 header("location:".URLAD."list-subcategory");
}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">
	
         <?php require_once "header.php"; ?>
		 
        <div class="wrapper row-offcanvas row-offcanvas-left">
        
		<?php require_once "menu.php"; ?>
		 
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        List <?=$pagehead?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">List <?=$pagehead?></li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content invoice">
				
				<div class="pull-right">
				<button type="buttom" class="btn btn-success" name="Reset" onClick="window.location='<?=URLAD?><?=$add_url?>'"><span class="fa fa-plus"></span>&nbsp;Add New</button>
				</div>
				<br clear="all"><br clear="all">
				
					<div class="row">
                        <div class="col-xs-12">
						   <div class="box box-primary">
                         
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                       <!-- text input -->
							          <div class="col-xs-6">
											<div class="form-group" >
												<label>Sub Category</label>
												<input type="text" class="form-control" value="<?=$sca?>" name="sca"  />
											</div>
										</div>
					                     <br clear="all">
                                        <div class="box-footer">
											<button type="submit" class="btn btn-primary" name="Reset"><span class="fa fa-eraser"></span>&nbsp;Reset</button>
											<button type="submit" class="btn btn-primary" name="Search"><span class="fa fa-search"></span>&nbsp;Search</button>
										</div>
									</form>
								</div>
							</div>
							
                           <div class="box">
                                <div class="box-body table-responsive">
								 <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                 <th>Sub Category</th>
                                                 <th>Group Name</th>
                                                 <th>Category</th>

                                                 <th>Price</th>
                                                 <th>Time</th>
                                                 <th>Offers</th>
                                                 <th>Months</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
										  	<?php
										if($row_count>0)
										{
										  foreach($res_arr as $key=>$val)
											  {
											  

											?>
                                            <tr>
                                                <td><?php echo $objgen->check_tag($val['subcategory']); ?></td>
                                                <td>
                                                    <?php  $result  = $objgen->get_Onerow("groups","AND id=".$val['group_id']);
                                     
                                                        echo $objgen->check_tag($result['group_title']); ?>
                                                </td>
                                                <td>
                                                  <?php  $result1  = $objgen->get_Onerow("categories","AND id=".$val['cat_id']);
                                     
                                                        echo $objgen->check_tag($result1['category']); ?>
                                                </td>
                                                <td><?php echo $objgen->check_tag($val['price']); ?></td>
                                                 <td><?php echo $objgen->check_tag($val['time']); ?></td>
                                                  <td><?php echo $objgen->check_tag($val['offers']); ?></td>
                                                  <td><?php echo $objgen->check_tag($val['months']); ?></td>

                                                <td><a href="<?=URLAD?>add-subcategory/?edit=<?=$val['id']?>&page=<?=$page?>" role="button" ><span class="fa fa-edit"></span></a></td>
                                                <td><a href="<?=URLAD?>list-subcategory/?del=<?=$val['id']?>&page=<?=$page?>" role="button" onClick="return confirm('Do you want to delete this Sub Category?')"><span class="fa fa-trash-o"></span></a></td>
                                            </tr>
                                          
                                         <?php
												}
											}
										 ?>
                                        </tbody>
                                       
                                    </table>
									<?php
									if($row_count > $pagesize) 
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">
										
											<div class="dataTables_paginate paging_bootstrap">
											    		
												<?php echo $pages; ?>

												
											</div>
											
									   </div>
									</div>
									<?php
										 }
									?>
									
                                </div><!-- /.box-body -->
                            </div><!-- /.box --> 
                        </div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
      </body>
</html>
