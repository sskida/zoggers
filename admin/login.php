<?php
require_once "includes/includepath.php";
$objgen		=	new general();

if($_COOKIE["swebin_user_ad"]!="")
$username = $_COOKIE["swebin_user_ad"];

if($_COOKIE["swebin_sec_ad"]!="")
$password = $objgen->decrypt_pass($_COOKIE["swebin_sec_ad"]);


if(isset($_POST['Login']))
{
  $username = trim($_POST['username']);
  $password = trim($_POST['password']);
  $remember_me = $_POST['remember_me'];
  if($username!="" && $password!="")
  {
	  $msg = $objgen->chk_Login('admin',$username,$password,'','admin_id','username','password','active',1,'*',$remember_me);
	  if($msg=="")
	  {

		header("location:".URLAD."home");
	  }
  }
  else
  {
   $msg = "Enter Username and Password.";
  }
}
?>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?> | Log in</title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box"> 
            <div class="header">Sign In</div>
            <form action="" method="post">
                <div class="body bg-gray">
				   <?php
				if($msg!="")
				{
				?>
				      <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
				 
				<?php
				}
				?>
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="Username" required maxlength="20" value="<?=$username?>"  />
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password" required maxlength="20" value="<?=$password?>" />
                    </div>          
                    <div class="form-group">
                        <input type="checkbox" name="remember_me" id="remember_me" value="yes" <?php if($_COOKIE["swebin_user_ad"]!="") { ?> checked="checked" <?php } ?> /> Remember me
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block" name="Login"><span class="fa fa-sign-in"></span> Sign me in</button>  
                </div>
            </form>

            <div class="margin text-center">
                <span>Copyright @ <?php echo SITE_NAME; ?></span>
             </div>
        </div>


        <!-- jQuery 2.0.2 -->
        <script src="<?=URLAD?>js/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="<?=URLAD?>js/bootstrap.min.js" type="text/javascript"></script>        

    </body>
</html>