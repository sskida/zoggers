<?php
$menu1  =  "";
//$head_url = basename($_SERVER["SCRIPT_NAME"]);
$head_url = $params[0];
if($head_url=='home')
  $menu1 = "active";
if($head_url=='add-user' || $head_url=='list-users')
  $menu2 = "active";
if($head_url=='reset-password')
  $menu3 = "active";
if($head_url=='add-city' || $head_url=='list-city')
  $menu4 = "active";
if($head_url=='add-location' || $head_url=='list-location')
  $menu5 = "active";
if($head_url=='add-category' || $head_url=='list-category')
  $menu6 = "active";
if($head_url=='aboutus')
  $menu7 = "active";
if($head_url=='list-user' || $head_url=='list-merchant' || $head_url=='view-user' || $head_url=='view-merchant')
  $menu8 = "active";
if($head_url=='list-shop' || $head_url=='view-shop')
  $menu9 = "active";
if($head_url=='list-reviews')
  $menu10 = "active";
if($head_url=='list-app')
  $menu11 = "active";
?>
   <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                     <!-- Sidebar user panel -->
                     <div class="user-panel">
                        <div class="pull-left image">
                            <img src="<?=URLAD?>img/avatar5.png" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello, <?=ucfirst($_SESSION['MYPR_adm_username'])?></p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                         
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="<?=$menu1?>">
                            <a href="<?=URLAD?>home">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>
						 <li class="<?=$menu7?>">
                            <a href="<?=URLAD?>portfolio">
                                <i class="fa fa-pencil"></i>
                                <span>Manage portfolio</span>
                                
                            </a>
                            
                        </li>
						<li class="<?=$menu4?>">
                           <a href="<?=URLAD?>list-city">
                                <i class="fa fa-truck"></i>
                                <span>Manage Cities</span>
                            </a>
                        </li>
						<li class="<?=$menu5?>">
                            <a href="<?=URLAD?>list-location">
                                <i class="fa fa-map-marker"></i>
                                <span>Manage Locations</span>
                            </a>
                         </li>
                         <li class="<?=$menu6?>">
                            <a href="<?=URLAD?>list-facilities">
                                <i class="fa fa-sitemap"></i>
                                <span>Manage Facilities</span>
                            </a>
                        </li>
                        <li class="<?=$menu6?>">
                            <a href="<?=URLAD?>list-products">
                                <i class="fa fa-sitemap"></i>
                                <span>Manage Products</span>
                            </a>
                        </li>
						 <li class="<?=$menu6?>">
                            <a href="<?=URLAD?>list-category">
                                <i class="fa fa-sitemap"></i>
                                <span>Manage Entities</span>
                            </a>
                        </li>
                        <li class="<?=$menu6?>">
                            <a href="<?=URLAD?>list-groups">
                                <i class="fa fa-sitemap"></i>
                                <span>Manage Services</span>
                            </a>
                        </li>
                        <li class="<?=$menu6?>">
                            <a href="<?=URLAD?>list-subcategory">
                                <i class="fa fa-sitemap"></i>
                                <span>Manage Sub Services</span>
                            </a>
                        </li>
						 <li class="treeview <?=$menu8?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Manage Reg. Users</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
							    <li><a href="<?=URLAD?>list-user"><i class="fa fa-caret-right"></i> List Users</a></li>
                                <li><a href="<?=URLAD?>list-merchant"><i class="fa fa-caret-right"></i> List Merchants</a></li>
                            </ul>
                        </li>
						 <li class="<?=$menu9?>">
                           <a href="<?=URLAD?>list-shop">
                                <i class="fa fa-tasks"></i>
                                <span>Manage Shops</span>
                            </a>
                        </li>
						 <li class="<?=$menu10?>">
                            <a href="<?=URLAD?>list-reviews">
                                <i class="fa fa-comments"></i>
                                <span>Manage Reviews</span>
                            </a>
                        </li>
						  <li class="<?=$menu11?>">
                            <a href="<?=URLAD?>list-app">
                                <i class="fa fa-calendar"></i>
                                <span>Manage Appointments</span>
                            </a>
                        </li> 
					  <?php
					  if($_SESSION['MYPR_adm_type']=='admin')
					  {
					  ?>
                       <li class="<?=$menu2?>">
                            <a href="<?=URLAD?>list-users">
                                <i class="fa fa-user"></i>
                                <span>Admin Users</span>
                            </a>
                        </li>
						<?php
						}
						?>
                        <li class="<?=$menu3?>">
                            <a href="<?=URLAD?>reset-password">
                                <i class="fa fa-key"></i> <span>Reset password</span>
                            </a>
                        </li>
                        <li class="treeview <?=$menu8?>">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Manage Reports</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li>
                                    <a href="<?=URLAD?>user-login-log">
                                        <i class="fa fa-user"></i> <span>Login Logs</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=URLAD?>user-registration-logs">
                                        <i class="fa fa-user"></i> <span>Registration Logs</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=URLAD?>service-searched-log">
                                        <i class="fa fa-user"></i> <span>Service Searched Visitor Logs</span>
                                    </a>
                                </li>
                                 <li>
                                    <a href="<?=URLAD?>service-visitor-logs">
                                        <i class="fa fa-user"></i> <span>Service Visitor Logs</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=URLAD?>rescheule-booking-log">
                                        <i class="fa fa-user"></i> <span>Rescheuled Booking Logs</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        
                       
                         <li>
                            <a href="<?=URLAD?>logout">
                                <i class="fa fa-sign-out"></i> <span>Logout</span>
                            </a>
                        </li>
                     
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
