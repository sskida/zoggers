<?php
require_once "includes/includepath.php";
require_once "chk_login.php";

$objval	=   new validate();
$objgen		=	new general();


$pagehead = "Product";

$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if($_GET['msg']==1)
{
  $msg2 = "Product Created Successfully.";
}

if(isset($_POST['Create']))
{
   
   $cat_id  	    = $objgen->check_input($_POST['cat_id']);
   $product_name       = $objgen->check_input($_POST['product_name']);
    
$errors = array();
	/*
	$rules		=	array();
	$rules[] 	= "required,category,Enter the Category";
	$errors  	= $objval->validateFields($_POST, $rules);*/
   
    $Product_exit = $objgen->chk_Ext("products","product_name='$product_name' AND cat_id='$cat_id'");
	if($Product_exit>0)
	{
		$errors[] = "This Product Name is already exists.";
	}



   if(empty($errors))
	{
		 
		 $msg = $objgen->ins_Row('products','product_name,cat_id',"'".$product_name."','".$cat_id."'");
     if($msg=="")
		 {
      //echo'<pre>';print_r($errors);exit;
			   header("location:".URLAD."add-product/?msg=1");
		 }
	}
}

if(isset($_GET['edit']))
{

       $id = $_GET['edit'];
	   $result     	 = $objgen->get_Onerow("products","AND id=".$id);
	   $product_name        = $objgen->check_tag($result['product_name']);
     $categoryid     = $objgen->check_tag($result['cat_id']);
       
}
if(isset($_POST['Update']))
{    
    $cat_id         = $objgen->check_input($_POST['cat_id']);
    $product_name       = $objgen->check_input($_POST['product_name']);
    
	
	$city_exit = $objgen->chk_Ext("products","product_name='$product_name' and id<>".$id);
	if($cat_exit>0)
	{
		$cat_exit[] = "This Product Name is already exists.";
	}
	
   $errors = array();
   /*$rules		=	array();
   $rules[] 	= "required,category,Enter the Category";
   $errors  	= $objval->validateFields($_POST, $rules);*/

   if(empty($errors))
	{
		 			 
	  $msg = $objgen->upd_Row('products',"product_name='".$product_name."',cat_id='".$cat_id."'","id=".$id);

      if($msg=="")
	  {
		  header("location:".URLAD."list-products/?msg=2&page=".$page);
	  }
	  
	}
}
if(isset($_POST['Cancel']))
{
	  header("location:".URLAD."list-products");
}


$pagesize   =   1000;
$page       = isset($_REQUEST['page'])  ?   $_REQUEST['page']   :   "1";

$row_count = $objgen->get_AllRowscnt("categories",$where);

if($row_count>0)
{
    $res_arr = $objgen->get_AllRows("categories",$pagesize*($page-1),$pagesize,"id desc",$where);
}

















?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">

         <?php require_once "header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">  

		<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
				<section class="content-header">
                    <h1><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></li>
                    </ol>
                </section>
      <!-- Main content -->
                <section class="content">
					<div class="row" >
                        <div class="col-md-6">
                          <div class="box box-primary">
                            <div class="box-header">
                                    <h3 class="box-title">Enter <?=$pagehead?> Informations</h3>
                            </div><!-- /.box-header -->
                                <div class="box-body">
                                    <form id="Product" role="form" action="" method="post" enctype="multipart/form-data" >
                                     <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>

                                        <div class="form-Product" >
                                            <label>Choose Category</label>
                             <select class="form-control"  name="cat_id">
                             <option value="">Select</option>
                               <?php             
                                if($row_count>0)
                                {
                                  foreach($res_arr as $key=>$val)
                                      { ?>
                                            <option value="<?php echo $objgen->check_tag($val['id']); ?>" <?php $nowid=$objgen->check_tag($val['id']); if($categoryid == $nowid){ echo 'selected'; } ?>><?php echo $objgen->check_tag($val['category']); ?></option>

                                    <?php
                                    }
                                }
                             ?>
                             </select>



                                        </div>
                                        <div class="form-Product" >
                                            <label>Product Name</label>
                                            <input type="text" class="form-control" value="<?=$product_name?>" name="product_name"  required />
                                        </div>
                                        
                                        <div class="box-footer">
                                           <?php
                                            if(isset($_GET['edit']))
                                            {
                                            ?>
                                            <button class="btn btn-primary" type="submit" name="Update"><span class="fa fa-thumbs-o-up"></span>&nbsp;Update</button>
                                            <button class="btn btn-primary" type="submit" name="Cancel"><span class="fa fa-undo"></span>&nbsp;Cancel</button>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <button type="submit" class="btn btn-primary" name="Create"><span class="fa fa-save"></span>&nbsp;Save</button>
                                         
                                         <?php
                                            }
                                            ?>
                                    </div>
									</form>
								</div>
							</div>
						</div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>

    </body>
</html>

<script type="text/javascript">
jQuery(document).ready(function($) {
    $('#product').bootstrapValidator({
         
           message: 'This value is not valid',
           feedbackIcons: {
               valid: 'glyphicon glyphicon-ok',
               invalid: 'glyphicon glyphicon-remove',
               validating: 'glyphicon glyphicon-refresh'
           },
           fields: {
               product_name: {
                   validators: {
                       notEmpty: {
                           message: '*required'
                       }
                   }
               },
               
              

           }


        });
    
    
});// End
</script>