<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
require_once "chk_type.php";
$objval	=   new validate();
$objgen		=	new general();

$pagehead = "User";

$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if($_GET['msg']==1)
{
  $msg2 = "User Created Successfully.";
}

if(isset($_POST['Create']))
{
   
   $username   	    = $objgen->check_input($_POST['username']);
   $user_type   	= $objgen->check_input3($_POST['user_type']);
   $password   	    = trim($_POST['password']);
	
   $rules		=	array();
   $rules[] 	= "required,username,Enter the Username";
   $rules[] 	= "required,password,Enter the Password";
   $errors  	= $objval->validateFields($_POST, $rules);
   
    $brd_exit = $objgen->chk_Ext("admin","username='$username'");
	if($brd_exit>0)
	{
		$errors[] = "This user is already exists.";
	}

   if(empty($errors))
	{
		 
		 $msg = $objgen->ins_Row('admin','username,password,user_type',"'".$username."','".$objgen->encrypt_pass($password)."','".$user_type."'");
		 if($msg=="")
		 {
			   header("location:".URLAD."add-user/?msg=1");
		 }
	}
}

if(isset($_GET['edit']))
{

       $id = $_GET['edit'];
	   $result     		= $objgen->get_Onerow("admin","AND admin_id=".$id);
	   $username     = $objgen->check_tag($result['username']);
       $user_type     = $objgen->check_tag($result['user_type']);
	   $password     = $objgen->decrypt_pass($result['password']);


}
if(isset($_POST['Update']))
{    
   $username   	    = $objgen->check_input($_POST['username']);
   $user_type   	= $objgen->check_input3($_POST['user_type']);
   $password   	    = trim($_POST['password']);
	
	$brd_exit = $objgen->chk_Ext("admin","username='$username' and admin_id<>".$id);
	if($brd_exit>0)
	{
		$errors[] = "This user is already exists.";
	}
	
	$errors = array();
   $rules		=	array();
   $rules[] 	= "required,username,Enter the Username";
   $rules[] 	= "required,password,Enter the Password";
   $errors  	= $objval->validateFields($_POST, $rules);

   if(empty($errors))
	{
		 			 
	  $msg = $objgen->upd_Row('admin',"username='".$username."',password='".$objgen->encrypt_pass($password)."',user_type='".$user_type."'","admin_id=".$id);
	  if($msg=="")
	  {
		  header("location:".URLAD."list-users/?msg=2&page=".$page);
	  }
	  
	}
}
if(isset($_POST['Cancel']))
{
	 header("location:".URLAD."list-users");

}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">

         <?php require_once "header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">  

		<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
				<section class="content-header">
                    <h1><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></li>
                    </ol>
                </section>
      <!-- Main content -->
                <section class="content">
					<div class="row" >
                        <div class="col-md-6">
                          <div class="box box-primary">
                            <div class="box-header">
                                    <h3 class="box-title">Enter <?=$pagehead?> Informations</h3>
                            </div><!-- /.box-header -->
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                     <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                        <!-- text input -->
                                        <div class="form-group" >
                                            <label>Username</label>
                                            <input type="text" class="form-control" value="<?=$username?>" name="username"  maxlength="20" required />
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputPassword1">Password</label>
                                            <input type="password" class="form-control"  name="password" id="exampleInputPassword1" value="<?=$password?>" required  maxlength="20" />
                                        </div>
                                         <div class="form-group">
                                            <label>User Type</label>
                                            <select class="form-control" name="user_type">
                                                <option value="staff" <?php if($user_type=='staff') { ?> selected="selected" <?php } ?> >Staff</option>
                                                <option value="admin" <?php if($user_type=='admin') { ?> selected="selected" <?php } ?> >Admin</option>
                                            </select>
                                        </div>
                                        <div class="box-footer">
                                           <?php
                                            if(isset($_GET['edit']))
                                            {
                                            ?>
                                            <button class="btn btn-primary" type="submit" name="Update"><span class="fa fa-thumbs-o-up"></span>&nbsp;Update</button>
                                            <button class="btn btn-primary" type="submit" name="Cancel"><span class="fa fa-undo"></span>&nbsp;Cancel</button>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <button type="submit" class="btn btn-primary" name="Create"><span class="fa fa-save"></span>&nbsp;Save</button>
                                         
                                         <?php
                                            }
                                            ?>
                                    </div>
									</form>
								</div>
							</div>
						</div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>

    </body>
</html>
