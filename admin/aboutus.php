<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
$objval	=   new validate();
$objgen		=	new general();

$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if($_GET['msg']==2)
{
  $msg2 = "About Us Edited Successfully.";
}

$id = 1;
$result     	= $objgen->get_Onerow("pages","AND id=".$id);
$title   		= $objgen->check_tag($result['title']);
$description    = $objgen->basedecode($result['description']);

if(isset($_POST['Update']))
{    
    
	  
   	$title  	    	= $objgen->check_input($_POST['title']);
	$description  	    = $_POST['description'];
	
	$rules		=	array();
    $rules[] 	= "required,title,Enter the Title";
	$rules[] 	= "required,description,Enter the Description";
	$errors  	= $objval->validateFields($_POST, $rules);

   if(empty($errors))
	{
		 			 
	  $msg = $objgen->upd_Row('pages',"title='".$title."',description='".$objgen->baseencode($description)."'","id=".$id);
	  if($msg=="")
	  {
		  header("location:".URLAD."aboutus/?msg=2&page=".$page);
	  }
	  
	}
}



?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">

         <?php require_once "header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">  

		<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
				<section class="content-header">
                    <h1><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> About Us</h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> About Us</li>
                    </ol>
                </section>
      <!-- Main content -->
                <section class="content">
					<div class="row" >
                        <div class="col-md-12">
                          <div class="box box-primary">
                            <div class="box-header">
                                    <h3 class="box-title">Enter About Us Informations</h3>
                            </div><!-- /.box-header -->
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                     <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
									   <div class="form-group" >
                                            <label>Title</label>
                                            <input type="text" class="form-control" value="<?=$title?>" name="title"  required />
                                        </div>
									
									 <div class="form-group">
                                            <label>Description</label>
											<textarea rows="3" class="form-control" name="description"><?=$description?></textarea>
										 </div>
                                         <div class="box-footer">
                                          
                                            <button class="btn btn-primary" type="submit" name="Update"><span class="fa fa-thumbs-o-up"></span>&nbsp;Update</button>
                                         
                                    </div>
									</form>
								</div>
							</div>
						</div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
	  <script src="<?=URLAD?>js/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?=URLAD?>js/plugins/ckfinder/ckfinder.js"></script>
        <script type="text/javascript">
            $(function() {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                var editor = CKEDITOR.replace('description');
				CKFinder.setupCKEditor( editor, '<?=URLAD?>js/plugins/ckfinder' ) 
            });
        </script>
	 
    </body>
</html>
