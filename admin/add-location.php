<?php
require_once "includes/includepath.php";
require_once "chk_login.php";
require_once "../country/country.php";
$objval	=   new validate();
$objgen		=	new general();

$con_arr = country();

$pagehead = "Location";

$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if($_GET['msg']==1)
{
  $msg2 = "Location Created Successfully.";
}

if(isset($_POST['Create']))
{
   
   	$location  	    = $objgen->check_input($_POST['location']);
	$city   	    = $objgen->check_input($_POST['city']);
	$country   	    = $objgen->check_input($_POST['country']);
	
	$rules		=	array();
	$rules[] 	= "required,location,Enter the Location";
	$rules[] 	= "letters_only,location,Location field must only contains letters (a-Z)";
	$rules[] 	= "required,country,Select the Country";
	$rules[] 	= "required,city,Select the City";
	$errors  	= $objval->validateFields($_POST, $rules);
   
    $loc_exit = $objgen->chk_Ext("location","city_id='$city' and location='$location' and country='$country'");
	if($loc_exit>0)
	{
		$errors[] = "This location is already exists.";
	}

   if(empty($errors))
	{
		 
		 $msg = $objgen->ins_Row('location','city_id,location,country',"'".$city."','".$location."','".$country."'");
		 if($msg=="")
		 {
			   header("location:".URLAD."add-location/?msg=1");
		 }
	}
}

if(isset($_GET['edit']))
{

       $id = $_GET['edit'];
	   $result     	 = $objgen->get_Onerow("location","AND id=".$id);
	   $city         = $objgen->check_tag($result['city_id']);
	   $country      = $objgen->check_tag($result['country']);
	   $location     = $objgen->check_tag($result['location']);

}
if(isset($_POST['Update']))
{    
    $city   	    = $objgen->check_input($_POST['city']);
	$location  	    = $objgen->check_input($_POST['location']);
	$country   	    = $objgen->check_input($_POST['country']);

	
	$city_exit = $objgen->chk_Ext("location","city_id='$city' and location='$location' and country='$country' and id<>".$id);
	if($city_exit>0)
	{
		$errors[] = "This location is already exists.";
	}
	
   $errors = array();
   $rules		=	array();
   $rules[] 	= "required,location,Enter the Location";
   $rules[] 	= "letters_only,location,Location field must only contains letters (a-Z)";
   $rules[] 	= "required,city,Select the City";
   $rules[] 	= "required,country,Select the Country";
   $errors  	= $objval->validateFields($_POST, $rules);

   if(empty($errors))
	{
		 			 
	  $msg = $objgen->upd_Row('location',"city_id='".$city."',location='".$location."',country='".$country."'","id=".$id);
	  if($msg=="")
	  {
		  header("location:".URLAD."list-location/?msg=2&page=".$page);
	  }
	  
	}
}
if(isset($_POST['Cancel']))
{
	  header("location:".URLAD."list-location");
}

$city_count = 0;
if($country!="")
{

	$where = " and country='".$country."'";
	$city_count = $objgen->get_AllRowscnt("city",$where);
	if($city_count>0)
	{
	  $city_arr = $objgen->get_AllRows("city",0,$city_count,"city asc",$where);
	}


}


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo TITLE; ?></title>
        <?php require_once "header-script.php"; ?>
    </head>
    <body class="skin-blue">

         <?php require_once "header.php"; ?>

        <div class="wrapper row-offcanvas row-offcanvas-left">  

		<?php require_once "menu.php"; ?>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
				<section class="content-header">
                    <h1><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=URLAD?>home"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active"><?php if(isset($_GET['edit'])){ echo "Edit"; } else { echo "Add";  } ?> <?=$pagehead?></li>
                    </ol>
                </section>
      <!-- Main content -->
                <section class="content">
					<div class="row" >
                        <div class="col-md-6">
                          <div class="box box-primary">
                            <div class="box-header">
                                    <h3 class="box-title">Enter <?=$pagehead?> Informations</h3>
                            </div><!-- /.box-header -->
                                <div class="box-body">
                                    <form role="form" action="" method="post" enctype="multipart/form-data" >
                                     <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                        <!-- text input -->
                                        <div class="form-group" >
                                            <label>Location</label>
                                            <input type="text" class="form-control" value="<?=$location?>" name="location"  required />
                                        </div>
										<div class="form-group">
                                            <label>Country</label>
                                            <select class="form-control" name="country" id="country">
												<option value="">Select</option>
												<?php
												foreach($con_arr as $key=>$val)
													{
												?>
                                                <option value="<?=$key?>" <?php if($key==$country) { ?> selected="selected" <?php } ?> ><?=$val?></option>
												<?php
													}
												?>
                                             </select>
                                        </div>
										 <div class="form-group" id="ajax_div">
                                            <label>City</label>
                                            <select class="form-control" name="city" required>
												<option value="">Select</option>
												<?php
												if($city_count>0)
												{
													foreach($city_arr as $key=>$val)
													{
												?>
                                                <option value="<?=$val['id']?>" <?php if($val['id']==$city) { ?> selected="selected" <?php } ?> ><?=$objgen->check_tag($val['city']);?></option>
												<?php
													}
												}
												?>
                                             </select>
                                        </div>
                                        <div class="box-footer">
                                           <?php
                                            if(isset($_GET['edit']))
                                            {
                                            ?>
                                            <button class="btn btn-primary" type="submit" name="Update"><span class="fa fa-thumbs-o-up"></span>&nbsp;Update</button>
                                            <button class="btn btn-primary" type="submit" name="Cancel"><span class="fa fa-undo"></span>&nbsp;Cancel</button>
                                            <?php
                                            }
                                            else
                                            {
                                            ?>
                                            <button type="submit" class="btn btn-primary" name="Create"><span class="fa fa-save"></span>&nbsp;Save</button>
                                         
                                         <?php
                                            }
                                            ?>
                                    </div>
									</form>
								</div>
							</div>
						</div><!-- /.col -->
                    </div>

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

     <?php require_once "footer.php"; ?>
	 <script>
		$( "#country" ).change(function() {
		    var val = $(this).val();
			$.ajax({
					  type: "GET",
					  url: "<?=URLAD?>ajax.php",
					  data: {pid : 1, c :  val },
					  success: function (result) {
					      // alert(result);
						   //do somthing here
						   $('#ajax_div').html(result);
					  }
				 });
			});
		</script>

    </body>
</html>
