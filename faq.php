<?php
require_once "includes/includepath.php";
$objgen	=	new general();

$id = $_GET['id'];
$shop = $objgen->get_Onerow("shop","AND id=".$id);
$merchant_id = $shop['merchant_id'];

$cat = $shop['shop_type'];
if($cat=='spa')
{
   $class = 'class="health"';
   $class2 = 'health';
}
else if($cat=='gym, areobics')
{
   $class = 'class="wellness"';
   $class2 = 'wellness';
}
else if($cat=='gym')
{
   $class = 'class="wellness"';
   $class2 = 'wellness';
}
else
{
   $class = 'class="health"';
   $class2 = 'health';
}


if(isset($_POST['book_submit']))
{
	$_SESSION['book_data'] = $_POST;	
	$section_name = $_POST['section'];

	if($_SESSION['ma_log_id_usr']=="" || $_SESSION['ma_usr_name_usr']=="")
	{
		header("location:".URL."search-details/?id=".$id."&section=".$section_name."&nologged=1#tab_section");
	}
	else
	{
		$user_id = $_SESSION['ma_log_id_usr'];
		$checkin_date = $_POST['checkin_date'];
		
		$guest_count = $_POST['guest_count'];
		$price = $_POST['price'];
		$total_price = $guest_count*$price;

		$checkin_date = implode("-",array_reverse(explode("-",$checkin_date)));

		$objgen->ins_Row('appointments','app_date, 	app_time,user_id,shop_id,mer_id,quantity,price,total_price',"'".$checkin_date."','','".$user_id."','".$id."','".$merchant_id."','".$guest_count."','".$price."','".$total_price."'");

		$_SESSION['booking_done'] = "booking done";



	header("location:".URL."search-details/?id=".$id."&section=".$section_name."#tab_section");

	}
}

if(isset($_POST['login']))
{
  $email = trim($_POST['email_login']);
  $password = trim($_POST['password_login']);

  if($email!="" && $password!="")
  {
	  $msg5 = $objgen->chk_Login('users',$email,$password,'','id','email','password','active',2);

	  if($msg5=="")
	  {

			$password = $objgen->encrypt_pass($password);

			$user = $objgen->get_Onerow("users","AND email='".$email."' AND password='".$password."'");

			$user_id = $user['id'];
			$user_email = $user['email'];
			$user_name = $user['name'];

			$checkin_date = $_SESSION['book_data']['checkin_date'];
			
			$guest_count = $_SESSION['book_data']['guest_count'];
			$price = $_SESSION['book_data']['price'];
			$total_price = $guest_count*$price;

			$checkin_date = implode("-",array_reverse(explode("-",$checkin_date)));

			$objgen->ins_Row('appointments','app_date, 	app_time,user_id,shop_id,mer_id,quantity,price,total_price',"'".$checkin_date."','','".$user_id."','".$id."','".$merchant_id."','".$guest_count."','".$price."','".$total_price."'");

			$_SESSION['booking_done'] = "booking done";

			unset($_SESSION['book_data']);

			$_SESSION['ma_log_id_usr'] 	 = $user_id;
			$_SESSION['ma_usr_name_usr']	= $user_email;
			$_SESSION['ma_name_usr']		= $user_name;

			$section_name = $_POST['section'];

			header("location:".URL."search-details/?id=".$id."&section=".$section_name."#tab_section");

	  }
	  else
	  {
	    echo "<script>alert('".$msg5."');</script>";
	  }
  }
  else
  {
   echo "<script>alert('Enter Username and Password.')</script>";
  }

}

if(isset($_POST['review']))
{

	$user_id = $_SESSION['ma_log_id_usr'];
	$title = $_POST['title'];
	$long_desc = $_POST['long_desc'];
	$score = $_POST['score'];
	$today = date("Y-m-d");

	$objgen->ins_Row('review','shop_id,title,long_desc,created_date,user_id,star,status',"'".$id."','".$title."','".$long_desc."','".$today."','".$user_id."','".$score."','active'");

	$section_name = $_POST['section'];

	header("location:".URL."search-details/?id=".$id."&section=".$section_name."#tab_section");

}

?>
<!doctype html>
<html>
    <head>
    <meta charset="utf-8">
    <!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=TITLE?></title>
    <?php include 'head-script.php'; ?>
		<link rel="stylesheet" href="<?=URL?>raty/lib/jquery.raty.css">
    <link rel="stylesheet" href="<?=URL?>css/jquery-ui.css">
    <style>
#map-canvas {
	height: 350px;
}
</style>
<!--    <script>

var MY_MAPTYPE_ID = 'custom_style';

function initialize() {
	var featureOpts = [ { "elementType": "geometry", "stylers": [ { "gamma": 1 }, { "lightness": -2 }, { "saturation": -50 }, { "weight": 1 }, { "visibility": "simplified" } ] } ];

	 var mapOptions = {
			//scroll:{x:jQuery(window).scrollLeft(),y:jQuery(window).scrollTop()},
			zoom: 11,
			center: new google.maps.LatLng(18.520430,  73.856744),
		panControl: true,
		panControlOptions: {
			position: google.maps.ControlPosition.LEFT_CENTER
		},

		zoomControl: true,
		zoomControlOptions: {
			//style: google.maps.ZoomControlStyle.SMALL,
			position: google.maps.ControlPosition.LEFT_CENTER
		},

			scaleControl: false,
			scrollwheel: false,
			streetViewControl: false,
			mapTypeControlOptions: {
				mapTypeIds: [MY_MAPTYPE_ID]
					},
			mapTypeId: MY_MAPTYPE_ID,
			};



  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

var companyLogo = new google.maps.MarkerImage('http://smarton.weblusive.com/wp-content/themes/smarton/images/google-marker.png',
			new google.maps.Size(40,57),
			new google.maps.Point(0,0),
			new google.maps.Point(20,-20)
		);

// To add the marker to the map, use the 'map' property
var marker = new google.maps.Marker({
    position:  new google.maps.LatLng(18.520430,  73.856744),
    map: map,
    title:"Hello World!",
	icon:companyLogo
});


google.maps.event.addListener(marker, 'click', toggleBounce);
function toggleBounce() {

  if (marker.getAnimation() != null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}
var styledMapOptions = {
		name: 'SmartON'
	  };

	  var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions)
	  ;


	   map.mapTypes.set(MY_MAPTYPE_ID, customMapType);



}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&' +
      'callback=initialize';
  document.body.appendChild(script);
}

window.onload = loadScript;

    </script>-->
    </head>

    <body>
<?php include 'header.php'; ?>
<div class="clearfix"></div>
<div class="inner-bg <?=$class2?>">
      <div class="container">
    <h1><?php echo $shop['shop_name']; ?></h1>
    <h3><a href="<?=URL?>">Home</a>/Search</h3>
  </div>
    </div>

<?php
		
		$images_folder = "images/photos";

		$qry_photos = "select * from photos where shop_id='".$id."'";
		$photos = $objgen->get_AllRows_qry($qry_photos);

		if(!empty($photos))
		{

			$shop_image_top = "";
			$carousel_images = $carousel_thumb_images = "";
			$i=0;
			foreach($photos as $key=>$value)
			{

				if(!$i)
				{
					$selected = 'class="selected"';
				}

				$photo_file = $photos[$i]['image'];

				$file_path = $images_folder."/".$photo_file;
				$thumb_path = $images_folder."/thumb_".$photo_file;

				if(($photos[$i]['catgeory']=="Exterior")&&(file_exists($file_path)))
				{
					$shop_image_top = $file_path;
				}

				if(file_exists($file_path))
				{

					$carousel_images .= '<span id="'.$i.'"><img src="'.URL.$images_folder.'/'.$photo_file.'" alt=""/></span>';

				}

				if(file_exists($thumb_path))
				{

					$carousel_thumb_images .= '<a href="#'.$i.'" '.$selected.'><img src="'.URL.$thumb_path.'" alt=""/></a>';

				}
				$i++;
			}
		}

?>


<section>
        <div class="container">
       <h1 class="title-1">Frequently Asked Questions</h1>
       <h6 class="faq-desc">Find answers to the most frequently asked questions about FitCharm</h6>
       <div class="tabmenu">
       
  
  <input id="tab1" type="radio" name="tabs" checked>
  <label for="tab1">Consumers</label>
    
  <input id="tab2" type="radio" name="tabs">
  <label for="tab2">Merchants</label>
    
  
    
  <section id="content1">
    <p>

    <div class="block">
  <input type="radio" name="city" id="cityA" checked />   
  <label for="cityA"><span>1. What type of services can I offer on Desileaks</span></label>
  <div class="info">
    <p>You can offer Spa, Salon,Beauty Parlor, Mehandi, Tattoo, Gym, Wt Loss and Gain and Sports. We are exclusive platform for free lancers and home service providers in 8 main services.
    </p>
      
  </div>
</div>
<div class="block">
  <input type="radio" name="city" id="cityB"/>
  <label for="cityB"><span>2. How does listing model works</span></label>
  <div class="info">
    <p>Listing model gives information about your business. Customers cant book appointment with you. You don’t get access to Merchant Dashboard. </p>
  </div>
</div>
<div class="block">
  <input type="radio" name="city" id="cityC" />
  <label for="cityC"><span>3. How to make the payment</span></label>
  <div class="info">
    <p>We accept payment through cheque, NEFT and IPMS. We don’t accept cash payment</p>
  </div>
</div>


<div class="block">
  <input type="radio" name="city" id="cityd" />
  <label for="cityd"><span>4. How to access my profile</span></label>
  <div class="info">
    <p>You can access the profile on www.desileaks.com\merchants </p>
  </div>
</div>



<div class="block">
  <input type="radio" name="city" id="citye" />
  <label for="citye"><span>5. What are the benefits of creating profile </span></label>
  <div class="info">
    <p>Displaying your profile gives you direct access to the millions users who are visiting Desileaks website and mobile app. You can upload video, photo, services, prices, offers, employee information etc Zoggers customized mobile app made exclusively for merchants. </p>
  </div>
</div>



<div class="block">
  <input type="radio" name="city" id="cityf" />
  <label for="cityf"><span>6. How can I manage crowd on Sat & Sun</span></label>
  <div class="info">
    <p>Desileaks pre booking system can manage not only new customers but also existing customers. This will ensure no waiting, no que etc Customers can book your off-peak hours on Sat and Sun</p>
  </div>
</div>


<div class="block">
  <input type="radio" name="city" id="cityg" />
  <label for="cityg"><span>7. How to manage both offline and online customers</span></label>
  <div class="info">
    <p>Migration of your existing customers to Desileaks will convert customers from offline to online.</p>
  </div>
</div>



<div class="block">
  <input type="radio" name="city" id="cityh" />
  <label for="cityh"><span>8. How to upgrade from listing to partner model</span></label>
  <div class="info">
    <p>You have fill online form and need to send the payment to Desileaks</p>
  </div>
</div>


<div class="block">
  <input type="radio" name="city" id="cityi" />
  <label for="cityi"><span>9. What happens when a customer books through Desileaks</span></label>
  <div class="info">
    <p>You and customer will receive an email or sms as soon as customer book an appointment with you. We will send reminder email or sms to both of you 12 hrs before upcoming appointment. Post appointment we will ask customer to rate and review and will remind them to refer your business with friends.</p>
  </div>
</div>


<div class="block">
  <input type="radio" name="city" id="cityj" />
  <label for="cityj"><span>10. How will I know when a customer books through Desileaks</span></label>
  <div class="info">
    <p>You will receive email or sms from Desileaks as soon as customer books you. You will also receive the notification on your mobile app. You can also check your appointments on your business info page on Desileaks.</p>
  </div>
</div>



<div class="block">
  <input type="radio" name="city" id="cityk" />
  <label for="cityk"><span>11. Can customers pay through Desileaks</span></label>
  <div class="info">
    <p>We don’t have payment gateway system. Customer will pay you directly. We don’t take any kind of commission from merchants.</p>
  </div>
</div>



<div class="block">
  <input type="radio" name="city" id="cityl" />
  <label for="cityl"><span>12. What happens when a customer cancells an appointment</span></label>
  <div class="info">
    <p>we will notify you through email or sms as soon as customer cancels and encourage them to reschedule their appointment.</p>
  </div>
</div>


<div class="block">
  <input type="radio" name="city" id="citym" />
  <label for="citym"><span>13. What are the benefits of promoting on Desileaks</span></label>
  <div class="info">
    <p>Better visibiity either on home page or search page. In few seconds your deals/offers/discounts are displayed to millions of uers. Better utilization of off-peak hours. Desileaks will refer you to the customers in "recommended for you" list</p>
  </div>
</div>



<div class="block">
  <input type="radio" name="city" id="cityn" />
  <label for="cityn"><span>14. How can off-peak pricing help my bizz</span></label>
  <div class="info">
    <p>You can choose "happy hours" to push hard discounts to attrct more customers.</p>
  </div>
</div>





<div class="block">
  <input type="radio" name="city" id="cityp" />
  <label for="cityp"><span>15. Does Desileaks share my customer info with me</span></label>
  <div class="info">
    <p>Of course! In fact you can see not only current appointments but also past and upcoming appointments on Desileaks merchant dashboard. You can see customer name, contact number, address etc</p>
  </div>
</div>



<div class="block">
  <input type="radio" name="city" id="cityq" />
  <label for="cityq"><span>15. Does Desileaks share my customer info with meWhy should my existing customers use Desileaks</span></label>
  <div class="info">
    <p>Desileaks free automated marketing tool will be used to bring back your existing customers. Thank you mail and review request after every successful appointment. Rebook reminder to  customers as per the due period selected by them. Mail "recommended for you" list to customers. Free"Book Us Online" button on your website so customers can book you 24*7.</p>
  </div>
</div>






    </p>
  </section>
    
  <section id="content2">
<div class="block">
  <input type="radio" name="city" id="cityd"/>
  <label for="cityd"><span>1. Lorem Ipsum is simply dummy text?</span></label>
  <div class="info">
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
  </div>
</div>

<div class="block">
  <input type="radio" name="city" id="citye"/>
  <label for="citye"><span>2. Lorem Ipsum is simply dummy text?</span></label>
  <div class="info">
    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
  </div>
</div>
  </section>

 
    

       
       </div>
       
        </div>
    </section>
<?php include 'footer.php'; ?>
<?php include 'login-form.php'; ?>
<?php include 'footer-script.php'; ?>
<script>// when .modal-wide opened, set content-body height based on browser height; 200 is appx height of modal padding, modal title and button bar

$(".modal-wide").on("show.bs.modal", function() {
  var height = $(window).height() - 200;
  $(this).find(".modal-body").css("max-height", height);
});


$( document.body ).on( 'click', '.dropdown-menu li', function( event ) {

   var $target = $( event.currentTarget );

   $target.closest( '.btn-group' )
      .find( '[data-bind="label"]' ).text( $target.text() )
         .end()
      .children( '.dropdown-toggle' ).dropdown( 'toggle' );

   return false;

});
</script>
<script>
	$(function() {
		$( "#slider-range" ).slider({
			range: true,
			min: 0,
			max: 500,
			values: [ 75, 300 ],
			slide: function( event, ui ) {
				$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
			}
		});
		$( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
			" - $" + $( "#slider-range" ).slider( "values", 1 ) );
	});
	$('.date ul li a').tooltip();
	</script>
<script src="<?=URL?>js/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('.carousel').each(function(){
        $(this).carousel({
            pause: true,
            interval: false
        });
    });
});​
</script>
<script src="<?=URL?>js/jcarousellite_1.0.1c4.js" type="text/javascript"></script>

<!--start slider-->

<script type="text/javascript">
$(function() {
	$(".newsticker-jcarousellite").jCarouselLite({
		vertical: true,
		hoverPause:true,
		visible:1,
		auto:600,
		speed:3000
	});
});
</script>


<script type="text/javascript">

	$(document).ready(function()
	{
		var count = 1;
		$("#count_display").html(count);
		var price = <?php echo $price; ?>;
		var total = count*price;
		$("#price_display").html(total);

		$("#total_price").html(total);
	});

	$( "select" ).change(function(){
		var count = $("#count_select option:selected").val();

		$("#count_display").html(count);
		var price = <?php echo $price; ?>;
		var total = count*price;
		$("#price_display").html(total);

		$("#total_price").html(total);

	});

</script>

<script>
  $(function() {
    $( "#datepicker1" ).datepicker({ dateFormat: 'dd-mm-yy' });
    $( "#datepicker2" ).datepicker({ dateFormat: 'dd-mm-yy' });
  });
</script>


<script src="<?=URL?>raty/lib/jquery.raty.js"></script>
<script src="<?=URL?>raty/demo/javascripts/labs.js" type="text/javascript"></script>

<script>
	$('#rating').raty({
		path    : '<?php echo URL ?>raty/lib/images/',
		starOff : 'star-off.png',
		starOn  : 'star-on.png'
	});
</script>


<script type="text/javascript" src="<?=URL?>js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript">$(function(){$('#carousel span').append('<img src="<?=URL?>img/gui/carousel_glare.png" class="glare"/>');$('#thumbs a').append('<img src="<?=URL?>img/gui/carousel_glare_small.png" class="glare"/>');$('#carousel').carouFredSel({responsive: true,circular: false,auto: false,items:{visible: 1,width: 200,height: '56%'},scroll:{fx: 'directscroll'}});$('#thumbs').carouFredSel({responsive: true,circular: false,infinite: false,auto: false,prev: '#prev',next: '#next',items:{visible:{min: 2,max: 4},width: 150,height: '66%'}});$('#thumbs a').click(function(){$('#carousel').trigger('slideTo', '#' + this.href.split('#').pop() );$('#thumbs a').removeClass('selected');$(this).addClass('selected');return false;});});</script><script type="text/javascript">$(document).ready(function(){$("#recent-works2-carousel").owlCarousel({navigation : true, slideSpeed : 300, paginationSpeed : 400, singleItem : false, pagination : false, items : 4, navigationText: [ "<i class='icon-angle-left animation'></i>", "<i class='icon-angle-right animation'></i>"],});});</script>
</body>
</html>
