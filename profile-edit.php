<?php
require_once "chk_login_mer.php";

$objgen		=	new general();
$objval	    =   new validate();

$id 		  = $_SESSION['ma_log_id_mer'];
$result       = $objgen->get_Onerow("merchants","AND id=".$id);
$name         = $objgen->check_tag($result['name']);
$phone        = $objgen->check_tag($result['phone']);
$address       = $objgen->check_tag($result['address']);
$city         = $objgen->check_tag($result['city']);
$email        = $objgen->check_tag($result['email']);
$email_update = $objgen->check_tag($result['email_update']);

if(isset($_POST['update']))
{    
   
   $name 	   		= $objgen->check_input($_POST['name']);
   $phone       	= $objgen->check_input($_POST['phone']);
   $address 	    = $objgen->check_input($_POST['address']);
   $city 	        = $objgen->check_input($_POST['city']);
   $email 	        = $objgen->check_input($_POST['email']);
   $email_update 	= $objgen->check_input($_POST['email_update']);
   
 
   $rules		=	array();
   $rules[] 	= "required,name,Enter the Name";
   $rules[] 	= "required,phone,Enter the Phone";
   $errors  	= $objval->validateFields($_POST, $rules);
   
     $sh_exit = $objgen->chk_Ext("merchants","email='$email' and id <>".$id);
	if($sh_exit>0)
	{
		$errors[] = "This email is already exists.";
	}


   if(empty($errors))
	{
		 			 
	  $msg = $objgen->upd_Row('merchants',"name='".$name."',phone='".$phone."',address='".$address."',city='".$city."',email='".$email."',email_update='".$email_update."'","id=".$id);
	  
	  if($msg=="")
		 {
		   $msg2 = "Profile Modified Successfully.";
		 }

	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>
</head>

<body>
<?php include 'header-dash.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <?php include 'merchant-menu.php'; ?>


<aside class="right-side">
  <section class="content-header-top">
    <h1> <i class="fa fa-suitcase"></i> Edit Profile </h1>
   
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-6">
            <form role="form" action="" method="post" enctype="multipart/form-data" >
            <div class="box padding-both">
			
			   <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
              
              <div class="form-group">
                <label>Merchant Name *</label>
                <input type="text"  class="form-control" name="name" value="<?=$name?>" required />
              </div>
              <div class="form-group">
                <label>Email *</label>
                <input type="text"  class="form-control" name="email" value="<?=$email?>"  required />
              </div>
                <div class="form-group">
                <label>Merchant Address *</label>
                <input type="text"  class="form-control" name="address"  value="<?=$address?>"  required />
              </div>
                <div class="form-group">
                <label>Location/City *</label>
                <input type="text"  class="form-control" name="city" value="<?=$city?>" required />
              </div>
                <div class="form-group">
                <label>Phone Number *</label>
                <input type="text"  class="form-control" name="phone" value="<?=$phone?>"  required/>
              </div>
                
                 <div class="form-group">
                <input type="checkbox"  value="yes" name="email_update" <?php if($email_update=='yes') { ?> checked="checked" <?php } ?> /> Send me occasional email updates
              </div> 
                
                
              <div class="box-footer">
                <button id="submit" class="cl2-green common-btn" name="update" type="submit">Update</button>
              </div>
            </div>
          </form>
        </div>
    </div>
  </section>
</aside>
</div>
<?php include 'footer-script-dash.php'; ?>
</body>
</html>
