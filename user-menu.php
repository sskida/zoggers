
<aside class="left-side sidebar-offcanvas" <?php if( $_SESSION['ma_log_id_usr']) {?>style="    background: none repeat scroll 0 0 white;"<?php }?>>
    <section class="sidebar">
      <div class="side-bg"> 
        <!--<div class="side-btn-top">
        <ul>
          <li class="side-clr-blue" title="Home"><a href="index.php" ><i class="fa-side-color fa fa-home"></i></a></li>
          <li class="side-clr-green" title="Add Project"><a href="add-project-view.php"><i class="fa-side-color fa fa-signal"></i></a></li>
          <li class="side-clr-eblue" title="Add Users"><a href="add-clients.php"><i class="fa-side-color fa fa-users"></i></a></li>
          <li class="side-clr-gray" title="Settings"><a href="profile.php"><i class="fa-side-color fa fa-gears"></i></a></a></li>
        </ul>
      </div>-->
        <div class="user-panel">
          <div class="side-use image"> <i class="fa fa-user fa-5x"></i></div>
          <div class="pull-left info">
            <p>Welcome, <br>
              <span> <?=$_SESSION['ma_name_usr']?> </span></p>
            <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--> </div>
        </div>
        <div id="dash-side" <?php if( $_SESSION['ma_log_id_usr']) {?>style="background: none repeat scroll 0 0 white;"<?php }?>>
          <div class="dash-side-box" <?php if( $_SESSION['ma_log_id_usr']) {?>style="background: none repeat scroll 0 0 white;"<?php }?>><a href="<?=URL?>user-dashboard" style="color:black"><i class="dash-sid-color fa fa-tachometer" style="color:black;"></i><span>Dashboard</span></a>
            <span><?php echo $_SESSION['ma_name_usr'];?> </span>
            <div class="side"></div>
          </div>
        </div>
      </div>
      <div class="add-nav nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_2" data-toggle="tab">Menus</a></li>
        </ul>
        <div class="add-nav tab-content"> 
          
          <!-- /.tab-pane -->
          <div class="tab-pane active" id="tab_2">
            <ul class="sidebar-menu" <?php if( $_SESSION['ma_log_id_usr']) {?>style="background: none repeat scroll 0 0 white;"<?php }?>>
              <li> <a href="<?=URL?>view-appointments-user.php" style="    color: black;"> <i class="fa fa-calendar"></i> <span>View My Appointments</span> </a> </li>
			  <li> <a href="<?=URL?>view-reviews-user.php" style="    color: black;"> <i class="fa fa-suitcase"></i> <span>View My Reviews</span> </a> </li>
			  <li> <a href="<?=URL?>my-favourites.php" style="    color: black;"> <i class="fa fa-heart"></i> <span>My Favourites</span> </a> </li>
              <li> <a href="<?=URL?>view-profile-user.php" style="    color: black;"> <i class="fa fa-user"></i> <span>My Profile</span> </a> </li>
			  <li> <a href="<?=URL?>reset-password-usr.php" style="    color: black;"> <i class="fa fa-cogs"></i> <span>Reset Password</span> </a> </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </aside>