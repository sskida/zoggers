<?php
require_once "chk_login_mer.php";
$objgen		=	new general();

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if(isset($_GET['del']))
{
   $id= $_GET['del'];
   $msg     = $objgen->del_Row("appointments","id=".$id." and mer_id=".$_SESSION['ma_log_id_mer']);
    if($msg=="")
   {
	header("location:".URL."view-appointments.php?msg=3&page=".$page);
   }
}

if($_GET['msg']==3)
{

  $msg2 = "Apointment Deleted Successfully.";
}



$where = " and mer_id=".$_SESSION['ma_log_id_mer'];
$row_count = $objgen->get_AllRowscnt("appointments",$where);
if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array(), 1, WEBLINK."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("appointments",$pagesize*($page-1),$pagesize,"id desc",$where);
}


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>
</head>

<body>
<?php include 'header-dash.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'merchant-menu.php'; ?>
  <aside class="right-side">
    <section class="content-header-top">
      <h1> <i class="fa fa-suitcase"></i> Appointments </h1>
    </section>
    <section class="content">
     <div class="col-xs-12">
        <div class="box-body table-responsive">
			
			   <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Shop Name</th>
                <th>User Details</th>
                <th>Date and Time</th>
				<th>Amount</th>
                <th>Email </th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
				<?php
				if($row_count>0)
				{
					foreach($res_arr as $key=>$val)
				    {
					
					$user_res     	 = $objgen->get_Onerow("users","AND id=".$val['user_id']);
					$shop_res     	 = $objgen->get_Onerow("shop","AND id=".$val['shop_id']);
					
				 ?>
              <tr>
                <td><?php echo $objgen->check_tag($shop_res['shop_name']); ?></td>
                <td>Name : <?php echo $objgen->check_tag($user_res['name']); ?><br />Phone : <?php echo $objgen->check_tag($user_res['phone']); ?><br />City : <?php echo $objgen->check_tag($user_res['city']); ?><br />Gender : <?php echo $objgen->check_tag($user_res['gender']); ?></td>
                
                <td><?php echo date("d-m-Y",strtotime($val['app_date'])); ?><br /><?php echo $objgen->check_tag($val['app_time']); ?></td>
               <td>Quantity : <?php echo $objgen->check_tag($val['quantity']); ?><br />Price : <?php echo $objgen->check_tag($val['quantity']); ?><br />Total Price : <?php echo $objgen->check_tag($val['total_price']); ?></td>
			    <td><?php echo $objgen->check_tag($user_res['email']); ?></td>
              
                <td><a class="arb-icon cl7-light-blue" href="<?=URL?>view-appointments.php?del=<?=$val['id']?>&page=<?=$page?>" onClick="return confirm('Do you want to delete this Appointment?')" ><i class="fa fa-trash-o"></i>Delete</a></td>
              </tr>
			  <?php
			  }
			  }
			  ?>
            </tbody>
          </table>
		  
		  <?php
									if($row_count > $pagesize) 
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">
										
											<div class="dataTables_paginate paging_bootstrap">
											    		
												<?php echo $pages; ?>

												
											</div>
											
									   </div>
									</div>
									<?php
										 }
									?>
									
        </div>
        <!-- /.box-body --> 
        
      </div>
    </section>
  </aside>
</div>
<?php include 'footer-script-dash.php'; ?>
<script src="<?=URL?>js/jquery.dataTables.js" type="text/javascript"></script> 
<script src="<?=URL?>js/dataTables.bootstrap.js" type="text/javascript"></script> 
<script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
</body>
</html>
