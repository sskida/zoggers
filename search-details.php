<?php include "header-front-end.php";
if(isset($_GET['shop_id']) && !empty($_GET['shop_id'])){
   $shop_id =  base64_decode($_GET['shop_id']);
  $shop_sql = $objgen->get_Onerow("shop","AND id='".$shop_id."'");
  $shop_name = $shop_sql['shop_name'];    
  $category_id = $shop_sql['categories'];
  $cat_sql = $objgen->get_Onerow("categories","AND id='".$category_id."'");
  $category_name = $cat_sql['category'];
  $IP = $_SERVER['REMOTE_ADDR'];
  $date = date("Y-m-d");
  if(!empty($_SESSION['ma_log_id_usr'])) {
    $user =$objgen->get_Onerow("users","AND id=".$_SESSION['ma_log_id_usr']);
    
    $name=$user['name'];
    $ff = $objgen->ins_Row('service_visitor_log','category_id,category_name,user_id,user_name,ip_address,date,shop_id,shop_name',"'".$category_id."','".$category_name."','".$_SESSION['ma_log_id_usr']."','".$name."','".$IP."','".$date."','".$shop_id ."','".$shop_name ."'");
  }else{
    $ff = $objgen->ins_Row('service_visitor_log','category_id,category_name,ip_address,date,shop_id,shop_name',"'".$category_id."','".$category_name."','".$IP."','".$date."','".$shop_id ."','".$shop_name ."'");
  }
  $sql_shop = "SELECT s.*,l.location,c.city as city_name, categories.icon,categories.category FROM shop s INNER JOIN categories ON categories.id=s.categories INNER JOIN location l ON l.id=s.locality INNER JOIN city c ON c.id = s.city WHERE s.id='".$shop_id."'";
  $shop_arr = $objgen->get_AllRows_qry($sql_shop);   
}

if (isset($_POST['book_submit'])) {
 
    if ($_SESSION['ma_log_id_usr'] == "" || $_SESSION['ma_usr_name_usr'] == "") {
        header("location:" . URL . "search-details.php?shop_id=" . base64_encode($shop_id) . "");
    } else {
       
        $cc = $_SESSION['book_sub_cat'];
        $edit_id = $_POST['edit_id'];
        $user_id = $_SESSION['ma_log_id_usr'];
        $shop_id = $_SESSION['shop_id'];
        $merchant_id = $_SESSION['merchant_id'];
        $booking_date = $_POST['booking_date'];
        $slots_booked = $_POST['slots_booked'];
        $cost_per_slot = $_POST['cost_per_slot'];
        $guest_count = '1';
        $total_price = $guest_count * $cost_per_slot;
        $explode = explode('|', $slots_booked);

       

        foreach ($explode as $slot) {
            if (strlen($slot) > 0) {
                $msg_booking = $objgen->ins_Row('appointments', 'app_date,app_time,user_id,shop_id,mer_id,quantity,price,
               total_price,subcat_id', "'" . $booking_date . "','" . $slot . "','" . $user_id . "','" . $shop_id . "',
              '" . $merchant_id . "','" . $guest_count . "','" . $cost_per_slot . "','" . $total_price . "','" . $cc . "'");
            $booking_id = $objgen->get_insetId();

            }
        }

        $qry_users = "select * from users where id='" . $user_id . "'";
        $users_user_email = $objgen->get_AllRows_qry($qry_users);
        $user_email = $users_user_email[0]['email'];
        $user_phone = $users_user_email[0]['phone'];
        $user_name = $users_user_email[0]['name'];


        $qry_users_email = "select * from admin where user_type='admin'";
        $ad_email = $objgen->get_AllRows_qry($qry_users_email);
        $admin_email = $ad_email[0]['email'];
        

        $qry_users_e = "select * from merchants where id='" . $merchant_id . "'";
        $users = $objgen->get_AllRows_qry($qry_users_e);
        $merchant_email = $users[0]['email'];
        $merchant_phone = $users[0]['phone'];
        $merchant_name = $users[0]['name'];

        if ($edit_id) {

            
            $msg = $objgen->del_Row("appointments", "id=" . $edit_id . " and user_id=" . $_SESSION['ma_log_id_usr']);
            $update = $objgen->upd_Row("appointments","rescheduled='1'","id=" . $booking_id);

            $subject = "Reschedule Booking Information";


            $message_merchant = "<EOF
                  <html>
                <body>
                  <section>
                    <div style='width:900px;margin-top:23px;'>
                      <div style='position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
                        
                        <div style='background-color:#FFF;position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
                            <div style='padding: 20px;'>
                              <p>Dear " . $merchant_name . ",<p></br>

                              <p>This is to inform you that Mr/Ms. " . $user_name . " needs you.</p></br>

                              <p>A Session has been booked which will be on " . $booking_date . " at " . $slots_booked . ".</p><br>

                              <p>For more details check your account page. You can direct contact them on following details(Contact Details).<br></p>
                              <p>Mobile number - ".$user_phone."</p>
                              <p>Looking forward to your presence.</p></br>

                              <p>Thank you</p>
                              <p>Zoggrs</p>              
                            </div>
                        </div>
                        <div style='width:100%;background-color:#f1f1f1;position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
                            <div style='padding: 20px;text-align:center;'>
                              <h2 style='text-align:center;padding-bottom: 25px;'>Ready to get started?</h2>
                              <a href='#' style='background-color: #e36d69;padding: 10px 80px 10px;color: #FFF;'>Start Exploring!</a>
                            </div>
                        </div>
                        <div style='width:100%;background-color:#FFF;position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
                            <div style='padding: 20px;text-align:center;'>
                              <h2 style='text-align:center;padding-bottom: 25px;'>Get The App!</h2>
                              
                            </div>
                        </div>
                        <div style='width:100%;background-color:#e36d69;position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
                            <div style='padding: 20px;text-align:center;'>
                              <h2 style='color:#FFF;text-align:center;padding-bottom: 25px;'>Love Zoggrs? Connect with us! </h2>
                              <div>
                                <a href='#' style='padding: 6px;'><img style='width: 60px;' src='http://zoggrs.com/img/fb.png'></a>
                                <a href='#' style='padding: 6px;'><img style='width: 60px;' src='http://zoggrs.com/img/twitter.png'></a>
                                <a href='#' style='padding: 6px;'><img style='width: 60px;' src='http://zoggrs.com/img/link.png'></a>
                                <a href='#' style='padding: 6px;'><img style='width: 60px;' src='http://zoggrs.com/img/google.png'></a>
                                <a href='#' style='padding: 6px;'><img style='width: 60px;' src='http://zoggrs.com/img/pri.png'></a>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </body>
                </html>";

                            
                            $message_user = "<EOF
                                  <html>
                <body>
                  <section>
                    <div style='width:900px;margin-top:23px;'>
                      <div style='position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
                        
                        <div style='background-color:#FFF;position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
                            <div style='padding: 20px;'>
                              <p>Hi " . $user_name . ",<p></br>

                              <p>Great!</p></br>

                              <p>Your appointment with " . $merchant_name . " has been scheduled which will be on " . $booking_date . " at " . $slots_booked . ".</p><br>
                              <p>If you have any questions regarding your appointment feel free to contact on +91-0000000000.</p>
                              <p>If for any reason you wish to cancel your appointment, we appreciate a prompt and early notification from your side.</p>
                              <p>Looking forward to your presence.</p></br>

                              <p>Thank you</p>
                              <p>Zoggrs</p>              
                            </div>
                        </div>
                        <div style='width:100%;background-color:#f1f1f1;position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
                            <div style='padding: 20px;text-align:center;'>
                              <h2 style='text-align:center;padding-bottom: 25px;'>Ready to get started?</h2>
                              <a href='#' style='background-color: #e36d69;padding: 10px 80px 10px;color: #FFF;'>Start Exploring!</a>
                            </div>
                        </div>
                        
                      </div>
                    </div>
                  </section>
                </body>
                </html>";  




            $to = "$user_email";
            

            $type = 'HTML'; // or HTML
            $charset = 'utf-8';
            $mail = 'no-reply@' . str_replace('www.', '', $_SERVER['SERVER_NAME']);
            $uniqid = md5(uniqid(time()));
            $headers = 'From: ' . $mail . "\n";
            $headers .= 'Message-ID: <' . $uniqid . '@' . $_SERVER['SERVER_NAME'] . ">\n";
            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Date: ' . gmdate('D, d M Y H:i:s', time()) . "\n";
            $headers .= 'Content-type: text/' . $type . ';charset=' . $charset . '' . "\n";
            
            $x = mail($to, $subject, $message_user, $headers);
            $y = mail($admin_email, $subject, $message, $headers);
            $y = mail($merchant_email, $subject, $message_merchant, $headers);
        } else {

            $_SESSION['booking_done'] = "booking done";

            $subject = "Booking Information";


            $message_merchant = "<EOF
                  <html>
<body>
  <section>
    <div style='width:900px;margin-top:23px;'>
      <div style='position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
        
        <div style='background-color:#FFF;position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
            <div style='padding: 20px;'>
              <p>Dear " . $merchant_name . ",<p></br>

              <p>This is to inform you that Mr/Ms. " . $user_name . " needs you.</p></br>

              <p>A Session has been booked which will be on " . $booking_date . " at " . $slots_booked . ".</p><br>

              <p>For more details check your account page. You can direct contact them on following details(Contact Details).<br></p>
              <p>Mobile number - ".$user_phone."</p>
              <p>Looking forward to your presence.</p></br>

              <p>Thank you</p>
              <p>Fit N Charm</p>              
            </div>
        </div>
        <div style='width:100%;background-color:#f1f1f1;position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
            <div style='padding: 20px;text-align:center;'>
              <h2 style='text-align:center;padding-bottom: 25px;'>Ready to get started?</h2>
              <a href='#' style='background-color: #e36d69;padding: 10px 80px 10px;color: #FFF;'>Start Exploring!</a>
            </div>
        </div>
        
      </div>
    </div>
  </section>
</body>
</html>";

            
            $message_user = "<EOF
                  <html>
<body>
  <section>
    <div style='width:900px;margin-top:23px;'>
      <div style='position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
        
        <div style='background-color:#FFF;position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
            <div style='padding: 20px;'>
              <p>Hi " . $user_name . ",<p></br>

              <p>Great!</p></br>

              <p>Your appointment with " . $merchant_name . " has been scheduled which will be on " . $booking_date . " at " . $slots_booked . ".</p><br>
              <p>If you have any questions regarding your appointment feel free to contact on +91-0000000000.</p>
              <p>If for any reason you wish to cancel your appointment, we appreciate a prompt and early notification from your side.</p>
              <p>Looking forward to your presence.</p></br>

              <p>Thank you</p>
              <p>Fit N Charm</p>              
            </div>
        </div>
        <div style='width:100%;background-color:#f1f1f1;position: relative;min-height: 1px;padding-left: 15px;padding-right: 15px;float: left;'>
            <div style='padding: 20px;text-align:center;'>
              <h2 style='text-align:center;padding-bottom: 25px;'>Ready to get started?</h2>
              <a href='#' style='background-color: #e36d69;padding: 10px 80px 10px;color: #FFF;'>Start Exploring!</a>
            </div>
        </div>
        
      </div>
    </div>
  </section>
</body>
</html>";      


            $to = "$user_email";
            

            $type = 'html'; // or HTML
            $charset = 'utf-8';
            $mail = 'no-reply@' . str_replace('www.', '', $_SERVER['SERVER_NAME']);
            $uniqid = md5(uniqid(time()));
            $headers = 'From: ' . $mail . "\n";

            $headers .= 'Message-ID: <' . $uniqid . '@' . $_SERVER['SERVER_NAME'] . ">\n";
            $headers .= 'MIME-Version: 1.0' . "\n";
            $headers .= 'Date: ' . gmdate('D, d M Y H:i:s', time()) . "\n";

            $headers .= 'Content-type: text/' . $type . ';charset=' . $charset . '' . "\n";

            $x = mail($to, $subject, $message_user, $headers);
            $y = mail($admin_email, $subject, $message_merchant, $headers);
            $y = mail($merchant_email, $subject, $message_merchant, $headers);
        }
        $_SESSION['message'] = 'Thank You for booking With Us.Please Check Your Email.';
        
        header("location:" . URL . "view-appointments-user.php");
    }
}

?>
<div class="row">
  <ul class="breadcrumb2" style="margin-top:120px;">
    <li><a href="#">Home</a></li>
    <li><a href="#"><?php echo $shop_arr[0]['city_name'];?></a></li>
    <li><a href="#"><?php echo ucfirst($shop_arr[0]['location']);?></a></li>
    <li><a href="#"><?php echo ucfirst($shop_arr[0]['category']);?></a></li>
    <li><a href="#"><?php echo ucfirst($shop_arr[0]['shop_name']);?></a></li>
  </ul>
</div>
<div class="row">
  <div class="fade_9"></div>
        <div class="modal_9">
            <img class="loader" src="front-end-images/ajax-loader.gif" />
        </div>
  <div class="col-sm-9 col-md-9 col-lg-9 col-xs-12">
    <div class="content_shadow_c">
    <!-- -------------------- Slider --------------------  -->
    <script type="text/javascript" src="front-end-js/jssor.slider-20.min.js"></script>
<script>
        jssor_1_slider_init = function() {
            
            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,$Zoom:1,$Easing:{$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad},$Opacity:2},
              {$Duration:1000,$Zoom:11,$SlideOut:true,$Easing:{$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,$Zoom:1,$Rotate:1,$During:{$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
              {$Duration:1000,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.8}},
              {$Duration:1200,x:0.5,$Cols:2,$Zoom:1,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:4,$Cols:2,$Zoom:11,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear},$Opacity:2},
              {$Duration:1200,x:0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Left:$Jease$.$Swing,$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
              {$Duration:1000,x:-4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.8}},
              {$Duration:1200,x:-0.6,$Zoom:1,$Rotate:1,$During:{$Left:[0.2,0.8],$Zoom:[0.2,0.8],$Rotate:[0.2,0.8]},$Easing:{$Left:$Jease$.$Swing,$Zoom:$Jease$.$Swing,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$Swing},$Opacity:2,$Round:{$Rotate:0.5}},
              {$Duration:1000,x:4,$Zoom:11,$Rotate:1,$SlideOut:true,$Easing:{$Left:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.8}},
              {$Duration:1200,x:0.5,y:0.3,$Cols:2,$Zoom:1,$Rotate:1,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.7}},
              {$Duration:1000,x:0.5,y:0.3,$Cols:2,$Zoom:1,$Rotate:1,$SlideOut:true,$Assembly:2049,$ChessMode:{$Column:15},$Easing:{$Left:$Jease$.$InExpo,$Top:$Jease$.$InExpo,$Zoom:$Jease$.$InExpo,$Opacity:$Jease$.$Linear,$Rotate:$Jease$.$InExpo},$Opacity:2,$Round:{$Rotate:0.7}},
              {$Duration:1200,x:-4,y:2,$Rows:2,$Zoom:11,$Rotate:1,$Assembly:2049,$ChessMode:{$Row:28},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.7}},
              {$Duration:1200,x:1,y:2,$Cols:2,$Zoom:11,$Rotate:1,$Assembly:2049,$ChessMode:{$Column:19},$Easing:{$Left:$Jease$.$InCubic,$Top:$Jease$.$InCubic,$Zoom:$Jease$.$InCubic,$Opacity:$Jease$.$OutQuad,$Rotate:$Jease$.$InCubic},$Opacity:2,$Round:{$Rotate:0.8}}
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Rows: 2,
                $Cols: 6,
                $SpacingX: 14,
                $SpacingY: 12,
                $Orientation: 2,
                $Align: 156
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 960);
                    refSize = Math.max(refSize, 300);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", $Jssor$.$WindowResizeFilter(window, ScaleSlider));
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            //responsive code end
        };
    </script>

    <style>
        
        
        .jssora05l, .jssora05r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 40px;
            cursor: pointer;
            background: url('img/a17.png') no-repeat;
            overflow: hidden;
        }
        .jssora05l { background-position: -10px -40px; }
        .jssora05r { background-position: -70px -40px; }
        .jssora05l:hover { background-position: -130px -40px; }
        .jssora05r:hover { background-position: -190px -40px; }
        .jssora05l.jssora05ldn { background-position: -250px -40px; }
        .jssora05r.jssora05rdn { background-position: -310px -40px; }

        
        .jssort01-99-66 .p {
            position: absolute;
            top: 0;
            left: 0;
            width: 99px;
            height: 66px;
        }
        
        .jssort01-99-66 .t {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            border: none;
        }
        
        .jssort01-99-66 .w {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
        }
        
        .jssort01-99-66 .c {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 95px;
            height: 62px;
            border: #000 2px solid;
            box-sizing: content-box;
            background: url('img/t01.png') -800px -800px no-repeat;
            _background: none;
        }
        
        .jssort01-99-66 .pav .c {
            top: 2px;
            _top: 0px;
            left: 2px;
            _left: 0px;
            width: 95px;
            height: 62px;
            border: #000 0px solid;
            _border: #fff 2px solid;
            background-position: 50% 50%;
        }
        
        .jssort01-99-66 .p:hover .c {
            top: 0px;
            left: 0px;
            width: 97px;
            height: 64px;
            border: #fff 1px solid;
            background-position: 50% 50%;
        }
        
        .jssort01-99-66 .p.pdn .c {
            background-position: 50% 50%;
            width: 95px;
            height: 62px;
            border: #000 2px solid;
        }
        
        * html .jssort01-99-66 .c, * html .jssort01-99-66 .pdn .c, * html .jssort01-99-66 .pav .c {
            /* ie quirks mode adjust */
            width /**/: 99px;
            height /**/: 66px;
        }
        
    </style>
    <div id="featured">
      <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 960px; height: 480px; overflow: hidden; visibility: hidden; ">
        <!-- Loading Screen -->
        <!-- <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div> -->
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 240px; width: 720px; height: 480px; overflow: hidden;">
            <?php 
            $gal_photos = "SELECT  * FROM photos WHERE shop_id='" . $shop_id . "' AND catgeory='Interior' ORDER BY id DESC";
            $gallary_photos = $objgen->get_AllRows_qry($gal_photos);

            if($gallary_photos) {
              foreach ($gallary_photos as $gall) {   
                $images_folder = "/uploads/large";
                $images_thumb_folder = "/uploads/thumb";
                $photo_file = $gall['image'];
                $thumb_file = $gall['thumb'];

                $large_image_top = $images_folder . "/" . $photo_file;
                $thumb_path = $images_thumb_folder . "/" . $thumb_file;
            ?>
            <div style="display: none;">
                <img data-u="image" src="<?php echo $large_image_top; ?>" />
                <img data-u="thumb" src="<?php echo $thumb_path; ?>" />
            </div>
            <?php } } ?>
        </div>
        <!-- Thumbnail Navigator -->
        <div data-u="thumbnavigator" class="jssort01-99-66" style="position:absolute;left:0px;bottom:0px;top:0px;width:240px;height:480px;" data-autocenter="2">
            <!-- Thumbnail Item Skin Begin -->
            <div data-u="slides" style="cursor: default;">
                <div data-u="prototype" class="p">
                    <div class="w">
                        <div data-u="thumbnailtemplate" class="t"></div>
                    </div>
                    <div class="c"></div>
                </div>
            </div>
            <!-- Thumbnail Item Skin End -->
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora05l" style="top:158px;left:248px;width:40px;height:40px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;" data-autocenter="2"></span>
        
    </div>
    <script>
        jssor_1_slider_init();
    </script>
    </div>        
    <!-- -------------------- Tab -----------------  -->
    <div class="client_brief_info_c">
      <ul class="tab_ul">
        <li class="tab overview_icon active">Overview</li>
        <li class="tab services_icon">Services</li>
        <li class="tab facilities_icon">Facilities</li>
        <li class="tab contact_icon">Contact</li>
        <li class="tab reviews_icon">Reviews</li>
        <li class="tab right_border portfolio_icon">Portfolio</li>
      </ul>
      <div class="clear"></div>
      <div class="client_brief_info overview">
        
        <p><?php echo ucfirst($shop_arr[0]['overview']);?> </p>
        
      </div>
      <div class="client_brief_info services">
        
         <div class="booking-block">
            
            <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 ">
              <form name="booking" id="booking-form" method="get" action="" enctype="application/x-www-form-urlencoded">
                <div class="panel panel-default" style="margin-right: 28px !important; margin-left: 20px !important;">
 
                    <!-- Default panel contents -->
                      <?php 
                        $sqla_groups = "SELECT distinct g.* FROM 
                        groups g INNER JOIN shop_categories sc 
                        ON sc.group_id=g.id  WHERE 
                        sc.shop_id='".$shop_id."' ";
                        $resulta_groups = $objgen->get_AllRows_qry($sqla_groups);
                        
                    foreach ($resulta_groups as $groupa) { ?>
                    <div class="panel-heading group" style="background-position: 96.4% 50%; "><?php echo $groupa['group_title']?></div>
                    <div class="groups" style="display:none;">
                    <ul class="list-group" style="margin-bottom: 0px !important;">
                        <?php
                          $sqla_subcat = "SELECT distinct sc.id,ssc.cat_id, sc.subcategory,ssc.price as 
                          ssc_price,ssc.time as ssc_time,ssc.offers as 
                          ssc_offers,ssc.months as ssc_months FROM subcategories sc INNER 
                          JOIN shop_categories ssc ON ssc.sub_cat=sc.id WHERE sc.group_id 
                          ='".$groupa['id']."' AND shop_id='".$shop_id."'"; 
                          $resulta_subcat = $objgen->get_AllRows_qry($sqla_subcat);
                          foreach ($resulta_subcat as $subcata) { ?> 
                          <li class="list-group-item row" style="margin: 0px;">
                              <div class="col-xs-12 col-sm-4 col-md-4 ">
                                <?php echo $subcata['subcategory']?>
                              </div>
                              <div class="col-xs-12 col-sm-2 col-md-2 ">
                                <?php echo '&#x20B9; '.$subcata['ssc_price']?>
                              </div>
                              <?php if($subcata['cat_id']==15) {?>
                              <div class="col-xs-12 col-sm-2 col-md-2 ">
                                <span class="service_duration"><?php echo $subcata['ssc_months'].' months'?></span>
                              </div>
                              <?php } else { ?>
                              <div class="col-xs-12 col-sm-2 col-md-2 ">
                                <span class="service_duration"><?php echo $subcata['ssc_time'].' min'?></span>
                              </div>
                              <?php } ?>
                              <div class="col-xs-12 col-sm-3 col-md-3 ">
                                <?php if($subcata['ssc_offers']) {?>
                                <i class="fa fa-tags"></i>
                              <?php }?>
                                <?php echo $subcata['ssc_offers']?>
                                
                              </div>
                              <div class="col-xs-12 col-sm-1 col-md-1 ">
                                <div class="material-switch pull-right">
                                  <input type="radio" name="book_sub_cat" value="<?php echo $subcata['id'] ?>" >
                                </div>
                              </div>
                            </li>
                      <?php } ?>
                    </ul>
                  </div>
                  <?php } ?>
                  <input type='hidden' name='model' value='yes'>
                    <input type='hidden' name='shop_id' value='<?php echo base64_encode($shop_id); ?>'>  
                    <button type="submit" style="margin-top:10px;margin-left: 24em;" class="btn btn-primary" id="instant_book" name="book_submit"> Book Now</button>
                    
                    </div>
                    </form>
                        
                        
                </div> 
            </div>
        </div>      
      </div>

        <div class="client_brief_info">
          
          <table class="tbl client_tbl tbl_border" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <th width="10%">&nbsp;</th>
              <th>Facilities</th>
              <th>Available</th>
              <th>Unavailable</th>
            </tr>
            <?php 
                $sql_shop_facility = "SELECT facility_id FROM shop_facilities WHERE shop_id='".$shop_arr[0]['id']."' 
                  AND  cat_id='".$shop_arr[0]['categories']."'"; 
              $shop_facility_ids = $objgen->get_AllRows_qry($sql_shop_facility); 
              $getFaclityChk = array( );
                        foreach ($shop_facility_ids as $key => $value) {
                      $subArr2 = $value['facility_id'];
                array_push($getFaclityChk,$subArr2);
              }
              $ff= "SELECT * FROM facilities WHERE cat_id='".$shop_arr[0]['categories']."'";
              $facility =$objgen->get_AllRows_qry($ff);
              $facilityCount=1;
            ?>
            <?php foreach ($facility as $fact) {
            ?>
            <tr>
              <td ><span class="<?php echo $fact['icon']; ?> fa-lg icon"></span></td>
              <td><?php echo $fact['facility']; ?></td>
              <td>
                <?php
                if(in_array($fact['id'], $getFaclityChk)) { ?>
                  <span class="available_sign"></span>
                <?php } else { ?> 
                &nbsp; 
                <?php } ?>
              </td>
              <td>
                <?php
                if(!in_array($fact['id'], $getFaclityChk)) { ?>
                  <span class="available_not_sign"></span>
                <?php } else { ?>
                  &nbsp;
                <?php } ?>
              </td>
            </tr>
            <?php }?>
                
          </table>
        </div>
        <div class="client_brief_info contact">
          
          <div class="address_c left">
            <h2 style="color: hsl(338, 80%, 71%) !important;font-size: 15px;margin-bottom: 12px;"><b> ADDRESS</b></h2>
            <p> <?php echo $shop_arr[0]['building_name'];?>,<br />
                <?php echo $shop_arr[0]['street'];?>, <br />
              <?php echo $shop_arr[0]['landmark'];?>, <br />
              <?php echo $shop_arr[0]['location'] . ',' .$shop_arr[0]['city_name'] ." - ".$shop_arr[0]['pincode'];?>
            </p>
            
          </div>
          <div  class="sttle-11 right">
            <?php
            $sql_gym =$objgen->get_Onerow("gym","AND shop_id=".$shop_arr[0]['id']);  ?>
              <h2><b>OPENING HOURS</b></h2>
              <li><span>MON - FRI</span><?php echo $sql_gym['work_morn_from'] . " - " . $sql_gym['work_morn_to'] ;?></li>
              <h2><b>BREAK HOURS</b></h2>
              <li><?php echo $sql_gym['work_even_from'] . " - " . $sql_gym['work_even_to'] ;?></li>
              <li><span><?php echo $sql_gym['week_off'];?></span>Closed</li>
          </div>
          <div class="map_c right">
            <?php
            $address= $shop_arr[0]['building_name'] . ' , ' . $shop_arr[0]['street'] . ' , ' . $shop_arr[0]['landmark']. ' , ' . $shop_arr[0]['location']. ' , ' . $shop_arr[0]['city_name'];
            $address = str_replace(" ", "+", $address);
            $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
            $json = json_decode($json);
            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};?>
          
            <iframe src="https://maps.google.com/maps?q=<?php echo $lat;?>,<?php echo $long;?>&z=17&t=h&output=embed" width="100%" height="450" frameborder="0" style="border:0"></iframe>
                
          </div>
          <div class="clear"></div>
        </div>
            
                    
        <div class="client_brief_info reviews">
          
          <div class="review_rit_sort_c">
              <input type="button" style="font-size: 1em; border-style: none solid none none; border-right-width: 1px; border-right-color: rgb(216, 216, 216); height: 42px;  margin-top: 0px; border-radius: 0px; box-shadow: none; margin-bottom: 0px !important; padding-left: 15px !important; " class="review_write_btn    write_icon left btnreview" value="Write a review">

                <input type="button" style="font-size: 1em; border-style: none solid none none; border-right-width: 1px; border-right-color: rgb(202, 202, 202); height: 43px;  border-radius: 0px; box-shadow: none; margin-top: -1px; margin-bottom: 0px !important; background: rgba(0, 0, 0, 0);" class="close_rating_write_c   left btnreview" value="Close review">
              
             
            
            <ul class="cost_rating_poulatiry_ul  right">


              <li><span class="desc">Rating</span></li>
              <li><span class="desc">Date</span></li>

            </ul>
            <div class="clear"></div>
          </div>
              
          
            <style type="text/css">
              .star-rating {
                font-size: 0;
                white-space: nowrap;
                display: inline-block;
                width: 250px;
                height: 50px;
                overflow: hidden;
                position: relative;
                background: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjREREREREIiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
                background-size: contain;
              }
              .star-rating i {
                opacity: 0;
                position: absolute;
                left: 0;
                top: 0;
                height: 100%;
                width: 20%;
                z-index: 1;
                
                background-image: url('data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IiB3aWR0aD0iMjBweCIgaGVpZ2h0PSIyMHB4IiB2aWV3Qm94PSIwIDAgMjAgMjAiIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDIwIDIwIiB4bWw6c3BhY2U9InByZXNlcnZlIj48cG9seWdvbiBmaWxsPSIjYjUyMjU4IiBwb2ludHM9IjEwLDAgMTMuMDksNi41ODMgMjAsNy42MzkgMTUsMTIuNzY0IDE2LjE4LDIwIDEwLDE2LjU4MyAzLjgyLDIwIDUsMTIuNzY0IDAsNy42MzkgNi45MSw2LjU4MyAiLz48L3N2Zz4=');
                background-size: contain;
              }
              .star-rating input {
                -moz-appearance: none;
                -webkit-appearance: none;
                opacity: 0;
                display: inline-block;
                width: 20%;
                height: 100%;
                margin: 0;
                padding: 0;
                z-index: 2;
                position: relative;
              }
              .star-rating input:hover + i,
              .star-rating input:checked + i {
                opacity: 1;
              }
              .star-rating i ~ i {
                width: 40%;
              }
              .star-rating i ~ i ~ i {
                width: 60%;
              }
              .star-rating i ~ i ~ i ~ i {
                width: 80%;
              }
              .star-rating i ~ i ~ i ~ i ~ i {
                width: 100%;
              }
              .choice {
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                text-align: center;
                padding: 20px;
                display: block;
              }
            </style>
            <script type="text/javascript">
              $(':radio').change(  function(){

                $('.choice').text( this.value + ' stars' );
              } )
            </script>
            
              <link rel="stylesheet" type="text/css" href="<?=URL?>front-end-css/colorbox.css">
            
            <div class="rating_write_c" style="margin-bottom: 10px;">
            <div id="view_review">
              <div class="fade_1"></div>
                <div class="modal_1">
                    <img class="loader" src="front-end-images/ajax-loader.gif" />
                </div>
            <div class="rating_write_caption">
              <h2>Write a review</h2>
            </div>
              <div class="review_popup_con" style="text-align:center;">
                <span class="star-rating">
                  <input type="radio" name="rating" value="1"><i></i>
                  <input type="radio" name="rating" value="2"><i></i>
                  <input type="radio" name="rating" value="3"><i></i>
                  <input type="radio" name="rating" value="4"><i></i>
                  <input type="radio" name="rating" value="5"><i></i>
                </span>
                  
              </div>
              <div class="tag_service_c">
                <h4 style="margin-left: 10px; color: #737171;">Tag Service</h4>                      
                <div class="" id="c_b">
                  
                  <?php 
                  $sql_groups = "SELECT distinct g.* FROM 
                  groups g INNER JOIN shop_categories sc 
                  ON sc.group_id=g.id  WHERE 
                  sc.shop_id='".$shop_id."' ";
                    $result_groups = $objgen->get_AllRows_qry($sql_groups);
                    foreach ($result_groups as $group) {?>
                    <div class="col-md-2 border-1 textarea_c" style="width: auto;" >
                    <input class="form-class group_check" type="checkbox" name="group[]" value="<?php echo $group['id']?>">
                    <label style="font-weight: 400 !important;"><?php echo $group['group_title']?></label>   
                    </div>
                    <?php }

                  ?>

                </div>
              </div>
              <div class="clear"></div>
              <div class="tag_service_c">                     
                  <h4 style="margin-left: 10px;padding-top: 10px;color: #716F6F;">Description</h4>
                  <div class="border-1 textarea_c">
                    <textarea class="textarea" name="description" style=" border: 1px solid #CACACA;" cols="4"  placeholder="What would you tell your friends about the experience? Give us all the details- what you loved, what could have been better and anything that took you by surprise. Remember to please keep it clean."></textarea>
                  </div>
                
              </div>
              
              <input type="hidden" id="hidden_user_id" name="user_id" value="<?php echo $_SESSION['ma_log_id_usr']; ?>"/>
              <input type="hidden" name="shop_id_user" value="<?php echo $shop_id; ?>"/> 
              <a style="font-size: 1em; border: 1px solid #808080 !important; width:150px; color: #FFF !important;" class="btn_post" href="#" id="post_review">Post Review</a>              
              
            
            <div class="clear"></div>
          </div>
          </div>
            <style>
              .user_ratings ul{margin:0;padding:0;}
              .user_ratings li
              {
                cursor:pointer;
                list-style-type: none;
                display: inline-block;
                color: #F0F0F0;
                text-shadow: 0 0 1px #666666;
                font-size:20px;
                margin-top: -5px;
                }
              .user_ratings .highlight, .user_ratings .selected {color:hsl(338, 81%, 71%);text-shadow: 0 0 1px hsl(338, 81%, 71%);}
            </style>
            <div id="sorting">
            <?php
              if($_SESSION['ma_log_id_usr']!=""){
                $sql_review = "SELECT r.*,u.name,u.city FROM review r INNER JOIN users u ON u.id=r.user_id  WHERE r.shop_id='".$shop_arr[0]['id']."' AND r.user_id='".$_SESSION['ma_log_id_usr']."'  ORDER BY id DESC";
              }else{ 
                  $sql_review = "SELECT r.*,u.name,u.city FROM review r INNER JOIN users u ON u.id=r.user_id  WHERE r.shop_id='".$shop_arr[0]['id']."'ORDER BY id DESC";
              }
              $result_review = $objgen->get_AllRows_qry($sql_review);
              if($result_review) { 
              foreach ($result_review as $review) {
              ?>
              <div class="user_review_c margin_bottom-5">
                <div class="fade_1"></div>
                <div class="modal_1">
                    <img class="loader" src="front-end-images/ajax-loader.gif" />
                </div>
              <div class="user_photo_name_c margin_bottom-5">
                <img class="review_user_photo left" src="front-end-images/well-login_and_access_icon.png" width="81" height="81" alt="User Photo" />
                <div class="user_name left margin_left-1 margin_top-1">
                  <div class="name_add left">
                    <h2 style="margin-top:0px !important;">Mrs.<?php echo ucfirst($review['name']); ?></h2>
                    <p><?php echo ucfirst($review['city']); ?></p>
                  </div>
                  <div class="left  margin_left-5" style="border-right: 1px solid #DDD;padding-right: 10em;">
                    <p>Date: <?php echo date('d M Y',strtotime($review['created_date'])); ?></p>
                    <div class="user_ratings">
                      <p class="left margin_right-3">Rating: </p>
                      <ul class="star_rating_ul left margin_top-2" >
                        <?php
                        for($i=1;$i<=5;$i++) {
                          $selected = "";
                            if(!empty($review["star"]) && $i<=$review["star"]) {
                            $selected = "selected";
                            }
                            ?>
                            <li class='<?php echo $selected; ?>' >&#9733;</li>  
                        <?php }  ?>
                      </ul>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <?php if($_SESSION['ma_log_id_usr']!=""){ ?>
                    <div class="left  margin_left-5" style="padding-top: 10px;">
                      <a href="#" id="<?php echo $review['id']?>" style="color:black;font-size: 13px;" onclick="get_review(this.id)" class="btnuser">EDIT</a> 
                      <a href="#" id="<?php echo $review['id']?>" style="color:black;font-size: 13px; " onclick="delete_review(this.id,<?php echo $shop_arr[0]['id']; ?>)" class="btnuser">DELETE</a>
                    </div>                    
                    <div class="clear"></div>
                  <?php } ?>
                </div>
                <div class="clear"></div>
              </div>
              <p class="review_p"><?php echo ucfirst(trim($review['long_desc'])); ?></p>
                <?php 
                  $groupIds= explode(',',$review['group_id']);

                  for($i=0; $i<count($groupIds);$i++){
                    
                    $gruop_title = $objgen->get_Onerow("groups","AND id=".$groupIds[$i] );
                    ?>
                     <div class="service-tag"><a href="#"><?php echo ucfirst($gruop_title['group_title']); ?></a></div>
                <?php }?></div>
            <?php   }  } else {?>
              <div class="user_review_c margin_bottom-5">
            <p class="review_p" style="text-align:center">Does not have a review yet.</p>
            </div>
            <?php }
              ?>
          </div>
        </div>
        <div class="client_brief_info Portfolio">
          
          <div class="address_c ">
            <?php 
            $qry_locations="select * from portfolio where shop_id='".$shop_arr[0]['id']."'";
              $res_arr = $objgen->get_AllRows_qry($qry_locations);
              if($res_arr) { 
            ?>
            <?php foreach ($res_arr as $img_val) {?>
            <ul class="thumbnails list-unstyled">
                <li class="col-md-4">
                    <div class="thumbnail" style="padding: 0">
                        <div style="padding:4px">
                          <a class="group1" href="<?=URLAD?>/uploads/<?php echo $img_val['image']?>"><img style="height: 129px;width:100%"src="<?=URLAD?>/uploads/<?php echo $img_val['image']?>"></a>
                        </div>
                        
                
                    </div>
                </li>
            </ul>


             <?php }?> 
                <?php }else{ ?>
                <div class="col-md-12">
                  <h1>Does not have a portfolio yet</h1>
                </div>
                <?php } ?>  
              
            
          </div>
          
           
          <div class="clear"></div>
        </div>
      </div>
            
      </div>
  </div>


  
    <div class="sml_col col-sm-3 col-md-3 col-lg-3 col-xs-12">
    <div class="padding-5">
      
      <div class="client_rating_c">
        <div class="client_name_add_c center_align">
          <h3><b><?php echo ucfirst($shop_arr[0]['shop_name']);?></b></h3>  
          <p class="searched_location"><?php echo ucfirst($shop_arr[0]['location']);?>, <?php echo ucfirst($shop_arr[0]['city_name']);?></p>
          <div class="specification_c">
            
                <i class="<?php echo $shop_arr[0]['icon'];?> fa-lg" style="margin-right: 5px;"></i>
                <?php echo ucfirst($shop_arr[0]['category']);?>
            
          </div>
          <div class="clear"></div>               
        </div>
        <div class="client_rating left">
        <?php 
            $sql_review ="SELECT * FROM review WHERE shop_id='".$shop_arr[0]['id']."'";
            $result_review= $objgen->get_AllRows_qry($sql_review);

             $count = count($result_review);
             $stars = 0;
             foreach ($result_review as $review) {
              $stars=$stars+$review['star'];
             }
             
             if($count>0) {
               $avg1 = ($stars/$count);

             $avg=number_format($avg1, 2, '.', '');
              if($avg<2){
                $result = fmod($avg,2);
                if($result<5){
                  $message="Average";
                 }elseif($result>5){
                  $message="Above average";
                 }
              } elseif ($avg<3) {
                $result = fmod($avg,3);
                if($result<5){
                   $message="Good";
                 }elseif($result>5){
                   $message="Very good";
                 }
              }elseif ($avg<4) {
                $result = fmod($avg,4);
                 if($result<5){
                   $message="Good";
                 }elseif($result>5){
                   $message="Very good";
                 }
              }elseif ($avg=5) {
                $result = fmod($avg,5);
                if($result<5){
                   $message="Superb";
                 }elseif($result>5){
                   $message="Very good";
                 }
              }else{
                 $message="Poor";
              }

             
            } else {
              $avg=0;
              $message ="Below Average";
            }
            
              
          ?>
          <p>Rating</p>
          <h1><?php if($count==0){ echo $avg=0; }else {echo substr(number_format($avg, 2),0,3);}?></h1>
          <div class="clear"></div>
            
        </div>
        <div class="client_pert_rating right">
        
          <div class="user_ratings">
            
            <ul class="star_rating_ul left margin_top-2" >
              <?php
              for($i=1;$i<=5;$i++) {
                $selected = "";
                  if(!empty($avg) && $i<=$avg) {
                  $selected = "selected";
                  }
                  ?>
                  <li class='<?php echo $selected; ?>' >&#9733;</li>  
              <?php }  ?>
            </ul>
            <div class="clear"></div>
          </div>
        
          <div class="clear"></div>
          
          <h4><b><?php echo $message; ?></b></h4>
        </div>
        <div class="clear"></div>
      </div>
      <div class="link_c">
        
        <h4 class="write_icon review_btn" ><b>Review</b></h4>
      </div>
      
    </div>
   
    <div class="">
      <a href="#" style="width: 96%; margin-left: 6px !important;" class="btn btn-primary booking_btn">Book a service</a>
      
      <!-- <h4>Generate Offer</h4> -->
    </div>
    <div class="popup_c generate_offer_popup_c">
    <div class="close"></div>
    
      <img src="front-end-images/logo.png" alt="logo" />
        <div class="popup_con center_align">
          <div class="offer_code">
            <h4>ACHER5265</h4>
          </div>
          <p>Show this code at the venue</p>
        </div>
    </div>
    <div class="client_add_rating_c border-1">
      <div class="left border-right-1">
      <?php 
        if(empty($_SESSION['ma_log_id_usr'])) {?>
            <span class="rating_span add_d fave_<?php echo $shop_arr[0]['id']?>">
              <a href="#" id="<?php echo $shop_arr[0]['id']?>" onclick="add_favuorite(<?php echo $shop_arr[0]['id']?>,0)" style="color: black;">
                <span>&nbsp;</span>
                <span class="deco" id="deco_<?php echo $shop_arr[0]['id']?>" style="margin-top: .95em !important;">Add to list </span>
              </a>
            </span>
        <?php }else{ 
          $favourite=$objgen->chk_Ext("favourites","shop_id='".$shop_arr[0]['id']."' AND user_id='".$_SESSION['ma_log_id_usr']."'");
            if($favourite>0) {?>
                <span class="rating_span added_d fave_<?php echo $shop_arr[0]['id']?>">
                  <a href="#" id="<?php echo $shop_arr[0]['id']?>"  onclick="add_favuorite(<?php echo $shop_arr[0]['id']?>,<?php echo $_SESSION['ma_log_id_usr'];?>)" style="color: black;">
                    <span>&nbsp;</span>
                    <span class="deco" id="deco_<?php echo $shop_arr[0]['id']?>" style="margin-top: .95em !important;">Added to list </span>
                  </a>
                </span>
            <?php }else{?>
                <span class="rating_span add_d fave_<?php echo $shop_arr[0]['id']?>">
                    <a href="#" id="<?php echo $shop_arr[0]['id']?>" onclick="add_favuorite(<?php echo $shop_arr[0]['id']?>,<?php echo $_SESSION['ma_log_id_usr'];?>)" style="color: black;">
                      <span>&nbsp;</span>
                      <span class="deco" id="deco_<?php echo $shop_arr[0]['id']?>" style="margin-top: .95em !important;">Add to list </span>
                    </a>
              </span>
          <?php } } ?>
      </div>
      <div class="left">
        <span class="rating_span">
        <?php
         $shop_review = $objgen->get_AllRowscnt("review","AND shop_id='".$shop_arr[0]['id']."'");
        ?>
          <span class="rating_bg"><?php echo $shop_review; ?></span>
          <span>Reviews</span>
        </span>   
      </div>  
    <div class="clear"></div>
    </div>
    <div class="visited_user_c">
      <h4 class="center_align"><b>More Options nearby</b></h4>
      <?php
        $session_categories = $_SESSION['cat'];
        $lat =$shop_arr[0]['lattitude'];
        $long =$shop_arr[0]['longitude']; 
       $sql = "SELECT id,shop_name,lattitude,longitude, ( 3959 * acos( cos( radians('" . $lat . "') ) * cos( radians( lattitude ) ) * cos( radians( longitude ) - radians('" . $long . "') ) + sin( radians('" . $lat . "') ) * sin( radians( lattitude ) ) ) ) AS distance FROM shop WHERE categories IN ($session_categories) HAVING distance < 25 ORDER BY distance LIMIT 0 , 5";
        $result = $objgen->get_AllRows_qry($sql);
        foreach ($result as $result_shop) {
          $sid = $result_shop['id'];

          $res_photo = $objgen->get_Onerow("photos","AND shop_id='".$sid."' AND catgeory='Interior' AND set_profile_pic='true'");
        if($sid!=$shop_arr[0]['id']) {
      ?>  
      <div class="visited_user">
        <a href ="<?=URL?>search-details.php?shop_id=<?php echo base64_encode($sid);  ?>"><img src="uploads/thumb/<?php echo $res_photo['thumb'];?>" width="100%" class="left" alt="" /></a>
        <div class="visited_user_nm_add">
          <a href ="<?=URL?>search-details.php?shop_id=<?php echo base64_encode($sid);  ?>" style="color:black;"><h5><b><?php echo $result_shop['shop_name']; ?></b></h5></a>
          <?php
            $sql_loc ="SELECT l.*,c.city as shop_city FROM location l INNER JOIN shop s ON s.locality=l.id INNER JOIN city c ON c.id=s.city WHERE s.id='".$sid."'";
            $loc_result = $objgen->get_AllRows_qry($sql_loc);
            foreach ($loc_result as $locality_shop) {
          ?>
            <p class="searched_location"><?php echo $locality_shop['location'].', '. $locality_shop['shop_city'];?></p>
          <?php } ?>
        </div>
        <?php
          $sql_rating ="SELECT * FROM review WHERE shop_id='".$sid."'"; 
          $rating_result = $objgen->get_AllRowscnt_qry($sql_rating);
          
        ?>
        <div class="visited_user_rating"><?php echo $rating_result;?></div>
        <div class="clear"></div>
      </div>
      <?php } }
      ?>
    </div>        
  </div>
</div>    

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="text-align:center" id="myModalLabel">Book appointment</h4>
      </div>
      <div class="fade_7"></div>
        <div class="modal_7">
            <img class="loader" src="front-end-images/ajax-loader.gif" />
        </div>
      <div class="modal-body">

       <?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

if (isset($_GET['book_sub_cat'])) {

$mArr = array();
$sub_cat = $_GET['book_sub_cat'];

$qry_cat = "select * from shop_categories where sub_cat=$sub_cat and shop_id=$shop_id";
$subcat_row = $objgen->get_AllRows_qry($qry_cat);
$subcat_row[0]['price'];
$subcat_row[0]['time'];
$price_amount = $price_amount + $subcat_row[0]['price'];
$time = $time + $subcat_row[0]['time'];

$qry_shop = "select * from shop where id=$shop_id";
$shop_rows = $objgen->get_AllRows_qry($qry_shop);
$shop_id = base64_decode($_GET['shop_id']);
$edit_id = $_GET['edit_id'];
$_SESSION['book_sub_cat'] = $_GET['book_sub_cat'];
$_SESSION['work_start_time'] = $shop_rows[0]['work_start_time'];
$_SESSION['work_end_time'] = $shop_rows[0]['work_end_time'];
$_SESSION['edit_id'] = $edit_id;
$_SESSION['booktime'] = $time;
$_SESSION['bookprice'] = $price_amount;
$_SESSION['shop_id'] = $shop_id;
$_SESSION['merchant_id'] = $shop_rows[0]['merchant_id'];
//echo $time.'----'.$price;
$qry_gym = "select * from gym where shop_id=$shop_id";
$gym_rows = $objgen->get_AllRows_qry($qry_gym);
$_SESSION['week_off'] = $gym_rows[0]['week_off'];
$_SESSION['break_start_time'] = $gym_rows[0]['work_even_from'];
$_SESSION['break_end_time'] = $gym_rows[0]['work_even_to'];
}

if (isset($_GET['month']))
$month = $_GET['month'];
else
$month = date("m");
if (isset($_GET['year']))
$year = $_GET['year'];
else
$year = date("Y");
if (isset($_GET['day']))
$day = $_GET['day'];
else
$day = 0;
// Unix Timestamp of the date a user has clicked on
$selected_date = mktime(0, 0, 0, $month, 01, $year);
// Unix Timestamp of the previous month which is used to give the back arrow the correct month and year 
$back = strtotime("-1 month", $selected_date);
// Unix Timestamp of the next month which is used to give the forward arrow the correct month and year 
$forward = strtotime("+1 month", $selected_date);
?>
<link href="<?= URL ?>style.css" rel="stylesheet" type="text/css">
<?php
if ($_GET['model'] == 'yes') {


include('classes/class_calendar.php');
$calendar = new booking_diary();    // Call calendar function
$calendar->make_calendar($selected_date, $back, $forward, $day, $month, $year,$ajax="false");
}
?>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>




<div class="login_popup_c radius_corner_6" id="user_login_popup_check">
                <div class="pop_up_close"></div>
                <h2 class="popup_caption">Login to Zoggrs</h2>
                <div class="err" id="user_err_check"></div>   
                <div class="hr"></div>
                <div class="left_div">
                    <form method="post" name="user_login" >
                            <div class="lbl_input_c">
                                <div class="lbl_c">Email Address</div>
                                <div class="input_c">
                                    <input type="email" id="username_check" required name="email_login" value="" placeholder="Enter your email id" />                            
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="lbl_input_c">
                                <div class="lbl_c">Password</div>
                                <div class="input_c">
                                    <input type="password" id="pass_check" required name="password_login" value="" placeholder="Enter your password" />                            
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="lbl_input_c">
                                <div class="lbl_c">&nbsp;</div>
                                <div class="input_c"><input style="width: 250px !important;" type="submit" id="user-login-check" name="user_login" value="Submit" /></div>
                                <div class="clear"></div>
                            </div>
                            <div class="lbl_input_c" style="padding-top:0;padding-bottom:0;">
                                <div class="lbl_c"></div>
                                <div class="input_c"><span class="forgot_pass" id="user_forgot_pass">Forgot password?</span></div>
                                <div class="clear"></div>
                            </div>
                            <div class="fade_1"></div>
                            <div class="modal_1">
                                <img class="loader" src="front-end-images/ajax-loader.gif" />
                            </div>
                    </form>
                </div>
                <div class="left" style="width: 8%;text-align: center;padding-top: 5.6em;">OR</div>
                <div class="right_div">
                    <div class="link_c">
                        <a class="fb" href="#">Connect With Facebook</a>
                        <a class="google" href="#">Connect With Google</a>
                    </div>
                    <div class="checkBox_c">
                        <div>
                            <input id="checkbox1" type="checkbox" name="checkbox" value="1" checked="checked"><label for="checkbox1">We never post without your permission</label>
                        </div>
                        <div>
                            <input id="checkbox2" type="checkbox" name="checkbox" value="2" checked="checked"><label for="checkbox2">Gain access to various offers and benefits</label>
                        </div>
                        <div>
                            <input id="checkbox3" type="checkbox" name="checkbox" value="3" checked="checked"><label for="checkbox3">Get added flexibility to track your options </label>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
        </div>

 <style type="text/css">
.fade_1{
    display: none;
    position:absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #ababab;
    z-index: 1001;
    -moz-opacity: 0.8;
    opacity: .70;
    filter: alpha(opacity=80);
}

.modal_1 {
    display: none;

    position: absolute;
    top: 25%;
    left: 45%;
    width: 130px;
    height: 130px;
    padding: 30px 15px 0px;
    border-radius: 20px;
    background-color: white;
    z-index: 1002;
    text-align: center;
    overflow: auto;
}

.results_1 {
    font-size:1.25em;
    color:red
}
.fade_7{
    display: none;
    position:absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #ababab;
    z-index: 1001;
    -moz-opacity: 0.8;
    opacity: .70;
    filter: alpha(opacity=80);
}

.modal_7 {
    display: none;

    position: absolute;
    top: 25%;
    left: 45%;
    width: 130px;
    height: 130px;
    padding: 30px 15px 0px;
    border-radius: 20px;
    background-color: white;
    z-index: 1002;
    text-align: center;
    overflow: auto;
}

.results_7 {
    font-size:1.25em;
    color:red
}
.fade_9{
    display: none;
    position:absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #ababab;
    z-index: 1001;
    -moz-opacity: 0.8;
    opacity: .70;
    filter: alpha(opacity=80);
}
.modal_9 {
    display: none;

    position: absolute;
    top: 25%;
    left: 45%;
    width: 130px;
    height: 130px;
    padding: 30px 15px 0px;
    border-radius: 20px;
    background-color: white;
    z-index: 1002;
    text-align: center;
    overflow: auto;
}

.results_9 {
    font-size:1.25em;
    color:red
}
        </style>
<?php
  if ($_GET['model'] == 'yes') { 

    if ($_SESSION['ma_log_id_usr'] == "" || 
      $_SESSION['ma_usr_name_usr'] == "") { 
      //echo'sasasas';exit; ?>
      <script type="text/javascript">
      $.session.set("myVar", 'normal_login');
      $(".overlay").fadeIn(300);
        $("#user_login_popup_c").delay(200).fadeIn(300);
        $(".pop_up_close").click(function(){    
        $(this).parent().fadeOut(300);
        $(".overlay").delay(200).fadeOut(300);
      });
      </script>                     
  <?php } else {?>
      <script type="text/javascript">
      $('#myModal').modal('show')
      </script>
  <?php }
}
?>



  <?php include'footer-front-end.php'; ?>
  <script src="<?=URL?>front-end-js/jquery.colorbox-min.js"></script>
  <script type="text/javascript">

function xx(val) {

var check_array = [];

/*if ($(".outer_basket").css("display") == 'none') {
  $(".outer_basket").css("display", "block");
}*/

if (jQuery.inArray(val, check_array) == -1) {
  check_array.push(val);
} else {
  // Remove clicked value from the array
  check_array.splice($.inArray(val, check_array), 1);
}
slots = '';
hidden = '';
basket = 0;
cost_per_slot = $("#cost_per_slot").val();
//cost_per_slot = parseFloat(cost_per_slot).toFixed(2)
for (i = 0; i < check_array.length; i++) {
  slots += check_array[i] + '\r\n';
  hidden += check_array[i].substring(0, 8) + '|';
  basket = (basket + parseFloat(cost_per_slot));
}

// Populate the Selected Slots section
$(".selected_slots").html(slots);
// Update hidden slots_booked form element with booked slots
$(".slots_booked").val(hidden);
// Update basket total box
basket = basket.toFixed(2);
$(".total").html(basket);
// Hide the basket section if a user un-checks all the slots
}
$(".cash-on-service").click(function() {
  msg = '';

  if ($("#time_slot").val() == '')
  msg += 'Please enter a Time Slot\r\n';
  if ($("#name").val() == '')
  msg += 'Please enter a Name\r\n';
  if ($("#email_usr").val() == '')
  msg += 'Please enter an Email address\r\n';
  if ($("#phone").val() == '')
  msg += 'Please enter a Phone number\r\n';
  if (msg != '') {
  alert(msg);
  return false;
  }
});


function zz(shop_id,month,year,daynumber,edit_id)
{
  $.ajax({
          type: "POST",
          url: "<?=URL?>call-back.php",
          data: {edit_id : edit_id, shop_id : shop_id,month:month,year:year,day:daynumber },
          success: function (data) {
                  $(".outer_basket").css("display", "block");
                  //$(".outer_basket").css("display", "table-caption");

                 var result = $('<div />').append(data).find('#rr').html();
                 console.log(result);
                 
                  $("#ss").html(result);

                 
            },
          beforeSend:function()
          {
            
            $(".modal_7").show();
            $(".fade_7").show();
              
          },
          complete:function(){
            $(".modal_7").hide();
            $(".fade_7").hide();
          }
    });
}

$(".cost_rating_poulatiry_ul li span").click(function(){
  var shop_id ='<?php echo $shop_id ?>';
  
    if($(this).hasClass("desc")){
      $(this).removeClass("desc");
      $(this).addClass("asc");
      $(this).css('background-image','url("front-end-images/up_arrow.png")');
      var field = $(this).text();
      var order = "asc";
      
      $.ajax({
        type:"post",
        url:"<?=URL?>get-sorting.php",
        data: {shop_id : shop_id,order:order,field:field},
        success: function (res) {
          $("#sorting").html(res);
        }
       /* beforeSend:function()
       {
        $(".modal_1").show();
        $(".fade_1").show();
          
       },
       complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
       }*/
      });
    }else{
      $(this).removeClass("asc");
      $(this).addClass("desc");
      $(this).css('background-image','url("front-end-images/down_arrow.png")');
      var field = $(this).text();
      var order = "desc";
      $.ajax({
        type:"post",
        url:"<?=URL?>get-sorting.php",
        data: {shop_id : shop_id,order:order,field:field},
        success: function (res) {
          $("#sorting").html(res);
        }
        /*beforeSend:function()
       {
        $(".modal_1").show();
        $(".fade_1").show();
          
       },
       complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
       }*/
      }); 
      
    }
  });



$(document).ready(function(){
        //Examples of how to assign the Colorbox event to elements
        $(".group1").colorbox({rel:'group1'});

        //Example of preserving a JavaScript event for inline calls.
        $("#click").click(function(){ 

          $('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
          return false;
        });
      });


$(".review_write_btn").click(function(){
    var user_id = '<?php echo $_SESSION['ma_log_id_usr'];?>';
    if(user_id == '') {
      $(".overlay").fadeIn(300);
        $("#user_login_popup_check").delay(200).fadeIn(300);
        $(".pop_up_close").click(function(){    
        $(this).parent().fadeOut(300);
        $(".overlay").delay(200).fadeOut(300);
        });
    }else {
      $(".rating_write_c").slideDown(300);
            
      $(".close_rating_write_c").css("display","block");
      $(".review_write_btn").css("display","none");
      //$(this).css("display","none");
    }
  });


// User Lohin


  $("#user_err_check").css('display', 'none', 'important');
  $("#user-login-check").click(function(){
      
      username=$("#username_check").val();
      pass=$("#pass_check").val();
      
      $.ajax({
       type: "POST",
       url: "user-login-front-end.php",
      data: "mail="+username+"&pwd="+pass,
       success: function(html){
        var res = $.trim(html);
        
        //alert(res);
        
          if(res!=0){
            $("#hidden_user_id").val(res);
            $("#user_login_popup_check").fadeOut(300);
            $(".overlay").delay(200).fadeOut(300);
            $(".rating_write_c").slideDown(300);
            $(".review_write_btn").css("display","none");
           $(".close_rating_write_c").css("display","block");

      //$(this).css("display","none");
                
          }
          else{
            $("#user_err_check").css('display', 'inline', 'important');
            $("#user_err_check").html("<img src='front-end-images/alert.png' style='height: 18px;' />Wrong username or password");
          }

       },
       beforeSend:function()
       {
        $(".modal_1").show();
        $(".fade_1").show();
          
       },
       complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
       }
    });
  return false;
  });

$('#post_review').click(function() {
    var rating = $("input[type='radio'][name=rating]:checked").val();
    var allVals = [];
    $('#c_b :checked').each(function() {
      allVals.push($(this).val());
    });
   
    var description = $("textarea[name=description]").val();
    var user_id = $("input[name=user_id]").val();;
    var shop_id_user = $("input[name=shop_id_user]").val();
    if($('.group_check').is(':checked') )
    {
      if(user_id =='' || shop_id_user == '' || rating =='') {
        var msg = "All Fields Required";
        alert(msg);
        
        return false;
      }else {
        $.ajax({
          type: "POST",
          url: "add-review.php",
          data: {
            stars : rating, 
            group : allVals,
            description : description,
            user_id : user_id,
            shop_id : shop_id_user 
          },
         success: function(html){
          

            $.session.set("review", 4);
            $(".rating_write_c").slideUp(300);
            $(".review_write_btn").css("display","block");
            $(".close_rating_write_c").css("display","none");
            var result = $('<div />').append(html).find('#sorting').html();
             $("#sorting").html(result);
        
         },
         beforeSend:function()
         {
          $(".modal_1").show();
          $(".fade_1").show();
            
         },
         complete:function(){
          $(".modal_1").hide();
          $(".fade_1").hide();
         }
        });
      }
    } else {
      alert("Please Check Tag Services");
    }
});
  


//EDIT REVIEW
  function edit_review(){
    
    var rating_edit = $("input[type='radio'][name=rating_edit]:checked").val();
    var allVals_edit = [];
    $('#c_b_edit :checked').each(function() {
      allVals_edit.push($(this).val());
    });
  
    var description_edit = $("textarea[name=description_edit]").val();
    var user_id_edit = $("input[name=user_id_edit]").val();
    var review_id_edit = $("input[name=review_id_edit]").val();
    var shop_id_edit = $("input[name=shop_id_edit]").val();
    if($('.group_check').is(':checked') )
    { 
      if(user_id_edit=='' || shop_id_edit=='' || rating_edit=='') {
        var msg = "All Fields Required";
        alert(msg);
        //$('#dialog1').show(msg);
        return false;
      }else {
        $.ajax({
          type: "POST",
          url: "edit-review.php",
          data: {
            stars : rating_edit, 
            group : allVals_edit,
            description : description_edit,
            user_id : user_id_edit,
            shop_id : shop_id_edit,
            review_id : review_id_edit  
          },
         success: function(html){
            $.session.set("review", 4);
            $(".rating_write_c").slideUp(300);
            $(".review_write_btn").css("display","block");
            $(".close_rating_write_c").css("display","none");
            var result = $('<div />').append(html).find('#sorting').html();
             $("#sorting").html(result);

          
         },
         beforeSend:function()
         {
          $(".modal_1").show();
          $(".fade_1").show();
            
         },
         complete:function(){
          $(".modal_1").hide();
          $(".fade_1").hide();
         }
        });
      }
    } else {
      alert("Please Check Tag Services");
    }
  }




    $("#instant_book").click(function() {
      
       $("#booking-form").submit(function (e) {
            // If it is not checked, prevent the default behavior (your submit)
            if (!$('input[name="book_sub_cat"]').is(':checked')) {
                alert("Please ensure an Subservice  is selected!");
                return false;
            }
        });
        
    });

function add_favuorite(id,user_id) { 
if (user_id == 0) {

  $.session.set("myVar", id);
    $(".overlay").fadeIn(300);
      $("#user_login_popup_c").fadeIn(300);

      $(".pop_up_close").click(function(){    
        $(this).parent().fadeOut(300);
        $(".overlay").delay(200).fadeOut(300);
      });
    } else { 
      var user_id = user_id;
      var shop_id = id;
      
     
      $.ajax({
          type: "POST",
          url: "add-favorites.php",
          data: {
            user_id : user_id,
            shop_id : shop_id 
          },
         success: function(html){
          var res =$.trim(html)
          if (res=='inserted') {
            
               $(".fave_"+shop_id).removeClass("add_d");
               $(".fave_"+shop_id).addClass("added_d");
            $('#deco_'+shop_id).text("Added to list");
            
          }
          if (res=='deleted') {
            
               $(".fave_"+shop_id).removeClass("added_d");
               $(".fave_"+shop_id).addClass("add_d");
            $('#deco_'+shop_id).text("Add to list");
            
          }
          
         },
         beforeSend:function()
       {
        $(".modal_9").show();
        $(".fade_9").show();
          
       },
       complete:function(){
        $(".modal_9").hide();
        $(".fade_9").hide();
       }
      });
    }
}

function get_review(id) { 

      var review_id = id;
      
     
      $.ajax({
          type: "POST",
          url: "get-review.php",
          data: {
            review_id : review_id,
            
          },
         success: function(html){
          var res =$.trim(html)
          
          $(".rating_write_c").slideDown(300);
          $(".close_rating_write_c").css("display","block");
          $(".review_write_btn").css("display","none");
          $("#view_review").html(res);
          
         },
         beforeSend:function()
       {
        $(".modal_1").show();
        $(".fade_1").show();
          
       },
       complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
       }
      });
   
}
function delete_review(id,shop_id) { 

      var review_id = id;
      var shop_id =shop_id;
      alert(shop_id);
     
     $.ajax({
          type: "POST",
          url: "delete-review.php",
          data: {
            review_id : review_id,
            shop_id : shop_id 
            
          },
         success: function(html){

            $.session.set("review", 4);
            $(".rating_write_c").slideUp(300);
            $(".review_write_btn").css("display","block");
            $(".close_rating_write_c").css("display","none");
            var result = $('<div />').append(html).find('#sorting').html();
            $("#sorting").html(result);
             
         },
        beforeSend:function()
        {
         $(".modal_1").show();
         $(".fade_1").show();
       },
      complete:function(){
        $(".modal_1").hide();
        $(".fade_1").hide();
       }
      });
   
}
</script>