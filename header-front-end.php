<?php //session_destroy(); 
require_once "includes/includepath.php";



$objgen     =   new general();

?>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="front-end-css/style.css" />  
    <link rel="stylesheet" type="text/css" href="front-end-css/healthcare.css" />
    <link href="front-end-css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- <link href="front-end-css/text-change.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link rel="stylesheet" type="text/css" href="front-end-css/bootstrap.css" />  -->
    <link rel="stylesheet" type="text/css" href="front-end-css/bootstrap-min.css" /> 
    <!-- <link rel="stylesheet" type="text/css" href="css/responsive.css" /> -->
    <script type="text/javascript" src="front-end-js/jquery.js"></script>

    <script src="front-end-js/jquery-1.11.2.min.js"></script>
    <script src="front-end-js/jquery.session.js"></script>
    <script src="front-end-js/bootstrap.js"></script>
    <!--<script src="front-end-js/modernizr.js"></script>
<script src="front-end-js/main.js"></script>-->
   
</head>
<body>
    
       <div class="overlay">   
    </div> 
    <div id="element"></div>
        <div class="navbar-wrapper header_c">

            <div class="container-fluid">
                <nav class="navbar navbar-fixed-top">
                    
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="<?=URL?>"><img src="front-end-images/zoggrs_logo.png" alt="Zoggrs Logo" /></a>
                                    
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <div id="navbar" class="navbar-collapse collapse">
                                    <div class="header_link_c">
                                        <ul class="nav navbar-nav">
                                            <li class="city_select" style="width:203px;">
                                                    <span class="location_icon left"></span>
                                                    <div class="area_select_c right" style="width:170px;">
                                                        <select id="city_id" name = "city"class="form-control" title="Select one" style="width: 100% !important;">
                                                            
                                                            <?php $cities = $objgen->get_AllRows('city');
                                                            foreach ($cities as $keys=>$value) {
                                                            ?>
                                                            <option value="<?php echo $value['id'];?>" <?php if($_SESSION['city']==$value['id']) { ?> selected='selected'<?php } ?>><?php echo $value['city']; ?></option>
                                                            <?php }
                                                            ?>
                                            
                                                        </select>
                                                    </div>
                                                    <span class="clear"></span>
                                                </li>
                                                <?php if(basename($_SERVER['PHP_SELF'])=="index.php" || basename($_SERVER['PHP_SELF'])=='index'){ ?>
                                                    <li><a class="how_it_work" href="#howItWork">HOW IT WORKS?</a></li>
                                                <?php } ?>
                                                <li class="reading_list_li" >
                                                    <a class="reading_list_link" href="#">MY STUFF</a>                      
                                                </li>
                                                <?php if(($_SESSION['ma_log_id_usr']!='') && ($_SESSION['ma_usr_name_usr']!='')) { ?>
                                                <!-- <li class="reading_list_li" ><a class="reading_list_link" href="<?URL?>user-dashboard.php">DASHBOARD</a></li>  -->
                                                <li><span class="login btnuser" id="user_logout"><a href="<?URL?>logout-usr.php">LOGOUT</a></span></li>
                                                <?php } else {?>

                                                <li><span class="login btnuser" id="user_login">USER LOGIN</span></li>
                                                <?php }?>
                                                <li><span class="info"></span></li>
                                            </ul>
                                            <div class="clear"></div>
                                            <div class="reading_list_c" <?php if(basename($_SERVER['PHP_SELF'])=="index.php" || basename($_SERVER['PHP_SELF'])=='index'){ ?> style="width:60%;left: 20%;"<?php } ?>>
                                                <span class="news_menu_direction_arrow"></span>                 
                                                <!-- <ul class="reading_list_ul"> -->
                                                    <div class="reading_list left">
                                                        <a class="my_reading_list" href="#" id="user_dashboard">
                                                            <span class="list_caption">My Dashboard</span>
                                                            <span class="list_info">Save any story to read later on your PC, Tablet or Mobile.</span>
                                                        </a>
                                                    </div>
                                                    <div class="reading_list left">
                                                        <a class="history" href="#" id="user_favourites">
                                                            <span class="list_caption">My Favourites</span>
                                                            <span class="list_info">Accesses to stories you’ve read so you can find and share them easily.</span>
                                                        </a>
                                                    </div>
                                                    <!-- <div class="reading_list_news left">
                                                        <h4 class="my_news pink">My News</h4>
                                                        <div  class="myNews_c">
                                                            <p style="font-size: 1em;">Sign in today to starting using my list and make the most of Zoggrs</p>                              
                                                            <ul class="myNews_ul">
                                                                <li>Save search results and articles to read later</li>
                                                                <li>Faster accesses to the news that interests you</li>
                                                                <li>Easier to share the articles you enjoyed!  </li>
                                                            </ul>
                                                        </div>
                                                    </div> -->
                                                    <div class="clear"></div>
                                            </div>            
                                        </div>
                      
                                    </div>
                                </div>        
                            </div> 
                            
                </nav>
            </div>

        </div>
        
        
        <!-- USER SIGNUP POPUP  -->
        <div class="signup_pop_up_c  radius_corner_6" id="user_signup_pop_up_c">
            <div class="overlay_usr" style="display:none;">   
            </div>
            <div class="pop_up_close"></div>
                <h2 class="popup_caption">User Sign up</h2>
                <div class="err" id="user_signup_err"></div>
                  
                <div class="hr"></div>
                <div class="left_div">
                    <form method="post" action="" id="sign_up">
                        
                        <div class="lbl_input_c">
                            <div class="lbl_c">Name<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="text" id="user_name" name="user_name" required value="" placeholder="Enter Name" />                         
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">Mobile<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="text" id="user_phone" name="user_phone" required value="" placeholder="Enter Mobile No" />                         
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">City<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="text" id="user_city" name="user_city" required value="" placeholder="Enter City" />                         
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">Email Address<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="email" id="user_email" name="user_email" required value="" placeholder="Enter email id" />                         
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">Password<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="password" id="user_pwd" name="user_pwd" required value="" placeholder="Enter password" />                         
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">Confirm Password<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="password" id="user_conf_pwd" name="user_conf_pwd" value="" required placeholder="Confirm password" />                           
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">&nbsp;</div>
                            <div class="input_c"><input type="submit" id="user_submit" name="subuser" value="Sign Up" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="fade_1"></div>
                            <div class="modal_1">
                                <img class="loader" src="front-end-images/ajax-loader.gif" />
                            </div>
                    </form>
                </div>
                <div class="left" style="width: 8%;text-align: center;padding-top: 5.6em;">OR</div>
                <div class="right_div">
                    <div class="link_c">
                        <a class="fb" href="#">Connect With Facebook</a>
                        <a class="google" href="#">Connect With Google</a>
                    </div>
                    <div class="checkBox_c">
                        <div>
                            <input id="checkbox1" type="checkbox" name="checkbox" value="1" checked="checked"><label for="checkbox1">We never post without your permission</label>
                        </div>
                        <div>
                            <input id="checkbox2" type="checkbox" name="checkbox" value="2"><label for="checkbox2">Gain access to various offers and benefits</label>
                        </div>
                        <div>
                            <input id="checkbox3" type="checkbox" name="checkbox" value="3"><label for="checkbox3">Get added flexibility to track your options </label>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
        </div>

        <!-- MERCHANT SIGNUP POPUP  -->
        <div class="signup_pop_up_c  radius_corner_6" id="mer_signup_pop_up_c">
                <div class="pop_up_close"></div>
                <h2 class="popup_caption">Merchant Sign up</h2>
                <div class="err" id="mer_signup_err"></div>   
                <div class="hr"></div>
                <div class="left_div">
                    <form method="post" action="" id="sign_up">
                        
                        <div class="lbl_input_c">
                            <div class="lbl_c">Name<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="text" id="mer_name" name="mer_name" required value="" placeholder="Enter Name" />                         
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">Mobile<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="text" id="mer_phone" name="mer_phone" required value="" placeholder="Enter Mobile No" />                         
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">City<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="text" id="mer_city" name="mer_city" required value="" placeholder="Enter City" />                         
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">Email Address</div>
                            <div class="input_c">
                                <input type="email" id="mer_email" name="mer_email" required value="" placeholder="Enter email id" />                         
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">Password<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="password" id="mer_pwd" name="mer_pwd" required value="" placeholder="Enter password" />                         
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">Confirm Password<b style="color:red;">*</b></div>
                            <div class="input_c">
                                <input type="password" id="mer_conf_pwd" name="mer_conf_pwd" value="" required placeholder="Confirm password" />                           
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="lbl_input_c">
                            <div class="lbl_c">&nbsp;</div>
                            <div class="input_c"><input type="submit" id="mer_submit" name="submerch" value="Sign Up" /></div>
                            <div class="clear"></div>
                        </div>
                        <div class="fade_1"></div>
                            <div class="modal_1">
                                <img class="loader" src="front-end-images/ajax-loader.gif" />
                            </div>
                    </form>
                </div>
                <div class="left" style="width: 8%;text-align: center;padding-top: 5.6em;">OR</div>
                <div class="right_div">
                    <div class="link_c">
                        <h1 style="color:gray;">Zoggrs is FREE!</h1>
                            
                    </div>
                    <div class="checkBox_c">
                        <label>SIGN UP TO GET:</label>
                        <div>
                            <input id="checkbox1" type="checkbox" name="checkbox" value="1" checked="checked"><label for="checkbox1">Your own control panel</label>
                        </div>
                        <div>
                            <label for="checkbox1">Scheduling and Booking management</label>
                        </div>
                        <div>
                            <input id="checkbox2" type="checkbox" name="checkbox" checked value="2"><label for="checkbox2"> Client emails and text messages</label>
                        </div>
                        <div>
                            <input id="checkbox3" type="checkbox" checked name="checkbox" value="3"><label for="checkbox3"> Listed in the Zoggrs directory </label>
                        </div>
                        <div>
                            <input id="checkbox3" type="checkbox" checked name="checkbox" value="3"><label for="checkbox3"> Client history tracking</label>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
        </div>
        
        
        <!-- MERCHANT LOGIN POPUP -->
            <div class="login_popup_c radius_corner_6" id="merchant_login_popup_c">
                <div class="pop_up_close"></div>
                <h2 class="popup_caption">Merchant Login</h2>
                <div class="err" id="add_err"></div>   
                <div class="hr"></div>
                <div class="left_div">
                    <form method="post" name="login" >
                            <div class="lbl_input_c">
                                <div class="lbl_c">Email Address</div>
                                <div class="input_c">
                                    <input type="email" id="email" required name="email_login" value="" placeholder="Enter your email id" />                            
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="lbl_input_c">
                                <div class="lbl_c">Password</div>
                                <div class="input_c">
                                    <input type="password" id="password" required name="password_login" value="" placeholder="Enter your password" />                            
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="lbl_input_c">
                                <div class="lbl_c">&nbsp;</div>
                                <div class="input_c"><input type="submit" id="login" name="Login" value="Login" /></div>
                                <div class="clear"></div>
                            </div>
                            <div class="lbl_input_c" style="padding-top:0;padding-bottom:0;">
                                <div class="lbl_c"></div>
                                <div class="input_c"><span class="forgot_pass" id="mer_forgot_pass">Forgot password?</span></div>
                                <div class="clear"></div>
                            </div>
                          <div class="fade_1"></div>
                            <div class="modal_1">
                                <img class="loader" src="front-end-images/ajax-loader.gif" />
                            </div>
                    </form>
                </div>
                <div class="left" style="width: 8%;text-align: center;padding-top: 5.6em;">OR</div>
                <div class="right_div">
                    <div class="link_c">
                        <h1 style="color:gray;">Zoggrs is FREE!</h1>
                            
                    </div>
                    <div class="checkBox_c">
                        <label>SIGN UP TO GET:</label>
                        <div>
                            <input id="checkbox1" type="checkbox" name="checkbox" value="1" checked="checked"><label for="checkbox1">Your own control panel</label>
                        </div>
                        <div>
                            <label for="checkbox1">Scheduling and Booking management</label>
                        </div>
                        <div>
                            <input id="checkbox2" type="checkbox" name="checkbox" checked value="2"><label for="checkbox2"> Client emails and text messages</label>
                        </div>
                        <div>
                            <input id="checkbox3" type="checkbox" checked name="checkbox" value="3"><label for="checkbox3"> Listed in the Zoggrs directory </label>
                        </div>
                        <div>
                            <input id="checkbox3" type="checkbox" checked name="checkbox" value="3"><label for="checkbox3"> Client history tracking</label>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
        </div>
        
        <!-- USER LOGIN POPUP -->
            <div class="login_popup_c radius_corner_6" id="user_login_popup_c">
                <div class="pop_up_close"></div>
                <h2 class="popup_caption">Login to Zoggrs</h2>

                <div class="err" id="user_err"></div>   
                <div class="hr"></div>
                <div class="left_div">
                    <form method="post" name="user_login" >
                            <div class="lbl_input_c">
                                <div class="lbl_c">Email Address</div>
                                <div class="input_c">
                                    <input type="email" id="username" required name="email_login" value="" placeholder="Enter your email id" />                            
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="lbl_input_c">
                                <div class="lbl_c">Password</div>
                                <div class="input_c">
                                    <input type="password" id="pass" required name="password_login" value="" placeholder="Enter your password" />                            
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="lbl_input_c">
                                <div class="lbl_c">&nbsp;</div>
                                <div class="input_c"><input type="submit" id="user-login" name="user_login" value="Submit"  style="width: 250px !important;" />
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="lbl_input_c" style="padding-top:0;padding-bottom:0;">
                                <div class="lbl_c"></div>
                                <div class="input_c"><span class="forgot_pass" id="user_forgot_pass">Forgot password?</span></div>
                                <div class="clear"></div>
                            </div>
                            <div class="lbl_input_c" style="padding-top:0;padding-bottom:0;margin-top:0;margin-bottom:0;">
                                <div class="lbl_c"></div>
                                <div class="input_c"><span class="registration">New to Zoggrs?</span> <span class="sign_up" id="user_sign_up">Sign up</span></div>
                                <div class="clear"></div>
                            </div>
                             <div class="fade_1"></div>
                            <div class="modal_1">
                                <img class="loader" src="front-end-images/ajax-loader.gif" />
                            </div>
                    </form>
                </div>
                <div class="left" style="width: 8%;text-align: center;padding-top: 5.6em;">OR

                </div>
                <div class="right_div" style="padding-top: 12px;">
                    <div class="link_c">
                        <a class="fb" href="#">Connect With Facebook</a>
                        <a class="google" href="#">Connect With Google</a>
                    </div>
                    <div class="checkBox_c">
                        <div>
                            <input id="checkbox1" type="checkbox" name="checkbox" value="1" checked="checked"><label for="checkbox1">We never post without your permission</label>
                        </div>
                        <div>
                            <input id="checkbox2" type="checkbox" name="checkbox" value="2" checked="checked"><label for="checkbox2">Gain access to various offers and benefits</label>
                        </div>
                        <div>
                            <input id="checkbox3" type="checkbox" name="checkbox" value="3" checked="checked"><label for="checkbox3">Get added flexibility to track your options </label>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
        </div>

        <!-- USER FORGOT POPUP  -->
        <div class="forgotPass_pop_up_c radius_corner_6" id="user_forgotPass_pop_up_c">
            <div class="pop_up_close"></div>
            <h2 class="popup_caption">Generate New Password</h2>
            <div class="err" id="forgot_user_err"></div>
            <div class="hr" ></div>
            <table class="tbl" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><label>Email-Id</label></td>
                    <td><input type="email" value="" id="for_user_email" name="for_user_email" required placeholder="Email-Id" /></td>
                </tr>
                <tr>
                    <td><label>Mob. No.</label></td>
                    <td><input type="text" id="for_user_mob" name="for_user_mob" required value="" placeholder="Mobile no." /></td>
                </tr>
                <tr>
                    <td><label>&nbsp;</label></td>
                    <td><input type="submit" id="user_forgot" value="Send" /></td>
                </tr>
            </table>
            <div class="fade_1"></div>
                            <div class="modal_1">
                                <img class="loader" src="front-end-images/ajax-loader.gif" />
                            </div>
        </div>
        <!--MERCHANT  FORGOT POPUP  -->
        <div class="forgotPass_pop_up_c radius_corner_6" id="mer_forgotPass_pop_up_c">
            <div class="pop_up_close"></div>
            <h2 class="popup_caption">Generate New Password</h2>
            <div class="err" id="forgot_mer_err"></div>
            <div class="hr" ></div>
            <table class="tbl" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><label>Email-Id</label></td>
                    <td><input type="email" id="for_mer_email" name="for_mer_email" required value="" placeholder="Email-Id" /></td>
                </tr>
                <tr>
                    <td><label>Mob. No.</label></td>
                    <td><input type="text" value="" id="for_mer_mob" name="for_mer_mob" required placeholder="Mobile no." /></td>
                </tr>
                <tr>
                    <td><label>&nbsp;</label></td>
                    <td><input type="submit" id="mer_forgot" value="Send" /></td>
                </tr>
            </table>
            <div class="fade_1"></div>
                            <div class="modal_1">
                                <img class="loader" src="front-end-images/ajax-loader.gif" />
                            </div>
        </div>
        <!-- INFO POPUP  -->
        <div class="info_pop_up_c radius_corner_6">
            <div class="pop_up_close"></div>
            <h2 class="popup_caption">INFO</h2> 
            <div class="hr"></div>
            <table class="tbl" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><a class="about">About</a></td>
                    <td><a class="contact">Contact</a></td>
                </tr>
                <tr>
                    <td><a class="contribute">Contribute</a></td>
                    <td><a class="faq">FAQ</a></td>
                </tr>
                <tr>
                    <td><a class="advertise">Advertise</a></td>
                    <td><a class="careers">Careers</a></td>
                </tr>
                <tr>
                    <td><a class="privacy_policy">Privacy Policy</a></td>
                    <td><a class="terms_conditions">Terms & Conditions</a></td>
                </tr>
            </table>
            <table class="social_tbl" width="" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td><img src="front-end-images/face_bk.png" alt="" /></td>
                    <td><img src="front-end-images/twi.png" alt="" /></td>
                    <td><img src="front-end-images/in.png" alt="" /></td>
                    <td><img src="front-end-images/gog.png" alt="" /></td>
                    <td><img src="front-end-images/yutube.png" alt="" /></td>
                    <td><img src="front-end-images/skype.png" alt="" /></td>
                </tr>
            </table>
        </div>
        <div class="container" >    
        <?php if(!empty($_SESSION['msg']) && isset($_SESSION['msg'])) { ?>
        <div class="alert alert-success alert-dismissible" role="alert" style="margin-top:90px;">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <!-- <strong>Warning!</strong> Better check yourself, you're not looking too good. -->
          <?php echo $_SESSION['msg'];
                unset($_SESSION['msg']); 
                //echo "shri".$_SESSION['msg'];
          ?>
        </div>
        <?php } ?>
