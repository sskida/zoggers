<?php
require_once "includes/includepath.php";
$objgen = new general();
if(isset($_POST['review_id']) && !empty($_POST['review_id']) ){
  $id = $_POST['review_id'];
  $user_id = $_SESSION['ma_log_id_usr'];
  $sql_review = $objgen->get_Onerow("review","AND id=".$id);
  $shop_id = $sql_review['shop_id'];
  $group_ids = explode(',',$sql_review['group_id'] );

  $result_user = $objgen->get_Onerow("users","AND id=".$user_id);
  ?>
     
              <div class="fade_1"></div>
                <div class="modal_1">
                    <img class="loader" src="front-end-images/ajax-loader.gif" />
                </div>
            <div class="rating_write_caption">
              <h2>Write a review</h2>
            </div>
              <div class="review_popup_con" style="text-align:center;">
                <span class="star-rating">
                <?php
                for($i=1;$i<=5;$i++) {
                  $selected = "";
                    if(!empty($sql_review["star"]) && $i<=$sql_review["star"]) {
                    $selected = "checked";
                    }
                    ?>
                    <input type="radio" name="rating_edit" value="<?php echo $i; ?>" <?php echo $selected;?> >
                     <?php if($selected) {?> 
                     <i class="star-rating"></i>
                     <?php } else {?>
                     <i ></i>
                <?php } }  ?>
                  
                  
                </span>
                  
              </div>
              <div class="tag_service_c">
                <h4 style="margin-left: 10px; color: #737171;">Tag Service</h4>                      
                <div class="" id="c_b_edit">
                  
                  <?php 
                   $sql_groups = "SELECT distinct g.* FROM 
                  groups g INNER JOIN shop_categories sc 
                  ON sc.group_id=g.id  WHERE 
                  sc.shop_id='".$shop_id."' ";
                    $result_groups = $objgen->get_AllRows_qry($sql_groups);
                    foreach ($result_groups as $group) {
                      
                      if(in_array($group['id'], $group_ids)){
                        
                        $checked = "checked";
                      }else {
                        $checked = "";
                      }
                      ?>
                    <div class="col-md-2 border-1 textarea_c" style="width: auto;" >
                    <input class="form-class group_check" type="checkbox" name="group_edit[]" value="<?php echo $group['id']?>" <?php echo $checked; ?>>
                    <label style="font-weight: 400 !important;"><?php echo $group['group_title']?></label>   
                    </div>
                    <?php }

                  ?>

                </div>
              </div>
              <div class="clear"></div>
              <div class="tag_service_c">                     
                  <h4 style="margin-left: 10px;padding-top: 10px;color: #716F6F;">Description</h4>
                  <div class="border-1 textarea_c">
                    <textarea class="textarea" name="description_edit" style=" border: 1px solid #CACACA;" cols="4"  placeholder="What would you tell your friends about the experience? Give us all the details- what you loved, what could have been better and anything that took you by surprise. Remember to please keep it clean."><?php echo ucfirst(trim($sql_review['long_desc'])); ?></textarea>
                  </div>
                
              </div>
              <input type="hidden"  name="user_id_edit" value="<?php echo $_SESSION['ma_log_id_usr']; ?>"/>
              <input type="hidden"  name="review_id_edit" value="<?php echo $id; ?>"/>
              <input type="hidden" name="shop_id_edit" value="<?php echo $shop_id; ?>"/> 
              <a  class="btn_post" href="#" onclick="edit_review();" style="font-size: 1em; border: 1px solid #808080 !important; width:150px; color: #FFF !important;">Post Review</a>              
              
            
            <div class="clear"></div>
          
  

<?php }
?>