<?php

/**
 * fileUpload.class.php
 *
 * No warning will be raised.  This is the recommended usage
 * @package fileUpload
 */

/**
 * This is a not a file-level DocBlock, because it precedes a class declaration
 *
 * This is also the recommended usage
 * @package fileUpload
 */

Class fileUpload

{
	var $pathFile;
	var $originalFileName;
	var $SO;
	var $sizeMaxFileName = 20;
	var $extensionFile;
	var $nameFile;

	/*
		Constructor of the Class
	*/
	function fileUpload()
	{
	}


    /*
		Seta a vari�vel de inst�ncia "$pathFile", que cont�m o local f�sico do servidor remoto onde o arquivo ser� salvo.
		Arrow the instance variable "$pathFile", that it contains the physical place of the remote server where the archive will be saved.
    */
	function setPath($pathFile)
	{
		$this->pathFile = $pathFile;
	}


	/*
		Seta um tamanho m�ximo para o nome do arquivo a ser gravado no servidor.
		Arrow a maximum size for the name of the archive to be recorded in the server.
	*/
	function setSizeMaxFileName($sizeMaxFileName)
	{
		$this->sizeMaxFileName = $sizeMaxFileName;
	}


    /*
		Seta o nome do arquivo que ser� gravado no servidor.
		Arrow the name of the archive that will be recorded in the server.
    */
	function setOriginalFileName($originalFileName)
	{
		$this->originalFileName = $originalFileName;
	}


	/*
		Retorna o nome do arquivo original j� formatado
		It returns the name from the formatted original archive already.
	*/
	function getOriginalFileName()
	{
		return $this->originalFileName;
	}


	/*
		Retorna apenas o nome do arquivo formatado e sem a extens�o
		It returns only the name from the formatted archive and without the extension
	*/
	function getNameFile()
	{
		return $this->nameFile;
	}

	/*
        Gera um n�mero rand�mico para ser concatenado com o nome do arquivo.
		It generates a random number to be concatenated with the name of the archive.
	*/
	function RandomName($nameLength) {
	    $name = "";
	    for ($index = 1; $index <= $nameLength; $index++)
		{
			 mt_srand((double) microtime() * 1000000);
	         $randomNumber = mt_rand(1, 62);
	         if ($randomNumber < 11)
	              $name .= Chr($randomNumber + 48 - 1); // [ 1,10] => [0,9]
	         else if ($randomNumber < 37)
	              $name .= Chr($randomNumber + 65 - 10); // [11,36] => [A,Z]
	         else
	              $name .= Chr($randomNumber + 97 - 36); // [37,62] => [a,z]
	    }
		$name = $name."_".time();
		$this->formatNameFile();
		$this->formatExtensionFile();
		$this->originalFileName = $this->nameFile."_".$name.".".$this->extensionFile;
	}


	/*
		Retira os poss�veis caracteres inv�lidos do nome do arquivo e os converte para caracteres min�sculas
		It removes the possible invalid characters of the name of the archive and it converts them for very small characters
	*/
	function InvalidCaracter($var)
	{
		$a="����������������������������& -!@#$%�&*()_+}=}{[]^~?/:;><,'�`\"\\�";
		$b="AaEeIiOoUuCcAaAaAaEeIiOoOoUue___________________________________";
		$var = strtr($var,$a,$b);
		$var = strtolower($var);
		return $var;
	}


	/*
		Seta a vari�vel de inst�ncia do nome do arquivo
		Arrow the variable of instance of the name of the archive
	*/
	function formatNameFile()
	{
		$originalFileName = $this->originalFileName;
		$posDivisionNameExtension = strpos($originalFileName,".");
	    $nameFile  = substr($originalFileName,0,$posDivisionNameExtension);
		$nameFile  = $this->InvalidCaracter($nameFile);
		$this->nameFile = $nameFile;
	}


	/*
		Seta a vari�vel de inst�ncia da extens�o do arquivo
		Arrow the 0 variable of instance of the extension of the archive
	*/
	function formatExtensionFile()
	{
		$originalFileName = $this->originalFileName;
		$invertFile = strrev($originalFileName);
		$posDivisionNameExtension = strpos($invertFile,".");
	    $extensionFile = substr($invertFile,0,$posDivisionNameExtension);
		$extensionFile = strrev($extensionFile);
		$extensionFile = $this->InvalidCaracter($extensionFile);
		$this->extensionFile = $extensionFile;
	}


	/*
		Retorna apenas a extens�o do arquivo
		It returns only the extension from the archive
	*/
	function getExtensionFile()
	{
		return $this->extensionFile;
	}

	/*
		Seta os valores do nome e da extens�o, caso n�o queira formatar	o nome do arquivo.
		Arrow the values of the name and the extension, in case that it does not want to format the name of the archive.
	*/
	function setNameAndExtension()
	{
		$this->formatNameFile();
		$this->formatExtensionFile();
	}

	/*
		Formata o "$originalFileName" para caractere min�sculos e caso o nome do arquivo passado tenha caracteres especiais o substituam.
		E caso o seu tamanho m�ximo seja setado pelo usu�rio � feito o truncamento.

		It formats "$originalFileName" for character the very small ones and case the name of the last archive has characters special substitutes it.
		E case its maximum size is setado by the user is made the truncation.
	*/
	function formatOriginalFileName()
	{
		$this->formatNameFile();
		$this->formatExtensionFile();
		$sizeFileName = strlen($this->nameFile);
		if ($sizeFileName>$this->sizeMaxFileName)
		{
	         $this->nameFile = substr($this->nameFile,0,$this->sizeMaxFileName);
		}
		$this->originalFileName = $this->nameFile. "." .$this->extensionFile;
	}


	/*
		Seta o Sistema Operacional (WINDOWS, UNIX/LINUX)
		Arrow the Operational system (WINDOWS, UNIX/LINUX)
	*/
	function setSO($SO)
	{
		$this->SO = $SO;
	}


	/*
		Configura a divis�o do Diret�rio caso seja LINUX/UNIX ou WINDOWS
		It configures the division of the Directory case is LINUX/UNIX or WINDOWS
	*/
	function getDivisionDIR()
	{
		$SO = $this->SO;
		$SO = strtolower($SO);
		switch ($SO)
		{
			case "w":
				$divisionDir = "\\";
				break;
			case "l":
				$divisionDir = "/";
				break;
			case "u":
				$divisionDir = "/";
				break;
		}

		return $divisionDir;
	}


	/*
		Verifica se j� existe um arquivo no servidor com o mesmo nome do arquivo que se deseja passar como par�metro.
		It verifies if already it the same exists an archive in the server with name of the archive that if it desires to pass as parameter.
	*/
	function isFileExist($file)
	{
		$x;
		$file = $this->originalFileName;
		$openDir = @opendir($this->pathFile);
		while (($filesServer=@readdir($openDir))!=false)
		{
			if (is_file($this->pathFile.$this->getDivisionDIR().$filesServer)):
				if ($filesServer==$file)
				{
					$x = true;
					break;
				}
				else
				{
					$x = false;
				}
			else:
				$x = false;
			endif;

		}
		@closedir($openDir);
		return $x;
	}


	/*
		Realiza o Upload pela fun��o copy()
		Carries through the Upload for the function	Copy().
	*/
	function uploadCopyFile($file)
	{ 
		$x = false;
		//echo $this->pathFile;echo '<br>';
		// $pathUpload = $this->pathFile.$this->getDivisionDIR().$this->originalFileName;
		$pathUpload = $this->pathFile."/".$this->originalFileName;
		$openDir = @opendir($this->pathFile);
    
 		if (@copy($file,$pathUpload)):
			if (@is_uploaded_file($file))
			{
				$x = true;
			}
		endif;
		@closedir($openDir);

		return $x;
	}

	/*
		Realiza o Upload pela fun��o move_uploaded_file()
		Carries through the Upload for the function	Move_uploaded_file().
	*/
	function uploadMoveFile($file)
	{
		$x = false;
		$pathUpload = $this->pathFile.$this->getDivisionDIR().$this->originalFileName;
 		if (@move_uploaded_file($file,$pathUpload)):
			if (@is_uploaded_file($file))
			{
				$x = true;
			}
		endif;
		return $x;
	}

	function deleteFile($file)
	{
		$x = false;
		$pathfile = $this->pathFile.$this->getDivisionDIR().$file;
 		if (file_exists($pathfile)):
			if (unlink($pathfile))
			{
				$x = true;
			}
		endif;
		return $x;
	}

	function createthumb($name,$filename,$new_w,$new_h){
		global $gd2;
		$system=explode(".",$name);

		if (preg_match("/jpg|jpeg|png/",$system[1])){
			 $src_img=imagecreatefromjpeg($name);
		}
		if (preg_match("/gif/",$system[1])){
			 $src_img=imagecreatefromgif($name);
		}
		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);
		if ($old_x > $old_y) {
			 $thumb_w=$new_w;
			 $thumb_h=$new_w*($old_y/$old_x);
		}
		if ($old_x < $old_y) {
			 $thumb_w=$new_h*($old_x/$old_y);
			 $thumb_h=$new_h;
		}
		if ($old_x == $old_y)
		{
			 $thumb_w=$new_w;
			 $thumb_h=$new_h;
		}

		if ($gd2==""){
			$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
			imagefill($dst_img,0,0,imagecolorallocate($dst_img,255,255,255));
			imagecopyresized($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
			}else{
			$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
			imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
		}

		if (preg_match("/gif/",$system[1])){
			imagegif($dst_img,$filename);
			chmod($filename,0777);
		} else {
			imagejpeg($dst_img,$filename);
			chmod($filename,0777);
		}
		imagedestroy($dst_img);
		imagedestroy($src_img);
	}
	function createlarge($name,$filename,$new_w,$new_h){
		
		global $gd2;
		$system=explode(".",$name);

		if (preg_match("/jpg|jpeg/",$system[1])){
			 $src_img=imagecreatefromjpeg($name);

		}
		if (preg_match("/gif/",$system[1])){
			 $src_img=imagecreatefromgif($name);
		}
		if (preg_match("/png/",$system[1])){
			 $src_img=imagecreatefromgif($name);
		}
		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);
		if ($old_x > $old_y) {
			 $thumb_w=$new_w;
			 $thumb_h=$new_w*($old_y/$old_x);
		}
		if ($old_x < $old_y) {
			 $thumb_w=$new_h*($old_x/$old_y);
			 $thumb_h=$new_h;
		}
		if ($old_x == $old_y)
		{
			 $thumb_w=$new_w;
			 $thumb_h=$new_h;
		}

		if ($gd2==""){
			$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
			imagefill($dst_img,0,0,imagecolorallocate($dst_img,255,255,255));
			imagecopyresized($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
			}else{
			$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
			imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
		}

		if (preg_match("/gif/",$system[1])){
			imagegif($dst_img,$filename);
			chmod($filename,0777);
		} else {

			imagejpeg($dst_img,$filename);
			chmod($filename,0777);
		}
		imagedestroy($dst_img);
		imagedestroy($src_img);
	}
	function createprofile($name,$filename,$new_w,$new_h){
		global $gd2;
		$system=explode(".",$name);

		if (preg_match("/jpg|jpeg/",$system[1])){
			 $src_img=imagecreatefromjpeg($name);
		}
		if (preg_match("/gif/",$system[1])){
			 $src_img=imagecreatefromgif($name);
		}
		if (preg_match("/png/",$system[1])){
			 $src_img=imagecreatefromgif($name);
		}
		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);
		if ($old_x > $old_y) {
			 $thumb_w=$new_w;
			 $thumb_h=$new_w*($old_y/$old_x);
		}
		if ($old_x < $old_y) {
			 $thumb_w=$new_h*($old_x/$old_y);
			 $thumb_h=$new_h;
		}
		if ($old_x == $old_y)
		{
			 $thumb_w=$new_w;
			 $thumb_h=$new_h;
		}

		if ($gd2==""){
			$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
			imagefill($dst_img,0,0,imagecolorallocate($dst_img,255,255,255));
			imagecopyresized($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
			}else{
			$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
			imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
		}

		if (preg_match("/gif/",$system[1])){
			imagegif($dst_img,$filename);
			chmod($filename,0777);
		} else {
			imagejpeg($dst_img,$filename);
			chmod($filename,0777);
		}
		imagedestroy($dst_img);
		imagedestroy($src_img);
	}
	
function make_thumb($src, $dest, $desired_width) {
	$source_image = imagecreatefromjpeg($src);
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	/* create the physical thumbnail image to its destination */
	imagejpeg($virtual_image, $dest);
}
function make_large($src, $dest, $desired_width) {
	$source_image = imagecreatefromjpeg($src);
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = 400;//floor($height * ($desired_width / $width));
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	/* create the physical thumbnail image to its destination */
	imagejpeg($virtual_image, $dest);
}
function make_profile($src, $dest, $desired_width) {
	$source_image = imagecreatefromjpeg($src);
	$width = imagesx($source_image);
	$height = imagesy($source_image);
	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = 150;//floor($height * ($desired_width / $width));
	
	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
	/* create the physical thumbnail image to its destination */
	imagejpeg($virtual_image, $dest);
}
} // end of class fileUpload





function set_image($field_name){
	if($_FILES["{$field_name}"]["name"]){
		$this_image = rand()."_".$_FILES["{$field_name}"]["name"];
	}
	else {
		$this_image = "";
	}
	return $this_image;
}


function file_upload($file, $file_tmp_name, $upload_path){

	// $upload_path = "uploads/";
	$upload_msg = "";
	$upload = new fileUpload();
	// $upload->setSO("W");
	$upload_large_path  = $upload_path.'/large';
	$upload_profile_path  = $upload_path.'/profile';
	$upload_thumb_path  = $upload_path.'/thumb';
	$upload_path;
	$upload->setPath($upload_path);
	$upload->setOriginalFileName($file);

	$extensionFile = $upload->getExtensionFile();
	$nameFileFull  = $upload->getOriginalFileName();
	$nameFile 	   = $upload->getNameFile();

	$srcfile		= $upload_path."/".$nameFileFull;
	$profilefile	= $upload_profile_path."/profile_".$nameFileFull;
	$largefile		= $upload_large_path."/large_".$nameFileFull;
	$thumbfile		= $upload_thumb_path."/thumb_".$nameFileFull;
// var_dump('large=='.$largefile);
// var_dump('==thumb='. $thumbfile);exit;
	$upload_status = array(0,'');

	// list($width, $height) = getimagesize($file_tmp_name);

	// if($width != 141 || $height != 221 && $height != 442):
		// $upload_msg = "<font color='#FF0000'>Please upload file having specified dimensions.</font>";
		// $upload_status = array(1,$upload_msg);
		// return $upload_status;
	// else:

		if($upload->isFileExist($file)):
			
			$upload_msg = "<font color='#FF0000'>The file is already existing in the server.</font>: <strong>".$file."</strong>";
			$upload_status = array(1,$upload_msg);
		else: 
			
			if (!($upload->uploadCopyFile($file_tmp_name))):
				
				$upload_msg = "<font color='#FF0000'>Unexpected error occurred when uploading the file.</font>";
				$upload_status = array(1,$upload_msg);
				
			else:
				// $upload->uploadCopyFile($file_tmp_name);
				// $upload->createprofile($srcfile,$profilefile,400,150);
			 //    $upload->createlarge($srcfile,$largefile,800,400);
				$upload->make_profile($srcfile,$profilefile,400);
				$upload->make_large($srcfile,$largefile,800);
				$upload->make_thumb($srcfile,$thumbfile,100);
				$upload_msg  = "The file '".$nameFileFull."' has been uploaded. <br>";
				$upload_status = array(0,$upload_msg);
			endif;
		endif;

	// endif;

	return $upload_status;
}


function upload_file($fieldname,$img_path,$db_obj,$merchant_id,$shop_id,$cat_photos,$id, $set_profile_pic)
{
	echo $fieldname;
	$update_success = $update_fail = "";
	$filename = set_image($fieldname);
	
	if($filename != "")
	{
		 $upload_status = file_upload($filename, $_FILES[$fieldname]["tmp_name"],$img_path);
		
		if(!$upload_status[0])
		{
			$update_success .= "<center><div class='alert alert-info' role='alert'>".$upload_status[1]."</div></center>";
			 $largeImage ="large_".$filename;
			$profileImage ="profile_".$filename;
			$thumbImage ="thumb_".$filename;
			$set_profile_pic;
			if(isset($id))
			{ 
				$db_obj->upd_Row('photos',"`image`='".$largeImage."', `thumb`='".$thumbImage."', `medium`='".$profileImage."', `set_profile_pic`='".$set_profile_pic."'","merchant_id='".$merchant_id."' AND`shop_id`='".$shop_id."' AND catgeory='".$cat_photos."' AND id='".$id."'");
			}
			else
			{
				
				$db_obj->ins_Row('photos','`merchant_id`,`shop_id`,`image`,`catgeory`,`thumb`,`medium`, `set_profile_pic`',
				"'".$merchant_id."','".$shop_id."','".$largeImage."','".$cat_photos."','".$thumbImage."','".$profileImage."', '".$set_profile_pic."'");
			}
		}
		else
		{
			$db_obj->upd_Row('photos',"`set_profile_pic`='".$set_profile_pic."'","merchant_id='".$merchant_id."' AND`shop_id`='".$shop_id."' AND catgeory='".$cat_photos."' AND id='".$id."'");
			$update_fail .= "<center><div class='alert alert-warning' role='alert'>".$upload_status[1]."</div></center>";
			$upload_success = false;
		}

	}
	else
		{
			
			$db_obj->upd_Row('photos',"`set_profile_pic`='".$set_profile_pic."'","merchant_id='".$merchant_id."' AND`shop_id`='".$shop_id."' AND catgeory='".$cat_photos."' AND id='".$id."'");
		}
	return array($update_success,$update_fail);
}


?>