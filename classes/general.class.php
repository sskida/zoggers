<?php
#####################################################
#                  | Page Info. |                   #
#####################################################
/*	PAGE  : general.class.php
	DESC  : Class contains functions based on 
			insrt,update,select,delete opertions.
*/
#####################################################

include_once "database.class.php";
include_once "resize.class.php";

class general
		{
			function __construct()
			{
				$this->now		= date("Y-m-d");
				$this->date		= date("Y-m-d H:i:s");
				$this->db		= new database();
				global $dbvr;
				$this->dbvr = $this->db->dbConnect();
				if(!$this->dbvr)
				 die("Error Connection".$this->db->ErrorInfo);
			}
			
			function __destruct()
			{
				$this->db->dbClose();
			}
			
			function get_insetId()
			{
				return $this->db->getInsertId();
			}
			
		  function extfind($filename) 
			{ 
				$filename = strtolower($filename) ; 
				$exts = @split("[/\\.]", $filename) ; 
				$n = count($exts)-1;
				$end = $exts[$n];
				$exts = substr($exts[$n-1],0,20);
				$exts .= date("YmdHis").".".$end;
				return $exts; 
			}
	
			# Fetch One Row
			function get_Onerow($table,$where,$col='*',$html=false)
			{
				$sql	= "SELECT $col FROM $table WHERE 1=1 $where";
				//echo $sql;exit;
			    $res	= $this->db->readValue($sql);
	    	   	return $res;
			}

			function new_table($sql){
				$res	= $this->db->setQuery($sql);
	    	   	return $res;
			}

			function get_MAxVal($table,$col,$as,$where="")
			{
				$sql	= "SELECT max($col) AS $as  FROM $table";
				if($where!="")
				{
					$sql	.= " WHERE $where";
				}
				//echo $sql;
			    $res	= $this->db->readValue($sql);
				return $res;
			}
			function get_MinVal($table,$col,$as,$col2,$where="")
			{
				$sql	= "SELECT min($col) AS $as, $col2 FROM $table";
				if($where!="")
				{
					$sql	.= " WHERE $where";
				}
				//echo $sql;
			    $res	= $this->db->readValue($sql);
				return $res;
			}
			function get_MAxVal2($table,$col,$where="")
			{
				$sql	= "SELECT $col FROM $table";
				if($where!="")
				{
					$sql	.= " WHERE $where";
				}
				//echo $sql;
			    $res	= $this->db->readValue($sql);
				return $res;
			}

			# Fetch One Rows Count
			function get_AllRowscnt($table,$where="",$group_by="",$col='*')
			{
				$sql	= "SELECT $col FROM $table WHERE 1=1";
				if($where!="")
					$sql	.= " $where";
				if($group_by!="")
					$sql	.= " GROUP BY $group_by";
				//echo $sql;
			    $res	= $this->db->numberOfRecords($sql);
	    	   	return $res;
			}
			# Fetch One Rows 
			function get_AllRows($table,$limit=0,$count=10,$order="",$where="",$group_by="",$col='*',$html=false)
			{
				$sql	= "SELECT $col FROM $table WHERE 1=1";
				if($where!="")
					$sql	.= " $where";
				if($group_by!="")
					$sql	.= " GROUP BY $group_by";
				if($order!="")
					$sql	.= " ORDER BY $order";
				
				$sql	.=	" LIMIT $limit,$count";
				 	//echo $sql;
			    $res	= $this->db->readValues($sql);
		/*		$new_res = array();
				foreach($res as $ky=>$val)
				{
					foreach($val as $ky1=>$val1)
					{
						if($html==false)
						 $new_res[$ky][$ky1] = htmlentities(stripslashes($val1));
						else
						 $new_res[$ky][$ky1] = stripslashes($val1);
					}
				}
		
	    	   	return $new_res;*/
				return $res;
			}
			
			
			# Fetch One Rows Count
			function get_AllRowscnt_qry($sql)
			{
				//echo $sql;
			    $res	= $this->db->numberOfRecords($sql);
	    	   	return $res;
			}

			function get_temp_AllRows_qry($sql){
				$res	= $this->db->readTempValues($sql);
				return $res;
			}

			# Fetch One Rows 
			function get_AllRows_qry($sql)
			{
			   //echo $sql;
			    $res	= $this->db->readValues($sql);
				return $res;
			}
			
			# Delete Row
			function del_Row($table,$where="")
			{
				$sql	=  "DELETE FROM $table";
				if($where!="")
					$sql	.= " WHERE $where";
				//echo $sql;exit;
				$res	=  $this->db->setQuery($sql);
				if($res)
					{
						return "";
					}
				else
					  return "Sorry !.Error occured .Please try Again!.";
			}
			# Insert Row
			function ins_Row($table,$colums,$values)
			{
				$sql	=  "INSERT INTO $table ($colums) VALUES ($values)";
				//echo $sql;exit;

				$res	=  $this->db->setQuery($sql);
				
				if($res)
				{
					return "";
				}
				else
			  	return "Sorry !.Error occured .Please try Again!.";
			}
			# Update Row
			function upd_Row($table,$set,$where)
			{
				$sql	=  "UPDATE $table SET $set WHERE $where";
				$res	=  $this->db->setQuery($sql);
				//echo $sql;exit;
				if($res)
					{
						return "";
					}
				else
					  return "Sorry !.Error occured .Please try Again!.";
			}
			# Check Exist
			function chk_Ext($table,$where,$col='*')
			{
				$sql	= "SELECT $col FROM $table WHERE $where";
				//echo $sql;
			    $res	= $this->db->numberOfRecords($sql);
	    	   	return $res;
			}
			
			function chk_morethan($username,$password)
			{
				$sql2	= "SELECT email FROM ma_users WHERE email='".addslashes($username)."' and password='".addslashes($password)."'";
			    $res2	= $this->db->numberOfRecords($sql2);
	    	   	return $res2;
			}
			
			function encrypt_pass($password)
			{
			
			   $key = 'swebin';
			   $string = $password; 
			   $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));
			   return $encrypted;
			}
			
			function decrypt_pass($password)
			{
				$key = 'swebin';
			    $encrypted = $password;
			    $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
			    return $decrypted;
			}
			
			# Check Login
			function chk_Login($table,$username,$password,$page,$ses_id='user_id',$ses_name='username',$ses_pass="password",$status="",$admin=0,$col='*',$remember_me='no',$st_coln='status')
			{

			    $encrypted = $this->encrypt_pass($password);
			   
				$sql	= "SELECT $col FROM $table WHERE $ses_name='".$this->check_input($username)."' and $ses_pass='".$encrypted."'";
				
				if($status!="")
				  $sql	.= " AND $st_coln='$status'";
				  
				 // echo $sql;exit;
			    $res	= $this->db->readValue($sql);
	    	   	if($res)
				{
					session_start();
					
					if($admin==0)
					{
						$_SESSION['ma_log_id_mer']		=  $res[$ses_id];
						$_SESSION['ma_usr_name_mer']	=  $res[$ses_name];
						$_SESSION['ma_name_mer']	    =  $res['name'];
											
							if($remember_me=='yes')
							{
								$expire=time()+10*365*60*60*24; //10 years, 60 sec * 60 min * 24 hours * 365 days
								setcookie("swebin_user", $username, $expire);
								setcookie("swebin_sec", $encrypted, $expire);
							}
							else
							{
							
								// set the expiration date to one hour ago
								setcookie("swebin_user", "", time()-3600);
								setcookie("swebin_sec", "", time()-3600);
							}
					}
					else if($admin==2)
					{
						$_SESSION['ma_log_id_usr']		=  $res[$ses_id];
						$_SESSION['ma_usr_name_usr']	=  $res[$ses_name];
						$_SESSION['ma_name_usr']	    =  $res['name'];
									
							if($remember_me=='yes')
							{
								$expire=time()+10*365*60*60*24; //10 years, 60 sec * 60 min * 24 hours * 365 days
								setcookie("swebin_user", $username, $expire);
								setcookie("swebin_sec", $encrypted, $expire);
							}
							else
							{
							
								// set the expiration date to one hour ago
								setcookie("swebin_user", "", time()-3600);
								setcookie("swebin_sec", "", time()-3600);
							}
					}
					else
					{
						$_SESSION['MYPR_adm_id']	     =  $res[$ses_id];
						$_SESSION['MYPR_adm_username']	 =  $res[$ses_name];
						$_SESSION['MYPR_adm_type']	     =  $res['user_type'];
						
							if($remember_me=='yes')
							{
								$expire=time()+10*365*60*60*24; //10 years, 60 sec * 60 min * 24 hours * 365 days
								setcookie("swebin_user_ad", $username, $expire);
								setcookie("swebin_sec_ad", $encrypted, $expire);
							}
							else
							{
							
								// set the expiration date to one hour ago
								setcookie("swebin_user_ad", "", time()-3600);
								setcookie("swebin_sec_ad", "", time()-3600);
							}
					}
					return "";
				
				}
				else if($res && $status!="" && $res['status']!=$status)
				{
					return "Sorry!.Your Account is Inactive Now.";
				}
				else
				{
					return "Invalid Username/Password";
				}

			}
			
			function chng_password($table,$col_pass,$POST,$col_id,$id)
			{
				
				$password	    =	$this->encrypt_pass(trim($POST['new_pwd']));
				$old_password	=	$this->encrypt_pass(trim($POST['old_pwd']));
				$sql1	= "SELECT $col_pass FROM $table WHERE $col_pass='$old_password' AND $col_id=$id";
				$res1	=  $this->db->numberOfRecords($sql1);
				if($res1==0)
				{
					return "Inavlid Old Password.";
				}
				else
				{
					$sql	=	"Update $table SET $col_pass='$password' where $col_id=$id";
					$res	=  $this->db->setQuery($sql);
					if($res)
						{
							return "";
						}
					else
						  return "Sorry !.Error occured .Please try Again!.";
				}
			}
			# Check Password
			function match_Pass($pass,$conf_pass,$min="",$max="")
			{
			  if($pass!=$conf_pass)
			  {
			  	 return "Confirm password Didn't Match";
			  }
			  else if($min!="" && $max!="")
			  {
			  	if($pass>$max || $pass<$min)
				  return "Password Must Contains $min - $max char.";
				else
					return	"";
			  }
			  else if($min!="")
			  {
			  	if($pass<$min)
					return "Password Must Contains Min. $min char.";
				else
					return	"";
			  }
			  else if($min!="")
			  {
			  	if($pass>$max)
				  return "Password Must Contains Min. $min char.";
				else
					return	"";
			  }
			  else
			  {
				return	"";
			  }
			}
			# Make Date Time
			function make_DateTime($date)
			{
				$date	 =	explode(" ",$date);
				$time_exp	=	explode(":",$date[1]);
				$date_exp	=	explode("-",$date[0]);
				if($date[0]==$this->now)
				{
					$date	=	"<font color='blue'><b>Today</b></font>".date(" g:i A", mktime($time_exp[0],$time_exp[1],$time_exp[2],$date_exp[1],$date_exp[2],$date_exp[0]));
				}
				else
				{
					$date	=	date("jS, M Y g:i A", mktime($time_exp[0],$time_exp[1],$time_exp[2],$date_exp[1],$date_exp[2],$date_exp[0]));
				}
				return $date;
			}
			
			# Make Date Time
			function make_Date($date)
			{
				$date_exp	 =	explode("-",$date);
				$date	=	date("jS, M Y [ l ]", mktime(0,0,0,$date_exp[1],$date_exp[2],$date_exp[0]));
				return $date;
			}
			
			function con_date($date)
			{
			  return date("d-m-Y",strtotime($date)); 
			}
			
			function con_date_db($date)
			{
			  return date("Y-m-d",strtotime($date)); 
			}
			
	# Remove Stripslashes Array
	

			function DetermineAgeFromDOB ($YYYYMMDD_In)
			{
			  // Parse Birthday Input Into Local Variables
			  // Assumes Input In Form: YYYYMMDD
			  $yIn=substr($YYYYMMDD_In, 0, 4);
			  $mIn=substr($YYYYMMDD_In, 4, 2);
			  $dIn=substr($YYYYMMDD_In, 6, 2);
			
			  // Calculate Differences Between Birthday And Now
			  // By Subtracting Birthday From Current Date
			  $ddiff = date("d") - $dIn;
			  $mdiff = date("m") - $mIn;
			  $ydiff = date("Y") - $yIn;
			
			  // Check If Birthday Month Has Been Reached
			  if ($mdiff < 0)
			  {
				// Birthday Month Not Reached
				// Subtract 1 Year From Age
				$ydiff--;
			  } elseif ($mdiff==0)
			  {
				// Birthday Month Currently
				// Check If BirthdayDay Passed
				if ($ddiff < 0)
				{
				  //Birthday Not Reached
				  // Subtract 1 Year From Age
				  $ydiff--;
				}
			  }
			  return $ydiff;
			}
			
	   
		function get_time_difference( $start, $end )
		{
			$uts['start']      =    strtotime( $start );
			$uts['end']        =    strtotime( $end );
			if( $uts['start']!==-1 && $uts['end']!==-1 )
			{
				if( $uts['end'] >= $uts['start'] )
				{
					$diff    =    $uts['end'] - $uts['start'];
					if( $days=intval((floor($diff/86400))) )
						$diff = $diff % 86400;
					if( $hours=intval((floor($diff/3600))) )
						$diff = $diff % 3600;
					if( $minutes=intval((floor($diff/60))) )
						$diff = $diff % 60;
					$diff    =    intval( $diff );            
					return( array('days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff) );
				}
				else
				{
					trigger_error( "Ending date/time is earlier than the start date/time", E_USER_WARNING );
				}
			}
			else
			{
				trigger_error( "Invalid date/time data detected", E_USER_WARNING );
			}
			return( false );
		}
		function currnt_page()
		{
		  	$currentFile = $_SERVER['REQUEST_URI'];
			$parts = Explode('/', $currentFile);
			return $parts[count($parts) - 1];
		}
		
		//Simple mail function with HTML header
		function sendmail($to, $subject, $message, $from) {
			$headers = "MIME-Version: 1.0" . "\r\n";
			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
			$headers .= 'From: ' . $from . "\r\n";
			
			$result = mail($to,$subject,$message,$headers);
			
			if ($result) return 1;
			else return 0;
		}
		function check_input($value)
		{
		  	$value2 = stripslashes(strip_tags(trim($value)));
			// Quote if not a number
			if($value2!="")
			$value3 =  mysqli_real_escape_string($this->dbvr,$value2);
			return $value3;
		}
		function check_input1($value)
		{
		  	$value2 = addslashes(trim($value));
			return $value2;
		}
		
		function check_input3($value)
		{
		  	$value = addslashes(strip_tags(trim($value)));
			return $value;
		}
		function check_input4($value)
		{
		  	$value2 = stripslashes(trim($value));
			// Quote if not a number
			if($value2!="")
			$value3 =  mysqli_real_escape_string($this->dbvr,$value2);
			return $value3;
		}
		
		function baseencode($value)
		{
		  	if($value!="")
			$value2 = mysqli_real_escape_string($this->dbvr,base64_encode(trim($value)));
			return $value2;
		}
		
		function basedecode($value)
		{
		  	$value2 = stripslashes(base64_decode(trim($value)));
		    return $value2;
		}
		
	    function check_strip($value)
		{
		  	$value2 = stripslashes($value);
			return $value2;
		}
		
	    function check_tag($value)
		{
		  	$value2 = stripslashes(strip_tags($value));
			return $value2;
		}

		function upload_file($filed_name,$pref='swebin',$file_type='image',$folder="photos/orginal")
		{
		     // Example
			// $upload = $objgen->upload_file("banner_name","banner_2");
			 //if($upload[1]!="")
				//$errors[] = $upload[1];
			// else
				//$banner_name = $upload[0];
			
			$error = "";
			$file = strtolower(basename($_FILES[$filed_name]["name"]));
			if($file!=NULL)
			{
				$exp_file = explode(".",$file);
				$extn		  =	end($exp_file);
				$new_extn 	  = ".".strtoupper($extn);
              if($file_type=='image')
				{
					if($new_extn!=".JPEG"	&&	$new_extn!=".JPG"	&&	$new_extn!=".PNG"	&&	$new_extn!=".GIF")
					{
						$error = "File Type is Not Supported.";
					}
				}
				
				if($file_type=='doc')
				{
					if($new_extn!=".PDF" && $new_extn!=".DOCS" && $new_extn!=".DOCX" && $new_extn!=".CSV" && $new_extn!=".XLS" && $new_extn!=".TXT" && $new_extn!=".XLSX")
					{
						$error = "File Type is Not Supported.";
					}
				}
				
			   if($file_type=='zip')
				{
					if($new_extn!=".zip")
					{
						$error = "File Type is Not Supported.";
					}
				}
				
					
				if($error=="")
				{
					$ext = $this->extfind($file);
					$file_name  = $pref."_".$ext;
					move_uploaded_file($_FILES[$filed_name]["tmp_name"],ROOT_SITE."/".$folder."/".$file_name);
				}
					
					
			}
			return array($file_name,$error);
		}
		
	    function upload_resize($filed_name,$pref='swebin',$file_type='image',$rez=array(),$unlink="null",$file_size="",$l=array(600,400,'crop'),$m=array(200,130,'crop'),$s=array(100,75,'auto'),$folder="photos/orginal")
		{
		     // Example
			// $upload = $objgen->upload_file("banner_name","banner_2");
			 //if($upload[1]!="")
				//$errors[] = $upload[1];
			// else
				//$banner_name = $upload[0];
			
			//$rez=array('l','m','s');
			
			/*    Resize by exact width/height. (exact)
    Resize by width - exact width will be set, height will be adjusted according to aspect ratio. (landscape)
    Resize by height - like Resize by Width, but the height will be set and width adjusted dynamically. (portrait)
    Auto determine options 2 and 3. If you're looping through a folder with different size photos, let the script determine how to handle this. (auto)
    Resize, then crop. This is my favourite. Exact size, no distortion. (crop)*/
	
			
			$error = "";
			$file = strtolower(basename($_FILES[$filed_name]["name"]));
			if($file!=NULL)
			{
				$exp_file = explode(".",$file);
				$extn		  =	end($exp_file);
				$new_extn 	  = ".".strtoupper($extn);
              if($file_type=='image')
				{
					if($new_extn!=".JPEG"	&&	$new_extn!=".JPG"	&&	$new_extn!=".PNG"	&&	$new_extn!=".GIF")
					{
						$error = "File Type is Not Supported.";
					}
				}
				
				if($file_type=='doc')
				{
					if($new_extn!=".PDF" && $new_extn!=".DOCS" && $new_extn!=".DOCX" && $new_extn!=".CSV" && $new_extn!=".XLS" && $new_extn!=".TXT" && $new_extn!=".XLSX")
					{
						$error = "File Type is Not Supported.";
					}
				}
				
			   if($file_type=='zip')
				{
					if($new_extn!=".zip")
					{
						$error = "File Type is Not Supported.";
					}
				}
				
				if($file_size!="")
				{
					if($_FILES[$filed_name]["size"] > $file_size)
					{
					  $error = "File Size is greater than $file_size kb.";
					}
				}
				
					
				if($error=="")
				{
					
					if($unlink!="null")
					{
					
					      if(file_exists(ROOT_SITE."/".$folder."/".$unlink))
	 						unlink(ROOT_SITE."/".$folder."/".$unlink);
							
						 if(file_exists(ROOT_SITE."/photos/large/".$unlink))
	 						unlink(ROOT_SITE."/photos/large/".$unlink);
							
							
						 if(file_exists(ROOT_SITE."/photos/medium/".$unlink))
	 						unlink(ROOT_SITE."/photos/medium/".$unlink);
							
							
						 if(file_exists(ROOT_SITE."/photos/small/".$unlink))
	 						unlink(ROOT_SITE."/photos/small/".$unlink);

					}
					
					//print_r($rez);exit;
					
					$ext = $this->extfind($file);
					$file_name  = $pref."_".$ext;
					move_uploaded_file($_FILES[$filed_name]["tmp_name"],ROOT_SITE."/".$folder."/".$file_name);
					
				
					 if(count($rez)>0)
					 {
					   if(in_array('l',$rez))
					   {
						   $image 		= new resize(ROOT_SITE."/".$folder."/".$file_name);
						   $image->resizeImage($l[0],$l[1],$l[2]);
						   $image->saveImage(ROOT_SITE."/photos/large/".$file_name);
					   }
					   if(in_array('m',$rez))
					   {
						   $image 		= new resize(ROOT_SITE."/".$folder."/".$file_name);
						   $image->resizeImage($m[0],$m[1],$m[2]);
						   $image->saveImage(ROOT_SITE."/photos/medium/".$file_name);
					   }
					   if(in_array('s',$rez))
					   {
						   $image 		= new resize(ROOT_SITE."/".$folder."/".$file_name);
						   $image->resizeImage($s[0],$s[1],$s[2]);
						   $image->saveImage(ROOT_SITE."/photos/small/".$file_name);
					   }
					 
					 }
				 
				}
					
					
			}
			return array($file_name,$error);
		}
		
		function del_Images($table,$column,$where)
		{
		
           $result     = $this->get_Onerow($table,$where);
           $unlink   	    = stripslashes($result[$column]);

	  	  if(file_exists(ROOT_SITE."/photos/orginal/".$unlink))
	 		unlink(ROOT_SITE."/photos/orginal/".$unlink);
							
		  if(file_exists(ROOT_SITE."/photos/large/".$unlink))
	 		unlink(ROOT_SITE."/photos/large/".$unlink);
							
							
		  if(file_exists(ROOT_SITE."/photos/medium/".$unlink))
	 		unlink(ROOT_SITE."/photos/medium/".$unlink);
							
							
		  if(file_exists(ROOT_SITE."/photos/small/".$unlink))
	 		unlink(ROOT_SITE."/photos/small/".$unlink);

		}
		
		function dashboard_count($table,$title,$color,$image,$file,$where="")
		{
		
		      $count = $this->get_AllRowscnt($table,$where);
			  
			  $output = "";
			  $output .='<div class="col-lg-3 col-xs-6"><div class="small-box '.$color.'"><div class="inner">';
			  $output .='<h3>'.$count.'</h3><p>'.$title.'</p></div>';
              $output .='<div class="icon"><i class="fa '.$image.'"></i></div>';
              $output .='<a href="'.$file.'" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a></div></div>';
						
			  return $output;
		}
		
		
		function truncate_str($str, $maxlen) {
			if ( strlen($str) <= $maxlen ) return $str;
			
			$newstr = substr($str, 0, $maxlen);
			if ( substr($newstr,-1,1) != ' ' ) $newstr = substr($newstr, 0, strrpos($newstr, " "));
			
			return $newstr;
			}
}
?>