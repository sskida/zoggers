<?php
require_once "includes/includepath.php";
	
class booking_diary {


// Mysqli connection
function __construct() {
$this->HostName			=	DB_HOST;
$this->UserName			=	DB_USER;
$this->Password			=	DB_PASSWORD;
$this->DatabaseName		=	DB_NAME;

$link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
mysqli_select_db($link, DB_NAME) or die(mysql_error());
    $this->link = $link;
}

// Settings you can change:


// Time Related Variables
public $booking_start_time          = "09:30";			// The time of the first slot in 24 hour H:M format  
public $booking_end_time            = "19:00"; 			// The time of the last slot in 24 hour H:M format  
public $booking_frequency           = 30;   			// The slot frequency per hour, expressed in minutes.  	

// Day Related Variables

public $day_format					= 1;				// Day format of the table header.  Possible values (1, 2, 3)   
															// 1 = Show First digit, eg: "M"
															// 2 = Show First 3 letters, eg: "Mon"
															// 3 = Full Day, eg: "Monday"
	
public $day_closed					= array("Monday", "Tuesday"); 	// If you don't want any 'closed' days, remove the day so it becomes: = array();
public $day_closed_text				= "CLOSED"; 		// If you don't want any any 'closed' remove the text so it becomes: = "";

// Cost Related Variables
public $cost_per_slot				= 20.50;			// The cost per slot
public $cost_currency_tag			= "&#8377;";		// The currency tag in HTML such as &euro; &pound; &yen;


//  DO NOT EDIT BELOW THIS LINE

public $day_order	 				= array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
public $day, $month, $year, $selected_date, $back, $back_month, $back_year, $forward, $forward_month, $forward_year, $bookings, $count, $days, $is_slot_booked_today;


/*========================================================================================================================================================*/


function make_calendar($selected_date, $back, $forward, $day, $month, $year,$ajax) {

    // $day, $month and $year are the $_GET variables in the URL
    $this->day = $day;    
    $this->month = $month;
    $this->year = $year;
    
	// $back and $forward are Unix Timestamps of the previous / next month, used to give the back arrow the correct month and year 
    $this->selected_date = $selected_date;       
    $this->back = $back;
    $this->back_month = date("m", $back);
    $this->back_year = date("Y", $back); // Minus one month back arrow
    
    $this->forward = $forward;
    $this->forward_month = date("m", $forward);
    $this->forward_year = date("Y", $forward); // Add one month forward arrow    
    
    // Make the booking array88
    $this->make_booking_array($year, $month,$j = 0,$ajax);
    
}


function make_booking_array($year, $month, $j = 0,$ajax) {
	$this->bookings = array();
  	$query = "SELECT * FROM appointments WHERE app_date LIKE '$year-$month%'"; 
    $result = mysqli_query($this->link, $query) or die(mysqli_error($this->link)); 
	$this->is_slot_booked_today = 0; // Defaults to 0

    while ($row = mysqli_fetch_array($result)) {
		
		$this->bookings_per_day[$row['app_date']][] = $row['app_time'];
		
        $this->bookings[] = array(
            "name" => $row['name'], 
            "date" => $row['app_date'], 
            "start" => $row['app_time']        
            ); 
		
		
		// Used by the 'booking_form' function later to check whether there are any booked slots on the selected day  		
		if($row['app_date'] == $this->year . '-' . $this->month . '-' . $this->day) {
			$this->is_slot_booked_today = 1;
		} 
	
	} // Close loop	
	
	$break_start = $_SESSION['break_start_time'];
	$break_end = $_SESSION['break_end_time'];
	/*echo "session_week_off=-".$day_closed = $_SESSION['week_off'];*/
	$totalBreak = strtotime($break_end)-strtotime($break_start);
	$break = $totalBreak/($_SESSION['booktime']*60);

	$breakTime =array();
	$cn = 0;
	$dateBooking = $_SESSION['basket_year'] . '-' . $_SESSION['basket_month'] . '-' . $_SESSION['basket_day'];
	for ($i=strtotime($break_start); $i <  strtotime($break_end); $i=$i+$_SESSION['booktime'] * 60) { 
		if($cn <= count($break)){
			//echo $cn;echo'<br>';
			$break_slot = date("H:i:s",$i);	
			//array_push($breakTime, $break_slot);
			$breakTime[] = array(
            "name" => '', 
            "date" => $dateBooking, 
            "start" => $break_slot       
            ); 
			$cn++;
		}
		  //echo $break;
		  //$breakTime['start'] = $break_slot;
		 
	}
	$this->arr=array();	
//echo '<pre>';print_r($this->bookings);echo'<br>';
//echo '<pre>';print_r($breakTime);
		@$this->arr = array_merge($this->bookings,$breakTime);
//echo '<pre>';print_r($this->arr);	exit;	
	$this->slots_per_day = 0;
	for($i = strtotime($_SESSION['work_start_time']); $i<= strtotime($_SESSION['work_end_time']); $i = $i + $_SESSION['booktime'] * 60) {
		$this->slots_per_day ++;
	}	
		
    $this->make_days_array($year, $month,$ajax);    
            
} // Close function

 
function make_days_array($year, $month,$ajax) { 

    // Calculate the number of days in the selected month                 
    $num_days_month = cal_days_in_month(CAL_GREGORIAN, $month, $year); 
    
    // Make $this->days array containing the Day Number and Day Number in the selected month	   
	
	for ($i = 1; $i <= $num_days_month; $i++) {	
	
		// Work out the Day Name ( Monday, Tuesday... ) from the $month and $year variables
        $d = mktime(0, 0, 0, $month, $i, $year); 
		
		// Create the array
        $this->days[] = array("daynumber" => $i, "dayname" => date("l", $d)); 		
    }   

	/*	
	Sample output of the $this->days array:
	
	[0] => Array
        (
            [daynumber] => 1
            [dayname] => Monday
        )

    [1] => Array
        (
            [daynumber] => 2
            [dayname] => Tuesday
        )
	*/
	
	$this->make_blank_start($year, $month,$ajax);
	$this->make_blank_end($year, $month,$ajax);	

} // Close function


function make_blank_start($year, $month,$ajax) {

	/*
	Calendar months start on different days
	Therefore there are often blank 'unavailable' days at the beginning of the month which are showed as a grey block
	The code below creates the blank days at the beginning of the month
	*/	
	
	// Get first record of the days array which will be the First Day in the month ( eg Wednesday )
	$first_day = $this->days[0]['dayname'];	$s = 0;
		
		// Loop through $day_order array ( Monday, Tuesday ... )
		foreach($this->day_order as $i => $r) {
		
			// Compare the $first_day to the Day Order
			if($first_day == $r && $s == 0) {
				
				$s = 1;  // Set flag to 1 stop further processing
				
			} elseif($s == 0) {

				$blank = array(
					"daynumber" => 'blank',
					"dayname" => 'blank'
				);
			
				// Prepend elements to the beginning of the $day array
				array_unshift($this->days, $blank);
			}
			
	} // Close foreach	

} // Close function
	

function make_blank_end($year, $month,$ajax) {

	/*
	Calendar months start on different days
	Therefore there are often blank 'unavailable' days at the end of the month which are showed as a grey block
	The code below creates the blank days at the end of the month
	*/
	
	// Add blank elements to end of array if required.
    $pad_end = 7 - (count($this->days) % 7);

    if ($pad_end < 7) {
	
		$blank = array(
			"daynumber" => 'blank',
			"dayname" => 'blank'
		);
	
        for ($i = 1; $i <= $pad_end; $i++) {							
			array_push($this->days, $blank);
		}
		
    } // Close if
		
	$this->calendar_top($ajax); 

} // Close function
   
    
function calendar_top($ajax) {

	// This function creates the top of the table containg the date and the forward and back arrows 

if($ajax == 'true') {
echo "
    <div id='lhs_ajax' style='margin-left: 75px;background-color: #FFF;'>
    <div class='selected-date'>Select a date</div>
    <div id='outer_calendar'>
    
	<table border='1' cellpadding='0' cellspacing='0' id='calendar'>
        <tr id='week'>
        <td align='left'><a href='?edit_id=".$_SESSION['edit_id']."&model=yes&shop_id=".base64_encode($_SESSION['shop_id'])."&month=" . date("m", $this->back) . "&amp;year=" . date("Y", $this->back) . "'>&laquo;</a></td>
        <td colspan='5' id='center_date'>" . date("F, Y", $this->selected_date) . "</td>    
        <td align='right'><a href='?edit_id=".$_SESSION['edit_id']."&model=yes&shop_id=".base64_encode($_SESSION['shop_id'])."&month=" . date("m", $this->forward) . "&amp;year=" . date("Y", $this->forward) . "'>&raquo;</a></td>
    </tr>
    <tr>";
}else {
	echo "
    <div id='lhs' style='margin-left: 75px;background-color: #FFF'>
    <div class='selected-date'>Select a date</div>
    <div id='outer_calendar'>
    
	<table border='1' cellpadding='0' cellspacing='0' id='calendar'>
        <tr id='week'>
        <td align='left'><a href='?edit_id=".$_SESSION['edit_id']."&model=yes&shop_id=".base64_encode($_SESSION['shop_id'])."&month=" . date("m", $this->back) . "&amp;year=" . date("Y", $this->back) . "'>&laquo;</a></td>
        <td colspan='5' id='center_date'>" . date("F, Y", $this->selected_date) . "</td>    
        <td align='right'><a href='?edit_id=".$_SESSION['edit_id']."&model=yes&shop_id=".base64_encode($_SESSION['shop_id'])."&month=" . date("m", $this->forward) . "&amp;year=" . date("Y", $this->forward) . "'>&raquo;</a></td>
    </tr>
    <tr>";
}

	
		
	/*
	Make the table header with the appropriate day of the week using the $day_format variable as user defined above
	Definition:
	
		1: Show First digit, eg: "M"
		2: Show First 3 letters, eg: "Mon"
		3: Full Day, eg: "Monday"		
		
	*/
	
	foreach($this->day_order as $r) {
	
		switch($this->day_format) {
		
			case(1): 	
				echo "<th>" . substr($r, 0, 1) . "</th>";					
			break;
			
			case(2):
				echo "<th>" . substr($r, 0, 3) . "</th>";			
			break;
			
			case(3): 	
				echo "<th>" . $r . "</th>";
			break;
			
		} // Close switch
	
	} // Close foreach

			
	echo "</tr>";   

	$this->make_cells();
    
} // Close function


function make_cells($table = '') {

	echo "<tr>";

	foreach($this->days as $i => $r) { // Loop through the date array

		$j = $i + 1; $tag = 0;	 		
		
		// If the the current day is found in the day_closed array, bookings are not allowed on this day  
		if($r['dayname']==$_SESSION['week_off']) {			
			echo "\r\n<td width='5' valign='top' class='closed'>" . $this->day_closed_text . "</td>";		
			$tag = 1;
		}
		

		// Past days are greyed out
		if (mktime(0, 0, 0, $this->month, sprintf("%02s", $r['daynumber']) + 1, $this->year) < strtotime("now") && $tag != 1) {		
			
			echo "\r\n<td width='5' valign='top' class='past'>";			
				// Output day number 
				if($r['daynumber'] != 'blank') echo $r['daynumber']; 

			echo "</td>";		
			$tag = 1;
		}
		

		// If the element is set as 'blank', insert blank day
		if($r['dayname'] == 'blank' && $tag != 1) {		
			echo "\r\n<td width='15' valign='top' class='unavailable'></td>";	
			$tag = 1;
		}
				
				
		// Now check the booking array $this->booking to see whether we have a booking on this day 				
		$current_day = $this->year . '-' . $this->month . '-' . sprintf("%02s", $r['daynumber']);

		if(isset($this->bookings_per_day[$current_day]) && $tag == 0) {
		
			$current_day_slots_booked = count($this->bookings_per_day[$current_day]);

				if($current_day_slots_booked < $this->slots_per_day) {
				
					echo "\r\n<td width='5' valign='top'>
					<a onclick='zz(".$_SESSION['shop_id'].",".$this->month.",".$this->year.",".sprintf("%02s", $r['daynumber']).",".$_SESSION['edit_id'].")' href='#' class='part_booked' title='This day is part booked'>" . 
					$r['daynumber'] . "</a></td>"; 
					$tag = 1;
				
				} else {
				
					echo "\r\n<td width='5' valign='top'>
					<a onclick='zz(".$_SESSION['shop_id'].",".$this->month.",".$this->year.",".sprintf("%02s", $r['daynumber']).",".$_SESSION['edit_id'].")' href='#' class='fully_booked' title='This day is fully booked'>" . 
					$r['daynumber'] . "</a></td>"; 
					$tag = 1;			
				
				} // Close else	
		
		} // Close if

		
		if($tag == 0) {
			if(empty($_SESSION['edit_id'])){
			$_SESSION['edit_id'] = 0;
			}
			echo "\r\n<td width='5' valign='top'>
			<a onclick='zz(".$_SESSION['shop_id'].",".$this->month.",".$this->year.",".sprintf("%02s", $r['daynumber']).",".$_SESSION['edit_id'].")' href='#' class='green' title='Please click to view bookings'>" . 
			$r['daynumber'] . "</a></td>";			
		
		}
		
		// The modulus function below ($j % 7 == 0) adds a <tr> tag to every seventh cell + 1;
			if($j % 7 == 0 && $i >1) {
			echo "\r\n</tr>\r\n<tr>"; // Use modulus to give us a <tr> after every seven <td> cells
		}		
		
	}		
		
	echo "</tr></table></div><!-- Close outer_calendar DIV -->";
	echo "</div><!-- Close LHS DIV -->";

	if(isset($_SESSION['basket_month']))
	$this->basket();
		
	
	// Check booked slots for selected date and only show the booking form if there are available slots	
	$current_day = $this->year . '-' . $this->month . '-' . $this->day;	
	$slots_selected_day = 0;
	
	if(isset($this->bookings_per_day[$current_day]))
	$slots_selected_day = count($this->bookings_per_day[$current_day]);
	
	if($this->day != 0 && $slots_selected_day < $this->slots_per_day) { 
		$this->booking_form();
	}
	
	
} // Close function


function booking_form() {
//<p>The following slots are available on <span> " . $this->day . "-" . $this->month . "-" . $this->year . "</span></p>
	echo "
	<div id='rr'>
	<div id='outer_booking' style=''>
	<p style='border: 1px solid #C1C1C1;width: 58%;margin-left: 75px;margin-top: 15px;padding-top: 5px;padding-bottom: 5px;padding-left: 10px;color: #000;font-weight: 600;margin-bottom: -10px;'>Date<span style='margin-left: 13px;'>". $this->day. '-' . $this->month . '-' . $this->year."</span></p>
	<div id='outer_price'>
		<div class='currency'>Cost:" . $this->cost_currency_tag . " </div>
		<div class='total'>" . $_SESSION['bookprice'] . "</div>
	</div>	<br>
	<label>Time</label>
	
	<select class='form-control' id='time_slot' name='' style='margin-left: 3px;' onchange='xx(this.value);'>
	<option value=''>SELECT TIME SLOTS</option>";
		// <select class='form-control' id='time_slot' name='' onchange='xx(this.value);'>
		// Create $slots array of the booking times
		//$this->booking_frequency
		for($i = strtotime($_SESSION['work_start_time']); $i<= strtotime($_SESSION['work_end_time']); $i = $i + $_SESSION['booktime'] * 60) {
			$slots[] = date("H:i:s", $i);  
		}			
		// Loop through $this->bookings array and remove any previously booked slots
		if($this->is_slot_booked_today == 1 ) { // $this->is_slot_booked_today created in function 'make_booking_array'
		
		if($this->arr) { 
			foreach($this->arr as $i => $b) {
				
				if($b['date'] == $this->year . '-' . $this->month . '-' . $this->day) {
				
					// Remove any booked slots from the $slots array
					$slots = array_diff($slots, array($b['start']));

				} // Close if
				
			} // Close foreach
			}
		
		}else {
			
		if($this->arr) { 
			foreach($this->arr as $i => $b) {
				
				if($b['date'] == $this->year . '-' . $this->month . '-' . $this->day) {
				  
					// Remove any booked slots from the $slots array
					$slots = array_diff($slots, array($b['start']));

				} // Close if
				
			} // Close Foreach
		}
	} // Close if
		
				
		
		// Loop through the $slots array and create the booking table
		
		foreach($slots as $i => $start) {			
			// Calculate finish time
			//$this->booking_frequency
			$finish_time = strtotime($start) + $_SESSION['booktime'] * 60; 
		
			echo "
			
			<option value='" . $start . " - " . date("H:i:s", $finish_time) . "'>" . $start . "</option>";
		
		} // Close foreach			
	
		echo "</select></div></div><!-- Close outer_booking DIV -->";
		

} // Close function


function basket($selected_day = '') {
//echo 'sujeet';exit;
	if(!isset($_SESSION['basket_day']))
	$day = '01';
	else
	$day = $_SESSION['basket_day'];	

	// Validate GET date values
	if(checkdate($_SESSION['basket_month'], $day, $_SESSION['basket_year']) !== false) {
		$selected_day = $_SESSION['basket_year'] . '-' . $_SESSION['basket_month'] . '-' . $day;	
	} else { 
		echo 'Invalid date!';
		exit();
	}
	$objgen	=	new general();
	$uid = $_SESSION['ma_log_id_usr'];
	$qry_cat = "select * from users where id=$uid";
	$subcat_row = $objgen->get_AllRows_qry($qry_cat);
	echo "<div class='outer_basket'>
		<div id='ss'></div>
		<div class='clear'></div>
	
			<div class='basket_details'>
			
				<form method='post' action='search-details.php'>
					
					<label>Name</label>
					<input name='name' id='name' type='text' class='form-control' value='".$subcat_row[0]['name']."'><br>
					
					<label>Email</label>
					<input name='email' id='email_usr' type='text' class='form-control' value='".$subcat_row[0]['email']."'>	<br>
					
					<label>Phone</label>
					<input name='phone' id='phone' type='text' class='form-control' value='".$subcat_row[0]['phone']."' style='margin-left:-5px;'>	<br>
					<input name='edit_id' id='edit_id' type='hidden' class='form-control' value='".$_SESSION['edit_id']."'>
					
					
														
					
					<input type='hidden' name='slots_booked' class='slots_booked'>
					<input type='hidden' name='cost_per_slot' id='cost_per_slot' value='" . $_SESSION['bookprice'] . "'>
					<input type='hidden' name='booking_date' value='" . $_SESSION['basket_year'] . '-' . $_SESSION['basket_month'] . '-' . $day . "'>
					<div class='row'>
					
					<input type='submit' class='btn btn-default cash-on-service' name='book_submit' value='Cash on Service' style='width:250px !important;margin-left:90px !important;'>
					
					</div>

				</form>
			
			</div><!-- Close basket_details DIV ---->
		
	</div><!-- Close outer_basket DIV -->";

} // Close function

                 
} // Close Class

?>