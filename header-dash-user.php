<header>
  <div class="header-top"> <a href="<?=URL?>index" class="logo"> <img src="<?=URL?>front-end-images/zoggrs_logo.png" alt="zoggrs"> </a>
    <nav class="navbar navbar-static-top" role="navigation" <?php if( $_SESSION['ma_log_id_usr']) {?>style="background: none repeat scroll 0 0 white;"<?php }?>> <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
      <div class="navbar-right">

        <ul class="nav navbar-nav">
         
          <!-- Tasks: style can be found in dropdown.less --> 
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa-color-top fa fa-user" <?php if( $_SESSION['ma_log_id_usr']) {?>style="color: black;"<?php }?>></i> <span> <i class="caret" <?php if( $_SESSION['ma_log_id_usr']) {?>style="color: black;"<?php }?>></i></span> </a>
            
            <ul class="dropdown-menu">

              <li class="user-header bg-light-blue"><i class="fa fa-user fa-5x"></i>
                <p> <?=$_SESSION['ma_name_usr']?> </p>
              </li>
              <li class="user-footer">
                <div class="pull-left"> <a href="<?=URL?>view-profile-user.php" class="btn btn-default btn-flat">Profile</a> </div>
                <div class="pull-right"> <a href="<?=URL?>logout-usr.php" class="btn btn-default btn-flat">Sign out</a> </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</header>