    <?php include"header-front-end.php"; ?>
    
     <link rel="stylesheet" type="text/css" href="front-end-css/wellness_style.css" />
     <style>
        .content {
            width:500px;
            font-size:36px;
            line-height:40px;
            font-family:'Muli';
            color:#808080;;
            height:40px;
            position:absolute;
            top:33%;
            left:34%;
            margin-top:0px;
            margin-left:-118px;
            
            &:before {
              content:'[';
              position:absolute;
              left:-30px;
              line-height:40px;
            }
            &:after {
              content:']';
              position:absolute;
              right:-30px;
              line-height:40px;
            }
            &:after, &:before {
              color:#16a085;
              font-size:42px;
              animation:2s linear 0s normal none infinite opacity;
              -webkit-animation:2s ease-out 0s normal none infinite opacity;
              -moz-animation:2s ease-out 0s normal none infinite opacity;
              -o-animation:2s ease-out 0s normal none infinite opacity;
            }
          }

          .visible {
            float:left;
            font-weight:normal;;
            overflow:hidden;
            height:40px;   
          }

           .content p {
            display:inline;
           /* float:left;*/
            margin:0;
          }

          .content ul {
            margin-top:0;
            padding-left:110px;
            text-align:left;
            list-style:none;
            animation:6s linear 0s normal none infinite change;
            -webkit-animation:6s linear 0s normal none infinite change;
            -moz-animation:6s linear 0s normal none infinite change;
            -o-animation:6s linear 0s normal none infinite change;
          }

          .content ul li {
            line-height:40px;
            margin:0;
          }

          @-webkit-keyframes opacity {
            0%   {opacity:0;}
            50%  {opacity:1;}
            100% {opacity:0;}
          }
          @keyframes opacity {
            0%   {opacity:0;}
            50%  {opacity:1;}
            100% {opacity:0;}
          }

          @-webkit-keyframes change {
            0%   {margin-top:0;}
            15%  {margin-top:0;}
            25%  {margin-top:-40px;}
            40%  {margin-top:-40px;}
            50%  {margin-top:-80px;}
            65%  {margin-top:-80px;}
            75%  {margin-top:-40px;}
            85%  {margin-top:-40px;}
            100% {margin-top:0;}
          }
          @keyframes change {
            0%   {margin-top:0;}
            15%  {margin-top:0;}
            25%  {margin-top:-40px;}
            40%  {margin-top:-40px;}
            50%  {margin-top:-80px;}
            65%  {margin-top:-80px;}
            75%  {margin-top:-40px;}
            85%  {margin-top:-40px;}
            100% {margin-top:0;}
          }
</style>
    <section class="container">
      <div class="main" >
       
       <link href='http://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>
        <h3 class="caption">
        <div class='content'>
          <div class='visible'>
            
            <ul>
              <li style="color: #41A8F7;font-weight: 300;">Fitness</li>
              <li style="color: #EC86B9;font-weight: 300;">Beauty</li>
              <li style="color: #41A8F7;font-weight: 300;">Fitness</li>
            </ul>
           
          </div>
           <p style="color: #808080;">
              , the smart way
            </p>
        </div>
        <img src="front-end-images/arrow_down.png" alt="" />
        </h3>
        <div class="tab_c">
          <!-- Main tabs  -->
            <div class="tab  top_radius_corner_3 margin_left-1 left">
              <a href="#" class="active_tab"><span  class="wellness_icon">Wellness</span></a>
            </div>
              <div class="tab_sub_btn_c top_radius_corner_3 clear">       
              <div class="sub_tab sub_tab1 left">
                <h4><span class="salon_spa active_tab">Salon & Spa</span></h4>
              </div>
              <div class="sub_tab sub_tab2 border_right left">
                <h4><span class="gym">Gym</span></h4>
              </div>
              <div class="clear"></div>
            </div>
            
            <div class="tab_info_c radius_corner_3 border_top_0">
              <form method="post" action="search.php">
                <div class="left"><input type="text"  id="spe_salon" name="subcategory" class="search_speciality" placeholder="Services" value="" /></div>
                <div class="left"><input type="text" id="location_salon" name="area" class="area_speciality" placeholder="Area" value="" /></div>
                <input type="hidden" id="salon_city" value="" name="city_id">
                <input type="hidden" id="category_all" value="17,16,14" name="category">
                <input type="submit" style="margin-left:14px;"class="btn " name="search" placeholder="Speciality" value="Search" />
                
              </form>
              <div class="clear"></div>
            </div>
            <div class="tab_info_c radius_corner_3 border_top_0">
              <form method="post" action="search.php">
                <div class="left"><input type="text" id="spe_gym" name="subcategory" class="search_speciality" placeholder="Services" value="" /></div>
                <div class="left"><input type="text" id="location_gym" name="area" class="area_speciality" placeholder="Area" value="" /></div>
                <input type="hidden" id="gym_city" value="" name="city_id">
                <input type="hidden" id="category_gym" value="15" name="category">
                <input type="submit" style="margin-left:14px;" class="btn" name="search" placeholder="Speciality" value="Search" />
                
              </form>
              <div class="clear"></div>
            </div>        
            <div class="tab_info_c radius_corner_3 border_top_0">
              <div class="left"><input type="text" class="search_speciality" placeholder="Speciality" value="" /></div>
              <div class="left"><input type="text" class="area_speciality" placeholder="Area" value="" /></div>
              
              <a href="search.php" style ="margin-left: 15px;height: 17px;padding-top: 2px;font-size: 1em; width: 53px;"class="btn left">Search</a>
              <div class="clear"></div>
            </div>    
        </div>
          
          <div class="clear"></div>
      </div>    
    </section>
      
      <div class="clear"></div>
      <div class="section or">
        <h1>OR</h1>
      </div>
      
      <div class="felling_lazy_c">
        <div class="felling_lazy_icon_c left">
          <a href="#recommendedForYou" class="right felling_lazy">
            Feeling Lazy?
            <span></span>
          </a>
          <div class="clear"></div>
        </div>
        <div class="felling_lazy_info_c left">
          <div class="left">
            <h4>Quickly access list of Wellness centers carefully selected by Desileaks just for you</h4>
          </div>  
          <div class="clear"></div>
        </div>
        <div class="clear"></div>
      </div>
      
      
      
      
      <span id="howItWork" style="display:block;padding-bottom: 3em;"></span>
      <div class="center_page_icon">      
        <div class="icon_c">
          <img src="front-end-images/how_it_works_icon.png" alt="how it works icon" />
        </div>
      </div>
      <div class="clear"></div>
      <section>
        <div class="section how_it_work_c">
          <h1>How does Desileaks work?</h1>
          <h5>An awesome experience in just three steps</h5>
        
          <div class="how_it_work_icon_c">
            <div class="icon_info_c left">
              <div class="icon_c">
                <a href="#"><img src="front-end-images/search_icon2.png" alt="Search" /></a>
              </div>
              <h2>Search</h2>
              <p>Search through our comprehensive list of healthcare and wellness centers</p>
            </div>
            <div class="arrow_c left">            
            </div>
            <div class="icon_info_c left">
              <div class="icon_c">
                <a href="#"><img src="front-end-images/select_icon.png" alt="Search" /></a>             
              </div>
              <h2>Select</h2>
              <p>Compare and pick up the best option for you</p>
            </div>
            <div class="arrow_c left">            
            </div>
            
            <div class="icon_info_c left">
              <div class="icon_c">
                <a href="#"><img src="front-end-images/login_and_access_icon.png" alt="Search" /></a>             
              </div>
              
              <h2>Login & Access Perks</h2>
              
              <p>Gain access to various offers and features just by login in to Desileaks<span id="recommendedForYou"></span></p>
                
            </div>
            <div class="clear"></div>
          </div>
          <div class="clear"></div>   
        
        </div>
      </section> 
      
      <div class="clear"></div>     
      <div class="center_page_icon">        
        <div class="icon_c">
          <img src="front-end-images/recommended_for_you_icon.png" alt="how it works icon" />
        </div>
      </div>
      <div class="clear"></div>
      <section>
        <div class="section how_it_work_c">
          <h1>Recommended For You</h1>
          <h5>We hand pick the best for you when you're in need of quick beauty fix or seamless health treatment</h5>
          
          <div class="recommended_for_you_c">
          <div id="recomanded"></div>
            
            <div class="clear"></div>
          </div>        
        </div>
      </section>
      <div class="clear"></div>     
      <div class="center_page_icon"  style="margin-top: 8em;">    
        <div class="icon_c">
          <img src="front-end-images/mission_icon.png" alt="how it works icon" />
        </div>
      </div>
      <div class="clear"></div>
     <section> 
      <div class="section our_mission_c">
        <h1>Our Mission</h1>
        <!-- <h5>Some line will go here</h5> -->
        
        <div class="four_column_c our_mission_icon_c">        
          <div class="col_four mission_info_icon left">
            <img src="front-end-images/find_joy_in_simplicity.png" alt="Search" />
            <div class="download_arrow">
            </div>
          </div>
          <div class="col_four mission_info_icon col_four_l_margin left">
            <img src="front-end-images/regain_focus.png" alt="Search" />
            <div class="download_arrow">
            </div>
          </div>
          <div class="col_four mission_info_icon col_four_l_margin left">
            <img src="front-end-images/deal_with_fear.png" alt="Search" />
            <div class="download_arrow">
            </div>            
          </div>    
          <div class="col_four mission_info_icon right">
            <img src="front-end-images/but_eventually.png" alt="Search" />  
            <div class="download_arrow">
            </div>
          </div>    
          <div class="clear" style="margin-bottom: 1.2em;"></div>
        </div>
        
        <div class="our_mission_info_c">
          <div class="mission_info">
            <h4>Find joy in Simplicity</h4>
            <p>We want you to quickly take an informed decision through a carefully designed simple and soothing interface </p>
          </div>
          <div class="mission_info">
            <h4>Regain Focus</h4>
            <p>We think that focus on your health and personality can make your life simpler</p>
          </div>
          <div class="mission_info">
            <h4>Deal with fear</h4>
            <p>People are sceptical about their looks &amp; health for many reasons. If we can somehow ease that fear, then why not !!</p>
          </div>
          <div class="mission_info">
            <h4>But eventually</h4>
            <p>We just honour your smile</p>
          </div>
        </div>
        
      </div>
    </section>
      <script>
    $(document).ready(function(){
    if (navigator.geolocation) {

        navigator.geolocation.getCurrentPosition(showPosition);


          
    } else { 


        x.innerHTML = "Geolocation is not supported by this browser.";
    }

    function showPosition(position) {
      

       var Latitude = position.coords.latitude ; 
       var Longitude = position.coords.longitude; 
       
       $.ajax({
        type:"POST",
        url:"/get-city-latlong.php",
        data:"lattitude="+Latitude+"&longitude="+Longitude,
        success: function(data){
                    $("#recomanded").html(data);
                }
       }); 
    }

  $('#city_id').change(function() {
   
    var city= $("#city_id").val(); 
    var currurl = window.location.pathname;
    var indexfile = currurl.lastIndexOf("/") + 1;
    var filename = currurl.substr(indexfile);
    var file ="index.php";
    var file1 ="index";
    if(!(filename == file || filename == file1)){
       
          $.post("city-post.php",
          {
              city: city
          },
          function(data, status){
              $.ajax({
                type: "POST",
                url: "get-recommanded-shops.php",
                data: "city="+data,
               success: function(html){
                $("#recomanded").html(html);
                window.location = "index.php";
               },
            });
          });
          
        
      }else {
        
          $.ajax({
            type: "POST",
            url: "get-recommanded-shops.php",
            data: "city="+city,
           success: function(html){
            $("#recomanded").html(html);
           },
        });
      }
  });

  var city= $("#city_id").val();
    
    $.ajax({
        type: "POST",
        url: "get-recommanded-shops.php",
        data: "city="+city,
       success: function(html){
        $("#recomanded").html(html);
       },
    });
});
    </script>
     <?php include "footer-front-end.php"?>
