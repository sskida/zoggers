<?php 
require_once "includes/includepath.php";
$objgen = new general();
$group = implode(",", $_POST['group']);
 $stars = trim($_POST['stars']);
$title = trim($_POST['title']);
$description = trim($_POST['description']);
 $user_id = trim($_POST['user_id']);
 $review_id = trim($_POST['review_id']);
 $shop_id = trim($_POST['shop_id']);
$date = date("Y-m-d");

if($review_id!="" && $stars!="" && $user_id!="" && $shop_id!=""){

	$msg_review = $objgen->upd_Row('review', "`shop_id`='" . $shop_id . "',`group_id`='" . $group . "',`long_desc`='" . $description . "',`user_id`='" . $user_id . "',`created_date`='" . $date . "',`star`='" . $stars . "'", "id=" . $review_id);
	if($msg_review==""){
	echo'true';
			if($_SESSION['ma_log_id_usr']!=""){
                $sql_review = "SELECT r.*,u.name,u.city FROM review r INNER JOIN users u ON u.id=r.user_id  WHERE r.shop_id='".$shop_id ."' AND r.user_id='".$_SESSION['ma_log_id_usr']."'  ORDER BY id DESC";
              }else{ 
                  $sql_review = "SELECT r.*,u.name,u.city FROM review r INNER JOIN users u ON u.id=r.user_id  WHERE r.shop_id='".$shop_id ."'ORDER BY id DESC";
              }
              $result_review = $objgen->get_AllRows_qry($sql_review);
              if($result_review) { 
              foreach ($result_review as $review) {
              ?>
              <div id="sorting">
              <div class="user_review_c margin_bottom-5" >
                <div class="fade_1"></div>
                <div class="modal_1">
                    <img class="loader" src="front-end-images/ajax-loader.gif" />
                </div>
              <div class="user_photo_name_c margin_bottom-5">
                <img class="review_user_photo left" src="front-end-images/well-login_and_access_icon.png" width="81" height="81" alt="User Photo" />
                <div class="user_name left margin_left-1 margin_top-1">
                  <div class="name_add left">
                    <h2 style="margin-top:0px !important;">Mrs.<?php echo ucfirst($review['name']); ?></h2>
                    <p><?php echo ucfirst($review['city']); ?></p>
                  </div>
                  <div class="left  margin_left-5" style="border-right: 1px solid #DDD;padding-right: 10em;">
                    <p>Date: <?php echo date('d M Y',strtotime($review['created_date'])); ?></p>
                    <div class="user_ratings">
                      <p class="left margin_right-3">Rating: </p>
                      <ul class="star_rating_ul left margin_top-2" >
                        <?php
                        for($i=1;$i<=5;$i++) {
                          $selected = "";
                            if(!empty($review["star"]) && $i<=$review["star"]) {
                            $selected = "selected";
                            }
                            ?>
                            <li class='<?php echo $selected; ?>' >&#9733;</li>  
                        <?php }  ?>
                      </ul>
                      <div class="clear"></div>
                    </div>
                  </div>
                  <?php if($_SESSION['ma_log_id_usr']!=""){ ?>
                    <div class="left  margin_left-5" style="padding-top: 10px;">
                      <a href="#" id="<?php echo $review['id']?>" style="color:black;font-size: 13px;" onclick="get_review(this.id)" class="btnuser">EDIT</a> 
                      <a href="#" id="<?php echo $review['id']?>" style="color:black;font-size: 13px; " onclick="delete_review(this.id,"<?php echo $shop_id; ?>")" class="btnuser">DELETE</a>
                    </div>                    
                    <div class="clear"></div>
                  <?php } ?>
                </div>
                <div class="clear"></div>
              </div>
              <p class="review_p"><?php echo ucfirst(trim($review['long_desc'])); ?></p>
                <?php 
                  $groupIds= explode(',',$review['group_id']);

                  for($i=0; $i<count($groupIds);$i++){
                    
                    $gruop_title = $objgen->get_Onerow("groups","AND id=".$groupIds[$i] );
                    ?>
                     <div class="service-tag"><a href="#"><?php echo ucfirst($gruop_title['group_title']); ?></a></div>
                <?php }?></div>
            <?php   }  } else {?>
              <div class="user_review_c margin_bottom-5">
            <p class="review_p" style="text-align:center">Does not have a review yet.</p>
            </div>
            <?php }?>
            </div> 
	<?php
	}
	else{
		echo $msg_review;
	}
}
else {
	echo "All fields required";exit;
}