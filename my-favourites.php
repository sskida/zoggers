<?php
require_once "chk_login_usr.php";
$objgen		=	new general();

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if(isset($_GET['del']))
{
   $id= $_GET['del'];
   $msg     = $objgen->del_Row("favourites","id=".$id." and user_id=".$_SESSION['ma_log_id_usr']);
    if($msg=="")
   {
	header("location:".URL."my-favourites.php?msg=3&page=".$page);
   }
}

if($_GET['msg']==3)
{

  $msg2 = "Favourite Removed Successfully.";
}



$where = " and user_id=".$_SESSION['ma_log_id_usr'];
$row_count = $objgen->get_AllRowscnt("favourites",$where);
if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array(), 1, WEBLINK."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("favourites",$pagesize*($page-1),$pagesize,"id desc",$where);
}


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>
</head>

<body>
<?php include 'header-dash-user.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
     <?php include 'user-menu.php'; ?>
  <aside class="right-side">
    <section class="content-header-top">
      <h1> <i class="fa fa-suitcase"></i> My favourites </h1>
    </section>
    <section class="content">
     <div class="col-xs-12">
        <div class="box-body table-responsive">
			
			   <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Shop Name</th>
                <th>Area</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
				<?php
				if($row_count>0)
				{
					foreach($res_arr as $key=>$val)
				    {
					$shop_res     	 = $objgen->get_Onerow("shop","AND id=".$val['shop_id']);
          $entity_res      = $objgen->get_Onerow("categories","AND id=".$shop_res['categories']);
          $location_res       = $objgen->get_Onerow("location","AND id=".$shop_res['locality']);
          $city_res        = $objgen->get_Onerow("city","AND id=".$shop_res['city']);
					
				 ?>
              <tr>
                <td>
                  <a href="<?=URL?>search-details.php?shop_id=<?php echo $shop_res['id'];  ?>">
                      <?php echo $objgen->check_tag($shop_res['shop_name']); ?>
                  </a><br/>
                  Entity:<?php echo $objgen->check_tag($entity_res['category']); ?> 
                </td>
			          <td>Building Name : <?php echo $objgen->check_tag($shop_res['building_name']); ?><br/>Street Name : <?php echo $objgen->check_tag($shop_res['street']); ?><br />Area : <?php echo $objgen->check_tag($location_res['location']); ?><br />City : <?php echo $objgen->check_tag($city_res['city']); ?></td>
                <td><a class="arb-icon cl7-light-blue" href="<?=URL?>my-favourites.php/?del=<?=$val['id']?>&page=<?=$page?>" onClick="return confirm('Do you want to delete this Favourite?')" ><i class="fa fa-trash-o"></i>Delete</a></td>
              </tr>
			  <?php
			  }
			  }
			  ?>
            </tbody>
          </table>
		  
		  <?php
									if($row_count > $pagesize) 
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">
										
											<div class="dataTables_paginate paging_bootstrap">
											    		
												<?php echo $pages; ?>

												
											</div>
											
									   </div>
									</div>
									<?php
										 }
									?>
									
        </div>
        <!-- /.box-body --> 
        
      </div>
    </section>
  </aside>
</div>
<?php include 'footer-script-dash.php'; ?>
<script src="<?=URL?>js/jquery.dataTables.js" type="text/javascript"></script> 
<script src="<?=URL?>js/dataTables.bootstrap.js" type="text/javascript"></script> 
<script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
</body>
</html>
