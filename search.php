<?php 
require_once "includes/includepath.php";

$objgen     =   new general();

if(isset($_POST['search']) && !empty($_POST['search'])){
		$city_id = $_POST ['city_id'];
		$_SESSION['city']=$city_id;
		$subcategory = $_POST ['subcategory'];
		$location = $_POST ['area'];
		$category = $_POST ['category'];
		
		
		
	if (empty($subcategory) && empty($location)) {
		
		 $sql_shop = "SELECT s.*,l.location,c.city as city_name, categories.category,categories.icon as cat_icon  FROM shop s INNER JOIN categories ON categories.id=s.categories INNER JOIN location l ON l.id=s.locality INNER JOIN city c ON c.id = s.city WHERE s.status='active' AND s.categories IN ($category) AND s.city='".$city_id."' ";
		
		
	}
	elseif(empty($subcategory) && !empty($location))
	{
		
		$locality = $objgen->get_Onerow("location","AND location='".$location."'");
		if($locality) {
			$location_id = $locality['id'];
			$location_name = $locality['location'];
			$sql_shop = "SELECT s.*,l.location,c.city as city_name, categories.category,categories.icon as cat_icon  FROM shop s INNER JOIN categories ON categories.id=s.categories INNER JOIN location l ON l.id=s.locality INNER JOIN city c ON c.id = s.city WHERE s.status='active' AND  s.locality = '".$location_id."' AND s.categories IN ($category)";
		} else {
			
			$locality = $objgen->get_Onerow("city","AND city='".$location."'");
			$shop_city_id = $locality['id'];
			$city_name = $locality['city']; 
			$sql_shop = "SELECT s.*,l.location,c.city as city_name, categories.category,categories.icon as cat_icon  FROM shop s INNER JOIN categories ON categories.id=s.categories INNER JOIN location l ON l.id=s.locality INNER JOIN city c ON c.id = s.city WHERE s.status='active' AND s.city = '".$city_id."' AND s.categories IN ($category)";
		}
	}
	elseif(!empty($subcategory) && empty($location))
	{
		
		$sub_cat_sql = $objgen->get_Onerow("subcategories","AND subcategory='".$subcategory."'");
		$subcategory_id = $sub_cat_sql['id'];
	    $subcategory_name = $sub_cat_sql['subcategory'];
	    $shop_category_id =$sub_cat_sql['cat_id'];
	    $cat_sql = $objgen->get_Onerow("categories","AND id='".$shop_category_id."'");
	    $category_name =$cat_sql['category']; 
		

		$sql_shop_subcat = "SELECT distinct(shop_id) FROM shop_categories WHERE sub_cat IN ($subcategory_id) ";
		$shop_subcat_arr = $objgen->get_AllRows_qry($sql_shop_subcat);
		$shop_id_arr =array();
		foreach ($shop_subcat_arr as $value) {

			$id = $value['shop_id'];
			array_push($shop_id_arr, $id);
		}
		 $shop_ids = implode(',', $shop_id_arr);

		  $sql_shop = "SELECT s.*,c.city as city_name,l.location, categories.category,categories.icon as cat_icon  FROM shop s INNER JOIN categories ON categories.id=s.categories INNER JOIN location l ON l.id=s.locality INNER JOIN city c ON c.id = s.city WHERE s.status='active' AND s.id IN ($shop_ids) AND s.categories IN ($category)";
	} else {

		$city = $objgen->get_Onerow("city","AND id='".$city_id."'");
		$city_name = $city['city'];

		$locality = $objgen->get_Onerow("location","AND location='".$location."' AND city_id='".$city_id."'");
		
		$location_id = $locality['id'];
		$location_name = $locality['location'];

		
		$sub_cat_sql = $objgen->get_Onerow("subcategories","AND subcategory='".$subcategory."'");
		$subcategory_id = $sub_cat_sql['id'];
	    $subcategory_name = $sub_cat_sql['subcategory'];

		

		$sql_shop_subcat = "SELECT distinct(shop_id) FROM shop_categories WHERE sub_cat IN ($subcategory_id)";
		$shop_subcat_arr = $objgen->get_AllRows_qry($sql_shop_subcat);
		$shop_id_arr =array();
		foreach ($shop_subcat_arr as $value) {

			$id = $value['shop_id'];
			array_push($shop_id_arr, $id);
		}
		 $shop_ids = implode(',', $shop_id_arr);
		 if($location_id) {
			$sql_shop = "SELECT s.*,c.city as city_name,l.location, categories.category,categories.icon as cat_icon  FROM shop s INNER JOIN categories ON categories.id=s.categories INNER JOIN location l ON l.id=s.locality INNER JOIN city c ON c.id = s.city WHERE s.status='active' AND  s.city='".$city_id."' AND s.locality= '".$location_id."' AND s.id IN ($shop_ids) AND s.categories IN ($category)";
		 
		 }else{
		 	$sql_city = $objgen->get_Onerow("city","AND city='".$location."'");
			$city_id = $sql_city['id'];
			$city_name = $sql_city['city'];
		  	$sql_shop = "SELECT s.*,c.city as city_name,l.location, categories.category,categories.icon as cat_icon  FROM shop s INNER JOIN categories ON categories.id=s.categories INNER JOIN location l ON l.id=s.locality INNER JOIN city c ON c.id = s.city WHERE s.status='active' AND s.city='".$city_id."' AND s.id IN ($shop_ids) AND s.categories IN ($category)";
		  }

		  
	}
	if(!empty($subcategory) &&  !empty($location)){
		  	$sub_cat_sql = $objgen->get_Onerow("subcategories","AND subcategory='".$subcategory."'");
			$sub_category_id = $sub_cat_sql['id'];
			$sub_category_name = $sub_cat_sql['subcategory'];
			$category_id = $sub_cat_sql['cat_id'];
	    	$cat_sql = $objgen->get_Onerow("categories","AND id='".$category_id."'");
	    	$category_name = $cat_sql['category'];
	    	$locality = $objgen->get_Onerow("location","AND location='".$location."'");
			$location_id = $locality['id'];
			$location_name = $locality['location'];
			$location_city = $locality['city_id'];
			$IP = $_SERVER['REMOTE_ADDR'];
			$date = date("Y-m-d");
			if(!empty($_SESSION['ma_log_id_usr'])) {
				$user =$objgen->get_Onerow("users","AND id=".$_SESSION['ma_log_id_usr']);
				$email = $user['email'];
				$mobile=$user['phone'];
				$name=$user['name'];
				$ff = $objgen->ins_Row('visitor_logs','service_id,service_name,email,mobile,location_id,city_id,user_id,user_name,ip_address,date,subcategory_id,subcategory_name',"'".$category_id."','".$category_name."','".$email."','".$mobile."','".$location_id."','".$location_city."','".$_SESSION['ma_log_id_usr']."','".$name."','".$IP."','".$date."','".$sub_category_id."','". $sub_category_name."'");
			}else{
				$ff = $objgen->ins_Row('visitor_logs','service_id,service_name,location_id,city_id,ip_address,date,subcategory_id,subcategory_name',"'".$category_id."','".$category_name."','".$location_id."','".$location_city."','".$IP."','".$date."','".$sub_category_id."','". $sub_category_name."'");
			}


		  } 
		  if(!empty($subcategory) &&  empty($location)){
		  	$sub_cat_sql = $objgen->get_Onerow("subcategories","AND subcategory='".$subcategory."'");
			$sub_category_id = $sub_cat_sql['id'];
			$sub_category_name = $sub_cat_sql['subcategory'];
			$category_id = $sub_cat_sql['cat_id'];
	    	$cat_sql = $objgen->get_Onerow("categories","AND id='".$category_id."'");
	    	 $category_name = $cat_sql['category'];
			$IP = $_SERVER['REMOTE_ADDR'];
			$date = date("Y-m-d");
			if(!empty($_SESSION['ma_log_id_usr'])) {
				$user =$objgen->get_Onerow("users","AND id=".$_SESSION['ma_log_id_usr']);
				$email = $user['email'];
				$mobile=$user['phone'];
				$name=$user['name'];
				$ff = $objgen->ins_Row('visitor_logs','service_id,service_name,email,mobile,city_id,user_id,user_name,ip_address,date,subcategory_id,subcategory_name',"'".$category_id."','".$category_name."','".$email."','".$mobile."','".$city_id."','".$_SESSION['ma_log_id_usr']."','".$name."','".$IP."','".$date."','".$sub_category_id ."','".$sub_category_name ."'");
			}else{
				$ff = $objgen->ins_Row('visitor_logs','service_id,service_name,city_id,ip_address,date,subcategory_id,subcategory_name',"'".$category_id."','".$category_name."','".$city_id."','".$IP."','".$date."','".$sub_category_id ."','".$sub_category_name ."'");
			}


		  } 
}else {
	
	 $sql_shop = "SELECT s.*,l.location,c.city as city_name, categories.category,categories.icon as cat_icon FROM shop s INNER JOIN categories ON categories.id=s.categories INNER JOIN location l ON l.id=s.locality INNER JOIN city c ON c.id = s.city WHERE s.status='active' AND s.city='".$_SESSION['city']."'";
}
$_SESSION['query'] = $sql_shop;
$shop_arr = $objgen->get_AllRows_qry($sql_shop);
$cc = array();
foreach ($shop_arr as $dd) {
		$ca =  $dd['categories'];
		array_push($cc,$ca);
	}
$result_cat = array_unique($cc);	
$res_session = implode(",", $result_cat);
$_SESSION['cat'] = $res_session;
$ss="SELECT * FROM categories WHERE id IN ($res_session)";
$pp =$objgen->get_AllRows_qry($ss);
$p_cat=array();
foreach ($pp as $v) {
 	$sp = $v['category'];
 	array_push($p_cat, $sp);
 } 
 $p_cat_name = implode(",", $p_cat);
 $city = $objgen->get_Onerow("city","AND id='".$_SESSION['city']."'");
 $city_name = $city['city'];
?>
	<?php include "header-front-end.php";?>	
		
	
		<div class="row" id="result1" >
			<div class="searched_string_c" style="margin-top: 130px;width: 97.5%;margin-left: 14px;">
				<h3 ><span class="result_counter"><?php echo count($shop_arr); ?></span> Results for <span class="searched_string">"<?php if(!empty($p_cat_name)){ echo $p_cat_name;} else {echo "Services"; }?> in <?php if(!empty($city_name)){ echo $city_name;} else {echo "all cities";}?>"</span></h3>
								
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
				<div class="left-part ">
					<div id="filterdata" class="filters_c ">
						<div class="padding-5 checkBox_c">
							<h2 class="filter_icon" style="color: hsl(338, 81%, 71%);font-size: 25px;font-weight: 400;">Filters</h2>
							<h4 class="filter_type">Service type</h4>
								<ul id="Service">
								<?php 
									if($result_cat){
										$shop_cat_sql ="SELECT * FROM categories WHERE id IN (".implode(',',$result_cat).")";
										$sql_categories =$objgen->get_AllRows_qry($shop_cat_sql);
										
									}else {
										$sql_categories =$objgen->get_AllRows("categories");
									}
									foreach ($sql_categories as $category) 
									{ ?> 
									<li class="filter_sub_type"><?php echo $category['category']; ?> 
										<ul> 
										<?php 
										$sql_groups ="SELECT * FROM groups WHERE cat_id='".$category['id']."' ";
										$group_arr = $objgen->get_AllRows_qry($sql_groups);
										foreach ($group_arr as $group)
										 { 
										?> 
											<li>
												<input type="checkbox" rel="<?php echo $group['id'];?>" value ="<?php echo $group['id'];?>" name="group[]" />
												<label for="massage"><?php echo $group['group_title']; ?></label>
											</li>
											<?php } ?>
										</ul>
									</li>
									<?php } ?>
								</ul>
							<h4 class="filter_type">Facilities </h4>
							<ul id="Facilities">
								<?php 
								
								foreach ($sql_categories as $category)
								{ ?> 
								<li class="filter_sub_type"><?php echo $category['category']; ?> 
									<ul> 
										<?php 
										$sql_facility ="SELECT * FROM facilities WHERE cat_id='".$category['id']."' ";
										$facility_arr = $objgen->get_AllRows_qry($sql_facility);
										foreach ($facility_arr as $facility)
										{ ?> 
										<li>
											<span class="wifi_dactive">
												<input type="checkbox" rel="<?php echo $facility['id'];?>" value="<?php echo $facility['id'];?>" name="facility[]" />
												<label for="<?php echo $facility['facility'];?>"><?php echo $facility['facility'];?></label>
											</span>
										</li>
										<?php } ?>
									</ul>
								</li>
								<?php } ?>
							</ul>
							<h4 class="filter_type" >Location </h4>
							<ul id="Location" >
							<?php
									if($shop_city_id){
										$sql_location ="SELECT * FROM location WHERE city_id='".$shop_city_id."' ";
									}else {
										$sql_location ="SELECT * FROM location WHERE city_id='".$_SESSION['city']."' ";	
									}
									
									$location_arr = $objgen->get_AllRows_qry($sql_location);
									//echo'<pre>';print_r($location_arr);
									foreach ($location_arr as $loc)
									{ ?>
										<li class="filter_sub_type location " style="background-image:none !important;">
											<span> 
												<input type="checkbox" rel="<?php echo $loc['id'];?>"   value="<?php echo $loc['id']; ?>" name="facility[]" />
												<label for="location"><?php echo $loc['location']; ?></label>
											</span>
										</li>
										<input type="hidden" name="city_id" value="<?php echo $city['id']; ?>"/>
									 <?php } ?>
									</ul>
								
							<h4 class="filter_type">Products used</h4>
							<ul id="productsused">
							<?php
							
							foreach ($sql_categories as $category)
							 { ?>
								<li class="filter_sub_type"><?php echo $category['category']; ?>
									<ul> 
									<?php 
									 $sql_products ="SELECT * FROM products WHERE cat_id='".$category['id']."' ";
									$product_arr = $objgen->get_AllRows_qry($sql_products);
									foreach ($product_arr as $product) { ?>
										<li>
											<input type="checkbox" rel="<?php echo $product['id'];?>" name="product[]" />
											<label for="<?php echo $product['product_name'];?>"><?php echo $product['product_name'];?></label>
										</li>
										<?php } ?>
									</ul>
								</li>
								<?php } ?>
							</ul>
						</div>
					</div>

				<div class="sponsored">
					<h1>Featured</h1>
					<?php
					$sql_sponsored ="SELECT s.*,p.medium,l.location,c.city as sponsored_city FROM shop s INNER JOIN photos p ON p.shop_id=s.id INNER JOIN city c ON c.id=s.city INNER JOIN location l  ON l.id=s.locality WHERE s.sponsored='Yes' AND s.status='active' AND p.catgeory='Interior' AND p.set_profile_pic='true' AND s.city='".$_SESSION['city']."'AND s.categories IN ($res_session) ORDER BY s.id DESC LIMIT 5";
					$res_sponsored = $objgen->get_AllRows_qry($sql_sponsored);
					foreach ($res_sponsored as $sponsored) {
					?> 
					<p>
						<a href="<?=URL?>search-details.php?shop_id=<?php echo  base64_encode($sponsored['id']);  ?>" style="color:black;"><h4><?php echo ucfirst($sponsored['shop_name']);?></h4></a>
						<h5><?php echo ucfirst($sponsored['location']).', '.ucfirst($sponsored['street']).', '.ucfirst($sponsored['sponsored_city']);?></h5>		
						<a href="<?=URL?>search-details.php?shop_id=<?php echo  base64_encode($sponsored['id']);  ?>"><img src="uploads/profile/<?php echo $sponsored['medium'];?>" alt="" style="height: 154px; width: 225px;" /></a>
					</p>
					<hr>
					<?php }
					?>
				</div>
			 </div>
			</div>
			<div class="col-md-9 col-sm-9 col-lg-9 col-xs-12">
				<div class="searched_reasults_c ">
					
				<div id="shops">
					<!-- Reasult 1 -->
					<?php
					$i=0; 
					foreach ($shop_arr as $value) {
						 
					?>
					<div class="searched_reasult_c">
						<div class="reasult_info col-md-6">
							<div class="name_no_c">
								<div class="address left">
									<a href="<?=URL?>search-details.php?shop_id=<?php echo  base64_encode($value['id']);  ?>" style="color:black;"><h3><b><?php echo ucfirst($value['shop_name']);?></b></h3>	</a>
									<p class="searched_location"><?php echo $value['location'];?>, <?php echo $value['city_name']; ?></p>
									
									<div class="clear"></div>
									
								</div>		
								<table class="tbl tbl_border right" border="0" cellpadding="0" cellspacing="0">
									<tr>

										<td><div class="specification_c">
										
										<i class="<?php echo $value['cat_icon'];?> fa-lg" ></i>
										
									</div>
									<div class="clear"></div></td>
										
									</tr>
									<tr>
										<td colspan="1">
											<div class="contact_no_website_c">
												<span class="contact_no"><?php echo $value['category'];?></span>
												
											</div>
										</td>									
									</tr>
								</table>
								<div class="clear"></div>
							</div>
							<div class="description">
								<p><?php echo substr($value['overview'], 0, 200) . " ..";?></p>
								<a href="<?=URL?>search-details.php?shop_id=<?php echo  base64_encode($value['id']); ?>" style="color:black;width: 400px; margin-left: 0px !important;color: #FFF;background-color: #337ab7;" class="btn btn-primary booking_btn" onclick="setsession();">Book Now</a>
							</div>
							
							
						
						</div>
						<?php
						
					    $sql_shop_photo = "SELECT medium FROM photos WHERE shop_id='".$value['id']."'  AND set_profile_pic='true' AND catgeory='Interior' ";
						$shop_photo = $objgen->get_AllRows_qry($sql_shop_photo);
						 //echo "photo=".$shop_photo[0]['thumb'];
						 ?>
						<div class="reasult_img col-md-6">
								<a href="<?=URL?>search-details.php?shop_id=<?php echo  base64_encode($value['id']);  ?>"><img src="uploads/profile/<?php echo $shop_photo[0]['medium'];?>" alt="" style="height: 206px; width: 100%;" /></a>
						</div>					
						<div class="clear"></div>
						
						
						
						<div class="rating_facilities_c">
								
									
									<table class="rating_tbl" border="0" width="40%" cellpadding="0" cellspacing="0">
										<tr>
											<td>
												<span class="rating_span">
												<?php 
													$sql_review ="SELECT * FROM review WHERE shop_id='".$value['id']."'";
													$result_review= $objgen->get_AllRows_qry($sql_review);

													 $count = count($result_review);
													 $stars = 0;
													 foreach ($result_review as $review) {
													 	$stars=$stars+$review['star'];
													 }
													 //echo $stars."--";
													 if($count>0) {
													  $avg1 = ($stars/$count);

														$avg=number_format($avg1, 2, '.', '');
											              if($avg<2){
											                $result = fmod($avg,2);
											                if($result<5){
											                  $message="Average";
											                 }elseif($result>5){
											                  $message="Above average";
											                 }
											              } elseif ($avg<3) {
											                $result = fmod($avg,3);
											                if($result<5){
											                   $message="Good";
											                 }elseif($result>5){
											                   $message="Very good";
											                 }
											              }elseif ($avg<4) {
											                $result = fmod($avg,4);
											                 if($result<5){
											                   $message="Good";
											                 }elseif($result>5){
											                   $message="Very good";
											                 }
											              }elseif ($avg=5) {
											                $result = fmod($avg,5);
											                if($result<5){
											                   $message="Superb";
											                 }elseif($result>5){
											                   $message="Very good";
											                 }
											              }else{
											                 $message="Poor";
											              }
													 
													} else {
														$avg=0;
														$message ="Below Average";
													}
													
													 
												?>
													<span class="rating_bg"><?php if($count==0){ echo $avg=0; }else {echo substr(number_format($avg, 2),0,3);}?></span>
													<span><?php  echo $message;?></span>
												</span>
													
											</td>
											
											<td>
												<span class="rating_span">
												
												<?php 
													//$sql_shop_review = "SELECT * FROM review WHERE shop_id='".$value['id']."'  AND status='active'";
													$shop_review = $objgen->get_AllRowscnt("review","AND shop_id='".$value['id']."'AND status='active'");

													?>
													<span class="rating_bg"><?php echo $shop_review; ?></span>
													<span>Reviews</span>
												</span>
											</td>
											<td>
												<?php 
													if(empty($_SESSION['ma_log_id_usr'])) {?>
													<span class="rating_span add_d fave_<?php echo $value['id']?>">
														<a href="#" onclick="add_fave(<?php echo $value['id']?>,0)" style="color: black;">
															
															<span  class="deco" id="deco_<?php echo $value['id']?>" style="margin-top: 36px;">Add</span>
														</a>
													</span>
												<?php } else {
														$favourite=$objgen->chk_Ext("favourites","shop_id='".$value['id']."' AND user_id='".$_SESSION['ma_log_id_usr']."'");
     													if($favourite>0) {
												?>
													<span class="rating_span added_d fave_<?php echo $value['id']?>">
														<a href="#"  onclick="add_fave(<?php echo $value['id']?>,<?php echo $_SESSION['ma_log_id_usr'];?>)" style="color: black;">
															
															<span  class="deco" id="deco_<?php echo $value['id']?>" style="margin-top: 36px;">Added</span>
														</a>
													</span><?php } else { ?>
													
													<span class="rating_span add_d fave_<?php echo $value['id']?>">
														<a href="#"  onclick="add_fave(<?php echo $value['id']?>,<?php echo $_SESSION['ma_log_id_usr'];?>)" style="color: black;">
															
															<span  class="deco" id="deco_<?php echo $value['id']?>" style="margin-top: 36px;">Add</span>
														</a>
													</span>
													<?php  } }?>
												
												</td>
											</tr>
										</table>





									<div class="table-responsive">
	  							       <table class="table facilities_tbl" border="0" width="60%" cellpadding="0" cellspacing="0">
										<?php 
										 $sql_shop_facility = "SELECT facility_id FROM shop_facilities WHERE shop_id='".$value['id']."' 
																  AND  cat_id='".$value['categories']."'"; 
										
										$shop_facility_ids = $objgen->get_AllRows_qry($sql_shop_facility); 
										 $getFaclityChk = array( );
	         
								          foreach ($shop_facility_ids as $shop_facility) {

								            $subArr2 = $shop_facility['facility_id'];
								           
								            array_push($getFaclityChk,$subArr2);
								          
								          }	$sql_facility = "SELECT * FROM facilities WHERE  cat_id='".$value['categories']."'LIMIT 10";
											$facility =$objgen->get_AllRows_qry($sql_facility);
											$facilityCount=1;
										?>

										<tr>
											<?php foreach ($facility as $fact) {
												
											?>
											<td><i class="<?php echo $fact['icon']; ?> fa-lg" <?php if(in_array($fact['id'], $getFaclityChk)) {?>style="color:#CD5C5C;"<?php } else {?>style="color: #B1B1B1;"<?php }?>></i><span class="facilities_span"><?php echo $fact['facility']; ?></span></td>

											<?php if($facilityCount==5) {?>
											</tr>
											<tr>
										<?php } ?>
											
									<?php $facilityCount++; }
										?>
										</tr>
									</table>
								 </div> 
								<div class="clear"></div>
							</div>
						
						
						<!-- Featureed Services Horizontal scroll -->
						<div>
								<div id="reatured_services_c">
									<div id="arrowL">
									</div>
									<div id="arrowR">
									</div>
									<div class="over_div">
										<div id="list-reatured_services">
											<div class='list'>
												<?php 
												 $sql_shop_groups = "SELECT distinct g.icon, g.group_title,sc.* FROM shop_categories sc INNER JOIN 	groups g ON g.id=sc.group_id WHERE sc.shop_id='".$value['id']."' 
													AND  sc.cat_id='".$value['categories']."' GROUP BY g.group_title LIMIT 5"; 
												
												$shop_groups_id = $objgen->get_AllRows_qry($sql_shop_groups); 
												foreach ($shop_groups_id as $groups_id) {
												?>
												<div class='item'>
													<span><i class="<?php echo $groups_id['icon'];?> fa-lg" ></i></span>
													<h4><?php echo ucfirst($groups_id['group_title']);?></h4>
												</div>
										<?php }
										?>							
											
												
											</div>
										</div>
									</div>
								</div>
								
								<div class="clear"></div>
							</div>
						</div>
					
					<?php  $i++; } ?>
					
				</div>
			</div>
		</div>
		<div class="fade_2"></div>
                            <div class="modal_2">
                                <img class="loader" src="front-end-images/ajax-loader.gif" />
                            </div>
	</div>
<style type="text/css">
          .fade_2{
    display: none;
    position:absolute;
    top: 0%;
    left: 0%;
    width: 100%;
    height: 100%;
    background-color: #ababab;
    z-index: 1001;
    -moz-opacity: 0.8;
    opacity: .70;
    filter: alpha(opacity=80);
}

.modal_2 {
    display: none;

    position: absolute;
    top: 10%;
    left: 40%;
    width: 130px;
    height: 130px;
    padding: 30px 15px 0px;
    border-radius: 20px;
    background-color: white;
    z-index: 1002;
    text-align: center;
    overflow: auto;
}

.results_2 {
    font-size:1.25em;
    color:red
}
        </style>
			
			

<?php include 'footer-front-end.php';?>
<script src="front-end-js/jquery.session.js"></script>
<script>
$(document).ready(function(){
	$('#filterdata input').click( function (){
		var product_arr = [];	
		$('#productsused input').each(function( index ) {
			if($(this).is(':checked')){
				product_arr.push($( this ).attr('rel'));
		    	
			}
		 });
		var location_arr = [];
		
		$('#Location input').each(function( index ) {
			if($(this).is(':checked')){
				location_arr.push($( this ).attr('rel'));
		    }
		
		});
		var facility_arr = [];
		$('#Facilities input').each(function( index ) {
			if($(this).is(':checked')){
				facility_arr.push($( this ).attr('rel'));
		    }
		});
		var service_arr = [];
		
		
		$('#Service input').each(function(index) {
			if ($(this).is(':checked') ) {
				service_arr.push($( this ).attr('rel'));
			}
		});

		
		$.ajax( {
		  type: "POST",
		  url: "<?=URL?>filter-subcat.php",
		  data: { 
		        service:service_arr,
		        facility:facility_arr,
		        product:product_arr,
		        location:location_arr
		        },
		  success:function(data){
		  	
		      var result = $('<div />').append(data).find('#result').html();
		      $("#result1").html(result);
		      $("#result1").addClass('active');
		      $("#shops").html(data);
		       

		 },
		 beforeSend:function(){
		 	
		      $(".modal_2").show();
		      $(".fade_2").show();
		 },
		 complete:function() {
		 	
		      $(".modal_2").hide();
		      $(".fade_2").hide();
		 },
		 fail:function(jqXHR,textStatus,errorThrown){
		    
		    console.log(jqXHR);
		    console.log(textStatus);
		    console.log(errorThrown);
		}});
		
	});


 
});

function add_fave(id,user_id) { 
	
if (user_id == 0) {

	$.session.set("myVar", id);
	  $(".overlay").fadeIn(300);
      $("#user_login_popup_c").fadeIn(300);

      $(".pop_up_close").click(function(){    
        $(this).parent().fadeOut(300);
        $(".overlay").delay(200).fadeOut(300);
      });
    } else { 
      var user_id = user_id;
      var shop_id = id;
      
     
      $.ajax({
          type: "POST",
          url: "<?=URL?>add-favorites.php",
          data: {
            user_id : user_id,
            shop_id : shop_id 
          },
         success: function(html){
          var res =$.trim(html)
          
          if (res=='inserted') {
          	
          	   $(".fave_"+shop_id).removeClass("add_d");
          	   $(".fave_"+shop_id).addClass("added_d");
          	$('#deco_'+shop_id).text("Added");
            
          }
          if (res=='deleted') {
          	
          	   $(".fave_"+shop_id).removeClass("added_d");
          	   $(".fave_"+shop_id).addClass("add_d");
          	$('#deco_'+shop_id).text("Add");
            
          }
          
         },
         beforeSend:function(){
		 	
		      $(".modal_2").show();
		      $(".fade_2").show();
		 },
		 complete:function() {
		 	
		      $(".modal_2").hide();
		      $(".fade_2").hide();
		 },
      });
    }
}

</script>