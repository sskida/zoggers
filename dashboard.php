<?php
session_start();
require_once "chk_login_mer.php";
$objgen		=	new general();
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>


</head>

<body>
<?php include 'header-dash.php'; ?>
<?php 

    if($_SESSION['msg']) {?>
       <div style="background-color:green;height:30px;" >
           <h3 class="caption" style="color:white;font-size:12px;">  
           <?php 
                echo $_SESSION['msg'];
            ?>  </h3>
        </div> 
      <?php } 
      ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <?php include 'merchant-menu.php'; ?>
  <aside class="right-side">
    <section class="content-header-top">
      <h1> <i class="fa fa-tachometer"></i> Dashboard </h1>
    </section>
    <section class="content">
      <div class="outlet"> 
        <!-- Start .outlet --> 
        <!-- Page start here ( usual with .row ) -->
        <div class="row"> 
          <!-- Start .row -->
			<?php  
			$where = " and mer_id=".$_SESSION['ma_log_id_mer'];
			$row_app = $objgen->get_AllRowscnt("appointments",$where);
			?>
		 <!-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="carousel-tile carousel vertical slide">
              <div class="carousel-inner">
                <div class="item">
                  <div class="tile dashboxcl8">
                    <div class="tile-icon"> <i class="fa fa-calendar"></i> </div>
                    <div class="tile-content">
                      <div data-to="107" data-from="0" class="number countTo"><?=$row_app?></div>
                      <h3><a href="<?=URL?>view-appointments" style="color:#FFFFFF">Appointments</a></h3>
                    </div>
                  </div>
                </div>
                <div class="item active">
                  <div class="tile dashboxcl1"> 
                    <!-- tile start here 
                    <div class="tile-icon"> <i class="fa fa-calendar"></i> </div>
                    <div class="tile-content">
                      <div data-to="5" data-from="0" class="number countTo"><?=$row_app?></div>
                      <h3><a href="<?=URL?>view-appointments" style="color:#FFFFFF">Appointments</a></h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Carousel -
          </div> -->
		  
		  	<?php  
			$where = " and merchant_id=".$_SESSION['ma_log_id_mer'];
			$row_app = $objgen->get_AllRowscnt("shop",$where);
			?>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="carousel-tile carousel slide">
              <div class="carousel-inner">
                <div class="item active">
                  <div class="tile dashboxcl4"> 
                    <!-- tile start here -->
                    <div class="tile-icon"> <i class="fa fa-briefcase"></i> </div>
                    <div class="tile-content">
                      <div data-to="45" data-from="0" class="number countTo"><?=$row_app?></div>
                      <h3><a href="<?=URL?>manage-business" style="color:#FFFFFF">Business</a></h3>
                    </div>
                  </div>
                </div>
                <div class="item">
                  <div class="tile dashboxcl5"> 
                    <!-- tile start here -->
                    <div class="tile-icon"> <i class="fa fa-briefcase"></i> </div>
                    <div class="tile-content">
                      <div data-to="3548" data-from="0" class="number countTo"><?=$row_app?></div>
                      <h3><a href="<?=URL?>manage-business" style="color:#FFFFFF">Business</a></h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Carousel --> 
          </div>
        </div>
      </div>
      <div class="row">
        <section class="col-lg-12 connectedSortable"> </section>
        <div class="col-md-5"> </div>
      </div>
    </section>
  </aside>
</div>
<?php include 'footer-script-dash.php'; ?>
</body>
</html>
