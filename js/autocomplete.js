// autocomplet : this function will be executed every time we change the text
function autocomplet() {
	var min_length = 0; // min characters to display the autocomplete
	var keyword = $('#search-input').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'get_data.php',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#suggestions').show();
				$('#suggestions').html(data);
			}
		});
	} else {
		$('#suggestions').hide();
	}
}

function autocompleteFiltered() {
	var min_length = 0; // min caracters to display the autocomplete
	var keyword = $('#search-input').val();
	if (keyword.length >= min_length) {
		$.ajax({
			url: 'get_filtered_data.php',
			type: 'POST',
			data: {keyword:keyword},
			success:function(data){
				$('#filtered').show();
				$('#filtered').html(data);
			}
		});
	} else {
		$('#filtered').hide();
	}
}

// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('#search-input').val(item);
	// hide proposition list
	$('#suggestions').hide();
	autocompleteFiltered();
}

function set_item_filtered(item) {
	// change input value
	$('#search-input2').val(item);
	// hide proposition list
	$('#filtered').hide();
}
