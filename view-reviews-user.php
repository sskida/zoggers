<?php
require_once "chk_login_usr.php";
$objgen		=	new general();

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if(isset($_GET['del']))
{
   $id= $_GET['del'];
   $msg     = $objgen->del_Row("review","id=".$id." and user_id=".$_SESSION['ma_log_id_usr']);
    if($msg=="")
   {
	header("location:".URL."view-reviews-user.php?msg=3&page=".$page);
   }
}

if($_GET['msg']==3)
{

  $msg2 = "Review Deleted Successfully.";
}



$where = " and user_id=".$_SESSION['ma_log_id_usr'];
$row_count = $objgen->get_AllRowscnt("review",$where);
if($row_count>0)
{
  
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array(), 1, WEBLINK."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("review",$pagesize*($page-1),$pagesize,"id desc",$where);
}


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>
</head>

<body>
<?php include 'header-dash-user.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
     <?php include 'user-menu.php'; ?>
  <aside class="right-side">
    <section class="content-header-top">
      <h1> <i class="fa fa-suitcase"></i> Reviews </h1>
    </section>
    <section class="content">
     <div class="col-xs-12">
        <div class="box-body table-responsive">
			
			   <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>
           <div class="table-responsive">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Shop Name</th>
                <th>Date</th>
                <!-- <th>Title</th> -->
				<th>Description</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
				<?php
				if($row_count>0)
				{
					foreach($res_arr as $key=>$val)
				    {

					$shop_res     	 = $objgen->get_Onerow("shop","AND id=".$val['shop_id']);
					
				 ?>
              <tr>
                <td><?php echo $objgen->check_tag($shop_res['shop_name']); ?></td>

                <td><?php echo date("d-m-Y",strtotime($val['created_date'])); ?></td>
                <!-- <td><?php echo $objgen->check_tag($val['title']); ?></td> -->
                <td><?php echo substr($objgen->check_tag($val['long_desc']), 0, 50); ?></td>
                <td><!-- <a class="arb-icon cl7-light-blue" href="<?=URL?>view-reviews-user.php?edit=<?=$val['id']?>&page=<?=$page?>" ><i class="fa fa fa-edit"></i>Edit</a> -->
                <a class="arb-icon cl7-light-blue" href="<?=URL?>view-reviews-user.php?del=<?=$val['id']?>&page=<?=$page?>" onClick="return confirm('Do you want to delete this Review?')" ><i class="fa fa-trash-o"></i>Delete</a></td>

              </tr>
			  <?php
			  }
			  }
			  ?>
            </tbody>
          </table>
		  </div>
		  <?php
									if($row_count > $pagesize) 
									{
									?>
									<div class="row pull-right">
	                                    <div class="col-xs-12">
										
											<div class="dataTables_paginate paging_bootstrap">
											    		
												<?php echo $pages; ?>

												
											</div>
											
									   </div>
									</div>
									<?php
										 }
									?>
									
        </div>
        <!-- /.box-body --> 
        
      </div>
    </section>
  </aside>
</div>
<?php include 'footer-script-dash.php'; ?>
<script src="<?=URL?>js/jquery.dataTables.js" type="text/javascript"></script> 
<script src="<?=URL?>js/dataTables.bootstrap.js" type="text/javascript"></script> 
<script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
</body>
</html>
