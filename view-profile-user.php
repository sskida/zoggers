<?php
require_once "chk_login_usr.php";
$objgen		=	new general();


$id 		  = $_SESSION['ma_log_id_usr'];
$result       = $objgen->get_Onerow("users","AND id=".$id);
$name         = $objgen->check_tag($result['name']);
$phone        = $objgen->check_tag($result['phone']);
$gender       = $objgen->check_tag($result['gender']);
$city         = $objgen->check_tag($result['city']);
$email        = $objgen->check_tag($result['email']);


?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>
</head>

<body>
<?php include 'header-dash-user.php'; ?>



<div class="wrapper row-offcanvas row-offcanvas-left">
    <?php include 'user-menu.php'; ?>


<aside class="right-side">
  <section class="content-header-top">
    <h1> <i class="fa fa-suitcase"></i> View Profile </h1>
   
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-6">
          <form>
            <div class="box padding-both">
              <div class="img-circle-border"><i class="fa fa-user fa-3x"></i></div>
              <div class="profile-view">
              <h1><?=$name?></h1>
              <h2><a href="mailto:<?=$email?>"><?=$email?></a></h2>
              <h2><?=$phone?></h2>
              <p><?=$gender?></p>
              <p><?=$city?></p>
               
                <a class="cl2-green common-btn" href="<?=URL?>profile-edit-user.php">Edit</a>
              
              </div>
              <div class="padding-both"></div>
                
                
                
            
            </div>
          </form>
        </div>
    </div>
  </section>
</aside>
</div>
<?php include 'footer-script-dash.php'; ?>
</body>
</html>
