<?php 
require_once "includes/includepath.php";

if (isset($_POST['month'])) { 
	$month = $_POST['month'];
	$_SESSION['basket_month'] = $month;
}else { 
	$month = date("m");
}
if (isset($_POST['year'])) { 
	$year = $_POST['year'];
	$_SESSION['basket_year'] = $year;
}else { 
	$year = date("Y");
}

if (isset($_POST['day'])) { 
	$day = $_POST['day'];
	$_SESSION['basket_day'] = $day;
}else { 
	$day = 0;
}
// Unix Timestamp of the date a user has clicked on
$selected_date = mktime(0, 0, 0, $month, 01,$year );
// Unix Timestamp of the previous month which is used to give the back arrow the correct month and year 
$back = strtotime("-1 month", $selected_date);
// Unix Timestamp of the next month which is used to give the forward arrow the correct month and year 
$forward = strtotime("+1 month", $selected_date);


include('classes/class_calendar.php');
$calendar = new booking_diary();    // Call calendar function
$ss = $calendar->make_calendar($selected_date, $back, $forward, $day, $month, $year,$ajax='true');
print_r($ss);exit;
?>

