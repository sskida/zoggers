<?php
require_once "chk_login_usr.php";

$objgen		=	new general();
$objval	    =   new validate();

if(isset($_POST['Change']))
{
  
   $old 		= trim($_POST['old_pwd']);
   $pass 		= trim($_POST['new_pwd']);
   $conf_pass 	= trim($_POST['conf_password']);
   
   $rules		=	array();
   $rules[] 	= "required,old_pwd,Enter the Old Password";
   $rules[] 	= "required,new_pwd,Enter the New Password";
   $rules[] 	= "required,conf_password,Enter the Conf. Password";
   
   $errors  	= $objval->validateFields($_POST, $rules);
   if(empty($errors))
	{
	   $msg = $objgen->match_Pass($pass,$conf_pass);
	   if($msg=="")
	   {
		 $msg = $objgen->chng_password('users','password',$_POST,'id',$_SESSION['ma_log_id_usr']);
		 if($msg=="")
		 {
		   $msg2 = "Password Changed Successfully.";
		 }
	   }
	}
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>
</head>

<body>
<?php include 'header-dash-user.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
   <?php include 'user-menu.php'; ?>


<aside class="right-side">
  <section class="content-header-top">
    <h1> <i class="fa fa-suitcase"></i> Reset Password </h1>
   
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-6">
            <form role="form" action="" method="post" enctype="multipart/form-data" >
            <div class="box padding-both">
			
			   <?php
                                    if($msg2!="")
                                    {
                                    ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                        <b>Alert!</b> <?php echo $msg2; ?>
                                    </div>
                                 
                                    <?php
                                    }
                                    ?>

                                     <?php
                                       if (!empty($errors)) {
                                        ?>
                                         <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Please fix the following errors:</b> <br>
                                             <?php
                                                    foreach ($errors as $error1)
                                                     echo "<div> - ".$error1." </div>";
                                                ?>
                                    </div>
   
                                      <?php
                                         } 
                                         ?>
                                        
                                    <?php
                                    if($msg!="")
                                    {
                                    ?>
                                   <div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        <b>Alert!</b> <?php echo $msg; ?>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                        <div class="form-group" >
                                            <label>Old Password</label>
                                            <input type="password" class="form-control" value="<?=$old?>" name="old_pwd" required />
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputPassword1">New Password</label>
                                            <input type="password" class="form-control"  name="new_pwd"  value="<?=$pass?>" required />
                                        </div>
                                         <div class="form-group">
                                            <label for="exampleInputPassword1">Confirm Password</label>
                                            <input type="password" class="form-control"  name="conf_password"  value="<?=$conf_pass?>" required/>
                                        </div>
                                        <div class="box-footer">
                                           <button id="submit" class="cl2-green common-btn" name="Change" type="submit">Change</button>
                                       </div>
                
            
            </div>
          </form>
        </div>
    </div>
  </section>
</aside>
</div>
<?php include 'footer-script-dash.php'; ?>
</body>
</html>
