<?php
require_once "chk_login_mer.php";
$objgen		=	new general();

$objPN		= 	new page(1);
/** Page Settings  **/
$pagesize	=	20;
$page	 	= isset($_REQUEST['page'])	?	$_REQUEST['page']	:	"1";

if($_GET['msg']==1)
{
 	$msg2 =" Success!";
}
else if($_GET['msg']==2)
{
  $msg2 = "Error occurred!";
}
else if($_GET['msg']==3)
{
  $msg2 = "Shop Deleted Successfully.";
}

if(isset($_GET['del']))
{
   $id= $_GET['del'];
   $msg     = $objgen->del_Row("ad_exp","shop_id=".$id." and merchant_id=".$_SESSION['ma_log_id_mer']);
   $msg     = $objgen->del_Row("other_info","shop_id=".$id." and merchant_id=".$_SESSION['ma_log_id_mer']);
   $msg     = $objgen->del_Row("refer","shop_id=".$id." and merchant_id=".$_SESSION['ma_log_id_mer']);
   $msg     = $objgen->del_Row("spa","shop_id=".$id." and merchant_id=".$_SESSION['ma_log_id_mer']);
   $msg     = $objgen->del_Row("gym","shop_id=".$id." and merchant_id=".$_SESSION['ma_log_id_mer']);
   $msg     = $objgen->del_Row("photos","shop_id=".$id." and merchant_id=".$_SESSION['ma_log_id_mer']);

   $msg     = $objgen->del_Row("shop","id=".$id." and merchant_id=".$_SESSION['ma_log_id_mer']);
   $msg1     = $objgen->del_Row("shop_facilities","shop_id=".$id." and merchant_id=".$_SESSION['ma_log_id_mer']);
    if($msg=="" && $msg1=="" )
   {
	header("location:".URL."manage-business.php?msg=3&page=".$page);
   }
}

$where = " and 	merchant_id=".$_SESSION['ma_log_id_mer'];
$row_count = $objgen->get_AllRowscnt("shop",$where);
if($row_count>0)
{
  $objPN->setCount($row_count);
  $objPN->pageSize($pagesize);
  $objPN->setCurrPage($page);
  $objPN->setDispType('PG_BOOSTRAP');
  $pages = $objPN->get(array(), 1, WEBLINK."/".$params[0]."/", "", "active");
  $res_arr = $objgen->get_AllRows("shop",$pagesize*($page-1),$pagesize,"id desc",$where);
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">

<meta name="viewport" content="width=device-width; initial-scale=1.0">
<title><?=TITLE?></title>
<?php include 'header-script-dash.php'; ?>
</head>

<body>
<?php include 'header-dash.php'; ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
  <?php include 'merchant-menu.php'; ?>


<aside class="right-side">
  <section class="content-header-top">
    <h1> <i class="fa fa-suitcase"></i> Business Profile </h1>
   <!-- <div class="flo-r">
    <h2>Doc.Ref #</h2>
      <input type="text" class="text-f" placeholder="FNC">
      </div>-->
  </section>
  <section class="content">
    <div class="row">
      <div class="col-lg-12">
        <div class="box-body table-responsive">
		 <?php
			if($msg2!="")
			{
			?>
			<div class="alert alert-success alert-dismissable">
					<i class="fa fa-check"></i>
					<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
					<b>Alert!</b> <?php echo $msg2; ?>
			</div>

			<?php
			}
			?>
		<a class="btn-ad" href="<?=URL?>select-business.php"><i class="fa fa-plus"></i> Add</a>
		<div id="select-business"></div>
		<table id="example1" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Shop Name</th>
					<th>Shop Type</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>

			  	<?php
			   if($row_count>0)
	        {
					 foreach($res_arr as $key=>$val)
						{
							/*if($val['shop_type'] == "gym")
							{
								$link = "add-business";
							}
							else if($val['shop_type'] == "spa")
							{
								$link = "add-business-spa";
							}
							else
							{
							   $link = "add-business-gen";
							}*/
							$link = "add-business"
			    ?>
			<tr>
				<td><?php echo $objgen->check_tag($val['shop_name']); ?></td>
	<td><?php echo ucfirst($val['shop_type']); ?></td>
				<td><?php echo $objgen->check_tag($val['status']); ?></td>

				<td>
				<a href="<?=URL?><?=$link?>/?id=<?=$val['id']?>" ><button class="arb-icon cl8-dred cl7-light-blue"><i class="fa fa-edit"></i>Edit</button></a>
			<a href="search-details/?id=<?=$val['id']?>" target="_blank">	<button class="arb-icon cl8-dred cl7-light-blue"><i class="fa fa-eye"></i>View</button></a>
					 <a href="<?=URL?>manage-business/?del=<?=$val['id']?>&page=<?=$page?>" role="button"class="arb-icon cl8-dred"  onClick="return confirm('Do you want to delete this Shop?')"><i class="fa fa-trash-o"></i>Delete</a></td>
			</tr>
	<?php
		}
	}
	?>

		</tbody>
	</table>

					<?php
					if($row_count > $pagesize)
					{
					?>
					<div class="row pull-right">
						<div class="col-xs-12">

						<div class="dataTables_paginate paging_bootstrap">

								<?php echo $pages; ?>

							</div>

						 </div>
					</div>
					<?php
						 }
					?>
        </div>
        <!-- /.box-body -->

      </div>
    </div>
  </section>
</aside>
</div>
<?php include 'footer-script-dash.php'; ?>
<script src="<?=URL?>js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?=URL?>js/dataTables.bootstrap.js" type="text/javascript"></script>
<script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>
</body>
</html>
