$(window).scroll(function() {

    if ($(this).scrollTop() == 0) {
        $('.header_c').removeClass("active_header");
    }
    else {
        $('.header_c ').addClass("active_header");
    }
});
$(document).ready(function(){	
	
	$('.how_it_work').click(function(){

		$('html, body').animate({
			scrollTop: $( $(this).attr('href') ).offset().top
		}, 500);
		return false;
	});
	$('.felling_lazy').click(function(){

		$('html, body').animate({
			scrollTop: $( $(this).attr('href') ).offset().top
		}, 500);
		return false;
	});

	
	/* Search Tab */
	$(".tab_info_c").eq(0).css("display","block");
	$(".sub_tab").click(function(){
		var x = $(this).index();
		$(".tab_info_c").slideUp(300);
		$(".tab_info_c").eq(x).slideDown(300);
		$(".sub_tab h4 span").removeClass("active_tab");
		$(this).find("span").addClass("active_tab");
	});
	
	
	/* Select Location */
	$(".header_link_c ul li.city_select").mouseenter(function(){
		$(".sbOptions").slideDown(300);
		$(".sbToggle").css("background-position","0px -16px");
	});
	
	
	$(".header_link_c ul li.city_select").mouseleave(function(){
		$(".sbOptions").slideUp(300,function(){
			$(".sbToggle").css("background-position","0px -116px");
		});
	});
	$(".header_link_c ul li.city_select").click(function(){
		$(".sbOptions").slideUp(300,function(){
			$(".sbToggle").css("background-position","0px -116px");
		});
	});
	
	
	/* NEWS (Header contents) */	
	$(".header_link_c > ul > li").mouseenter(function(){
		var Isclass = $(this).hasClass("reading_list_li");
		
		if(Isclass == true){		
			
			$(".reading_list_c").fadeIn(300);
		}
		else
		{
			
			$(".reading_list_c").fadeOut(300);
		}
	});	
	$(".header_link_c").mouseleave(function(){
		$(".reading_list_c").fadeOut(300);
	});
	
	
	/* LOGIN Merchant */
	$("#merchant_login").click(function(){		
		$(".overlay").fadeIn(300);
		$("#merchant_login_popup_c").delay(200).fadeIn(300);
	});
	$(".pop_up_close").click(function(){		
		$(this).parent().fadeOut(300);
		$(".overlay").delay(200).fadeOut(300);
	});

	/* LOGIN user */

	$("#user_login").click(function(){	

		
		$.session.set("myVar", 'normal_login');
		$(".overlay").fadeIn(300);
		$("#user_login_popup_c").delay(200).fadeIn(300);
	});
	$(".pop_up_close").click(function(){		
		$(this).parent().fadeOut(300);
		$(".overlay").delay(200).fadeOut(300);
	});
	
	/* USER SIGNUP user_sign_up*/
	$("#usr_sign_up").click(function(){		
		
		$(".overlay").fadeIn(300);
		$("#user_login_popup_c").fadeOut(1);
		$("#user_forgotPass_pop_up_c").fadeOut(1);
		$("#user_signup_pop_up_c").fadeIn(300);
	});
	$("#user_sign_up").click(function(){		
		
		$(".overlay").fadeIn(300);
		$("#user_login_popup_c").fadeOut(1);
		$("#user_forgotPass_pop_up_c").fadeOut(1);
		$("#user_signup_pop_up_c").fadeIn(300);
	});
	/* Merchant SIGNUP */
	$("#mer_sign_up").click(function(){		
		
		$(".overlay").fadeIn(300);
		$("#merchant_login_popup_c").fadeOut(1);
		$("#mer_forgotPass_pop_up_c").fadeOut(1);
		$("#mer_signup_pop_up_c").fadeIn(300);
	});
	/* USER FORGOT PASSWORD */
	$("#user_forgot_pass").click(function(){		
		
		$(".overlay").fadeIn(300);
		$("#user_login_popup_c").fadeOut(1);
		$("#user_signup_pop_up_c").fadeOut(1);
		$("#user_forgotPass_pop_up_c").fadeIn(300);
	});
	/* MERCHANT FORGOT PASSWORD */
	$("#mer_forgot_pass").click(function(){		
		
		$(".overlay").fadeIn(300);
		$("#merchant_login_popup_c").fadeOut(1);
		$("#mer_signup_pop_up_c").fadeOut(1);
		$("#mer_forgotPass_pop_up_c").fadeIn(300);
	});
	
	$(".close_rating_write_c").click(function(){		
		$(".rating_write_c").slideUp(300);
		$(".review_write_btn").css("display","block");
		$(this).css("display","none");
	});
	
	
	$(".pop_up_close").click(function(){		
		$(this).parent().fadeOut(300);
		$(".overlay").delay(200).fadeOut(300);
	});
	
	
	/* GENERATE OFFER popup  */
	$(".generate_offer").click(function(){		
		$(".overlay").fadeIn(300);
		$(".generate_offer_popup_c").delay(100).fadeIn(300);
	});
	
	$(".close").click(function(){		
		$(".overlay").fadeOut(300);
		$(this).parent().delay(100).fadeOut(300);
	});
	
	
	/* INFO */
	$(".info").click(function(){		
		$(".overlay").fadeIn(300);
		$(".info_pop_up_c").delay(200).fadeIn(300);
	});
	
	
	
	
	/* OUR MISSION */
	$(".mission_info_icon:eq(0)").addClass("activeicon_c");
	$(".download_arrow:eq(0)").css("display","block");
	$(".mission_info:eq(0)").css("display","block");
	
	$(".mission_info_icon").hover(
		function(){
			var index = $(this).index();
			
			$(".mission_info_icon").removeClass("activeicon_c");
			$(".download_arrow").css("display","none");
			$(".download_arrow").eq(index).css("display","block");
			$(this).addClass("activeicon_c");
			$(".mission_info").css("display","none");
			$(".mission_info").eq(index).fadeIn(300);
		},
		function(){
			
		}
	);
	
	
	
	
	
	/* SEARCHED RESULT PAGE*/
	
	/* The button with search result right side div, change the direction of arrow on click */

			
	$(".filter_type").click(function(){

		var display = $(this).next('ul').css("display");
		
		if(display == 'none'){

			$(".filters_c ul").slideUp(200);
			$(".filter_type").css('background-image','url("front-end-images/plus_circle.png")');
			$(this).next('ul').slideDown(200);
			$(this).css('background-image','url("front-end-images/minus_circle.png")');
		}
		else {
			$(this).next('ul').slideUp(200);
			$(this).css('background-image','url("front-end-images/plus_circle.png")');
		}
		
	});

	/* Slide down filter of each type */
	$(".filter_sub_type").click(function(){

		if($(this).find('ul').hasClass('active') ){
			$(this).find('ul').removeClass('active');
			$(this).find('ul').slideUp();
			$(this).css('background-image','url("front-end-images/plus_circle.png")');
			$(this).css('background-position-y','9px');
		}else{
			$(this).find('ul').addClass('active');
			$(this).find('ul').slideDown();
			if($(this).hasClass('location')){
				
				$(this).css('background','none');	
			}else {
				
				$(this).css('background-image','url("front-end-images/minus_circle.png")');
				$(this).css('background-position-y','6px');
			}
		}
		

		
	});
	/* Slide down filter of each type of services */
	$(".group").click(function(){
		if($(this).next('.groups').hasClass('active') ){
			
			$(this).next('.groups').removeClass('active');
			$(this).next('.groups').slideUp();
			$(this).css('background-image','url("front-end-images/plus_circle.png")');
		}else{
			
			$(this).next('.groups').addClass('active');
			$(this).next('.groups').slideDown();
			$(this).css('background-image','url("front-end-images/minus_circle.png")');
		}
		

		
	});
	
	/* Show phone no ro website */
	$(".wesite_nm").click(function(){
		
		
		
		$(this).parents("tr").next("tr").find(".contact_no").css("display","none");
		$(this).parents("tr").next("tr").find(".website").css("display","block");
	});
	$(".pho_no").click(function(){
		$(this).parents("tr").next("tr").find(".contact_no").css("display","block");
		$(this).parents("tr").next("tr").find(".website").css("display","none");
	});
	
	
	
	// Features Services scrolling horizontally
	    
        visible = 2, //Set the number of items that will be visible
        index = 0, //Starting index
        
    
    $('div#arrowR').click(function(){
		var $item = $(this).parent().find(".item");
		endIndex = ( $item.length / visible ) - 1; //End index
		
		
        if(index < endIndex ){
          index++;
          $item.animate({'left':'-=160px'});
        }
    });
    
    $('div#arrowL').click(function(){
		var $item = $(this).parent().find(".item");
		endIndex = ( $item.length / visible ) - 1; //End index
        if(index > 0){
          index--;            
          $item.animate({'left':'+=160px'});
        }
    });
	
	
	/* Client Page */
	
	/* General information about client will show in tabular style, there are several tabs like, overview, services, facilities ect.. */
	// By default the first tab information keep open and other will hidden.
	 function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}
	var service =  GetURLParameter('service');
	
	if($.session.get("review")==1){
		
		$(".client_brief_info").eq(0).css("display","none");
		 var index = 1;
    	 var brief_info_index = $(".services").css("display");
    
	    if(brief_info_index == 'none'){
	      
	      $(".client_brief_info").css("display","none");
	      $(".client_brief_info_c .tab").removeClass("active");

	     
	      $(".client_brief_info").eq(index).css("display","block");
	      $(".client_brief_info_c .tab").eq(index).addClass("active");
	       $.session.clear("review");  
	    }
	} else if($.session.get("review")==4) {
		
		$(".client_brief_info").eq(0).css("display","none");
			var index = 4;
			var brief_info_index = $(".reviews").css("display");
			if(brief_info_index == 'none'){
				$(".client_brief_info").css("display","none");
				$(".client_brief_info_c .tab").removeClass("active");

				$(".client_brief_info").eq(index).css("display","block");
				$(".client_brief_info_c .tab").eq(index).addClass("active");
				$.session.clear("review");	
			}
	}else {
		
			$(".client_brief_info").eq(0).css("display","block");
	}
	// When tab will click, the related tabs information will display
	$(".client_brief_info_c .tab").click(function(){

		var index = $(this).index();
		var brief_info_index = $(".client_brief_info").eq(index).css("display");
	
		
		if(brief_info_index == 'none') {
			$(".client_brief_info").css("display","none");
			$(".client_brief_info_c .tab").removeClass("active");
			
			$(".client_brief_info").eq(index).css("display","block");
			$(".client_brief_info_c .tab").eq(index).addClass("active");			
			
		}
	});
	$(".review_btn").click(function(){
		
		var index = 4;
		var brief_info_index = $(".reviews").css("display");
		if(brief_info_index == 'none'){
			$(".client_brief_info").css("display","none");
			$(".client_brief_info_c .tab").removeClass("active");

			$(".client_brief_info").eq(index).css("display","block");
			$(".client_brief_info_c .tab").eq(index).addClass("active");	
		}
	});
	$(".booking_btn").click(function(){
		
		var index = 1;
		var brief_info_index = $(".services").css("display");
		
		if(brief_info_index == 'none'){
			$(".client_brief_info").css("display","none");
			$(".client_brief_info_c .tab").removeClass("active");

			$(".client_brief_info").eq(index).css("display","block");
			$(".client_brief_info_c .tab").eq(index).addClass("active");	
		}
	});
	
});
