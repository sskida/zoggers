
<aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
      <div class="side-bg"> 
        <!--<div class="side-btn-top">
        <ul>
          <li class="side-clr-blue" title="Home"><a href="index.php" ><i class="fa-side-color fa fa-home"></i></a></li>
          <li class="side-clr-green" title="Add Project"><a href="add-project-view.php"><i class="fa-side-color fa fa-signal"></i></a></li>
          <li class="side-clr-eblue" title="Add Users"><a href="add-clients.php"><i class="fa-side-color fa fa-users"></i></a></li>
          <li class="side-clr-gray" title="Settings"><a href="profile.php"><i class="fa-side-color fa fa-gears"></i></a></a></li>
        </ul>
      </div>-->
        <div class="user-panel">
          <div class="side-use image"> <i class="fa fa-user fa-5x"></i></div>
          <div class="pull-left info">
            <p>Welcome, <br>
              <span> <?=$_SESSION['ma_name_mer']?> </span></p>
            <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--> </div>
        </div>
        <div id="dash-side">
          <div class="dash-side-box"><a href="<?=URL?>dashboard" style="color:#FFFFFF"><i class="dash-sid-color fa fa-tachometer"></i>Dashboard</a>
            <div class="side"></div>
          </div>
        </div>
      </div>
      <div class="add-nav nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#tab_2" data-toggle="tab">Menus</a></li>
        </ul>
        <div class="add-nav tab-content"> 
          
          <!-- /.tab-pane -->
          <div class="tab-pane active" id="tab_2">
            <ul class="sidebar-menu">
              <li> <a href="<?=URL?>manage-business.php"> <i class="fa fa-suitcase"></i> <span>Manage Business</span> </a> </li>
              <!-- <li> <a href="<?=URL?>view-appointments"> <i class="fa fa-calendar"></i> <span>View Appointments</span> </a> </li> -->
              <li> <a href="<?=URL?>view-profile"> <i class="fa fa-user"></i> <span>My Profile</span> </a> </li>
			  <li> <a href="<?=URL?>reset-password-mer"> <i class="fa fa-cogs"></i> <span>Reset Password</span> </a> </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </aside>